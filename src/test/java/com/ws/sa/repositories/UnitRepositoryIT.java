package com.ws.sa.repositories;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.DataSetFormat;
import com.github.database.rider.core.api.exporter.ExportDataSet;
import com.ws.BaseDataIT;
import com.ws.sa.repositories.unit.UnitRepository;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.repositories.user.UserRepository;
import com.ws.sa.entities.user.User;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Vladislav Shelengovskiy on 04.08.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Transactional
public class UnitRepositoryIT extends BaseDataIT {

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UUID userId = UUID.fromString("8b062d46-0e86-4e70-970e-e669950b289d");

    @Ignore
    @Test
    @DataSet(cleanBefore = true)
    @Commit
    @ExportDataSet(format = DataSetFormat.JSON, includeTables = "UNITS", dependentTables = true, outputName = "target/exported/json/generated-units.json")
    public void generateDataSet() throws Exception {
        Unit root = new Unit();
        root.setName("root");
        root.setPath(" ");
        unitRepository.save(root);

        Unit root2 = new Unit();
        root2.setName("root2");
        root2.setPath(" ");
        unitRepository.save(root2);

        Unit subroot = new Unit();
        subroot.setParent(root);
        subroot.setName("subroot");
        subroot.setPath("/".concat(root.getId().toString()));
        unitRepository.save(subroot);

        Unit subroot2 = new Unit();
        subroot2.setParent(root);
        subroot2.setName("subroot");
        subroot2.setPath("/".concat(root.getId().toString()));
        unitRepository.save(subroot2);

        Unit subsubroot = new Unit();
        subroot.setParent(subroot);
        subroot.setName("subsubroot");
        subroot.setPath("/".concat(subroot.getId().toString()).concat(subroot.getPath()));
        unitRepository.save(subsubroot);

        User user = new User();
        user.setUnits(Stream.of(subroot).collect(Collectors.toSet()));
        user.setAuthority(authorityRepository.findByAuthority("ROLE_SECURITY_OFFICER"));
        user.setUsername("officer");
        user.setPassword(passwordEncoder.encode("officer"));
        user.setEmail("officer@risk-guard.com");
        userRepository.save(user);
    }

    // Region HasAccess

    @DataSet(value = "datasets/repositories/generated-units.json", cleanBefore = true)
    @Test
    public void testHasAccessUserUnit() {
        // Arrange
        UUID unitId = UUID.fromString("bea71106-6c6a-414b-88ad-9a1844a5b8c3");

        // Act & Assert
        Assertions.assertThat(unitRepository.hasAccess(unitId, userId)).isTrue();
    }

    @DataSet(value = "datasets/repositories/generated-units.json", cleanBefore = true)
    @Test
    public void testHasAccessParentUnit() {
        // Arrange
        UUID unitId = UUID.fromString("54658629-659f-420a-b1d9-1a0c85640315");

        // Act & Assert
        Assertions.assertThat(unitRepository.hasAccess(unitId, userId)).isFalse();
    }

    @DataSet(value = "datasets/repositories/generated-units.json", cleanBefore = true)
    @Test
    public void testHasAccessChildUnit() {
        // Arrange
        UUID unitId = UUID.fromString("9cd2b18f-8718-4910-a3f1-4c782a2cf49c");

        // Act & Assert
        Assertions.assertThat(unitRepository.hasAccess(unitId, userId)).isTrue();
    }

    @DataSet(value = "datasets/repositories/generated-units.json", cleanBefore = true)
    @Test
    public void testHasAccessOneParentUnit() {
        // Arrange
        UUID unitId = UUID.fromString("4df4e285-72d0-4df7-9485-ab8d181666ad");

        // Act & Assert
        Assertions.assertThat(unitRepository.hasAccess(unitId, userId)).isFalse();
    }

    @DataSet(value = "datasets/repositories/generated-units.json", cleanBefore = true)
    @Test
    public void testHasAccessSomeUnit() {
        // Arrange
        UUID unitId = UUID.fromString("4666c5e6-d1aa-465a-99a9-d3d75c823f9c");

        // Act & Assert
        Assertions.assertThat(unitRepository.hasAccess(unitId, userId)).isFalse();
    }

    // End region

    @DataSet(value = "datasets/repositories/generated-units.json", cleanBefore = true)
    @Test
    public void testDeleteBranchFromUsers() {
        // Arrange
        UUID unitId = UUID.fromString("bea71106-6c6a-414b-88ad-9a1844a5b8c3");

        // Act
        unitRepository.deleteBranchFromUsers(unitId.toString());

        // Assert
        User manager = userRepository.findOne(UUID.fromString("5b0b8a3b-c8fd-4f08-92e1-a1740bdc837f"));
        Assertions.assertThat(manager.getUnits()).hasSize(1);
        Assertions.assertThat(manager.getUnits().iterator().next().getId().toString())
                  .isEqualTo("4666c5e6-d1aa-465a-99a9-d3d75c823f9c");

        User officer = userRepository.findOne(UUID.fromString("8b062d46-0e86-4e70-970e-e669950b289d"));
        Assertions.assertThat(officer.getUnits()).isEmpty();
    }
}
