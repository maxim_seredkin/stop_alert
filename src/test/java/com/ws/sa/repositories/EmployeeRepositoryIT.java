package com.ws.sa.repositories;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.DataSetFormat;
import com.github.database.rider.core.api.exporter.ExportDataSet;
import com.ws.BaseDataIT;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.repositories.employee.EmployeeRepository;
import com.ws.sa.repositories.unit.UnitRepository;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.repositories.user.UserRepository;
import com.ws.sa.entities.user.User;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Vladislav Shelengovskiy on 04.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Transactional
public class EmployeeRepositoryIT extends BaseDataIT {

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UUID userId = UUID.fromString("f4d2a48d-55ee-4606-894a-899b6a51b7c5");

    @Ignore
    @Test
    @DataSet(cleanBefore = true)
    @Commit
    @ExportDataSet(format = DataSetFormat.JSON, includeTables = "EMPLOYEES", dependentTables = true, outputName = "target/exported/json/generated-employees.json")
    public void generateDataSet() throws Exception {
        Unit root = new Unit();
        root.setName("root");
        root.setPath(" ");
        unitRepository.save(root);

        Employee employee = new Employee();
        employee.setFirstName("some name");
        employee.setEmail("some@mail.ru");
        employee.setUnit(root);
        employeeRepository.save(employee);

        User user = new User();
        user.setUnits(Stream.of(root).collect(Collectors.toSet()));
        user.setAuthority(authorityRepository.findByAuthority("ROLE_SECURITY_OFFICER"));
        user.setUsername("officer");
        user.setPassword(passwordEncoder.encode("officer"));
        user.setEmail("officer@risk-guard.com");
        userRepository.save(user);
    }

    // Region HasAccess

    @DataSet(value = "datasets/repositories/generated-employees.json", cleanBefore = true)
    @Test
    public void testHasAccessUserEmployee() {
        // Arrange
        UUID employeeId = UUID.fromString("347b9363-01e8-4ae5-91c7-b37dc4169049");

        // Act & Assert
        Assertions.assertThat(employeeRepository.hasAccess(employeeId, userId)).isTrue();
    }

    @DataSet(value = "datasets/repositories/generated-employees.json", cleanBefore = true)
    @Test
    public void testHasAccessChildUnitEmployee() {
        // Arrange
        UUID employeeId = UUID.fromString("ac09b58f-178d-44da-8da4-9b5f0bdeec8e");

        // Act & Assert
        Assertions.assertThat(employeeRepository.hasAccess(employeeId, userId)).isTrue();
    }

    @DataSet(value = "datasets/repositories/generated-employees.json", cleanBefore = true)
    @Test
    public void testHasAccessSomeEmployee() {
        // Arrange
        UUID employeeId = UUID.fromString("c7180d19-843b-4451-ac48-ea5f81f7b342");

        // Act & Assert
        Assertions.assertThat(employeeRepository.hasAccess(employeeId, userId)).isFalse();
    }

    // End region
}
