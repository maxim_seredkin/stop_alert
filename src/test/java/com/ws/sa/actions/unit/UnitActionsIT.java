package com.ws.sa.actions.unit;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.DataSetFormat;
import com.github.database.rider.core.api.exporter.ExportDataSet;
import com.ws.BaseDataIT;
import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.enums.EntityStatus;
import com.ws.sa.repositories.employee.EmployeeRepository;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.actions.unit.arguments.CreateUnitArgument;
import com.ws.sa.actions.unit.arguments.DeleteUnitArgument;
import com.ws.sa.actions.unit.arguments.UpdateUnitArgument;
import com.ws.sa.repositories.unit.UnitRepository;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.dtos.unit.UnitFullDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.forms.unit.DeleteUnitForm;
import com.ws.sa.forms.unit.UnitForm;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.repositories.user.UserRepository;
import com.ws.sa.entities.user.User;
import com.ws.sa.repositories.authority.AuthorityRepository;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.unit.UnitActions.*;
import static com.ws.sa.actions.unit.arguments.CreateUnitArgument.from;
import static com.ws.sa.actions.unit.arguments.DeleteUnitArgument.from;
import static com.ws.sa.actions.unit.arguments.UpdateUnitArgument.from;

/**
 * Created by Vladislav Shelengovskiy on 04.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public class UnitActionsIT extends BaseDataIT {

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    @Qualifier("unitActionAllocator")
    private ActionAllocator<UnitActions> allocator;

    @Autowired
    private UnitMapper mapper;

    @Ignore
    @Test
    @DataSet(cleanBefore = true)
    @Commit
    @ExportDataSet(format = DataSetFormat.JSON, includeTables = "UNITS", dependentTables = true, outputName = "target/exported/json/generated-units.json")
    public void generateDataSet() throws Exception {
        Unit root = new Unit();
        root.setName("root");
        root.setPath(" ");
        unitRepository.save(root);

        Unit root2 = new Unit();
        root.setName("root2");
        root.setPath(" ");
        unitRepository.save(root2);

        User officer = new User();
        officer.setAuthority(authorityRepository.findByAuthority("ROLE_SECURITY_OFFICER"));
        officer.setUsername("officer");
        officer.setPassword(passwordEncoder.encode("officer"));
        officer.setEmail("officer@risk-guard.com");
        userRepository.save(officer);

        User manager = new User();
        manager.setAuthority(authorityRepository.findByAuthority("ROLE_MANAGER"));
        manager.setUsername("manager");
        manager.setPassword(passwordEncoder.encode("manager"));
        manager.setEmail("manager@risk-guard.com");
        userRepository.save(manager);
    }

    @DataSet(value = "datasets/actions/unit/generated-units.json", cleanBefore = true)
    @WithUserDetails("admin")
    @Test
    public void testCreate() {
        // Arrange
        String name = "some name";

        // Act
        UnitFullDTO unitDTO = mapper.toFull(allocator.<CreateUnitArgument, Unit>instanceOf(CREATE)
                                                    .execute(from(new UnitForm(name,
                                                                               UUID.fromString("8afb3fcf-3c13-4638-b5d2-5d012e228983"),
                                                                               Stream.of(UUID.fromString("db4408ec-ed74-467b-b470-de74493c0dac"),
                                                                                         UUID.fromString("5b0b8a3b-c8fd-4f08-92e1-a1740bdc837f"))
                                                                                     .collect(Collectors.toList())))));

        // Assert
        Assertions.assertThat(unitDTO.getId()).isNotNull();
        Assertions.assertThat(unitDTO.getPath()).hasSize(1);
        UnitLightDTO node = unitDTO.getPath().iterator().next();
        Assertions.assertThat(node.getId()).isEqualTo(UUID.fromString("8afb3fcf-3c13-4638-b5d2-5d012e228983"));
        Assertions.assertThat(node.getName()).isEqualTo("root");

        Unit unit = unitRepository.findOne(unitDTO.getId());

        Assertions.assertThat(unit).isNotNull();
        Assertions.assertThat(unit.getName()).isEqualTo(name);
        Assertions.assertThat(unit.getChilds()).isEmpty();
        Assertions.assertThat(unit.getEmployees()).isEmpty();
        Assertions.assertThat(unit.getStatus()).isEqualTo(EntityStatus.ENABLED);

        Unit parent = unit.getParent();
        Assertions.assertThat(parent.getId()).isEqualTo(UUID.fromString("8afb3fcf-3c13-4638-b5d2-5d012e228983"));
        Assertions.assertThat(parent.getName()).isEqualTo("root");

        Assertions.assertThat(unit.getManagers()).hasSize(1);
        User manager = unit.getManagers().iterator().next();
        Assertions.assertThat(manager.getId()).isEqualTo(UUID.fromString("5b0b8a3b-c8fd-4f08-92e1-a1740bdc837f"));

        Assertions.assertThat(unit.getOfficers()).hasSize(1);
        User officer = unit.getOfficers().iterator().next();
        Assertions.assertThat(officer.getId()).isEqualTo(UUID.fromString("db4408ec-ed74-467b-b470-de74493c0dac"));
    }

    @DataSet(value = "datasets/actions/unit/generated-units.json", cleanBefore = true)
    @WithUserDetails("admin")
    @Transactional
    @Test
    public void testUpdate() {
        // Arrange
        UUID id = UUID.fromString("bea71106-6c6a-414b-88ad-9a1844a5b8c3");
        String name = "some name 2";

        // Act
        Unit unitDTO = allocator.<UpdateUnitArgument, Unit>instanceOf(UPDATE)
                .execute(from(id,
                              new UnitForm(name,
                                           UUID.fromString("8afb3fcf-3c13-4638-b5d2-5d012e228983"),
                                           Stream.of(UUID.fromString("5b0b8a3b-c8fd-4f08-92e1-a1740bdc837f"))
                                                 .collect(Collectors.toList()))));

        // Assert
        Assertions.assertThat(unitDTO.getId()).isEqualTo(id);

        Unit unit = unitRepository.findOne(unitDTO.getId());

        Assertions.assertThat(unit).isNotNull();
        Assertions.assertThat(unit.getName()).isEqualTo(name);
        Assertions.assertThat(unit.getChilds()).hasSize(1);
        Assertions.assertThat(unit.getEmployees()).hasSize(1);
        Assertions.assertThat(unit.getStatus()).isEqualTo(EntityStatus.ENABLED);
        Assertions.assertThat(unit.getPath()).isEqualTo("/8afb3fcf-3c13-4638-b5d2-5d012e228983");

        Unit parent = unit.getParent();
        Assertions.assertThat(parent.getId()).isEqualTo(UUID.fromString("8afb3fcf-3c13-4638-b5d2-5d012e228983"));
        Assertions.assertThat(parent.getName()).isEqualTo("root");

        unit = unitRepository.findOne(UUID.fromString("8afb3fcf-3c13-4638-b5d2-5d012e228983"));
        Assertions.assertThat(unit.getPath().trim()).isEmpty();

        unit = unitRepository.findOne(UUID.fromString("a4929faf-5f0c-483f-8d5e-b669e613015d"));
        Assertions.assertThat(unit.getPath().trim()).isEqualTo("/8afb3fcf-3c13-4638-b5d2-5d012e228983");

        unit = unitRepository.findOne(UUID.fromString("54658629-659f-420a-b1d9-1a0c85640315"));
        Assertions.assertThat(unit.getPath().trim()).isEmpty();

        unit = unitRepository.findOne(UUID.fromString("bea71106-6c6a-414b-88ad-9a1844a5b8c3"));
        Assertions.assertThat(unit.getPath().trim()).isEqualTo("/8afb3fcf-3c13-4638-b5d2-5d012e228983");

        unit = unitRepository.findOne(UUID.fromString("4df4e285-72d0-4df7-9485-ab8d181666ad"));
        Assertions.assertThat(unit.getPath().trim()).isEqualTo("/54658629-659f-420a-b1d9-1a0c85640315");

        unit = unitRepository.findOne(UUID.fromString("9cd2b18f-8718-4910-a3f1-4c782a2cf49c"));
        Assertions.assertThat(unit.getPath().trim()).isEqualTo("/8afb3fcf-3c13-4638-b5d2-5d012e228983/bea71106-6c6a-414b-88ad-9a1844a5b8c3");
    }

    @DataSet(value = "datasets/actions/unit/generated-units.json", cleanBefore = true)
    @Transactional
    @Test
    public void testDelete() {
        // Arrange
        UUID id = UUID.fromString("bea71106-6c6a-414b-88ad-9a1844a5b8c3");
        UUID successorId = UUID.fromString("8afb3fcf-3c13-4638-b5d2-5d012e228983");

        // Act
        allocator.<DeleteUnitArgument, Void>instanceOf(DELETE).execute(from(id, new DeleteUnitForm(successorId)));

        // Assert
        Unit unit = unitRepository.findOne(id);
        Assertions.assertThat(unit.isDeleted()).isTrue();
        Assertions.assertThat(unit.getUsers()).isEmpty();

        Unit child = unitRepository.findOne(UUID.fromString("9cd2b18f-8718-4910-a3f1-4c782a2cf49c"));
        Assertions.assertThat(child.isDeleted()).isTrue();
        Assertions.assertThat(child.getUsers()).isEmpty();

        User manager = userRepository.findOne(UUID.fromString("5b0b8a3b-c8fd-4f08-92e1-a1740bdc837f"));
        Assertions.assertThat(manager.getUnits()).isEmpty();

        User officer = userRepository.findOne(UUID.fromString("db4408ec-ed74-467b-b470-de74493c0dac"));
        Assertions.assertThat(officer.getUnits()).hasSize(1);
        Assertions.assertThat(officer.getUnits().iterator().next().getId().toString()).isEqualTo("a4929faf-5f0c-483f-8d5e-b669e613015d");

        Employee employee = employeeRepository.findOne(UUID.fromString("347b9363-01e8-4ae5-91c7-b37dc4169049"));
        Assertions.assertThat(employee.getUnit().getId()).isEqualTo(successorId);

        employee = employeeRepository.findOne(UUID.fromString("c7180d19-843b-4451-ac48-ea5f81f7b342"));
        Assertions.assertThat(employee.getUnit().getId()).isEqualTo(successorId);

        employee = employeeRepository.findOne(UUID.fromString("ac09b58f-178d-44da-8da4-9b5f0bdeec8e"));
        Assertions.assertThat(employee.getUnit().getId()).isNotEqualTo(successorId);

    }

    @DataSet(value = "datasets/actions/unit/generated-units.json", cleanBefore = true)
    @WithUserDetails("admin")
    @Test
    public void tesGet() {
        // Arrange
        UUID id = UUID.fromString("bea71106-6c6a-414b-88ad-9a1844a5b8c3");

        // Act
        Unit dto = allocator.<UuidArgument, Unit>instanceOf(FIND_ONE).execute(from(id));

        // Assert
        Assertions.assertThat(dto.getId()).isEqualTo(id);
        Assertions.assertThat(dto.getStatus()).isEqualTo(EntityStatus.ENABLED);
    }
}
