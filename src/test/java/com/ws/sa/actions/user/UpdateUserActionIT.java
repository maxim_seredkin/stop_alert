package com.ws.sa.actions.user;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.DataSetFormat;
import com.github.database.rider.core.api.exporter.ExportDataSet;
import com.ws.BaseDataIT;
import com.ws.sa.entities.authority.Authority;
import com.ws.sa.entities.user.User;
import com.ws.sa.enums.Role;
import com.ws.sa.forms.user.UserForm;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.repositories.user.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Commit;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.ws.sa.actions.user.arguments.UpdateUserArgument.from;

/**
 * Created by Vladislav Shelengovskiy on 04.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Transactional
public class UpdateUserActionIT extends BaseDataIT {

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UpdateUserAction action;

    @Ignore
    @Test
    @Commit
    @DataSet(cleanBefore = true)
    @ExportDataSet(format = DataSetFormat.JSON, includeTables = "USERS", dependentTables = true, outputName = "target/exported/json/update-user-action-it.json")
    public void generateDataSet() throws Exception {
        Authority authority = new Authority();
        authority.setAuthority(Role.SECURITY_OFFICER.getAuthority());
        authority = authorityRepository.save(authority);

        User officer = new User();
        officer.setAuthority(authority);
        officer.setUsername("officer");
        officer.setPassword(passwordEncoder.encode("officer"));
        officer.setEmail("officer@risk-guard.com");
        userRepository.save(officer);
    }

    @Test
    @WithUserDetails("admin")
    @DataSet(value = "datasets/actions/user/update-user-action-it.json", cleanBefore = true)
    public void testExecute() {
        // Arrange
        UUID userId = UUID.fromString("c495fdea-7a34-4c59-bf50-921954c2ea6b");

        User user = userRepository.findOne(userId);

        // Act
        User returnedUser = action.execute(from(userId,
                                                UserForm.builder()
                                                        .authority(user.getAuthority().getId())
                                                        .email(user.getEmail())
//                                                        .username(user.getUsername())
                                                        .username("new username")
//                                                        .firstName("new firstName")
//                                                        .middleName("new middleName")
//                                                        .secondName("new secondName")
                                                        .build()));

        // Assert
        Assertions.assertThat(returnedUser.getId()).isEqualTo(userId);

        user = userRepository.findOne(userId);

        Assertions.assertThat(user).isNotNull();
    }
}
