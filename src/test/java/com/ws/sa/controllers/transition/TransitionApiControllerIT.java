package com.ws.sa.controllers.transition;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.DataSetFormat;
import com.github.database.rider.core.api.exporter.ExportDataSet;
import com.sun.org.apache.regexp.internal.RE;
import com.ws.BaseDataIT;
import com.ws.sa.entities.authority.Authority;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.entities.redirect.Redirect;
import com.ws.sa.entities.task.Task;
import com.ws.sa.entities.transition.Transition;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import com.ws.sa.enums.Role;
import com.ws.sa.enums.TaskStatus;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.repositories.employee.EmployeeRepository;
import com.ws.sa.repositories.mail.MailRepository;
import com.ws.sa.repositories.mail_template.MailTemplateRepository;
import com.ws.sa.repositories.task.TaskRepository;
import com.ws.sa.repositories.transition.TransitionRepository;
import com.ws.sa.repositories.unit.UnitRepository;
import com.ws.sa.repositories.user.UserRepository;
import com.ws.sa.utils.CalendarUtil;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.Commit;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Vladislav Shelengovskiy on 22.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public class TransitionApiControllerIT extends BaseDataIT {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private MailRepository mailRepository;

    @Autowired
    private TransitionRepository transitionRepository;

    @Autowired
    private MailTemplateRepository mailTemplateRepository;

    @Ignore
    @Test
    @Commit
    @DataSet(cleanBefore = true)
    @ExportDataSet(format = DataSetFormat.JSON, includeTables = "USERS", dependentTables = true, outputName = "target/exported/json/create-transition-action-it.json")
    public void generateDataSet() throws Exception {
        Authority authority = new Authority();
        authority.setAuthority(Role.SECURITY_OFFICER.getAuthority());
        authority = authorityRepository.save(authority);

        User admin = new User();
        admin.setAuthority(authority);
        admin.setUsername("admin");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setEmail("admin@risk-guard.com");
        admin = userRepository.save(admin);

        MailTemplate template = new MailTemplate();
        template.setName("test temp");
        template.setSubject("test sub");
        template.setFromAddress("test add");
        template.setFromPersonal("test fish");
        template.setRedirectUri("http://yandex.ru");
        template = mailTemplateRepository.save(template);

        Unit unit = new Unit();
        unit.setName("some name");
        unit.setPath(" ");
        unit = unitRepository.save(unit);

        Employee employee = new Employee();
        employee.setFirstName("test employee");
        employee.setUnit(unit);
        employee = employeeRepository.save(employee);

        Redirect redirect = new Redirect();
        redirect.setRedirectUri(template.getRedirectUri());

        Task task = new Task();
        task.setAuthor(admin);
        task.setName("some name");
        task.setEmployeesByHand(Collections.singletonList(employee));
        task.setTemplate(template);
        task.setStatus(TaskStatus.EXECUTION);
        task.setStartDate(new Date());
        task.setEndDate(CalendarUtil.addDays(new Date(), 3));
        task.setRedirect(redirect);
        task = taskRepository.save(task);

        Mail mail = new Mail();
        mail.setUnit(unit);
        mail.setEmployee(employee);
        mail.setTask(task);
        mail.setSend(true);
        mailRepository.save(mail);
    }

    @Test
    @WithUserDetails("admin")
    @DataSet(value = "datasets/controllers/transition/create-transition-action-it.json", cleanBefore = true)
    public void testExecute() throws Exception {
        // Arrange
        UUID taskId = UUID.fromString("ce4262f6-6da5-4bab-a261-2b863f4a402d");
        UUID mailId = UUID.fromString("0a3fcf5b-1fcc-4db8-8397-b6c02e81ac4f");
        String userAgent = "Mozilla/5.0 (Linux; U; Android 4.1.1; en-gb; Build/KLP) " +
                           "AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30";

        // Act
        mockMvc.perform(get("/api/transitions/{id}", mailId)
                                .header("User-Agent", userAgent)
                                .contentType(APPLICATION_JSON_VALUE))
               .andExpect(status().is(302));
        Thread.sleep(100);

        // Assert
        Collection<Transition> transitions = transitionRepository.findAllByMail_Task_Id(taskId);
        Assertions.assertThat(transitions).hasSize(1);

        Transition transition = transitions.iterator().next();
        Assertions.assertThat(transition.getIp()).isEqualTo("127.0.0.1");
        Assertions.assertThat(transition.getBrowser()).contains("Safari");
        Assertions.assertThat(transition.getOs()).contains("Android");
        Assertions.assertThat(transition.getAdditionalInfo()).isEqualTo(userAgent);

        Assertions.assertThat(transition.getMail().getCountTransitions()).isEqualTo(1);
    }
}
