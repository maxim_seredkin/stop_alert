/**
 * Created by Baser on 25.01.2017.
 */
var path = require('path');
var helpers = require('./helpers');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  // devtool: 'source-map',
  cache: true,
  entry: {
    style: path.join(__dirname, 'src', 'style.ts'),
    vendor: path.join(__dirname, 'src', 'vendor.ts'),
    app: path.join(__dirname, 'src', 'main.ts')
  },
  output: {
    path: helpers.root('resources', 'static'),
    filename: "[name].js",
    publicPath: "resources/",
    chunkFilename: '[id].chunk.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: ['babel-loader?presets[]=es201']
    }, {
      test: /\.ts/,
      use: ['babel-loader?presets[]=es2015', 'ts-loader', 'angular2-template-loader?keepUrl=true']
    }, {
      test: /\.html$/,
      use: 'raw-loader'
    }, {
      test: /\.(eot|woff2?|ttf)(\?\S*)?$/,
      use: 'file-loader?name=fonts/[name].[ext]'
    }, {
      test: /\.(svg|png|jpe?g|gif)(\?\S*)?$/,
      use: 'file-loader?name=img/[name].[ext]'
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract({
        publicPath: '../',
        fallback: "style-loader",
        use: ["css-loader"]
      })
    }, {
      test: /main\.scss$/,
      use: ExtractTextPlugin.extract({
        publicPath: '../',
        fallback: "style-loader",
        use: ['css-loader', 'sass-loader']
      })
    }, {
      test: /\.scss$/,
      exclude: /main\.scss$/,
      loaders: ['to-string-loader', 'css-loader', 'sass-loader']
    }, {
      test: /\.json$/,
      use: 'json-loader?name=[name].[ext]'
    }]
  },
  resolve: {
    extensions: [".js", ".ts", '.json'],
    modules: ["node_modules"]
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['app', 'vendor'],
      children: true,
      async: true
    }),
    new ExtractTextPlugin("style/[name].css"),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html')
    }),
    new CopyWebpackPlugin([{
      from: path.join(__dirname, 'src', 'app_config.json'),
      to: 'app_config.json'
    }]),
    // new webpack.optimize.UglifyJsPlugin({ // https://github.com/angular/angular/issues/10618
    //   mangle: {
    //     keep_fnames: true
    //   }
    // })
  ]
};
