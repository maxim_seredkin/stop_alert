import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";

import {LoginComponent} from './login.component';

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule
    ],
    declarations: [LoginComponent],
    exports: [LoginComponent]
})
export class LoginModule {
}
