import { Component, OnInit } from '@angular/core';

import {UserService} from '../../services/user.service';
import {Router} from "@angular/router";
import {AppCoreService} from "../../services/app-core.service";

@Component({
    selector: 'app-login',
    template: require('./login.component.html'),
    styles: [require('../../../style/login.scss')],
})
export class LoginComponent implements OnInit {

    constructor(private user: UserService, private router: Router, private core: AppCoreService) {
        //if(this.user.isLogged()) { this.router.navigate(['']);}
        this.getLicenseSettings();
    }

    ngOnInit() {

    }

    username: string = "";
    password: string = "";
    error: boolean = false;
    errorText: string = "";

    emptyLogin: boolean = false;
    emptyPassword: boolean = false;

    public licenseSettings: Object;

    login(): void {
        this.error = false;
        this.emptyLogin = false;
        this.emptyPassword = false;

        if(!(this.username.length == 0 || this.password.length == 0))
        {
            this.user.login(this.username,  this.password)
                .then((data) => {
                })
                .catch((err) => {
                    this.error = true;
                    let error_obj = JSON.parse(err.text());

                    if(error_obj['error'] == "unauthorized") {
                        this.errorText = error_obj['error_description'];
                        return;
                    }

                    if(err.status == 400 || err.status == 401) {
                        this.errorText = "Ошибка: логин или пароль не верны!";
                    } else {
                        this.errorText = "При выполнении запроса произошла ошибка";
                    }
            });
        }
        else {
            this.emptyLogin = (this.username.length == 0);
            this.emptyPassword = (this.password.length == 0);
        }
    }

    forgotPassword() {
        this.router.navigate(['forgot-password']);
    }

    getLicenseSettings() {
        this.core.get("api/settings/license")
            .then((res) => {
                this.licenseSettings = res['_embedded'];
                if(this.licenseSettings['status'] == "FAILURE" || this.licenseSettings['status'] == "EXPIRED") {
                    this.user.licenseActive = false;
                }
                else {
                    this.user.licenseActive = true;
                }
            });
    }
}
