/**
 * Created by Baser on 27.03.2017.
 */
import { Component } from '@angular/core';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";


@Component({
    selector: 'forgot-password',
    template: require('./forgot-password.component.html'),
    styles: [require('../../../style/login.scss')],
})

export class ForgotPasswordComponent {
    public trueRecover: boolean = false;
    public recoverLogin: string = "";

    public error: boolean = false;
    public errorText: string = "";

    constructor(private user: UserService, private router: Router) {
        
    }
    
    recoverPassword() {
        this.error = false;
        this.errorText = "";

        if(this.recoverLogin.length == 0)
        {
            this.error = true;
            this.errorText = "Введите логин или email";
            return;
        }

        this.user.passwordRecover(this.recoverLogin)
            .then((data) => {
                if(data.json()['code'] == 404) {
                    this.error = true;
                    this.errorText = "Пользователя с указанным логином не существует";
                }
                if(data.json()['code'] == 1014) {
                    this.error = true;
                    this.errorText = data.json()['code']['_embedded'];
                }
                if(data.json()['code'] == 200) {
                    this.toogleRecover();
                }
            });
    }

    toogleRecover() {
        this.trueRecover = !this.trueRecover;
    }

    toLogin() {
        this.router.navigate(['login']);
    }
}