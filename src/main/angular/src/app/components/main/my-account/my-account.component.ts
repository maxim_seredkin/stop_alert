/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 18.01.2017.
 */
import {Component} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../models/user/user";
import {UserContext} from "../../../models/user/user-context";

@Component({
    selector: 'my-account',
    template: require('./my-account.component.html')
})

export class MyAccountComponent {

    public oldPassword:string = "";
    public newPassword:string = "";
    public confirmPassword:string = "";

    public user:User;
    public changedUser:User;

    public fillFormError:boolean = false;
    public oldPasswordError: boolean = false;
    public formMessage:string = "";

    public registerDateToString = "";

    constructor(private userService:UserService, private userContext:UserContext) {
        this.userService.getAccountData()
            .then((data) => {
                this.user = data;
                this.registerDateToString = new Date(this.user.registerDate).toLocaleString("ru", {day: 'numeric', month: 'long', year: 'numeric'});
                if (this.user.authority.authority == 'ROLE_ADMINISTRATOR') {
                    this.changedUser = this.user.clone();
                }
            })
    }

    saveUser() {
        this.fillFormError = false;
        this.formMessage = "";
        this.oldPasswordError = false;

        if (this.user.authority.authority != 'ROLE_ADMINISTRATOR') {
            if (this.newPassword != "") {

                if(!this.user.validPassword(this.newPassword)) {
                    this.fillFormError = true;
                    this.formMessage = "Пароль должен содержать не менее 8-ми символов, в числе которых дожны быть символы верхнего и нижнего регистра + цифры";
                    this.newPassword == "";
                    return;
                }

                if (this.newPassword == this.confirmPassword) {
                    this.userService.saveUser(this.newPassword, this.oldPassword)
                        .then((res) => {
                            if(res['code'] == 412) {
                                this.fillFormError = true;
                                this.oldPasswordError = true;
                                this.formMessage = "Неверный старый пароль!";
                            }
                            if(res['code'] == 200) {
                                this.formMessage = "Пароль успешно изменен";
                                this.oldPassword = "";
                                this.newPassword = "";
                                this.confirmPassword = "";
                            }
                        })
                        .catch(() => {
                            this.fillFormError = true;
                            this.oldPasswordError = true;
                            this.formMessage = "Неверный старый пароль!";
                        })
                }
                else {
                    this.fillFormError = true;
                    this.formMessage = "Пароли не совпадают!";
                }
            }
        }
        else {
            //Проверка изменения модели
            if (!this.changedUser.equal(this.user) || !(this.changedUser.password['renewed'] == "")) {
                //Проверка заполненности полей
                if (this.changedUser.valid()) {
                    //Проверка правлиьности ввода email
                    if (this.changedUser.validEmail()) {
                        //Проверка наличия нового пароля
                        if (this.changedUser.password['renewed'] != "") {
                            if (this.changedUser.password['renewed'] == this.confirmPassword) {

                                if(!this.user.validPassword(this.changedUser.password['renewed'])) {
                                    this.fillFormError = true;
                                    this.formMessage = "Пароль должен содержать не менее 8-ми символов, в числе которых дожны быть символы верхнего и нижнего регистра + цифры";
                                    this.changedUser.password['renewed'] == "";
                                    return;
                                }
                                this.userService.editUser(this.changedUser)
                                    .then((resp) => {
                                        if(resp['code'] == 200) {
                                            this.formMessage = "Информация успешно изменена";
                                            //Обновление данных о пользователе
                                            this.userService.getAccountData()
                                                .then((res:User) => {
                                                    this.user = res;
                                                    this.changedUser = this.user.clone();
                                                    this.confirmPassword = "";
                                                    this.userService.fullName = this.user['fullName'];
                                                    this.userService.username = this.user['username'];
                                                })
                                                .catch((err) => {
                                                    this.formMessage = "При выполнении запроса произошла ошибка!";
                                                });
                                        }
                                        if(resp['code'] == 412) {
                                            this.formMessage = "Введен неверный старый пароль!";
                                            this.oldPasswordError = true;
                                            this.fillFormError = true;
                                        }
                                    })
                                    .catch((err) => {
                                        this.formMessage = "При выполнении запроса произошла ошибка!";
                                    });
                            }
                            else {
                                this.fillFormError = true;
                                this.formMessage = "Пароли не совпадают";
                                this.changedUser.password['renewed'] = "";
                                this.confirmPassword = "";
                            }
                        }
                        else {
                            this.changedUser.password['renewed'] = null;
                            this.userService.editUser(this.changedUser)
                                .then(() => {
                                    this.userService.getAccountData()
                                        .then((res:User) => {
                                            this.user = res;
                                            this.changedUser = this.user.clone();
                                            this.formMessage = "Информация успешно изменена";
                                            this.userService.fullName = this.user['fullName'];
                                            this.userService.username = this.user['username'];
                                        })
                                        .catch((err) => {
                                            this.formMessage = "При выполнении запроса произошла ошибка!";
                                        });

                                })
                        }
                    }
                    else {
                        this.fillFormError = true;
                        this.formMessage = "Введен неправильный адрес электронной почты";
                        this.changedUser.email = "";
                    }
                }
                else {
                    this.fillFormError = true;
                    this.formMessage = "Не все поля заполнены!"
                }
            }
        }
    }

    roleToString(role) {
        let res = "";
        switch(role) {
            case "ROLE_MANAGER":
                res = "Менеджер";
                break;
            case "ROLE_ADMINISTRATOR":
                res = "Администратор";
                break;
            case "ROLE_SECURITY_OFFICER":
                res = "Инспектор информационной безопасности";
                break;
        }
        return res;
    }

}
