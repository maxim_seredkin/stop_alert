/**
 * Created by Baser on 13.03.2017.
 */
import {Component,  ViewChild} from '@angular/core';
import {AppCoreService} from "../../../../services/app-core.service";
import {Headers, Http} from "@angular/http";
import {SelectComponent} from "ng2-select/index";
import {Pagination} from "../../../../models/pagination";
import {EmployeeContext} from "../../../../models/employee/employee-context";
import {Router} from "@angular/router";
import 'rxjs/Rx';
import * as FileSaver from 'file-saver';
import {Config} from "../../../../services/config";
import {Modal} from "ngx-modal/index";
import {TreeSelectorComponent} from "../../tree-selector/tree-selector.controller";
import {UnitsTreeSelectorComponent} from "../../units-tree-selector/units-tree-selector.component";


@Component({
    selector: 'employees-load',
    template: require('./employees-load.component.html')
})

export class EmployeesLoadComponent {
    public formErrorText:string = "";
    public fillFormError:boolean = false;

    private paginationParams:Pagination;

    public load:boolean = false;

    public filterStatusValue:Array<Object> = [{"id": "ALL", "text": "Все записи"}];
    public filterStatus:string = "";

    public unitsTree:Array<Object>;
    public tagsTree:Array<Object>;

    public employeesDB:File;
    public fileLoaderPlaceHolder:string = "Переместите файл сюда или выберите из файла";

    public countErrors:number;
    public invalidEmployeesError:boolean = false;
    public invalidUnitsError:boolean = false;

    public employees:Array<Object> = [];
    public invalidEmployees:Array<Object> = [];
    public invalidUnits:Array<Object> = [];
    public employeesForTable:Array<Object> = [];

    public showAdvFilter:boolean = false;
    public updateAllow:boolean = false;
    public showValidEmployees:boolean;

    public filteredUnitName:string = "";
    public filteredSecondName:string = "";
    public filteredEmail:string = "";
    public filteredPosition:string = "";

    public uploadDBError:boolean = false;

    @ViewChild('statusSelector') statusSelector:SelectComponent;

    @ViewChild('loadErrorsModal') loadErrorsModal:Modal;

    @ViewChild('employeeModal') employeeModal:Modal;
    @ViewChild('employeeDeleteModal') employeeDeleteModal:Modal;
    @ViewChild('unitsAddModal') unitsAddModal:Modal;

    @ViewChild('employeeUnitSelector') employeeUnitSelector:UnitsTreeSelectorComponent;
    @ViewChild('employeeTagsSelector') employeeTagsSelector:TreeSelectorComponent;

    constructor(private core:AppCoreService, private employeeContext:EmployeeContext, private router:Router, private http:Http, private config:Config) {
        this.paginationParams = new Pagination();

        this.getUnitsTree();
        this.getTagsTree();

        this.filterStatus = "ALL";
    }

    getTemplateFile() {
        let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';

        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token_header);

        this.http.get(this.config.get('backendUrl')+"api/employees/import/sample", {headers: headers})
            .subscribe(data => {
                var blob = new Blob([data.text()], {type: 'text/csv;charset=UTF-8'});
                FileSaver.saveAs(blob, "EmployeeImportSample.csv");
            }),
            error => console.log("Error downloading the file."),
            () => console.info("OK");
    }

    uploadEmployees() {
        this.fillFormError = false;
        this.formErrorText = "";
    }

    onUpload(file:File) {
        this.employeesDB = file;
        this.fileLoaderPlaceHolder = file.name;
    }

    uploadFile() {
        if(typeof this.employeesDB == 'undefined') {
            return;
        }
        this.fillFormError = false;
        this.formErrorText = "";

        let formData:FormData = new FormData();
        formData.append('file', this.employeesDB, this.employeesDB.name);

        this.core.post_file("api/employees/import", formData)
            .then((res) => {
                let response = res['_embedded'];
                let loadErrors:boolean = false;

                this.invalidEmployees = response['invalidEmployees'];
                this.invalidUnits = response['invalidUnits'];
                this.employees = response['employees'];

                this.normalizeEmployee(this.invalidEmployees);
                this.normalizeEmployee(this.employees);
                this.normalizeUnit(this.invalidUnits);

                if (response['invalidUnits'].length > 0) {
                    loadErrors = true;
                    this.invalidUnitsError = true;
                }
                if (response['invalidEmployees'].length > 0) {
                    loadErrors = true;
                    this.invalidEmployeesError = true;
                    this.countErrors = response['invalidEmployees'].length;
                }
                if (loadErrors) {
                    this.loadErrorsModal.open();
                    this.employeesForTable = this.invalidEmployees;
                    this.showValidEmployees = false;
                    this.paginationParams.totalElements = this.invalidEmployees.length;
                } else {
                    this.employeesForTable = this.employees;
                    this.showValidEmployees = true;
                    this.paginationParams.totalElements = this.employees.length;
                }
                this.updatePagination(this.paginationParams);
                this.load = true;
            })
            .catch(() => {
                this.fillFormError = true;
                this.formErrorText = "При загрузке файла произошла ошибка!";
            });
    }

    validEmail(email) {
        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return (email.length >= 5 && EMAIL_REGEXP.test(email));
    }

    orgSelectorChange(selected, employee) {
        employee['selectedUnit'] = selected;
    }

    tagSelectorChange(selected, employee) {
        employee['selectedTags'] = selected;
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    findError(errorText:string, unit:Object):boolean {
        let flag = false;

        if (typeof unit['errors'] != "undefined") {
            for (let error of unit['errors']) {
                if (error == errorText) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    normalizeEmployee(units:Array<any>) {
        for (let item of units) {
            item['employeeTags'] = [];

            if (item['unit'] != null) {
                if (item['unit']['parent'] != null) {
                    let temp = item['unit']['parent'];
                    item['unit']['parent'] = [];
                    item['unit']['parent'].push(temp);
                } else {
                    item['unit']['parent'] = [];
                }
                let temp = item['unit'];
                item['unit'] = [temp];
            } else {
                item['unit'] = [];
            }
        }
    }

    normalizeUnit(units:Array<any>) {
        for (let item of units) {
            if (item['parent'] != null) {
                let temp = item['parent'];
                item['parent'] = [];
                item['parent'].push(temp);
            } else {
                item['parent'] = [];
            }
        }
    }

    toggleShowFilter() {
        if (this.showAdvFilter) {
            this.filteredSecondName = "";
            this.filteredEmail = "";
            this.filteredPosition = "";
        }
        this.showAdvFilter = !this.showAdvFilter;
    }

    public modalEmployeeId:string;
    public modalSecondName:string;
    public modalFirstName:string;
    public modalMiddleName:string;
    public modalUnit:Array<Object>;
    public modalEmail:string;
    public modalPosition:string;
    public modalEmployeeTags:Array<Object>;
    public employeeModalError:string = "";

    editEmployee(employee:Object) {
        this.employeeModalError = "";
        this.modalEmployeeId = employee['id'];
        this.modalSecondName = employee['secondName'];
        this.modalFirstName = employee['firstName'];
        this.modalMiddleName = employee['middleName'];
        this.modalUnit = employee['unit'];
        this.modalEmail = employee['email'];
        this.modalPosition = employee['position'];
        this.modalEmployeeTags = employee['employeeTags'];

        this.employeeModal.open();
        this.employeeUnitSelector.resetValue();

        this.employeeUnitSelector.resetValue();
        if (employee['unit'].length != 0) {
            if (!this.findError('UNIT_NOT_EXISTS', employee['unit'][0])) {
                this.employeeUnitSelector.getOutsideUnits(employee['unit']);
            }
        }

        this.employeeTagsSelector.resetValue();
        if (employee['employeeTags'].length != 0) {
            this.employeeTagsSelector.getOutsideUnits(employee['employeeTags']);
        }

        this.employeeModal.open();
    }

    deleteEmployee(id:string) {
        this.modalEmployeeId = id;
        this.employeeDeleteModal.open();
    }

    delEmployee() {
        this.removeEmployee(this.modalEmployeeId, this.employees);
        this.removeEmployee(this.modalEmployeeId, this.invalidEmployees);
        this.employeeDeleteModal.close();
        this.checkErrors();
        this.paginationParams.totalElements = this.employeesForTable.length;
        this.updatePagination(this.paginationParams);
    }

    saveEmployee() {
        this.employeeModalError = "";

        if (this.modalSecondName == "") {
            this.employeeModalError = "Не заполнена фамилия сотрудника";
            return;
        }

        if (this.modalFirstName == "") {
            this.employeeModalError = "Не заполнено имя сотрудника";
            return;
        }

        if (this.modalUnit.length == 0) {
            this.employeeModalError = "Не указано подразделение сотрудника";
            return;
        } else {
            if (this.findError('UNIT_NOT_EXISTS', this.modalUnit[0])) {
                this.employeeModalError = "Не указано подразделение сотрудника";
                return;
            }
        }

        if (this.modalEmail == "") {
            this.employeeModalError = "Не указан адрес электронной почты";
            return;
        }

        if (!this.validEmail(this.modalEmail)) {
            this.employeeModalError = "Введен некорректный email";
            return;
        }

        let employee = {};
        employee['secondName'] = this.modalSecondName;
        employee['firstName'] = this.modalFirstName;
        employee['middleName'] = this.modalMiddleName;
        employee['email'] = this.modalEmail;
        employee['position'] = this.modalPosition;
        employee['unit'] = this.modalUnit;
        employee['employeeTags'] = this.modalEmployeeTags;
        employee['errors'] = [];

        this.changeEmployeeValue(this.modalEmployeeId, employee, this.invalidEmployees);
        this.changeEmployeeValue(this.modalEmployeeId, employee, this.employees);

        this.employeeModal.close();
        this.checkErrors();
    }

    changeEmployeeValue(id:string, employeeObj:Object, employeesList:Array<Object>) {
        for (let employee of employeesList) {
            if (employee['id'] == id) {
                employee['secondName'] = employeeObj['secondName'];
                employee['firstName'] = employeeObj['firstName'];
                employee['middleName'] = employeeObj['middleName'];
                employee['email'] = employeeObj['email'];
                employee['position'] = employeeObj['position'];
                employee['unit'] = employeeObj['unit'];
                employee['employeeTags'] = employeeObj['employeeTags'];
                employee['errors'] = employeeObj['errors'];
            }
        }
    }

    removeEmployee(id:string, employeesList:Array<Object>) {
        for (let employee of employeesList) {
            if (employee['id'] == id) {
                employeesList.splice(employeesList.indexOf(employee), 1);
            }
        }
    }

    employeeUnitSelectorChange(selected) {
        this.modalUnit = selected;
    }

    employeeTagsSelectorChange(selected) {
        this.modalEmployeeTags = selected;
    }

    checkErrors() {
        let flag = false;
        let count = 0;
        for (let item of this.invalidEmployees) {
            let error = false;
            if (item['errors'].length != 0) {
                error = true;
            }
            if (item['unit'].length != 0) {
                if (typeof item['unit'][0]['errors'] != "undefined") {
                    if (item['unit'][0]['errors'].length != 0) {
                        error = true;
                    }
                }
            }
            if (error) {
                flag = true;
                count++;
            }
        }
        if (!flag) {
            this.updateAllow = true;
        }
        this.countErrors = count;
    }

    checkEmployeeErrors(employee:Object) {
        let error = false;
        if (employee['errors'].length != 0) {
            error = true;
        }
        if (employee['unit'].length != 0) {
            if (typeof employee['unit'][0]['errors'] != "undefined") {
                if (employee['unit'][0]['errors'].length != 0) {
                    error = true;
                }
            }
        }
        return error;
    }

    updateUnits() {
        if (this.updateAllow) {
            this.showValidEmployees = true;
            this.employeesForTable = this.employees;
            this.invalidEmployeesError = false;
            this.paginationParams.totalElements = this.employees.length;
            this.updatePagination(this.paginationParams);
        }
    }

    changePagination() {
        this.updatePagination(this.paginationParams);
    }

    updatePagination(pagination:Pagination) {
        let division = pagination.totalElements / pagination.size;
        pagination.totalPages = Math.floor(pagination.totalElements / pagination.size);
        pagination.totalPages += ((division % 1) != 0) ? 1 : 0;
    }

    applyFilter() {
        this.paginationParams.number = 1;
        if (this.invalidEmployeesError) {
            this.filterEmployees(this.invalidEmployees);
        } else {
            this.filterEmployees(this.employees);
        }

        this.paginationParams.totalElements = this.employeesForTable.length;
        this.updatePagination(this.paginationParams);
    }

    filterEmployees(employeesList:Array<Object>) {
        this.employeesForTable = [];
        if (this.filteredEmail == "" && this.filteredPosition == "" && this.filteredSecondName == "" && this.filteredUnitName == "") {
            this.employeesForTable = employeesList;
            return;
        }

        for (let employee of employeesList) {
            let addFlag = true;

            if (this.filteredUnitName != "") {
                if (employee['unit'].length != 0) {
                    if (employee['unit'][0]['name'].toLowerCase().indexOf(this.filteredUnitName.toLowerCase()) == -1) {
                        addFlag = false;
                    }

                    if(typeof employee['unit'][0]['errors'] != 'undefined') {
                        if (employee['unit'][0]['errors'].length != 0) {
                            addFlag = false;
                        }
                    }
                } else {
                    addFlag = false;
                }
            }

            if (this.filteredSecondName != "") {
                if (employee['secondName'].toLowerCase().indexOf(this.filteredSecondName.toLowerCase()) == -1) {
                    addFlag = false;
                }
            }

            if (this.filteredEmail != "") {
                if (employee['email'].toLowerCase().indexOf(this.filteredEmail.toLowerCase()) == -1) {
                    addFlag = false;
                }
            }

            if (this.filteredPosition != "") {
                if (employee['position'].toLowerCase().indexOf(this.filteredPosition.toLowerCase()) == -1) {
                    addFlag = false;
                }
            }

            if (addFlag) {
                this.employeesForTable.push(employee);
            }
        }
    }

    cancelAddUnit() {
        this.clearUnsetUnits();
    }

    clearUnsetUnits() {
        for (let employee of this.employees) {
            if (employee['unit'].length == 0) {
                continue;
            }
            if (this.findError('UNIT_NOT_EXISTS', employee['unit'][0])) {
                employee['unit'] = [];
                employee['errors'].push('UNIT_NOT_SET');
            }
        }
        let deleteEmployees = [];
        for (let employee of this.invalidEmployees) {
            if (employee['unit'].length == 0) {
                continue;
            }
            if (this.findError('UNIT_NOT_EXISTS', employee['unit'][0])) {
                employee['unit'] = [];
                employee['errors'].push('UNIT_NOT_SET');
            }

            if(!this.checkEmployeeErrors(employee)) {
                deleteEmployees.push(employee['id']);
            }
        }
        for(let item of deleteEmployees) {
            this.removeEmployee(item, this.invalidEmployees);
        }
        this.checkErrors();
        this.employeesForTable = this.invalidEmployees;
    }

    unitSelectorChange(obj:Object) {
        for (let unit of this.invalidUnits) {
            if (unit['id'] == obj['id']) {
                unit['parent'] = obj['selected'];
            }
        }
    }

    deleteUnit(id:string) {
        for (let unit of this.invalidUnits) {
            if(unit['id'] == id) {
                this.invalidUnits.splice(this.invalidUnits.indexOf(unit), 1);
            }
        }
    }
    
    public unitSaveError: boolean = false;

    checkSaveUnitsExists(unitsList: Array<Object>) {
        if(unitsList.length == 0) {
            if(this.unitSaveError) {
                return;
            } else {
                this.saveUnits();
                return;
            }
        }

        let params = {};
        if(unitsList[0]['parent'].length != 0) {
            params['filters'] = "'parent.id' EQ '"+unitsList[0]['parent'][0]['id']+"'";
        } else {
            params['filters'] = "'parent.id' EQ 'null'";
        }
        //params['searchs'] = "'name':'"+unitsList[0]['name']+"'";
        params['filters'] += ",'name' EQ '"+unitsList[0]['name']+"'";

        this.core.get('api/units/exists', params)
            .then((res) => {
                if(res['_embedded'] == true) {
                    this.unitSaveError = true;   
                }
                unitsList.splice(0, 1);
                this.checkSaveUnitsExists(unitsList);
            });
    }

    saveUnitsClick() {
        this.unitSaveError = false;

        if(this.invalidUnits.length != 0) {
            let tempUnits = JSON.parse(JSON.stringify(this.invalidUnits));
            this.checkSaveUnitsExists(tempUnits);
        }
    }

    saveUnits() {
        let postData = [];

        for(let unit of this.invalidUnits) {
            let form = {};
            form['name'] = unit['name'];
            if(unit['parent'].length > 0)
            {
                form['parent'] = unit['parent'][0]['id'];
            } else {
                form['parent'] = null;
            }
            postData.push(form);
        }

        postData = JSON.parse('{"form": ' +JSON.stringify(postData)+ '}');

        this.core.post_json('api/units/list', postData)
            .then((res) => {
                let response = res['_embedded'];
                this.setEmployeeUnit(this.employees, response);
                this.setEmployeeUnit(this.invalidEmployees, response);

                setTimeout(() => {
                    this.clearUnsetUnits();
                    this.unitsAddModal.close();
                    this.unitsTree = null;
                    this.getUnitsTree();

                    this.checkErrors();
                    if (!this.countErrors) {
                        this.showValidEmployees = true;
                        this.employeesForTable = this.employees;
                        this.invalidEmployeesError = false;
                        this.paginationParams.totalElements = this.employees.length;
                        this.updatePagination(this.paginationParams);
                    }
                }, 100);
            })
            .catch(() => {
                this.clearUnsetUnits();
                this.unitsAddModal.close();
            });
    }

    setEmployeeUnit(employeeList: Array<Object>, newUnits: Array<Object>) {
        for(let employee of employeeList) {
            if(employee['unit'].length > 0) {
                let count = 0;
                for(let unit of this.invalidUnits) {
                    if(employee['unit'][0]['id'] == unit['id']) {
                        employee['unit'][0] = newUnits[count];
                    }
                    count++;
                }
            }
        }
    }


    prepareToLoad() {
        for(let employee of this.employees) {
            employee['unit'] = employee['unit'][0]['id'];

            let employeeTags = [];
            for(let tag of employee['employeeTags']) {
                employeeTags.push(tag['id']);
            }
            employee['employeeTags'] = employeeTags;
        }
    }

    uploadEmployeesDB() {
        if(this.employees.length == 0) {
            return;
        }

        this.prepareToLoad();

        let form = {};
        form['employees'] = this.employees;

        let postData = JSON.parse('{"form": ' +JSON.stringify(form)+ '}');
        this.core.post_json('api/employees/import/save', postData)
            .then(()=>{
                this.router.navigate(['/employees']);
            })
            .catch(()=>{
                this.uploadDBError = true;
            });
    }

    getTagsText(employeeTags: Array<Object>) {
        let count: number = 0;
        let maxCount: number = 5;
        let tagsText = "";

        for(let tag of employeeTags) {
            if(count == maxCount) {
                break;
            }
            tagsText += tag['name']+', ';
            count++;
        }
        tagsText = tagsText.slice(0, tagsText.length-2);
        if(count == maxCount) {
            tagsText += ', > '+maxCount;
        }
        return tagsText;
    }
}