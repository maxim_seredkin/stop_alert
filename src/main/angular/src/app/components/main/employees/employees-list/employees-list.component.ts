/**
 * Created by Baser on 02.03.2017.
 */
import {Component, ViewChild, ElementRef} from '@angular/core';
import {Pagination} from "../../../../models/pagination";
import {AppCoreService} from "../../../../services/app-core.service";
import {Router, NavigationExtras} from "@angular/router";
import {Unit} from "../../../../models/unit";
import {Tag} from "../../../../models/tag/tag";
import {EmployeeContext} from "../../../../models/employee/employee-context";
import {Modal} from "ngx-modal/index";
import {UserService} from './../../../../services/user.service';


@Component({
    selector: 'employees-list',
    template: require('./employees-list.component.html')
})

export class EmployeesListComponent {
    private employees:Array<Object>;
    private pagination:Pagination;
    private filteredFullName:string = "";
    private filteredEmail:string = "";
    private filteredPosition: string = "";
    private unitsTree:Array<Unit>;
    private tagsTree:Array<Tag>;

    public modalEmployeeName: string;
    public modalHeader:string = "";
    public modalText:string = "";
    public modalSubmitButtonText:string = "";
    public modalDelete:boolean = false;
    public modalRecover:boolean = false;
    public modalAction:string = "";

    private selectedOrgs:Array<Unit>;
    private selectedTags:Array<Tag>;

    public selectEmployee: Object;

    public showAdvFilter:boolean = false;

    @ViewChild('orgSelector') orgSelector: ElementRef;
    @ViewChild('tagSelector') tagSelector: ElementRef;

    public modalError: boolean = false;
    public modalErrorText: string = "";

    @ViewChild('myModal')
    private modal:Modal;

    constructor(private core:AppCoreService, private router: Router, private employeeContext: EmployeeContext, private user: UserService) {
        this.pagination = new Pagination();
        let params = this.getRequestParams();
        this.getEmployeesList(params);
        this.getUnitsTree();
        this.getTagsTree();
    }

    editEmployee(id: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "action": "edit",
            }
        };
        this.router.navigate(['employees/', id], navigationExtras);
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    orgSelectorChange(selected) {
        this.selectedOrgs = selected;
    }

    tagSelectorChange(selected) {
        this.selectedTags = selected;
    }

    applyFilter() {
        this.pagination.number = 1;
        let params = this.getRequestParams();
        this.getEmployeesList(params);
    }

    changePagination(event:Pagination) {
        let params = this.getRequestParams();
        this.getEmployeesList(params);
    }

    getRequestParams():Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;

        params['filters'] = "";

        if (this.filteredFullName) {
            params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'fullName' EQ '${this.filteredFullName}'`;
        }

        if (this.filteredEmail) {
            params['filters'] += `${(params['filters']) ? ',' : ''}'email' EQ '${this.filteredEmail}'`;
        }

        if (this.filteredPosition) {
            params['filters'] += `${(params['filters']) ? ',' : ''}'position' EQ '${this.filteredPosition}'`;
        }

        if (this.selectedTags) {
            this.selectedTags.forEach(tag => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'employeeTags' EQ '${tag['id']}'`;
            });
        }

        if (this.selectedOrgs) {
            this.selectedOrgs.forEach(unit => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'units' EQ '${unit['id']}'`;
            });
        }

        params['sorts'] = ['secondName:ASC', 'email:ASC'];
        return params;
    }

    getEmployeesList(params) {
        this.core.get("api/employees", params)
            .then((res) => {
                this.employees = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    changeEmployee(id, action) {
        this.selectEmployee = this.getSelectedEmployee(id);

        this.modalEmployeeName = this.selectEmployee['shortName'];
        switch (action) {
            case "delete":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = (this.selectEmployee['status'] == 'ENABLED');
                this.modalHeader = "Удаление сотрудника";
                this.modalText = "Вы уверены, что хотите удалить сотрудника ";
                this.modalSubmitButtonText = "Удалить";
                break;
            case "recovery":
                this.modalAction = action;
                this.modalRecover = (this.selectEmployee['status'] == 'DELETED');
                this.modalDelete = false;
                this.modalHeader = "Восстановление сотрудника";
                this.modalText = "Вы уверены, что хотите восстановить сотрудника ";
                this.modalSubmitButtonText = "Восстановить";
                break;
        }
    }

    getSelectedEmployee(id) {
        let tmpEmployee = {};
        for (var i = 0; i < this.employees.length; i++) {
            if (this.employees[i]['id'] == id + "") {
                tmpEmployee = this.employees[i];
                break;
            }
        }
        return tmpEmployee;
    }

    changeEmployeeAction() {
        this.employeeContext.changeEmployeeStatus(this.selectEmployee['id'], this.modalAction)
            .then((res) => {
                if(res['code'] == 200) {
                    let params = this.getRequestParams();
                    this.getEmployeesList(params);
                    this.modal.close();
                }

                if(res['code'] == 1013) {
                    this.modalError = true;
                    this.modalErrorText = "Данный сотрудник используется в одной из задач, ожидающих выполнения (или приостановленных)";
                }
            });
    }

    toggleShowFilter() {
        if(this.showAdvFilter) {
            this.filteredEmail = "";
            this.filteredPosition = "";
            this.selectedOrgs = [];
        }
        this.showAdvFilter = !this.showAdvFilter;
    }
}
