/**
 * Created by Baser on 04.03.2017.
 */
import {Component} from '@angular/core';
import {EmployeeContext} from "../../../../models/employee/employee-context";
import {AppCoreService} from "../../../../services/app-core.service";
import {Router} from "@angular/router";
import {Employee} from "../../../../models/employee/employee";
import {Unit} from "../../../../models/unit";
import {Tag} from "../../../../models/tag/tag";
import {SelectorItem} from "../../../../models/selector-item";

@Component({
    selector: 'employee-create',
    template: require('./employee-create.component.html')
})

export class EmployeeCreateComponent {
    public newEmployee: Employee;

    public unitsTree: Array<Unit>;
    public tagsTree: Array<Tag>;

    public selectedUnits: Array<Unit> = [];
    public selectedTags: Array<Tag> = [];

    public fillFormError: boolean = false;
    public formErrorText: string = "";

    constructor(
        private employeeContext: EmployeeContext,
        private core: AppCoreService,
        private router: Router
    ) {
        this.newEmployee = new Employee("", "", "", "", [], [], "", "", "", "", "", "");
        this.getUnitsTree();
        this.getTagsTree();
    }

    createEmployee() {
        this.fillFormError = false;
        this.formErrorText = "";

        if(this.newEmployee.valid()) {
            if(this.newEmployee.validEmail()) {
                this.employeeContext.createEmployee(this.newEmployee)
                    .then(() => {
                        this.router.navigate(['/employees']);
                    })
                    .catch(() => {
                        this.formErrorText = "При добавлении сотрудника произошла ошибка";
                    });
            }
            else {
                this.fillFormError = true;
                this.formErrorText = "Введен неправильный адрес электронной почты";
                this.newEmployee.email = "";
            }
        }
        else {
            this.fillFormError = true;
            this.formErrorText = "Не все поля заполнены!"
        }
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    orgSelectorChange(selected) {
        // При фильтрации элементов в селекторе выбора организаций киадется событие change, игнорим этот кейс
        if(selected.target) {
            return;
        }
        this.selectedUnits = selected;
        this.orgItemsItemsToModel(this.selectedUnits);
    }

    tagSelectorChange(selected) {
        this.selectedTags = selected;
        this.tagItemsItemsToModel(this.selectedTags);
    }

    tagItemsItemsToModel(selectedArray: Array<SelectorItem>) {
        this.newEmployee.employeeTags = [];
        if(selectedArray.length != 0) {
            for(var item of selectedArray) {
                this.newEmployee.employeeTags.push(item);
            }
        }
    }

    orgItemsItemsToModel(selectedArray: Array<Unit>) {
        this.newEmployee.unit = [];
        if(selectedArray.length != 0) {
            for(var item of selectedArray) {
                this.newEmployee.unit.push(item);
            }
        }
    }
}