/**
 * Created by Baser on 06.03.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {EmployeeContext} from "../../../../models/employee/employee-context";
import {Router, ActivatedRoute} from "@angular/router";
import {AppCoreService} from "../../../../services/app-core.service";
import {Unit} from "../../../../models/unit";
import {Tag} from "../../../../models/tag/tag";
import {Employee} from "../../../../models/employee/employee";
import {SelectorItem} from "../../../../models/selector-item";
import {Modal} from "ngx-modal/index";

@Component({
  selector: 'employee-info',
  template: require('./employee-info.component.html')
})

export class EmployeeInfoComponent {
  public employeeEditParam = false;
  public employeeEdit = false;

  public unitsTree: Array<Unit>;
  public tagsTree: Array<Tag>;

  public employee: Employee;
  public changedEmployee: Employee;

  public selectedUnits: Array<Unit> = [];
  public selectedTags: Array<Tag> = [];

  public fillFormError: boolean = false;
  public formErrorText: string = "";

  public modalHeader: string = "";
  public modalText: string = "";
  public modalEmployeeName: string = "";
  public modalSubmitButtonText: string = "";
  public modalDelete: boolean = false;
  public modalRecover: boolean = false;
  public modalAction: string = "";

  public modalError: boolean = false;
  public modalErrorText: string = "";

  @ViewChild('myModal')
  private modal: Modal;

  constructor(private core: AppCoreService,
              private route: ActivatedRoute,
              private router: Router,
              private employeeContext: EmployeeContext) {
    this.route.queryParams.subscribe(params => {
      if (params['action'] == 'edit') {
        this.employeeEdit = true;
        this.employeeEditParam = true;
      }
    });
    this.getUnitsTree();
    this.getTagsTree();
  }

  saveEmployee():void {
    this.fillFormError = false;
    this.formErrorText = "";

    if (this.employee.equal(this.changedEmployee)) {
      if (this.employeeEditParam) {
        this.router.navigate(['/employees']);
      }
      else {
        this.employeeEdit = !this.employeeEdit;
      }
      return;
    }

    if (!this.changedEmployee.valid()) {
      this.fillFormError = true;
      this.formErrorText = "Не все поля заполнены!";
      return;
    }

    if (!this.changedEmployee.validEmail()) {
      this.fillFormError = true;
      this.formErrorText = "Введен неправильный адрес электронной почты";
      return;
    }

    this.employeeContext.saveEmployee(this.changedEmployee)
      .then(() => {
        if (this.employeeEditParam) {
          this.router.navigate(['/employees']);
        } else {
          this.employeeContext.getEmployeeById(this.route.snapshot.params['id'])
            .then((res) => {
              this.employee = res;
              this.changedEmployee = this.employee.clone();
              this.employeeEdit = !this.employeeEdit;
            });
        }
      })
      .catch(() => {
        this.fillFormError = true;
        this.formErrorText = "При добавлении сотрудника произошла ошибка";
      });
  }

  ngAfterViewInit() {
    this.employeeContext.getEmployeeById(this.route.snapshot.params['id'])
      .then((res: Employee) => {
        //Зарпет редактирования сотрудника со статусом отличным от "ENABLED"
        if (!(res.status != "ENABLED" && this.employeeEditParam == true)) {
          this.employee = res;
          if (this.employee) {
            this.changedEmployee = this.employee.clone();
            this.selectedUnits = this.employee.unit.slice();
            this.selectedTags = this.employee.employeeTags.slice();

          }
        } else {
          this.router.navigate(['/employees']);
        }
      });
  }

  getUnitsTree() {
    this.core.get("api/units/tree")
      .then((res) => {
        this.unitsTree = res['_embedded'];
      });
  }

  getTagsTree() {
    this.core.get("api/employee-tags/tree")
      .then((res) => {
        this.tagsTree = res['_embedded'];
      });
  }

  orgSelectorChange(selected) {
    // При фильтрации элементов в селекторе выбора организаций киадется событие change, игнорим этот кейс
    if(selected.target) {
      return;
    }
    this.selectedUnits = selected;
    this.orgItemsItemsToModel(this.selectedUnits);
  }

  tagSelectorChange(selected) {
    this.selectedTags = selected;
    this.tagItemsItemsToModel(this.selectedTags);
  }

  tagItemsItemsToModel(selectedArray: Array<SelectorItem>) {
    this.changedEmployee.employeeTags = [];
    if (selectedArray.length != 0) {
      for (var item of selectedArray) {
        this.changedEmployee.employeeTags.push(item);
      }
    }
  }

  orgItemsItemsToModel(selectedArray: Array<Unit>) {
    this.changedEmployee.unit = [];
    if (selectedArray.length != 0) {
      for (var item of selectedArray) {
        this.changedEmployee.unit.push(item);
      }
    }
  }

  toggleEdit() {
    this.selectedUnits = this.changedEmployee.unit.slice();
    this.selectedTags = this.changedEmployee.employeeTags.slice();

    this.employeeEdit = !this.employeeEdit;
  }

  changeEmployee(id, action) {
    this.modalEmployeeName = this.employee.shortName;
    switch (action) {
      case "delete":
        this.modalAction = action;
        this.modalRecover = false;
        this.modalDelete = (this.employee.status == 'ENABLED');
        this.modalHeader = "Удаление сотрудника";
        this.modalText = "Вы уверены, что хотите удалить сотрудника ";
        this.modalSubmitButtonText = "Удалить";
        break;
      case "recovery":
        this.modalAction = action;
        this.modalRecover = (this.employee.status == 'DELETED');
        this.modalDelete = false;
        this.modalHeader = "Восстановление сотрудника";
        this.modalText = "Вы уверены, что хотите восстановить сотрудника ";
        this.modalSubmitButtonText = "Восстановить";
        break;
    }
  }

  changeEmployeeAction() {
    this.employeeContext.changeEmployeeStatus(this.employee.id, this.modalAction)
      .then((res) => {
        if (res['code'] == 200) {
          this.employeeContext.getEmployeeById(this.route.snapshot.params['id'])
            .then((res) => {
              this.employee = res;
              this.changedEmployee = this.employee.clone();
              this.modal.close();
            });
        }
        if (res['code'] == 1013) {
          this.modalError = true;
          this.modalErrorText = "Данный сотрудник используется в одной из задач, ожидающих выполнения (или приостановленных)";
        }
      })
      .catch(() => {
        this.fillFormError = true;
        this.formErrorText = "При изменении статуса произошла ошибка";
        this.modal.close();
      });
  }
}
