/**
 * Created by Baser on 02.02.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {UserContext} from "../../../../models/user/user-context";
import {User} from "../../../../models/user/user";
import {Authority} from "../../../../models/user/authority";
import {SelectComponent} from "ng2-select/index";
import {AuthorityContext} from "../../../../models/user/authority-context";
import {AppCoreService} from "../../../../services/app-core.service";


@Component({
    selector: 'user-create',
    template: require('./user-create.component.html')
})

export class UserCreateComponent {
    public currentDateToString = new Date().toLocaleString("ru", {day: 'numeric', month: 'long', year: 'numeric'});

    public user:User;

    public fillFormError:boolean = false;
    public formErrorText:string = "";
    public oldPasswordError:boolean = false;

    public authoritiesList:Array<any>;
    public authorityItems:Array<any>;

    public unitsTree: Array<Object>;
    public selectedUnits: Array<Object>;

    @ViewChild(SelectComponent)
    private select:SelectComponent;


    constructor(private userContext:UserContext, private router:Router, private AuthorityContext:AuthorityContext, private core: AppCoreService) {
        this.user = this.userContext.newUser();
        this.getUnitsTree();
    }

    ngAfterViewInit() {
        //Получение списка ролей
        this.AuthorityContext.getAuthoritiesList()
            .then((data:Array<Authority>) => {
                this.authoritiesList = data;
                this.authorityItems = this.getAuthoritiesItems(this.authoritiesList);
            });
    }

    orgSelectorChange(selected) {
        this.selectedUnits = selected;
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }
    
    unitsItemsToModel(unitsArray) {
        if(!!unitsArray) {
            for (var unit of unitsArray) {
                this.user.units.push(unit['id']);
            }
        }
    }

    createUser() {
        this.fillFormError = false;
        this.formErrorText = "";
        this.oldPasswordError = false;

        this.authoritiesItemsToModel(this.authoritiesSelectorValue);
        this.unitsItemsToModel(this.selectedUnits);
        //Обнуление ошибок
        this.fillFormError = false;
        this.oldPasswordError = false;

        if (!(this.user.valid() && this.user.password != "")) {
            this.fillFormError = true;
            this.formErrorText = "Не все поля заполнены!";
            return;
        }

        this.userContext.userExist(this.user.username)
            .then((res) => {
                if (res['_embedded'] == true) {
                    this.fillFormError = true;
                    this.formErrorText = "Пользователь с таким логином уже существует";
                    this.user.username = "";
                    return;
                }

                //Проверка пароля на соблюдение требований безопасности
                if(!this.user.validPassword(this.user.password)) {
                    this.fillFormError = true;
                    this.formErrorText = "Пароль должен содержать не менее 8-ми символов, в числе которых дожны быть символы верхнего и нижнего регистра + цифры";
                    this.user.password = "";
                    return;
                }

                if (!this.user.validEmail()) {
                    this.fillFormError = true;
                    this.formErrorText = "Введен неправильный адрес электронной почты";
                    this.user.email = "";
                    return;
                }

                this.userContext.createUser(this.user)
                    .then(() => {
                        this.router.navigate(["/users"]);
                    })
                    .catch(() => {
                        this.formErrorText = "При выполнении запроса произошла ошибка!";
                    });
            });
    }

    //Запись выбранной роли в качестве id в измененную модель
    public authoritiesItemsToModel(item) {
        if(item != {}) {
            this.user.authority.id = item['id'];
        }
        else {
            this.user.authority.id = "";
        }
    }

    //Преобразование массива ролей в массив, который воспринимается селектором ролей
    public getAuthoritiesItems(authoritiesList:Array<Authority>) {
        var items = [];
        for (var item of authoritiesList) {
            items.push({"id": item['id'], "text": item['displayName']});
        }
        return items;
    }

    //Параметры и методы для работы селектора ролей
    private authoritiesSelectorValue:any = {id: ""};

    public selected(value:any):void {
        console.log('Selected value is: ', value);
    }

    public removed(value:any):void {
        console.log('Removed value is: ', value);
    }

    public typed(value:any):void {
        console.log('New search input: ', value);
    }

    public refreshValue(value:any):void {
        if (value.length == 0) {
            this.authoritiesSelectorValue = {id: ""};
        }
        else {
            this.authoritiesSelectorValue = value;
        }
    }
}