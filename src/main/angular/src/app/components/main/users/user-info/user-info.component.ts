/**
 * Created by Baser on 29.01.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserContext} from "../../../../models/user/user-context";
import {User} from "../../../../models/user/user";
import {AuthorityContext} from "../../../../models/user/authority-context";
import {Authority} from "../../../../models/user/authority";

import {SelectComponent} from "ng2-select/index";
import {AppCoreService} from "../../../../services/app-core.service";
import {Unit} from "../../../../models/unit";

@Component({
    selector: 'user-info',
    template: require('./user-info.component.html')
})

export class UserInfoComponent {

    public userEditParam = false;
    public userEdit = false;
    public user:User;
    public changedUser:User;
    public confirmPassword:string = "";

    public modalHeader:string = "";
    public modalText:string = "";
    public modalUserName:string = "";
    public modalSubmitButtonText:string = "";
    public modalDelete:boolean = false;
    public modalRecover:boolean = false;
    public modalAction:string = "";

    public fillFormError:boolean = false;
    public formErrorText:string = "";
    public passwordError:boolean = false;
    public oldPasswordError:boolean = false;

    public unitsTree:Array<Object>;
    public selectedUnits:Array<Unit>;

    public authoritiesList:Array<any>;
    public authorityItems:Array<any>;

    public registerDateToString = "";

    @ViewChild(SelectComponent)
    private select:SelectComponent;

    constructor(private core:AppCoreService, private userContext:UserContext, private AuthorityContext:AuthorityContext, private route:ActivatedRoute, private router:Router) {
        this.route.queryParams.subscribe(params => {
            if (params['action'] == 'edit') {
                this.userEdit = true;
                this.userEditParam = true;
            }
        });
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    unitsItemsToModel(unitsArray: Array<Unit>) {
        if(typeof unitsArray != "undefined") {
            this.changedUser.units = [];
            if (unitsArray.length != 0) {
                for (var unit of unitsArray) {
                    this.changedUser.units.push(unit);
                }
            }
        }
        else {
            this.changedUser.units = [];
        }
    }

    orgSelectorChange(selected) {
        this.selectedUnits = selected;
        this.unitsItemsToModel(this.selectedUnits);
    }

    ngAfterViewInit() {
        this.userContext.getUserById(this.route.snapshot.params['id'], 'new')
            .then((res:User) => {
                    //Зарпет редактирования пользователя со статусом "DELETED"
                    if (!(res.status == "DELETED" && this.userEditParam == true)) {
                        this.user = res;
                        if (this.user) {
                            this.changedUser = this.user.clone();
                            this.selectedUnits = this.changedUser.units;
                            this.registerDateToString = new Date(this.user.registerDate).toLocaleString("ru", {day: 'numeric', month: 'long', year: 'numeric'});
                        }
                        //Получение списка ролей
                        this.AuthorityContext.getAuthoritiesList()
                            .then((data:Array<Authority>) => {
                                this.authoritiesList = data;
                                this.authorityItems = this.getAuthoritiesItems(this.authoritiesList);
                                this.authoritiesSelectorValue = this.getAuthoritiesItems([this.user.authority]);
                                //Таймаут для ожидания инициализации компонента выбора роли, чтобы выдать ему
                                //начальное значение (текущую роль пользователя)
                                if (this.userEditParam) {
                                    setTimeout(() => {
                                        this.select.active = this.getAuthoritiesItems([this.user.authority]);
                                    }, 100);
                                }
                            });
                        //Получление дерева отделов
                        this.getUnitsTree();
                    } else {
                        this.router.navigate(['/users']);
                    }
                }
            );
    }

    toggleEdit() {
        this.userEdit = !this.userEdit;
        setTimeout(() => {
            this.select.active = this.getAuthoritiesItems([this.user.authority]);
        }, 100);
    }

    saveUser() {
        this.authoritiesItemsToModel(this.authoritiesSelectorValue);
        this.unitsItemsToModel(this.selectedUnits);
        //Обнуление ошибок
        this.fillFormError = false;
        this.passwordError = false;
        this.oldPasswordError = false;

        //Проверка изменения модели
        if (!(!this.changedUser.equal(this.user) || this.changedUser.password != "")) {
            if (this.userEditParam) {
                this.router.navigate(['users']);
            }
            else {
                this.userEdit = !this.userEdit;
            }
            return;
        }

        //Проверка заполненности полей
        if (!this.changedUser.valid()) {
            this.fillFormError = true;
            this.formErrorText = "Не все поля заполнены!";
            return;
        }

        //Проверка существования логина
        this.userContext.userExist(this.changedUser.username)
            .then((res) => {
                if (!(res['_embedded'] == false || this.changedUser.username == this.user.username)) {
                    this.fillFormError = true;
                    this.formErrorText = "Пользователь с таким логином уже существует";
                    this.changedUser.username = "";
                    return;
                }

                //Проверка пароля на соблюдение требований безопасности
                if(this.changedUser.password) {
                    if(!this.changedUser.validPassword(this.changedUser.password)) {
                        this.fillFormError = true;
                        this.formErrorText = "Пароль должен содержать не менее 8-ми символов, в числе которых дожны быть символы верхнего и нижнего регистра + цифры";
                        this.changedUser.password = "";
                        return;
                    }
                }

                //Проверка правлиьности ввода email
                if (!this.changedUser.validEmail()) {
                    this.fillFormError = true;
                    this.formErrorText = "Введен неправильный адрес электронной почты";
                    this.changedUser.email = "";
                    return;
                }

                this.userContext.editUser(this.changedUser)
                    .then(() => {
                        if (this.userEditParam) {
                            this.router.navigate(['users']);
                        } else {
                            //Обновление данных о пользователе
                            this.userContext.getUserById(this.route.snapshot.params['id'], 'new')
                                .then((res:User) => {
                                    this.user = res;
                                    this.changedUser = this.user.clone();
                                    this.userEdit = !this.userEdit;
                                    this.confirmPassword = "";
                                })
                                .catch((err) => {
                                    this.fillFormError = true;
                                    this.formErrorText = "При выполнении запроса произошла ошибка!";
                                });
                        }
                    })
                    .catch((err) => {
                        this.fillFormError = true;
                        this.formErrorText = "При выполнении запроса произошла ошибка!";
                    });

            })
            .catch((err) => {
                this.fillFormError = true;
                this.formErrorText = "При выполнении запроса произошла ошибка!";
            });
    }

    changeUser(id, action) {
        this.modalUserName = this.user.fullName + " (" + this.user.username + ")";
        switch (action) {
            case "disable":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = (this.user.status == 'ENABLED');
                this.modalHeader = "Блокировка пользователя";
                this.modalText = "Вы уверены, что хотите заблокировать пользователя ";
                this.modalSubmitButtonText = "Заблокировать";
                break;
            case "enable":
                this.modalAction = action;
                this.modalRecover = (this.user.status == 'LOCKED');
                this.modalDelete = false;
                this.modalHeader = "Разблокировка пользователя";
                this.modalText = "Вы уверены, что хотите разблокировать пользователя ";
                this.modalSubmitButtonText = "Разблокировать";
                break;
            case "recovery":
                this.modalAction = action;
                this.modalRecover = (this.user.status == 'DELETED');
                this.modalDelete = false;
                this.modalHeader = "Восстановление пользователя";
                this.modalText = "Вы уверены, что хотите восстановить пользователя ";
                this.modalSubmitButtonText = "Восстановить";
                break;
            case "delete":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = true;
                this.modalHeader = "Удаление пользователя";
                this.modalText = "Вы уверены, что хотите удалить пользователя ";
                this.modalSubmitButtonText = "Удалить";
                break;
        }
    }

    changeUserAction() {
        this.userContext.changeUserStatus(this.user.id, this.modalAction)
            .then(() => {
                this.userContext.getUserById(this.user.id, 'new')
                    .then((res:User) => {
                        this.user = res;
                        if (this.user) {
                            this.changedUser = this.user.clone();
                        }
                    });
            });
    }

    public authoritiesItemsToModel(item: Array<Object>) {
        this.changedUser.authority.id = item[0]['id'];
    }

    //Преобразование массива ролей в массив, который воспринимается селектором ролей
    public getAuthoritiesItems(authoritiesList:Array<Authority>) {
        var items = [];
        for (var item of authoritiesList) {
            items.push({"id": item['id'], "text": item['displayName']});
        }
        return items;
    }

    //Параметры и методы для работы селектора ролей
    private authoritiesSelectorValue:any = [{id: ""}];

    public selected(value:any):void {
        console.log('Selected value is: ', value);
    }

    public removed(value:any):void {
        console.log('Removed value is: ', value);
    }

    public typed(value:any):void {
        console.log('New search input: ', value);
    }

    public refreshValue(value:Object):void {
        if (!value.hasOwnProperty('id')) {
            this.authoritiesSelectorValue = [{id: ""}];
        }
        else {
            this.authoritiesSelectorValue = [value];
        }
    }

    roleToString(role) {
        let res = "";
        switch(role) {
            case "ROLE_MANAGER":
                res = "Менеджер";
                break;
            case "ROLE_ADMINISTRATOR":
                res = "Администратор";
                break;
            case "ROLE_SECURITY_OFFICER":
                res = "Инспектор информационной безопасности";
                break;
        }
        return res;
    }

}