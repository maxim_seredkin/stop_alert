/**
 * Created by Baser on 25.01.2017.
 */
import {Component, ViewChild, ElementRef} from '@angular/core';
import {AppCoreService} from "../../../../services/app-core.service";
import {Pagination} from "../../../../models/pagination";
import {Router, NavigationExtras} from "@angular/router";
import {AuthorityContext} from "../../../../models/user/authority-context";
import {Authority} from "../../../../models/user/authority";
import {UnitsTreeSelectorComponent} from "../../units-tree-selector/units-tree-selector.component";

@Component({
    selector: 'users-list',
    template: require('./users-list.component.html')
})

export class UsersListComponent {
    private users:Array<Object>;
    private pagination:Pagination;
    private filteredFullName:string = "";

    public modalHeader:string = "";
    public modalText:string = "";
    public modalUserName:string = "";
    public modalSubmitButtonText:string = "";
    public modalDelete:boolean = false;
    public modalRecover:boolean = false;
    public modalAction:string = "";

    private selectUser:Object;
    private selectedOrgs: Array<Object>;

    private unitsTree: Array<Object>;

    public showAdvFilter:boolean = false;

    public authorityItems:Array<any>;
    public authoritiesSelectorValue: Object = {id: "", text: ''};

    @ViewChild('orgSelector') orgSelector: UnitsTreeSelectorComponent;

    constructor(private core:AppCoreService,
                private router: Router,
                private AuthorityContext:AuthorityContext) {
        this.pagination = new Pagination();
        let params = this.getRequestParams();
        this.getUsersList(params);
        this.getUnitsTree();

        this.AuthorityContext.getAuthoritiesList()
            .then((data:Array<Authority>) => {
                this.authorityItems = this.getAuthoritiesItems(data);
                }
            );
    }

    changeUser(id, action) {
        this.selectUser = this.getSelectedUser(id);

        this.modalUserName = this.selectUser['fullName']+" "+"(" + this.selectUser['username'] + ")";
        switch (action) {
            case "disable":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = (this.selectUser['status'] == 'ENABLED');
                this.modalHeader = "Блокировка пользователя";
                this.modalText = "Вы уверены, что хотите заблокировать пользователя ";
                this.modalSubmitButtonText = "Заблокировать";
                break;
            case "enable":
                this.modalAction = action;
                this.modalRecover = (this.selectUser['status'] == 'LOCKED');
                this.modalDelete = false;
                this.modalHeader = "Разблокировка пользователя";
                this.modalText = "Вы уверены, что хотите разблокировать пользователя ";
                this.modalSubmitButtonText = "Разблокировать";
                break;
            case "recovery":
                this.modalAction = action;
                this.modalRecover = (this.selectUser['status'] == 'DELETED');
                this.modalDelete = false;
                this.modalHeader = "Восстановление пользователя";
                this.modalText = "Вы уверены, что хотите восстановить пользователя ";
                this.modalSubmitButtonText = "Восстановить";
                break;
            case "delete":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = true;
                this.modalHeader = "Удаление пользователя";
                this.modalText = "Вы уверены, что хотите удалить пользователя ";
                this.modalSubmitButtonText = "Удалить";
                break;
        }
    }

    //Преобразование массива ролей в массив, который воспринимается селектором ролей
    public getAuthoritiesItems(authoritiesList:Array<Authority>) {
        var items = [];
        for (var item of authoritiesList) {
            items.push({"id": item['id'], "text": item['displayName'], "authority": item['authority']});
        }
        return items;
    }

    getSelectedUser(id) {
        let tmpUser = {};
        for (var i = 0; i < this.users.length; i++) {
            if (this.users[i]['id'] == id + "") {
                tmpUser = this.users[i];
                break;
            }
        }
        return tmpUser;
    }

    changeUserAction() {
        this.core.put("api/users/" + this.selectUser['id'] + "/" + this.modalAction)
            .then(() => {
                this.selectUser = null;
                this.applyFilter();
            })
            .catch(() => {
                this.selectUser = null;
            });
    }


    applyFilter() {
        this.pagination.number = 1;
        let params = this.getRequestParams();
        this.getUsersList(params);
    }

    orgSelectorChange(selected) {
        this.selectedOrgs = selected;
    }

    changePagination(event:Pagination) {
        let params = this.getRequestParams();
        this.getUsersList(params);
    }

    public params = {
        'page': '1',
        'size': '1',
        'searchs': '"username":"admin"'
    };

    getUsersList(params) {
        this.core.get("api/users", params)
            .then((res) => {
                this.users = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getRequestParams():Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;
        params['searchs'] = "";
        params['filters'] = "";

        if (this.filteredFullName !== "") {
            params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'fullName' LIKE '${this.filteredFullName}'`;
        }

        if (this.authoritiesSelectorValue['id'] !== "") {
            params['filters'] += `${(params['filters']) ? ',' : ''}'authority.authority' LIKE '${this.authoritiesSelectorValue['authority']}'`;
        }

        if (this.selectedOrgs) {
            for (var unit of this.selectedOrgs) {
                if (params['filters'] == "") {
                    params['filters'] += "EXTRA:'units' EQ '"+unit['id']+"'";
                }
                else {
                    params['filters'] += ",EXTRA:'units' EQ '"+unit['id']+"'";
                }
            }
        }
        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        return params;
    }

    editUser(id: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "action": "edit",
            }
        };
        this.router.navigate(['users/', id], navigationExtras);
    }

    toggleShowFilter() {
        if (this.showAdvFilter) {
            this.orgSelector.resetValue();
            this.authoritiesSelectorValue = { id: "", text: "" };
            this.selectedOrgs = [];
        }
        this.showAdvFilter = !this.showAdvFilter;
    }

    refreshAuthoritiesValue(selected) {
        for(let item of this.authorityItems) {
            if(selected.id === item['id']) {
                this.authoritiesSelectorValue = item;
                break;
            }
        }
    }
}