/**
 * Created by Baser on 09.03.2017.
 */
import {ChangeDetectorRef, Component, Inject} from '@angular/core';
import {AppCoreService} from "../../../../services/app-core.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UUID} from "angular2-uuid";
import {SelectorItem} from "../../../../models/selector-item";
import {PageScrollInstance, PageScrollService} from "ng2-page-scroll/ng2-page-scroll";
import {DOCUMENT} from "@angular/platform-browser";

@Component({
  selector: 'employee-tags-edit',
  template: require('./employee-tags-edit.component.html')
})

export class EmployeeTagsEditComponent {
  private tagsTree: Array<Object>;
  public editEntityId: string;
  public pathToEntity: string;
  public isInit: boolean = false;

  constructor(private cdRef: ChangeDetectorRef,
              private pageScrollService: PageScrollService,
              @Inject(DOCUMENT) private document: Document,
              private core: AppCoreService,
              private route: ActivatedRoute,
              private router: Router) {
    this.getTagsTree();

    this.route.queryParams.subscribe(params => {
      if (params['editEntity']) {
        this.editEntityId = params['editEntity'];
      }
    });
    this.route.queryParams.subscribe(params => {
      if (params['path']) {
        this.pathToEntity = params['path'].split(",");
      }
    });
  }

  endInitComponent() {
    this.isInit = true;
    this.cdRef.detectChanges();
  }

  getTagsTree() {
    this.core.get("api/employee-tags/tree")
      .then((res) => {
        this.tagsTree = res['_embedded'];
      });
  }

  addEntity() {
    let uuid = UUID.UUID();

    this.tagsTree.push(new SelectorItem(uuid, "", [], null, true));
  }

  scrollToElement(id: string, duration: number) {
    let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, "#" + id);
    this.pageScrollService.start(pageScrollInstance);
  }
}
