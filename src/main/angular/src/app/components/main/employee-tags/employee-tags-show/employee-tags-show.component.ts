/**
 * Created by Baser on 10.03.2017.
 */
import {Component} from '@angular/core';
import {AppCoreService} from "../../../../services/app-core.service";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'employee-tags-show',
    template: require('./employee-tags-show.component.html')
})

export class EmployeeTagsShowComponent {
    public employeeTag:Object;

    constructor(
        private core: AppCoreService,
        private route: ActivatedRoute
    ) {
        
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let id = params['id'];
            this.getEmployeeTagById(id);
        });
    }

    getEmployeeTagById(id: string) {
        this.core.get("api/employee-tags/"+id)
            .then((res) => {
                this.employeeTag = res['_embedded'];
            })
    }
}