import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { MainComponent } from "./main.component";
import { LeftBarComponent } from "./left-bar/left-bar.component";
import { NavBarComponent } from "./nav-bar/nav-bar.component";
import { MyAccountComponent } from "./my-account/my-account.component";
import { MenuItemComponent } from "./left-bar/menu-item/menu-item.component";
import { UsersListComponent } from "./users/users-list/users-list.component";
import { UserInfoComponent } from "./users/user-info/user-info.component";
import { SelectModule } from "ng2-select/index";
import { PaginationComponent } from "./pagination/pagination.component";
import { ModalModule } from "ngx-modal";
import { UserCreateComponent } from "./users/user-create/user-create.component";
import { TreeSelectorComponent } from "./tree-selector/tree-selector.controller";
import { TreeViewComponent } from "./tree-selector/tree-view.component";
import { TaskCreateComponent } from "./tasks/task-create/task-create.component";
import { CalendarModule, CheckboxModule, RadioButtonModule, TriStateCheckboxModule } from 'primeng/primeng';
import { TasksListComponent } from "./tasks/tasks-list/tasks-list.component";
import { TaskInfoComponent } from "./tasks/task-info/task-info.component";
import { EmployeesListComponent } from "./employees/employees-list/employees-list.component";
import { EmployeeCreateComponent } from "./employees/employee-create/employee-create.component";
import { EmployeeInfoComponent } from "./employees/employee-info/employee-info.component";
import { EmployeeTagsEditComponent } from "./employee-tags/employee-tags-edit/employee-tags-edit.component";
import { EntityTreeComponent } from "./entity-tree/entity-tree.component";
import { Ng2PageScrollModule } from "ng2-page-scroll/ng2-page-scroll";
import { EmployeeTagsShowComponent } from "./employee-tags/employee-tags-show/employee-tags-show.component";
import { EmployeesLoadComponent } from "./employees/employees-load/employees-load.component";
import { FileUploaderComponent } from "./file-uploader/file-uploader.component";
import { UnitsEditComponent } from "./units/units-edit/units-edit.component";
import { UnitsShowComponent } from "./units/units-show/units-show.component";
import { TaskReportComponent } from "./reports/task/task-report/task-report.component";
import { ChartsModule } from "ng2-charts/index";
import { EmployeeReportComponent } from "./reports/employee/employee-report/employee-report.component";
import { NotFoundComponent } from "../error-component/not-found/not-found.component";
import { SafeHtmlPipe } from "../../pipes/save-html.pipe";
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";
import { ForbiddenComponent } from "../error-component/forbidden/forbidden.component";
import { TaskReportsListComponent } from "./reports/task/task-reports-list/task-reports-list.component";
import { HomeComponent } from "./home/home.component";
import { MailServerSettingsComponent } from "./mail-server/mail-server-settings.component";
import { LicenseSettingsComponent } from "./license/license-settings.component";
import { TextMaskModule } from "angular2-text-mask/dist/angular2TextMask";
import { UnitReportsListComponent } from "./reports/unit/unit-reports-list/unit-reports-list.component";
import { EmployeeReportsListComponent } from "./reports/employee/employee-reports-list/employee-reports-list.component";
import { UnitReportComponent } from "./reports/unit/unit-report/unit-report.component";
import { UnitsLoadComponent } from "./units/units-load/units-load.component";
import { UnitsLoadFilterComponent } from "./units/units-load/units-load-filter/units-load-filter.component";
import { UnitsErrorTableComponent } from "./units/units-load/units-error-table/units-error-table.component";
import { UnitsPaginationComponent } from "./units/units-load/units-pagination/units-pagination.component";
import { UnitsValidTableComponent } from "./units/units-load/units-valid-table/units-valid-table.component";
import { UnitsValidTableRowComponent } from "./units/units-load/units-valid-table/units-valid-table-row/units-valid-table-row.component";
import { MailTemplatesListComponent } from "./mail-templates/mail-templates-list/mail-templates-list.component";
import { MailTemplateCreateComponent } from "./mail-templates/mail-template-create/mail-template-create.component";
import { MailTemplateInfoComponent } from "./mail-templates/mail-template-info/mail-template-info.component";
import { SystemSettingsComponent } from "./system-settings/system-settings.component";
import { UnitsTreeSelectorComponent } from "./units-tree-selector/units-tree-selector.component";
import { UnitsTreeViewComponent } from "./units-tree-selector/units-tree-view.component";
import { EmployeesListPaginationComponent } from './tasks/employees-list-pagination/employees-list-pagination.component';
import { LdapSettingsComponent } from './ldap-settings/ldap-settings.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SelectModule,
    FormsModule,
    ModalModule,
    BrowserModule,
    CalendarModule,
    CheckboxModule,
    Ng2PageScrollModule.forRoot(),
    ChartsModule,
    RadioButtonModule,
    TextMaskModule,
    TriStateCheckboxModule
  ],
  declarations: [
    MainComponent,
    LeftBarComponent,
    NavBarComponent,
    MenuItemComponent,
    MyAccountComponent,
    UsersListComponent,
    UserInfoComponent,
    PaginationComponent,
    UserCreateComponent,
    TreeSelectorComponent,
    TreeViewComponent,
    TaskCreateComponent,
    TasksListComponent,
    TaskInfoComponent,
    EmployeesListComponent,
    EmployeeCreateComponent,
    EmployeeInfoComponent,
    EmployeeTagsEditComponent,
    EntityTreeComponent,
    EmployeeTagsShowComponent,
    EmployeesLoadComponent,
    FileUploaderComponent,
    UnitsEditComponent,
    UnitsShowComponent,
    TaskReportComponent,
    EmployeeReportComponent,
    NotFoundComponent,
    SafeHtmlPipe,
    ForgotPasswordComponent,
    ForbiddenComponent,
    TaskReportsListComponent,
    HomeComponent,
    MailServerSettingsComponent,
    LicenseSettingsComponent,
    UnitReportsListComponent,
    EmployeeReportsListComponent,
    UnitReportComponent,
    UnitsLoadFilterComponent,
    UnitsLoadComponent,
    UnitsErrorTableComponent,
    UnitsPaginationComponent,
    UnitsValidTableComponent,
    UnitsValidTableRowComponent,
    MailTemplatesListComponent,
    MailTemplateCreateComponent,
    MailTemplateInfoComponent,
    SystemSettingsComponent,
    UnitsTreeSelectorComponent,
    UnitsTreeViewComponent,
    EmployeesListPaginationComponent,
    LdapSettingsComponent
  ],
  exports: [
    MainComponent,
  ]
})
export class MainModule {
}
