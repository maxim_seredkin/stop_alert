/**
 * Created by Baser on 09.03.2017.
 */
import {
    Component, Input, Output, ViewChild, ElementRef, Inject, ChangeDetectorRef} from '@angular/core';
import {SelectorItem} from "../../../models/selector-item";
import {PageScrollService, PageScrollInstance} from "ng2-page-scroll/ng2-page-scroll";
import {DOCUMENT} from "@angular/platform-browser";
import {UUID} from 'angular2-uuid';
import {AppCoreService} from "../../../services/app-core.service";
import {Modal} from "ngx-modal/index";
import {Router, NavigationExtras} from "@angular/router";
import {EventEmitter} from "@angular/common/src/facade/async";
import {TreeSelectorComponent} from "../tree-selector/tree-selector.controller";
import {UserService} from "../../../services/user.service";

@Component({
    selector: 'entity-tree',
    template: require('./entity-tree.component.html')
})

export class EntityTreeComponent {
    @Input() entityArray:Array<SelectorItem>;
    @Input() entity:SelectorItem;

    @Input() orgsArray:Array<Object>;
    @Input() org:Object;
    @Input() editedOrg:Object;

    @Input() showEntity:Object;
    @Input() type:string;
    @Input() action:string;
    @Input() children:boolean;
    @Input() entityIndex:string;
    @Input() editEntityId:string;
    @Input() pathToEntity:Array<string>;

    //Для организаций
    @Input() managers:Array<Object>;
    @Input() officers:Array<Object>;

    //Для того чтобы элемент не скролился после повторного открытия
    //в случае наличия параметров editEntity и path
    @Input() isInit:boolean;
    //Событиие, которое отрабатывает когда найшелся элемент с id == editEntity
    @Output() endInit:EventEmitter<any> = new EventEmitter();
    //Событие для изменения количества сотрудников в отделе, который был выбран приемником
    @Output() successorAccept:EventEmitter<any> = new EventEmitter();

    public pathToEntityChild:Array<string>;

    public editedEntity:SelectorItem;

    public showChildren:boolean = false;
    public entityStatus:boolean;

    public newEntityFlag:boolean = false;
    public editedEntityFlag:boolean = false;

    public fillError:boolean = false;

    public isLoad:boolean = false;
    //Наличие обводки вокруг элемента
    public isOutline:boolean = false;

    //Для модалки (перемещение сотрудников)
    public modalUnitsTree:Array<Object>;
    public employeeMoveOrg:Array<Object>;
    public moveEmployeeError:boolean = false;

    public moveEmployeeModalError: boolean = false;

    @ViewChild('container')
    private container:ElementRef;

    @ViewChild('myModal')
    private modal:Modal;

    @ViewChild('moveEmployees')
    private moveEmployeesModal:Modal;

    @ViewChild('moveEmployees2')
    private moveEmployeesModalShow:Modal;

    @ViewChild('emptyUnitDeleteModal')
    private emptyUnitDeleteModal:Modal;

    @ViewChild('orgMoveSelector') orgMoveSelector:TreeSelectorComponent;

    public modalError: boolean = false;
    public modalErrorText: string = "";

    constructor(private pageScrollService:PageScrollService,
                @Inject(DOCUMENT) private document:Document,
                private core:AppCoreService,
                private router:Router,
                private cdRef:ChangeDetectorRef,
                private user: UserService) {
    }

    ngOnInit() {
        if (this.type == 'tags') {
            if (this.action == 'edit') {
                if (this.entity.name == "") {
                    this.newEntityFlag = true;
                    this.isOutline = true;
                }
                this.entityStatus = !this.entity.used;
                this.editedEntity = new SelectorItem(this.entity.id, this.entity.name, this.entity.nodes, this.entity.parent, this.entity.used)
            }

            if (this.pathToEntity != null && this.pathToEntity != undefined) {
                if (this.pathToEntity.length != 0) {
                    if (this.pathToEntity[0] == this.entity.id) {
                        this.showChildren = true;
                    }
                    this.pathToEntityChild = this.pathToEntity.slice(1, this.pathToEntity.length);
                }
            }
            if (!!this.editEntityId) {
                if (this.editEntityId == this.entity.id && this.isInit == false) {
                    this.isOutline = true;
                }
            }
        }

        if (this.type == 'units') {
            if (this.action == 'edit') {
                if (this.org['name'] == "") {
                    this.newEntityFlag = true;
                    this.isOutline = true;
                }
                this.editedOrg = JSON.parse(JSON.stringify(this.org));
            }

            if (this.pathToEntity != null && this.pathToEntity != undefined) {
                if (this.pathToEntity.length != 0) {
                    if (this.pathToEntity[0] == this.org['id']) {
                        this.showChildren = true;
                    }
                    this.pathToEntityChild = this.pathToEntity.slice(1, this.pathToEntity.length);
                }
            }
            if (!!this.editEntityId) {
                if (this.editEntityId == this.org['id'] && this.isInit == false) {
                    this.isOutline = true;
                }
            }
        }
        this.isLoad = true;
    }

    successorPop(event) {
        this.org['countEmployees'] -= event['count'];
        this.successorAccept.emit(event);
    }

    officerSelectorChange(value) {
        this.org['officers'] = value;
    }

    managerSelectorChange(value) {
        this.org['managers'] = value;
    }

    ngAfterViewInit() {
        if (this.type == 'tags') {
            if (this.newEntityFlag) {
                setTimeout(this.scrollToElement(this.entity.id, 450), 200);
            }
            if (!!this.editEntityId) {
                if (this.editEntityId == this.entity.id && this.isInit == false) {
                    this.scrollToElement(this.editEntityId, 1000);
                    this.endInit.emit();
                    this.cdRef.detectChanges();
                }
            }
        }

        if (this.type == 'units') {
            if (this.newEntityFlag) {
                setTimeout(this.scrollToElement(this.org['id'], 450), 200);
            }
            if (!!this.editEntityId) {
                if (this.editEntityId == this.org['id'] && this.isInit == false) {
                    this.scrollToElement(this.editEntityId, 1000);
                    this.endInit.emit();
                    this.cdRef.detectChanges();
                }
            }
        }
    }

    endInitPop() {
        this.endInit.emit();
    }

    toggleShow() {
        this.showChildren = !this.showChildren;
    }

    addEntity() {
        if (this.type == 'tags') {
            let uuid = UUID.UUID();

            if (!this.showChildren) {
                this.showChildren = !this.showChildren;
            }

            this.entity.nodes.push(new SelectorItem(uuid, "", [], new SelectorItem(this.entity.id, this.entity.name, [], null, this.entity.used), true));
        }

        if (this.type == 'units') {
            let uuid = UUID.UUID();

            if (!this.showChildren) {
                this.showChildren = !this.showChildren;
            }
            let parent = new Object();
            parent['id'] = this.org['id'];
            parent['name'] = this.org['name'];
            parent['managers'] = this.org['managers'];
            parent['officers'] = this.org['officers'];
            parent['countEmployees'] = this.org['countEmployees'];
            parent['status'] = 'ENABLED';

            let newOrg = new Object();
            newOrg['id'] = uuid;
            newOrg['name'] = "";
            newOrg['managers'] = [];
            newOrg['officers'] = [];
            newOrg['countEmployees'] = 0;
            newOrg['status'] = 'ENABLED';
            newOrg['nodes'] = [];
            newOrg['parent'] = parent;
            this.org['nodes'].push(newOrg);
        }
    }


    scrollToElement(id:string, duration:number) {
        let pageScrollInstance:PageScrollInstance = PageScrollInstance.simpleInstance(this.document, "#" + id)
        this.pageScrollService.start(pageScrollInstance);
    }

    delEntity() {
        this.modalError = false;
        this.modalErrorText = "";

        if (this.type == 'tags') {
            if (this.newEntityFlag) {
                this.entityArray.splice(this.entityArray.indexOf(this.entity), 1);
            }
            else {
                this.core.put("api/employee-tags/" + this.entity.id + "/delete")
                    .then((res) => {
                        if (res['code'] == 200) {
                            for (let item of this.entityArray) {
                                if (item.id == this.entity.id) {
                                    this.entityArray.splice(this.entityArray.indexOf(this.entity), 1);
                                }
                            }
                            this.modal.close();
                        }

                        if (res['code'] == 1013) {
                            this.modalError = true;
                            this.modalErrorText = "Данный тег используется в одной из задач, ожидающих выполнения (или приостановленных)";
                        }
                    });
            }
            if (this.entityArray.length == 0) {
                this.showChildren = false;
            }
        }

        if (this.type == 'units') {
            this.moveEmployeeError = false;
            if (this.newEntityFlag) {
                this.orgsArray.splice(this.orgsArray.indexOf(this.org), 1);
            }
            else {
                if (this.org['countEmployees'] != 0) {
                    if (this.employeeMoveOrg.length != 0) {
                        this.orgMoveSelector.resetValue();

                        let data = new Object();
                        data['successor'] = this.employeeMoveOrg[0]['id'];

                        let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                        this.core.put("api/units/" + this.org['id'] + "/delete", putData)
                            .then((res) => {
                                if (res['code'] == 200) {
                                    this.moveEmployeesModal.close();
                                    this.successorAccept.emit({
                                        "id": this.employeeMoveOrg[0]['id'],
                                        "count": this.org['countEmployees'],
                                        "parent": this.org['parent']
                                    });
                                    this.orgsArray.splice(this.orgsArray.indexOf(this.org), 1);
                                    this.emptyUnitDeleteModal.close();
                                }
                                if (res['code'] == 1013) {
                                    this.modalError = true;
                                    this.modalErrorText = "Данная организация используется в одной из задач, ожидающих выполнения (или приостановленных)";
                                }
                            });
                    }
                    else {
                        this.moveEmployeeError = true;
                    }
                }
                else {
                    let putData = JSON.parse('{"form": {}}');
                    this.core.put("api/units/" + this.org['id'] + "/delete", putData)
                        .then((res) => {
                            if (res['code'] == 200) {
                                this.emptyUnitDeleteModal.close();
                                this.orgsArray.splice(this.orgsArray.indexOf(this.org), 1);
                            }

                            if (res['code'] == 1013) {
                                this.modalError = true;
                                this.modalErrorText = "Данная организация используется в одной из задач, ожидающих выполнения (или приостановленных)";
                            }
                        });
                }
            }
        }
    }

    getNodesEmployeesCount(nodes:Array<Object>) {
        let count:number = 0;
        for (let item of nodes) {
            count += item['countEmployees'];
            if (item['nodes'] != null) {
                count += this.getNodesEmployeesCount(item['nodes']);
            }
        }
        return count;
    }

    orgEntityDelete() {
        this.modalError = false;
        this.modalErrorText = "";

        this.modalUnitsTree = [];
        this.employeeMoveOrg = [];
        this.modalUnitsTree = null;
        this.moveEmployeeModalError = false;

        let params = new Object();
        if (this.type == 'units' && this.action == 'show') {
            params['filters'] = "EXTRA:'units' NE '" + this.showEntity['id'] + "'";
        }

        if (this.type == 'units' && this.action == 'edit') {
            params['filters'] = "EXTRA:'units' NE '" + this.org['id'] + "'";
        }

        this.core.get('api/units/tree', params)
            .then((res) => {
                this.modalUnitsTree = res['_embedded'];
                if(this.modalUnitsTree.length == 0) {
                    this.modalError = true;
                    this.modalErrorText = "Нет доступных подразделений для переноса";
                }
            });

        if (this.action == 'edit') {
            if (this.org['countEmployees'] != 0) {
                this.moveEmployeesModal.open();
            }
            else {
                this.emptyUnitDeleteModal.open();
            }
        }
        if (this.action == 'show') {
            if (this.showEntity['countEmployees'] != 0) {
                this.moveEmployeesModalShow.open();
            }
            else {
                this.emptyUnitDeleteModal.open();
            }
        }
    }

    employeesMoveOrgChange(value) {
        this.employeeMoveOrg = value;
    }

    delShowEntity() {
        this.modalError = false;
        this.modalErrorText = "";

        if (this.type == 'tags') {
            this.core.put("api/employee-tags/" + this.showEntity['id'] + "/delete")
                .then((res) => {
                    if (res['code'] == 200) {
                        this.router.navigate(['employee-tags']);
                        this.modal.close();
                    }
                    if (res['code'] == 1013) {
                        this.modalError = true;
                        this.modalErrorText = "Данный тег используется в одной из задач, ожидающих выполнения (или приостановленных)";
                    }
                });
        }
        if (this.type == 'units') {
            if (this.showEntity['countEmployees'] != 0) {
                if (this.employeeMoveOrg.length != 0) {
                    this.orgMoveSelector.resetValue();

                    let data = new Object();
                    data['successor'] = this.employeeMoveOrg[0]['id'];

                    let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                    this.core.put("api/units/" + this.showEntity['id'] + "/delete", putData)
                        .then(() => {
                            this.moveEmployeesModalShow.close();
                            this.router.navigate(['units']);
                        });
                }
                else {
                    this.moveEmployeeError = true;
                }
            }
            else {
                let putData = JSON.parse('{"form": {}}');
                this.core.put("api/units/" + this.showEntity['id'] + "/delete", putData)
                    .then((res) => {
                        if (res['code'] == 200) {
                            this.emptyUnitDeleteModal.close();
                            this.router.navigate(['units']);
                        }

                        if (res['code'] == 1013) {
                            this.modalError = true;
                            this.modalErrorText = "Данная организация используется в одной из задач, ожидающих выполнения (или приостановленных)";
                        }
                    });
            }
        }
    }

    saveEntity() {
        this.fillError = false;
        if (this.type == "tags") {
            if (this.entity.name == "") {
                this.fillError = true;
            }
            else {
                let data = {};
                data['name'] = this.entity.name;
                if (this.entity.parent != null) {
                    data['parent'] = this.entity.parent.id;
                }
                data['used'] = false;
                let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                this.core.post_json("api/employee-tags", postData)
                    .then((res) => {
                        if (res['code'] == 200) {
                            this.newEntityFlag = false;
                            this.entity.id = res['_embedded']['id'];

                            this.editedEntity.id = this.entity.id;
                            this.editedEntity.name = this.entity.name;
                        }
                    })
            }
        }

        if (this.type == 'units') {
            if (this.org['name'] == "") {
                this.fillError = true;
            }
            else {
                let data = {};
                data['name'] = this.org['name'];

                if (this.org['parent'] != null) {
                    data['parent'] = this.org['parent']['id'];
                }
                else {
                    data['parent'] = null;
                }
                data['users'] = [];
                for (let user of this.org['officers']) {
                    data['users'].push(user['id']);
                }
                for (let user of this.org['managers']) {
                    data['users'].push(user['id']);
                }
                let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                this.core.post_json("api/units", postData)
                    .then((res) => {
                        if (res['code'] == 200) {
                            this.newEntityFlag = false;
                            this.org['id'] = res['_embedded']['id'];

                            this.editedOrg['id'] = this.org['id'];
                            this.editedOrg['managers'] = this.org['managers'].slice();
                            this.editedOrg['officers'] = this.org['officers'].slice();
                        }
                    })
            }
        }
    }

    editEntity() {
        this.fillError = false;
        if (this.type == "tags") {
            if (this.entity.name == "") {
                this.fillError = true;
            }
            else {
                let data = {};
                data['name'] = this.entity.name;
                if (this.entity.parent != null) {
                    data['parent'] = this.entity.parent.id;
                }
                data['used'] = false;
                let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                this.core.put("api/employee-tags/" + this.entity.id, postData)
                    .then((res) => {
                        if (res['code'] == 200) {
                            this.editedEntityFlag = false;
                            this.entity.used = !this.entityStatus;
                            this.editedEntity = new SelectorItem(this.entity.id, this.entity.name, this.entity.nodes, this.entity.parent, this.entity.used);

                            this.editedEntity.id = this.entity.id;
                            this.editedEntity.name = this.entity.name;
                        }
                    })
            }
        }
        else {
            if (this.org['name'] == "") {
                this.fillError = true;
            }
            else {
                let data = {};
                data['name'] = this.org['name'];
                if (this.org['parent'] != null) {
                    data['parent'] = this.org['parent']['id'];
                }
                else {
                    data['parent'] = null;
                }
                data['users'] = [];
                for (let user of this.org['officers']) {
                    data['users'].push(user['id']);
                }
                for (let user of this.org['managers']) {
                    data['users'].push(user['id']);
                }
                let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                this.core.put("api/units/" + this.org['id'], putData)
                    .then((res) => {
                        if (res['code'] == 200) {
                            this.editedEntityFlag = false;
                            this.editedOrg = res['_embedded'];

                            this.editedOrg['id'] = this.org['id'];
                            this.editedOrg['managers'] = this.org['managers'].slice();
                            this.editedOrg['officers'] = this.org['officers'].slice();
                        }
                    })
            }
        }
    }

    isChanged() {
        if (this.type == 'tags') {
            if (!this.newEntityFlag) {
                this.editedEntityFlag = !((this.editedEntity.name == this.entity.name) && (this.entityStatus == !this.editedEntity.used));
            }
        }

        if (this.type == 'units') {
            if (!this.newEntityFlag) {
                this.editedEntityFlag = !((this.editedOrg['name'] == this.org['name']) &&
                this.unitsEqual(this.org['managers'], this.editedOrg['managers']) &&
                this.unitsEqual(this.org['officers'], this.editedOrg['officers']));
            }
        }
    }

    //Для сравнения массивов
    unitsEqual(firstUnits:Array<any>, secondUnits:Array<any>) {
        var flag = true;
        if (firstUnits.length == secondUnits.length) {
            for (var unit of firstUnits) {
                for (var unit2 of secondUnits) {
                    if (unit['id'] != unit2['id']) {
                        flag = false;
                    }
                    else {
                        flag = true;
                        break;
                    }
                }
            }
        }
        else {
            flag = false;
        }

        return flag;
    }

    // getEntityStatusValue() {
    //     let status = false;
    //     this.entityStatus.subscribe((value) => {
    //         status = value;
    //     });
    //     return status;
    // }

    getStatusName(status) {
        let statusName = "";
        switch (status) {
            case "ENABLED":
                statusName = "активное";
                break;
            case "DELETED":
                statusName = "удален";
                break;
        }
        return statusName;
    }

    getUsedName(used) {
        let usedName = (used) ? "используется" : "не используется";
        return usedName;
    }

    editEntityInTree() {
        let path = [];
        if (this.showEntity['path'] != null) {
            for (let item of this.showEntity['path']) {
                path.push(item['id']);
            }
        }

        let navigationExtras:NavigationExtras = {
            queryParams: {
                "editEntity": this.showEntity['id'],
                "path": path
            }
        };
        this.router.navigate(['employee-tags'], navigationExtras);
    }

    editOrgInTree() {
        let path = [];
        if (this.showEntity['path'] != null) {
            for (let item of this.showEntity['path']) {
                path.push(item['id']);
            }
        }

        let navigationExtras:NavigationExtras = {
            queryParams: {
                "editEntity": this.showEntity['id'],
                "path": path
            }
        };
        this.router.navigate(['units'], navigationExtras);
    }

    toOtherTag(id:string) {
        this.core.get("api/employee-tags/" + id)
            .then((res) => {
                this.showEntity = res['_embedded'];
            })
        this.router.navigate(['employee-tags/' + id]);
    }

    toOtherUnit(id:string) {
        this.core.get("api/units/" + id)
            .then((res) => {
                this.showEntity = res['_embedded'];
            })
        this.router.navigate(['units/' + id]);
    }
}
