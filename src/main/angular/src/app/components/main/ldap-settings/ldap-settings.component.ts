import { Component } from '@angular/core';
import { AppCoreService } from "../../../services/app-core.service";

@Component({
    selector: 'ldap-settings',
    template: require('./ldap-settings.component.html')
})
export class LdapSettingsComponent {
    public ldapConnectionSettings: Object;
    public ldapConnectionSettingsCopied: Object;

    public formError: boolean = false;
    public formErrorText: string = "";
    public formInfoText: string = "";

    public addressError: boolean = false;
    public loading: boolean = false;

    public connectionStatus: boolean = false;

    public checkBoxState: Array<any> = [];

    constructor(private core: AppCoreService) {
        this.getLdapConnectionSettings();
    }

    getLdapConnectionSettings() {
        this.core.get("/api/settings/ldap")
            .then((res) => {
                this.ldapConnectionSettings = res['_embedded'];
                if (this.ldapConnectionSettings['useSSL']) {
                    this.checkBoxState.push("useSSL");
                }
                this.ldapConnectionSettingsCopied = JSON.parse(JSON.stringify(this.ldapConnectionSettings));
                this.checkConnection(this.ldapConnectionSettings);
            });
    }

    checkConnection(settings: Object) {
        this.formError = false;
        this.addressError = false;
        this.formErrorText = "";
        this.formInfoText = "";

        this.loading = true;

        this.ldapConnectionSettings['useSSL'] = this.checkBoxState[0] === "useSSL";

        let putData = JSON.parse('{"form": ' + JSON.stringify(this.ldapConnectionSettings) + '}');

        this.core.put("api/settings/ldap/verify", putData)
            .then((res) => {
                this.connectionStatus = res['_embedded'];
                this.loading = false;
            })
            .catch(() => {
                this.connectionStatus = false;
                this.loading = false;
            });
    }

    saveLdapConnectionSettings(): void {
        this.formError = false;
        this.addressError = false;
        this.formErrorText = "";
        this.formInfoText = "";

        this.ldapConnectionSettings['useSSL'] = this.checkBoxState[0] === "useSSL";

        if (this.isEqual(this.ldapConnectionSettings, this.ldapConnectionSettingsCopied)) {
            return;
        }

        if (!this.isValid(this.ldapConnectionSettings)) {
            this.formError = true;
            this.formErrorText = "Не все поля заполнены!";
            return;
        }

        if (this.addressError) {
            this.formError = true;
            this.formErrorText = "Неправильно задан адрес подключения!";
            return;
        }

        let putData = JSON.parse(`{"form": ${JSON.stringify(this.ldapConnectionSettings)}}`);

        this.core.put("api/settings/ldap", putData)
            .then(() => {
                this.ldapConnectionSettingsCopied = JSON.parse(JSON.stringify(this.ldapConnectionSettings));
                this.formInfoText = "Настройки сохранены";
                this.checkConnection(this.ldapConnectionSettings);
            })
            .catch(() => {
                this.formError = true;
                this.formErrorText = "При выполнении запроса произошла ошибка";
            });
    }

    isValid(settings: Object) {
        return settings['url'] && settings['port'] && settings['dn'] && settings['username'] && settings['password'];
    }

    isEqual(settings: Object, settingsCopied: Object) {
        return (
            (settings['url'] === settingsCopied['url']) &&
            (settings['port'] === settingsCopied['port']) &&
            (settings['dn'] === settingsCopied['dn']) &&
            (settings['useSSL'] === settingsCopied['useSSL']) &&
            (settings['username'] === settingsCopied['username']) &&
            (settings['password'] === settingsCopied['password'])
        );
    }

    checkAddress() {
        let IP_ADDR_REGEXP = new RegExp(
            "^(?:\\S+(?::\\S*)?@)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            "\\.?)(?::\\d{2,5})?(?:[/?#]\\S*)?$", "i"
        );

        if (this.ldapConnectionSettings['url'].match(IP_ADDR_REGEXP) && ((this.ldapConnectionSettings['port'] >= 0) && (this.ldapConnectionSettings['port'] <= 65535))) {
            this.addressError = false;
        } else {
            this.addressError = true;
        }
    }
}