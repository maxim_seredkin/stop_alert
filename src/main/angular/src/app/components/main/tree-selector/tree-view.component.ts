/**
 * Created by Baser on 04.02.2017.
 */
import {Component, Input, Output} from '@angular/core';
import {EventEmitter} from "@angular/common/src/facade/async";
import {SelectorItem} from "../../../models/selector-item";
import {Unit} from "../../../models/unit";

@Component({
    selector: 'tree-view',
    template: require('./tree-view.component.html')
})

export class TreeViewComponent {
    @Input('items') items: Array<SelectorItem>;
    @Input('child') child: boolean;
    @Input('type') type: string;
    @Input('inputUnitsArrayCopy') inputUnitsArrayCopy: Array<Object>;


    @Output() select: EventEmitter<any> = new EventEmitter();

    public tempFindedItem: SelectorItem;

    constructor() {
    }

    ngOnInit() {
        // console.log(this.items);
    }
    
    isUsed(item: Object) {
        if(item['used'] != null && item['used'] != undefined) {
            return item['used'];
        }
        else {
            return true;
        }
    }

    isShow(item: Object) {
        if(item['errors'] != null) {
            if(item['errors'].length > 0) {
                return false;
            }
        }
        return true;
    }

    selectItem(item: Object) {
        if(this.type == 'tag') {
            if(this.isEnable(item)) {
                this.select.emit(item);
            }
        }
        else {
            this.select.emit(item);
        }
    }

    pushItemUp(item: Object) {
        this.select.emit(item);
    }

    isEnable(unit: Object) {
        this.findUnit(unit, this.inputUnitsArrayCopy);
        return (this.tempFindedItem.nodes.length == 0);
    }

    //Поиск элемента в дереве юнитов
    findUnit(unit: Object, unitArray: Array<Object>) {
        for (var item of unitArray) {
            if(item['id'] == unit['id']){
                this.tempFindedItem =  new Unit(item['id'], item['name'], item['nodes'], item['parent']);
            }
            if(item['nodes'] != null && (typeof item['nodes'] != "undefined")){
                if (item['nodes'].length != 0) {
                    this.findUnit(unit, item['nodes']);
                }
            }
        }
    }
}
