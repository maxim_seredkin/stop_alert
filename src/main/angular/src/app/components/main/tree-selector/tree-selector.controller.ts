/**
 * Created by Baser on 04.02.2017.
 */
import {Component, Input, Output, ViewChild, ElementRef} from '@angular/core';
import {EventEmitter} from "@angular/common/src/facade/async";
import {Unit} from "../../../models/unit";
import {SelectorItem} from "../../../models/selector-item";

@Component({
    selector: 'tree-selector',
    template: require('./tree-selector.component.html')
})

export class TreeSelectorComponent {
    public selectedItems:Array<any> = [];
    public treeShow: boolean = false;

    @ViewChild('orgSelectorForm') el:ElementRef;
    @Output() change: EventEmitter<any> = new EventEmitter();
    @Output() initComplete: EventEmitter<any> = new EventEmitter();

    @Input() startValue: Array<Object>;
    @Input() inputUnitsArray: Array<Object>;
    @Input() placeHolderText: string;
    @Input() iconClass: string;

    @Input() singleSelect: boolean = false;
    @Input() error: boolean = false;
    @Input() exclude: Array<string> = [];

    @Input() showToolTip: boolean;

    public errorColor: boolean;

    public unitArray:Array<SelectorItem>;

    public inputUnitsArrayCopy: Array<Object>;

    @Input() type: string = "";

    ngOnInit() {
        if(typeof this.showToolTip == 'undefined') {
            this.showToolTip = true;
        }
        
        this.inputUnitsArrayCopy = JSON.parse(JSON.stringify(this.inputUnitsArray));
        this.unitArray = this.itemToUnitArray(this.inputUnitsArray);

        if(this.startValue != null) {
            for (var item of this.startValue) {
                this.initStartValue(item, this.unitArray);
            }
        }
    }


    //Заполнение селектора начальными значениями
    initStartValue(unit: Object, unitArray: Array<SelectorItem>) {
        for(var item of unitArray) {
            if(item['id'] == unit['id']) {
                this.deleteItem(unitArray, item);
                this.deleteSelectedItemChildren(this.selectedItems, item);
                this.selectedItems.push(item);
            }
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.initStartValue(unit, item.nodes);
                }
            }
        }
        this.initComplete.emit(this.selectedItems);
    }

    itemToUnitArray(items:Array<Object>) {
        //Заполнение массива юнитов
        var unitsArray:Array<SelectorItem> = [];
        for (var item of items) {
            if(!this.inArray(item['id'], this.exclude)) {
                unitsArray.push(new SelectorItem(item['id'], item['name'], this.itemToUnitArray(item['nodes']), item['parent'], item['used'], item['errors']));
            }
        }
        //Сортировка по алфавиту
        this.firstSort(unitsArray);
        return unitsArray;
    }

    firstSort(unitArray: Array<SelectorItem>) {
        for (var item of unitArray) {
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.firstSort(item.nodes);
                }
            }
        }
        this.sortArray(unitArray);
    }

    toggleTreeShow(status) {
        this.treeShow = status;
    }



    //Получение выбранного элемента из стороннего компонента с параметрами id и name.
    //Поиск этого компонента в дереве.
    public tempFindedItem: SelectorItem;

    //Смена основного дерева юнитов
    changeUnitsArray(unitsArray: Array<Object>) {
        this.inputUnitsArrayCopy = JSON.parse(JSON.stringify(unitsArray));
        this.unitArray = this.itemToUnitArray(unitsArray);
    }

    //Получение значения из внешнего компонента
    getOutsideUnit(unit: Object) {
        this.unitArray = this.itemToUnitArray(this.inputUnitsArray);
        this.selectedItems = [];
        this.findUnit(unit, this.unitArray);
        this.getSelectedItem(this.tempFindedItem);
        this.tempFindedItem = null;
        this.change.emit(this.selectedItems);
    }

    //Получение значениий из внешнего компонента без сброса значения
    getOutsideUnits(units: Array<Object>) {
        for(let unit of units) {
            this.findUnit(unit, this.unitArray);
            this.getSelectedItem(this.tempFindedItem);
            this.tempFindedItem = null;
            this.change.emit(this.selectedItems);
        }
    }

    //Сброс значения селектора
    resetValue() {
        this.unitArray = this.itemToUnitArray(this.inputUnitsArrayCopy);
        this.selectedItems = [];
    }

    //Установить значение селектора
    setValue(items: Array<any>) {
        for(var item of items) {
            this.findUnit(item, this.unitArray);
            if(this.tempFindedItem != null && this.tempFindedItem != undefined) {
                this.getSelectedItem(this.tempFindedItem);
            }
        }
    }

    //Поиск элемента в дереве юнитов
    findUnit(unit: Object, unitArray: Array<SelectorItem>) {
        for (var item of unitArray) {
            if(item.id == unit['id']){
                this.tempFindedItem =  new Unit(item.id, item.name, item['nodes'], item['parent']);
            }
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.findUnit(unit, item.nodes);
                }
            }
        }
    }

    //Получение выбранного отдела и перемещение в массив
    getSelectedItem(item:SelectorItem) {
        if(this.singleSelect == false) {
            this.deleteItem(this.unitArray, item);
            this.deleteSelectedItemChildren(this.selectedItems, item);
            this.selectedItems.push(item);
            this.change.emit(this.selectedItems);
        }
        else {
            this.resetValue();
            this.deleteItem(this.unitArray, item);
            this.selectedItems.push(item);
            this.change.emit(this.selectedItems);
        }
    }

    //Удаление подотделов выбранного отдела и перемещение их в свойство nodes выбранного отдела
    deleteSelectedItemChildren(selectedItems: Array<SelectorItem>, selectItem:SelectorItem) {
        var deleteItems: Array<SelectorItem> = [];
        for (var selected of selectedItems) {

            if(selected.parent != null) {

                if (selected.parent.id == selectItem.id) {
                    selectItem.nodes.push(selected);
                    deleteItems.push(selected);
                } else {
                    for (var item of selectItem.nodes) {
                        this.deleteSelectedItemChildren(selectedItems, item);
                    }
                }
            }
        }
        for(var item of deleteItems) {
            selectedItems.splice(selectedItems.indexOf(item), 1);
        }
    }

    //Удаление выбранного элемента из дерева
    deleteItem(items:Array<SelectorItem>, deleteItem:SelectorItem) {
        for (var item of items) {
            if (item.id == deleteItem.id) {
                items.splice(items.indexOf(item), 1);
                return true;
            }
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.deleteItem(item.nodes, deleteItem);
                }
            }
        }
    }

    //Удаление отдела из списка выбранных отделов и перемещение обратно в дерево отделов
    deleteSelectedItem(unitArray: Array<SelectorItem>, selectedItems: Array<SelectorItem>, item:SelectorItem) {
        if(this.singleSelect == false) {
            if (item.parent == null) {
                selectedItems.splice(this.selectedItems.indexOf(item), 1);
                unitArray.push(item);
            }
            else {
                for (var unitItem of unitArray) {
                    if (item.parent.id == unitItem.id) {
                        selectedItems.splice(this.selectedItems.indexOf(item), 1);
                        unitItem.nodes.push(item);
                        this.sortArray(unitItem.nodes);
                    }
                    else {
                        this.deleteSelectedItem(unitItem.nodes, selectedItems, item);
                    }
                }
            }
            this.sortArray(unitArray);
            this.change.emit(this.selectedItems);
        }
        else {
            this.resetValue();
            this.change.emit(this.selectedItems);
        }
    }

    //Сортировка массива по алфавиту
    sortArray(unitArray: Array<SelectorItem>) {
        unitArray.sort((a, b) => {
            var textA = a['name'].toUpperCase();
            var textB = b['name'].toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
    }

    //Существоание элемента с указанным id массиве
    inArray(unitId: string, unitTree: Array<string>) {
        let flag = false;
        for(let unit of unitTree) {
            if(unit == unitId) {
                flag = true;
            }
        }
        return flag;
    }

    //Модификация дерева (удаление запрещенных элементов)
    checkUnitTree() {
        this.resetValue();
        this.unitArray = this.itemToUnitArray(this.inputUnitsArray);
    }

    //Редактирование массива запрещенных элементовв
    changeExcludeArray(exclude: Array<string>) {
        this.exclude = exclude;
    }
}