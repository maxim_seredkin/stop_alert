/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 11.07.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {AppCoreService} from "../../../../services/app-core.service";
import {Router, ActivatedRoute} from "@angular/router";
import {Modal} from "ngx-modal/index";
import {SelectComponent} from "ng2-select/index";
import * as FileSaver from 'file-saver';
import {Config} from "../../../../services/config";
import {Http, Headers} from "@angular/http";

@Component({
    selector: 'task-info',
    template: require('./mail-template-info.component.html')
})

export class MailTemplateInfoComponent {
    public templateEditParam: boolean = false;
    public templateEdit: boolean = false;
    public template: Object;

    public templateGroupsItems: Array<Object>;

    public templateFile:File;
    public fileLoaderPlaceHolder: string = "Переместите файл сюда или выберите из файла";

    public urlError: boolean = true;
    public fillFormError: boolean = false;
    public formErrorText: string = "";

    public oldTemplateName: string;

    @ViewChild('templateGroupsSelector') templateGroupsSelector: SelectComponent;

    @ViewChild('mailTemplateShowModal') mailTemplateShowModal: Modal;

    constructor(
        private core:AppCoreService,
        private route:ActivatedRoute,
        private router:Router,
        private config: Config,
        private http: Http
    ) {
        this.route.queryParams.subscribe(params => {
            if (params['action'] == 'edit') {
                this.templateEditParam = true;
                this.templateEdit = true;
            }
        });
    }

    ngOnInit() {
        this.loadTemplate();
    }

    loadTemplate() {
        this.core.get('api/mail-templates/'+this.route.snapshot.params['id'])
            .then((res) => {
                this.template = {};
                this.template['id'] = res['_embedded']['id'];
                this.template['name'] = res['_embedded']['name'];
                this.oldTemplateName = res['_embedded']['name'];
                this.template['subject'] = res['_embedded']['subject'];
                this.template['fromPersonal'] = res['_embedded']['fromPersonal'];
                this.template['fromAddress'] = res['_embedded']['fromAddress'];
                this.template['redirectUri'] = res['_embedded']['redirectUri'];
                this.template['group'] = res['_embedded']['group'];
                this.template['description'] = res['_embedded']['description'];
                this.template['status'] = "ENABLED";

                this.getTemplateGroups();

                this.checkUrl();

                if(this.templateEditParam) {
                    if(this.template['group']) {
                        setTimeout(() => {
                            this.templateGroupsSelector.active = [{id: this.template['group']['id'], text: this.template['group']['name']}];
                        }, 100);
                    }
                }
            });
    }

    templateGroupsToItems(templateGroups: Array<Object>) {
        let groups = [];
        for(let group of templateGroups) {
            let item = {};
            item['id'] = group['id'];
            item['text'] = group['name'];
            groups.push(item);
        }
        return groups;
    }

    getTemplateGroups() {
        this.core.get('api/mail-template-groups')
            .then((res) => {
                this.templateGroupsItems = this.templateGroupsToItems(res['_embedded']);
            })
    }

    refreshTemplateGroupValue(value) {
        this.template['group'] = value['id'];
    }

    validEmail() {
        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return (this.template['fromAddress'].length >= 5 && EMAIL_REGEXP.test(this.template['fromAddress']));
    }

    checkUrl() {
        let URL_REGEXP = new RegExp(
            "^" +
            "(?:(?:https?|ftp)://)" +
            "(?:\\S+(?::\\S*)?@)?" +
            "(?:" +
            "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
            "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
            "|" +
            "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
            "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            "\\.?" +
            ")" +
            "(?::\\d{2,5})?" +
            "(?:[/?#]\\S*)?" +
            "$", "i"
        );

        if(this.template['redirectUri'].match(URL_REGEXP)) {
            this.urlError = false;
        } else {
            this.urlError = true;
        }
    }

    openLink() {
        if(this.urlError) {
            return;
        }
        window.open(this.template['redirectUri'],'_blank');
    }

    validTemplate() {
        return !!(
            this.template['name'] &&
            this.template['subject'] &&
            this.template['fromPersonal'] &&
            this.template['fromAddress'] &&
            this.template['redirectUri']
        );
    }

    editTemplate() {
        this.fillFormError = false;
        this.formErrorText = "";

        if(!this.validTemplate()) {
            this.fillFormError = true;
            this.formErrorText = "Заполнены не все поля";
            return;
        }

        if(!this.validEmail()) {
            this.fillFormError = true;
            this.formErrorText = "Неправильно заполнен email отправителя";
            return;
        }

        let formData:FormData = new FormData();
        if(this.templateFile) {
            formData.append('file', this.templateFile, this.templateFile.name);
        }
        
        if (this.template['group']) {
            this.template['group'] = this.template['group']['id'];
        }

        formData.append('payload', new Blob([JSON.stringify({ form: this.template})], {type: "application/json"}));

        let params = {};
        params['filters'] = `'name' EQ '${this.template['name']}'`;

        this.core.get('api/mail-templates/exists', params)
            .then((res) => {
                if(res['_embedded'] == false || this.oldTemplateName == this.template['name']) {
                    this.core.put_file("api/mail-templates/"+this.template['id'], formData)
                        .then(() => {
                            if(this.templateEditParam) {
                                this.router.navigate(['mail-templates']);
                            } else {
                                this.loadTemplate();
                                this.toggleEdit();
                            }
                        })
                        .catch(() => {
                            this.fillFormError = true;
                            this.formErrorText = "При редактировании шаблона произошла ошибка";
                        });
                } else {
                    this.fillFormError = true;
                    this.formErrorText = "Шаблон с таким именем уже существует!";
                }
            })
            .catch(() => {
                this.fillFormError = true;
                this.formErrorText = "При создании шаблона произошла ошибка";
            });

    }

    public templateName:string = "";
    public templateHtml:string = "";

    templatePreviewLoad(templateId:string) {
        let id:string = templateId;

        if (id != '') {
            this.core.get("api/mail-templates/" + id + "/example")
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }

    toggleEdit() {
        this.templateEdit = !this.templateEdit;

        if(this.templateEdit) {
            if(this.template['group']) {
                setTimeout(() => {
                    this.templateGroupsSelector.active = [{id: this.template['group']['id'], text: this.template['group']['name']}];
                    this.checkUrl();
                }, 100);
            }
        }
    }

    getTemplateFile() {
        let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';

        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token_header);

        this.http.get(this.config.get('backendUrl')+"api/mail-templates/sample", {headers: headers})
            .subscribe(data => {
                var blob = new Blob([data.text()], {type: 'text/csv;charset=UTF-8'});
                FileSaver.saveAs(blob, "MailTemplateSample.vm");
            }),
            error => console.log("Error downloading the file."),
            () => console.info("OK");
    }

    onUpload(file:File) {
        this.templateFile = file;
        this.fileLoaderPlaceHolder = file.name;
    }
}