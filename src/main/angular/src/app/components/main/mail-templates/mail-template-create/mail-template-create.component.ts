/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 11.07.2017.
 */
import {Component} from '@angular/core';
import {AppCoreService} from "../../../../services/app-core.service";
import {Router} from "@angular/router";
import {Http, Headers} from "@angular/http";
import * as FileSaver from 'file-saver';
import {Config} from "../../../../services/config";

@Component({
    selector: 'tasks-list',
    template: require('./mail-template-create.component.html')
})

export class MailTemplateCreateComponent {
    public template: Object;
    public urlError: boolean = true;
    public fillFormError: boolean = false;
    public formErrorText: string = "";

    public templateFile:File;
    public fileLoaderPlaceHolder: string = "Переместите файл сюда или выберите из файла";

    public templateGroupsItems: Array<Object>;

    constructor(
        private core:AppCoreService,
        private router: Router,
        private http: Http,
        private config: Config
    ) {
        this.template = {};
        this.template['name'] = "";
        this.template['subject'] = "";
        this.template['fromPersonal'] = "";
        this.template['fromAddress'] = "";
        this.template['redirectUri'] = "";
        this.template['group'] = null;
        this.template['description'] = "";
        this.template['status'] = "ENABLED";

        this.getTemplateGroups();
    }

    ngAfterViewInit() {

    }

    templateGroupsToItems(templateGroups: Array<Object>) {
        let groups = [];
        for(let group of templateGroups) {
            let item = {};
            item['id'] = group['id'];
            item['text'] = group['name'];
            groups.push(item);
        }
        return groups;
    }

    getTemplateGroups() {
        this.core.get('api/mail-template-groups')
            .then((res) => {
                this.templateGroupsItems = this.templateGroupsToItems(res['_embedded']);
            })
    }

    validEmail() {
        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return (this.template['fromAddress'].length >= 5 && EMAIL_REGEXP.test(this.template['fromAddress']));
    }

    checkUrl() {
        let URL_REGEXP = new RegExp(
            "^" +
            "(?:(?:https?|ftp)://)" +
            "(?:\\S+(?::\\S*)?@)?" +
            "(?:" +
            "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
            "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
            "|" +
            "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
            "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            "\\.?" +
            ")" +
            "(?::\\d{2,5})?" +
            "(?:[/?#]\\S*)?" +
            "$", "i"
        );

        if(this.template['redirectUri'].match(URL_REGEXP)) {
            this.urlError = false;
        } else {
            this.urlError = true;
        }
    }

    openLink() {
        if(this.urlError) {
            return;
        }
        window.open(this.template['redirectUri'],'_blank');
    }

    createTemplate() {
        this.fillFormError = false;
        this.formErrorText = "";

        if(!this.validTemplate()) {
            this.fillFormError = true;
            this.formErrorText = "Заполнены не все поля";
            return;
        }

        if(!this.validEmail()) {
            this.fillFormError = true;
            this.formErrorText = "Неправильно заполнен email отправителя";
            return;
        }

        if(!this.templateFile) {
            this.fillFormError = true;
            this.formErrorText = "Не загружен файл шаблона";
            return;
        }

        let formData:FormData = new FormData();
        formData.append('file', this.templateFile, this.templateFile.name);
        formData.append('payload', new Blob([JSON.stringify({ form: this.template})], {type: "application/json"}));

        let params = {};

        params['filters'] = `'name' EQ '${this.template['name']}'`;

        this.core.get('api/mail-templates/exists', params)
            .then((res) => {
                if(res['_embedded'] == false) {
                    this.core.post_file("api/mail-templates", formData)
                        .then(() => {
                            this.router.navigate(['mail-templates']);
                        })
                        .catch(() => {
                            this.fillFormError = true;
                            this.formErrorText = "При создании шаблона произошла ошибка";
                        });
                } else {
                    this.fillFormError = true;
                    this.formErrorText = "Шаблон с таким именем уже существует!";
                }
            })
            .catch(() => {
                this.fillFormError = true;
                this.formErrorText = "При создании шаблона произошла ошибка";
            });

    }

    validTemplate() {
        return !!(
            this.template['name'] &&
            this.template['subject'] &&
            this.template['fromPersonal'] &&
            this.template['fromAddress'] &&
            this.template['redirectUri']
        );
    }

    onUpload(file:File) {
        this.templateFile = file;
        this.fileLoaderPlaceHolder = file.name;
    }

    refreshTemplateGroupValue(value) {
        this.template['group'] = value['id'];
    }

    getTemplateFile() {
        let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';

        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token_header);

        this.http.get(this.config.get('backendUrl')+"api/mail-templates/sample", {headers: headers})
            .subscribe(data => {
                var blob = new Blob([data.text()], {type: 'text/csv;charset=UTF-8'});
                FileSaver.saveAs(blob, "MailTemplateSample.vm");
            }),
            error => console.log("Error downloading the file."),
            () => console.info("OK");
    }
}