/**
 * Created by Baser on 10.07.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {Pagination} from "../../../../models/pagination";
import {AppCoreService} from "../../../../services/app-core.service";
import {NavigationExtras, Router} from "@angular/router";
import {Modal} from "ngx-modal/index";
import {SelectComponent} from "ng2-select/index";

@Component({
    selector: 'tasks-list',
    template: require('./mail-templates-list.component.html')
})

export class MailTemplatesListComponent {
    private pagination:Pagination;
    public showAdvFilter:boolean = false;

    public mailTemplates: Array<Object>;

    @ViewChild('templateDeleteModal')
    public templateDeleteModal: Modal;

    //MODAL PARAMS
    public modalTemplateId: string;
    public modalTemplateName: string;
    public modalError: boolean = false;

    public templateGroupsItems: Array<Object>;

    //FILTER
    public filteredTemplateName: string = "";
    public filteredTemplateGroup: string = "";
    public filteredTemplateSubject: string = "";

    @ViewChild('templateGroupsSelector') templateGroupsSelector: SelectComponent;

    constructor(private core: AppCoreService, private router: Router) {
        this.pagination = new Pagination();
        this.getMailTemplates();
        this.getTemplateGroups();
    }

    templateGroupsToItems(templateGroups: Array<Object>) {
        let groups = [];
        groups.push({id: 'ALL', text: "Все"});
        for(let group of templateGroups) {
            let item = {};
            item['id'] = group['id'];
            item['text'] = group['name'];
            groups.push(item);
        }
        return groups;
    }

    getTemplateGroups() {
        this.core.get('api/mail-template-groups')
            .then((res) => {
                this.templateGroupsItems = this.templateGroupsToItems(res['_embedded']);
            })
    }

    getMailTemplates(filterParams?: Object) {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;

        params['filters'] = "'status' EQ 'ENABLED'";

        params['sorts'] = ['name:ASC'];

        if(filterParams) {
            if(filterParams['name'] != '') {
                params['searchs'] = `'name':'${filterParams['name']}'`;
            }
            if(filterParams['subject'] != '') {
                if(params['searchs']) {
                    params['searchs'] += `,'subject':'${filterParams['subject']}'`;
                } else {
                    params['searchs'] = `'subject':'${filterParams['subject']}'`;
                }
            }
            if(filterParams['group'] != '' && filterParams['group'] != 'ALL') {
                params['filters'] += `,'group.id' EQ '${filterParams['group']}'`;
            }
        }
        
        this.core.get('api/mail-templates', params)
            .then((res) => {
                this.mailTemplates = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.updatePagination();
            });
    }

    applyFilter() {
        let params = {name: '', group: '', subject: ''};

        if(this.filteredTemplateName != '') {
            params['name'] = this.filteredTemplateName;
        }
        if(this.filteredTemplateGroup != '') {
            params['group'] = this.filteredTemplateGroup;
        }
        if(this.filteredTemplateSubject != '') {
            params['subject'] = this.filteredTemplateSubject;
        }

        this.getMailTemplates(params);
    }

    toggleShowFilter() {
        if(this.showAdvFilter) {
            this.filteredTemplateSubject = "";
            this.filteredTemplateGroup = "ALL";
        }
        this.showAdvFilter = !this.showAdvFilter;
    }

    deleteTemplate(template: Object) {
        this.modalError = false;
        this.modalTemplateName = template['name'];
        this.modalTemplateId = template['id'];
        this.templateDeleteModal.open();
    }

    delTemplate() {
        this.core.put(`api/mail-templates/${this.modalTemplateId}/delete`)
            .then(() => {
                this.templateDeleteModal.close();
                this.getMailTemplates();
            })
            .catch(() => {
                this.modalError = true;
            })
    }

    editTemplate(id: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "action": "edit",
            }
        };
        this.router.navigate(['mail-templates/', id], navigationExtras);
    }

    changePagination() {
        this.getMailTemplates();
    }

    refreshTemplateGroupValue(value) {
        this.filteredTemplateGroup = value['id'];
    }
}