/**
 * Created by Baser on 30.01.2017.
 */
import {Component, Input, Output} from '@angular/core';
import {Pagination} from "../../../models/pagination";
import {EventEmitter} from "@angular/common/src/facade/async";

@Component({
    selector: 'pagination',
    template: require('./pagination.component.html'),
    inputs: ['params']
})

export class PaginationComponent {
    private pagination: Pagination;
    private newPage: number;

    @Input() set params(name: Pagination) {
        this.pagination = name;
    }

    @Output() change = new EventEmitter();

    constructor() {

    }

    ngOnInit() {
        this.newPage = this.pagination.number;
    }

    onKeyUp(e) {
        if(e.keyCode == 13) {
            this.onSubmit();
        }
        if(e.keyCode == 37) {
            this.reducePage();
        }
        if(e.keyCode == 39) {
            this.increasePage();
        }
    }

    changeSize(value) {
        if(typeof(value) == "number") {
            this.pagination.number = 1;
            this.newPage = 1;
            this.pagination.size = value;
            this.change.emit();
        }
    }
    
    increasePage() {
        if(this.pagination.number < this.pagination.totalPages) {
            this.pagination.number++;
            this.change.emit("");
            this.newPage = this.pagination.number;
        }
    }
    
    reducePage() {
        if(this.pagination.number > 1) {
            this.pagination.number--;
            this.newPage = this.pagination.number;
            this.change.emit("");
        }
    }

    onSubmit() {
        if(this.newPage != null && this.newPage > 0 && this.newPage <= this.pagination.totalPages && this.newPage != this.pagination.number) {
            this.pagination.number = this.newPage;
            this.change.emit("");
        }
        else {
            this.newPage = this.pagination.number;
        }
    }
}