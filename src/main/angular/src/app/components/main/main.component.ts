import {Component, OnInit, ViewChild} from '@angular/core';
import {AppCoreService} from "../../services/app-core.service";
import {UserService} from "../../services/user.service";
import {Modal} from "ngx-modal/index";


@Component({
    selector: 'main',
    template: require('./main.component.html'),
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

    @ViewChild('licenseFail')
    private licenseFailModal:Modal;

    public licenseSettings: Object;
    
    constructor(private core: AppCoreService, private user:UserService) {
        if(!user.load) {
            this.getLicenseSettings();
            this.user.load = true;
        }
    }

    ngOnInit() {
    }

    getLicenseSettings() {
        this.core.get("api/settings/license")
            .then((res) => {
                this.licenseSettings = res['_embedded'];
                if(this.licenseSettings['status'] == "FAILURE" || this.licenseSettings['status'] == "EXPIRED") {
                    if(this.user.getAuthority() == 'ROLE_ADMINISTRATOR') {
                        this.licenseFailModal.open();
                    }
                    this.user.licenseActive = false;
                }
                else {
                    this.user.licenseActive = true;
                }
            });
    }

}
