/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 12.07.2017.
 */
import { Component } from '@angular/core';
import {AppCoreService} from "../../../services/app-core.service";

@Component({
    selector: 'system-settings',
    template: require('./system-settings.component.html')
})
export class SystemSettingsComponent {
    public DBConnectionSettings: Object;
    public DBConnectionSettingsCopied: Object;
    public transitionSettings: Object;
    public transitionSettingsCopied: Object;

    public externalAddressSettings: Object;
    public externalAddressSettingsCopied: Object;

    public formError: boolean = false;
    public formErrorText: string = "";
    public formInfoText: string = "";

    public addressError: boolean = false;
    public transitionAddressError: boolean = false;
    public externalAddressError: boolean = false;
    public loading: boolean = false;

    public connectionStatus: boolean = false;

    public transitionFormError: boolean = false;
    public transitionFormErrorText: string = "";
    public transitionFormInfoText: string = "";

    public externalAddressFormError: boolean = false;
    public externalAddressFormErrorText: string = "";
    public externalAddressFormInfoText: string = "";

    constructor(private core: AppCoreService) {
        this.getDBConnectionSettings();
        this.getTransitionSettings();
        this.getExternalAddressSettings();
    }

    getExternalAddressSettings() {
        this.core.get("api/settings/address")
            .then((res) => {
                this.externalAddressSettings = res['_embedded'];
                this.externalAddressSettingsCopied = JSON.parse(JSON.stringify(this.externalAddressSettings));
            });
    }

    getDBConnectionSettings() {
        let res = {
            _embedded: {
                host: "192.168.0.39",
                port: 4201,
                database: "stop_alert",
                username: "admin",
                password: "@fsdfsf"
            }
        };
        //this.core.get("api/settings/server")
            //.then((res) => {
                this.DBConnectionSettings = res['_embedded'];
                this.DBConnectionSettingsCopied = JSON.parse(JSON.stringify(this.DBConnectionSettings));
                this.checkConnection(this.DBConnectionSettings);
           // });
    }

    getTransitionSettings() {
        this.core.get("api/settings/transition")
            .then((res) => {
                this.transitionSettings = res['_embedded'];
                this.transitionSettingsCopied = JSON.parse(JSON.stringify(this.transitionSettings));
            });
    }

    checkConnection(settings: Object) {
        this.formError = false;
        this.addressError = false;
        this.formErrorText = "";
        this.formInfoText = "";


        this.loading = true;
        let putData = JSON.parse('{"form": ' + JSON.stringify(this.DBConnectionSettings) + '}');

        setTimeout(() => {
            this.connectionStatus = true;
            this.loading = false;
        }, 2000);

        // this.core.put("api/settings/server/verify", putData)
        //     .then((res) => {
        //         this.connectionStatus = res['_embedded'];
        //         this.loading = false;
        //     })
        //     .catch(() => {
        //         this.connectionStatus = false;
        //         this.loading = false;
        //     });
    }

    saveDBConnectionSettings(): void {
        this.formError = false;
        this.addressError = false;
        this.formErrorText = "";
        this.formInfoText = "";

        if(this.isEqual(this.DBConnectionSettings, this.DBConnectionSettingsCopied)) {
            return;
        }

        if(!this.isValid(this.DBConnectionSettings)) {
            this.formError = true;
            this.formErrorText = "Не все поля заполнены!";
            return;
        }

        if(this.addressError) {
            this.formError = true;
            this.formErrorText = "Неправильно задан адрес подключения!";
            return;
        }

        let putData = JSON.parse('{"form": ' + JSON.stringify(this.DBConnectionSettings) + '}');

        this.core.put("api/settings/server", putData)
            .then(() => {
                this.DBConnectionSettingsCopied = JSON.parse(JSON.stringify(this.DBConnectionSettings));
                this.formInfoText = "Настройки сохранены";
                this.checkConnection(this.DBConnectionSettings);
            })
            .catch(() => {
                this.formError = true;
                this.formErrorText = "При выполнении запроса произошла ошибка";
            });
    }

    saveTransitionSettings(): void {
        this.transitionFormError = false;
        this.transitionFormErrorText = "";
        this.transitionFormInfoText = "";

        if(this.transitionSettings['uri'] == this.transitionSettingsCopied['uri']) {
            return;
        }

        if(this.transitionAddressError) {
            this.transitionFormError = true;
            this.transitionFormErrorText = "Неправильно задан адрес захвата переходов!";
            return;
        }

        let putData = JSON.parse('{"form": ' + JSON.stringify(this.transitionSettings) + '}');

        this.core.put("api/settings/transition", putData)
            .then(() => {
                this.transitionSettingsCopied = JSON.parse(JSON.stringify(this.transitionSettings));
                this.transitionFormInfoText = "Настройки сохранены";
            })
            .catch(() => {
                this.transitionFormError = true;
                this.transitionFormErrorText = "При выполнении запроса произошла ошибка";
            });
    }

    saveExternalAddressSettings(): void {
        this.externalAddressFormError = false;
        this.externalAddressFormErrorText = "";
        this.externalAddressFormInfoText = "";

        if(this.externalAddressSettings['uri'] == this.externalAddressSettingsCopied['uri']) {
            return;
        }

        if(this.externalAddressError) {
            this.externalAddressFormError = true;
            this.externalAddressFormErrorText = "Неправильно задан внешний адрес!";
            return;
        }

        let putData = JSON.parse('{"form": ' + JSON.stringify(this.externalAddressSettings) + '}');

        this.core.put("api/settings/address", putData)
            .then(() => {
                this.externalAddressSettingsCopied = JSON.parse(JSON.stringify(this.externalAddressSettings));
                this.externalAddressFormInfoText = "Настройки сохранены";
            })
            .catch(() => {
                this.externalAddressFormError = true;
                this.externalAddressFormErrorText = "При выполнении запроса произошла ошибка";
            });
    }

    isValid(settings: Object) {
        return (
            (settings['host'] != "") &&
            (settings['port'] != "") &&
            (settings['database'] != "")
        );
    }

    validPort(port: number) {
        return ((port >= 0) && (port <= 65535));
    }

    isEqual(settings: Object, settingsCopied: Object) {
        return (
            (settings['host'] == settingsCopied['host']) &&
            (settings['port'] == settingsCopied['port']) &&
            (settings['database'] == settingsCopied['database']) &&
            (settings['username'] == settingsCopied['username']) &&
            (settings['password'] == settingsCopied['password'])
        );
    }

    checkAddress() {
        let IP_ADDR_REGEXP = new RegExp(
            "^(?:\\S+(?::\\S*)?@)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            "\\.?)(?::\\d{2,5})?(?:[/?#]\\S*)?$", "i"
        );

        if(this.DBConnectionSettings['host'].match(IP_ADDR_REGEXP) && ((this.DBConnectionSettings['port'] >= 0) && (this.DBConnectionSettings['port'] <= 65535))) {
            this.addressError = false;
        } else {
            this.addressError = true;
        }
    }

    checkTransitionAddress() {
        let URL_REGEXP = new RegExp(
            "^" +
            // resource path
            "([/])"+
            "(\\S*\/[-a-zA-Z0-9@:%_\+.~#?&=]*)?" +
            "$", "i"
        );

        if(this.transitionSettings['uri'].match(URL_REGEXP)) {
            this.transitionAddressError = false;
        } else {
            this.transitionAddressError = true;
        }
    }

    checkExternalAddress() {
        let URL_REGEXP = new RegExp(
            "^" +
            // protocol identifier
            "(?:(?:https?|ftp)://)?" +
            // user:pass authentication
            "(?:\\S+(?::\\S*)?@)?" +
            "(?:" +
            // IP address exclusion
            // private & local networks
            //"(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
            //"(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
            //"(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
            // IP address dotted notation octets
            // excludes loopback network 0.0.0.0
            // excludes reserved space >= 224.0.0.0
            // excludes network & broacast addresses
            // (first & last IP address of each class)
            "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
            "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
            "|" +
            // host name
            "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            // domain name
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
            // TLD identifier
            "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            // TLD may end with dot
            "\\.?" +
            ")" +
            // port number
            "(?::\\d{2,5})?" +
            // resource path
            "(?:[/?#]\\S*)?" +
            "$", "i"
        );

        if(this.externalAddressSettings['uri'].match(URL_REGEXP)) {
            this.externalAddressError = false;
        } else {
            this.externalAddressError = true;
        }
    }
}