/**
 * Created by Baser on 13.03.2017.
 */
import {Component, Output, EventEmitter} from '@angular/core';
import {Http} from "@angular/http";
import "rxjs/add/operator/map";

@Component({
    selector: 'file-uploader',
    template: require('./file-uploader.component.html'),
    inputs:['activeColor','baseColor','overlayColor', 'placeholder','accept']
})
export class FileUploaderComponent {
    @Output() uploaded: EventEmitter<any> = new EventEmitter();

    activeColor: string = 'green';
    baseColor: string = '#ccc';
    overlayColor: string = 'rgba(255,255,255,0.5)';
    placeholder: string = "";

    dragging: boolean = false;
    loaded: boolean = false;
    imageLoaded: boolean = false;
    imageSrc: string = '';
    accept: string = '.csv';

    handleDragEnter() {
        this.dragging = true;
    }

    handleDragLeave() {
        this.dragging = false;
    }

    handleDrop(e) {
        e.preventDefault();
        this.dragging = false;
        this.handleInputChange(e);
    }

    handleImageLoad() {
        this.imageLoaded = true;
    }

    constructor(private http: Http) {

    }

    handleInputChange(event) {
        var file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
            this.uploaded.emit(file);
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        this.loaded = true;
    }

}