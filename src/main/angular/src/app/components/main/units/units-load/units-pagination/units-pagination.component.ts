/**
 * Created by Baser on 30.01.2017.
 */
import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Pagination} from "../../../../../models/pagination";

@Component({
    selector: 'units-pagination',
    template: require('./units-pagination.component.html'),
    inputs: ['params']
})

export class UnitsPaginationComponent {
    private pagination: Pagination;
    private newPage: number;

    public showPagination: boolean = true;

    @Input() set params(name: Pagination) {
        this.pagination = name;
    }

    @Output() change = new EventEmitter();

    constructor() {

    }

    ngOnInit() {
        this.newPage = this.pagination.number;
    }

    onKeyUp(e) {
        if(e.keyCode == 13) {
            this.onSubmit();
        }
        if(e.keyCode == 37) {
            this.reducePage();
        }
        if(e.keyCode == 39) {
            this.increasePage();
        }
    }

    changeSize(value) {
        if(typeof(value) == "number") {
            this.pagination.number = 1;
            this.newPage = 1;
            this.pagination.size = value;
            this.change.emit();
        }
    }
    
    increasePage() {
        if(this.pagination.number < this.pagination.totalPages) {
            this.pagination.number++;
            this.change.emit();
            this.newPage = this.pagination.number;
        }
    }
    
    reducePage() {
        if(this.pagination.number > 1) {
            this.pagination.number--;
            this.newPage = this.pagination.number;
            this.change.emit();
        }
    }

    onSubmit() {
        console.log(this.pagination);
        console.log(this.newPage);
        if(this.newPage != null && this.newPage > 0 && this.newPage <= this.pagination.totalPages && this.newPage != this.pagination.number) {
            this.pagination.number = this.newPage;
            this.change.emit();
        }
        else {
            this.newPage = this.pagination.number;
        }
    }

    changeParams(pagination: Pagination) {
        this.newPage = pagination.number;
        this.pagination.size = pagination.size;
        this.pagination.totalElements = pagination.totalElements;
        this.pagination.totalPages = pagination.totalPages;
        this.pagination.number = pagination.number;
        //this.change.emit();
    }

    getParams(): Pagination {
        return this.pagination;
    }

    refreshPagination() {
        this.showPagination = false;
        setTimeout(()=>{
            this.showPagination = true;
        },50);
    }
}