/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 29.05.2017.
 */
import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import * as FileSaver from 'file-saver';
import {Headers, Http} from "@angular/http";
import {Pagination} from "../../../../models/pagination";
import {Modal} from "ngx-modal/index";
import {AppCoreService} from "../../../../services/app-core.service";
import {Config} from "../../../../services/config";
import {TreeSelectorComponent} from "../../tree-selector/tree-selector.controller";
import {UUID} from 'angular2-uuid';
import {UnitsLoadFilterComponent} from "./units-load-filter/units-load-filter.component";
import {UnitsPaginationComponent} from "./units-pagination/units-pagination.component";
import {Router} from "@angular/router";
import {UnitsTreeSelectorComponent} from "../../units-tree-selector/units-tree-selector.component";

@Component({
    selector: 'units-load',
    template: require('./units-load.component.html')
})

export class UnitsLoadComponent {
    public pagination: Pagination;
    public load: boolean = false;
    public loadErrors: boolean = false;
    public errorsCount: number;

    public updateAllow: boolean = false;
    public loadAllow: boolean = false;

    public employeesDB: File;
    public fileLoaderPlaceHolder: string = "Переместите файл сюда или выберите из файла";

    public unitsTree: Array<any>;
    public DBUnitsTree: Array<any>;
    public invalidUnits: Array<any>;

    @ViewChild('countErrorsModal')
    private countErrorsModal: Modal;

    @ViewChild('unitDeleteModal')
    private unitDeleteModal: Modal;

    @ViewChild('unitModal')
    private unitModal: Modal;

    @ViewChild('unitParentSelector')
    private unitParentSelector: UnitsTreeSelectorComponent;

    @ViewChild('unitOfficersSelector')
    private unitOfficersSelector: TreeSelectorComponent;

    @ViewChild('unitManagersSelector')
    private unitManagersSelector: TreeSelectorComponent;

    @ViewChild('unitsTableFilter')
    private unitsTableFilter: UnitsLoadFilterComponent;

    @ViewChild('tablePagination')
    private tablePagination: UnitsPaginationComponent;

    public managers: Array<any>;
    public officers: Array<any>;

    public parentID: string = "";
    public parentName: string = "";

    public showValidTable: boolean = true;
    public showModalUnitParent: boolean = true;

    public rootManagers: Array<Object> = [];
    public rootOfficers: Array<Object> = [];

    public deleteModalId: string = "";
    public deleteModalUnit: Object;

    constructor(private http: Http,
                private core: AppCoreService,
                private config: Config,
                private cdRef: ChangeDetectorRef,
                private router: Router) {
        this.pagination = new Pagination();
    }

    ngAfterViewInit() {
        this.getManagers();
        this.getOfficers();
        this.getUnitsTree();
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.DBUnitsTree = res['_embedded'];
            });
    }

    getManagers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_MANAGER'";
        params['filters'] += ",'enabled' EQ 'true'";
        params['filters'] += ",'accountNonLocked' EQ 'true'";

        this.core.get('api/users', params)
            .then((res) => {
                this.managers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    getOfficers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_SECURITY_OFFICER'";
        params['filters'] += ",'enabled' EQ 'true'";
        params['filters'] += ",'accountNonLocked' EQ 'true'";

        this.core.get('api/users', params)
            .then((res) => {
                this.officers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    getTemplateFile() {
        let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';

        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token_header);

        this.http.get(this.config.get('backendUrl') + "api/units/import/sample", {headers: headers})
            .subscribe(data => {
                var blob = new Blob([data.text()], {type: 'text/csv;charset=UTF-8'});
                FileSaver.saveAs(blob, "UnitImportSample.csv");
            }),
            error => console.log("Error downloading the file."),
            () => console.info("OK");
    }

    onUpload(file: File) {
        this.employeesDB = file;
        this.fileLoaderPlaceHolder = file.name;
    }

    uploadFile() {
        if (typeof this.employeesDB == 'undefined') {
            return;
        }
        let formData: FormData = new FormData();
        formData.append('file', this.employeesDB, this.employeesDB.name);
        if (this.parentID != "") {
            formData.append('id', this.parentID);
        }

        this.core.post_file("api/units/import", formData)
            .then((res) => {
                let response = res['_embedded'];

                if (response['root'] != null) {
                    this.parentName = response['root']['name'];
                } else {
                    this.parentName = "Корневые организации";
                }

                this.errorsCount = response['invalids'].length;
                this.unitsTree = response['units'];
                this.invalidUnits = response['invalids'];

                this.normalizeUnitsTree(this.unitsTree);
                this.normalizeUnitsTree(this.invalidUnits);

                this.filteredInvalidItems = this.invalidUnits;
                this.filteredValidItems = this.unitsTree;

                this.pagination.number = 1;
                this.pagination.size = 10;

                if (this.errorsCount > 0) {
                    this.countErrorsModal.open();
                    this.loadErrors = true;

                    this.pagination.totalElements = this.filteredInvalidItems.length;
                    this.updatePagination(this.pagination);
                }
                else {
                    this.pagination.totalElements = this.filteredValidItems.length;
                    this.updatePagination(this.pagination);
                }

                if (response['managers'] != null) {
                    for (let item of response['managers']) {
                        let obj = {};
                        obj['id'] = item['id'];
                        obj['name'] = item['fullName'];
                        obj['path'] = item['path'];
                        this.rootManagers.push(obj);
                    }
                }

                if (response['officers'] != null) {
                    for (let item of response['officers']) {
                        let obj = {};
                        obj['id'] = item['id'];
                        obj['name'] = item['fullName'];
                        obj['path'] = item['path'];
                        this.rootOfficers.push(obj);
                    }
                }

                this.load = true;
            })
            .catch(() => {
            });
    }

    normalizeUnitsTree(units: Array<any>) {
        for (let item of units) {
            if (item['parent'] == null) {
                item['parent'] = [];
            } else {
                item['parent'] = [item['parent']];
            }
            item['officers'] = [];
            item['managers'] = [];
            if (item['nodes'] != null) {
                if (item['nodes'].length > 0) {
                    this.normalizeUnitsTree(item['nodes']);
                }
            }
            if (typeof item['nodes'] == 'undefined') {
                item['nodes'] = [];
            }
        }
    }

    showTree() {
        console.log(this.sortUnits(this.filteredValidItems));
    }

    usersToSelectorItem(users: Array<Object>) {
        let result: Array<Object> = [];

        for (let item of users) {
            let tmp_user = new Object();
            tmp_user['id'] = item['id'];
            tmp_user['name'] = item['shortName'] + " (" + item['username'] + ")";
            tmp_user['nodes'] = [];
            tmp_user['parent'] = null;
            tmp_user['used'] = true;
            result.push(tmp_user);
        }
        return result;
    }


    //UNIT MODAL
    public modalUnitName: string;
    public modalUnitNameOld: string;
    public modalUnitParent: Array<Object>;
    public modalUnitParentOld: Array<Object>;
    public modalUnitOfficers: Array<Object>;
    public modalUnitManagers: Array<Object>;
    public modalUnitId: string;
    public modalUnitNodes: Array<Object>;

    private modalManagersShow: boolean = false;
    private modalOfficersShow: boolean = false;

    public unitModalError: string = "";

    addUnit(parent: Array<Object>) {
        this.unitModalError = "";
        this.modalManagersShow = false;
        this.modalOfficersShow = false;
        this.clearModalValues();

        this.modalUnitId = "";
        this.modalUnitParent = [parent];
        this.modalUnitParentOld = [parent];
        this.modalUnitOfficers = [];
        this.modalUnitManagers = [];
        this.modalUnitName = "";
        this.modalUnitNameOld = "";
        this.modalUnitNodes = [];

        this.unitModal.open();
        this.unitParentSelector.resetValue();

        //Определение вышестоящих офицеров
        let excludeOfficers = [];
        for (let officer of this.rootOfficers.concat(this.getParentUsers(parent['id'], [], "", this.unitsTree, 'officers'))) {
            excludeOfficers.push(officer['id']);
        }
        this.unitOfficersSelector.changeExcludeArray(excludeOfficers);
        this.unitOfficersSelector.checkUnitTree();

        //Определение вышестоящих менеджеров
        let excludeManagers = [];
        for (let manager of this.rootManagers.concat(this.getParentUsers(parent['id'], [], "", this.unitsTree, 'managers'))) {
            excludeManagers.push(manager['id']);
        }
        this.unitManagersSelector.changeExcludeArray(excludeManagers);
        this.unitManagersSelector.checkUnitTree();


        this.unitParentSelector.changeExcludeArray([]);
        this.unitParentSelector.checkUnitTree();
        if (parent.length != 0) {
            this.unitParentSelector.getOutsideUnit(parent);
        }
    }

    editUnit(unit: Object) {
        this.unitModalError = "";
        this.modalManagersShow = false;
        this.modalOfficersShow = false;
        this.clearModalValues();

        this.modalUnitId = unit['id'];
        this.modalUnitParent = unit['parent'];
        this.modalUnitParentOld = unit['parent'];
        this.modalUnitOfficers = unit['officers'];
        this.modalUnitManagers = unit['managers'];
        this.modalUnitName = unit['name'];
        this.modalUnitNameOld = unit['name'];

        this.unitModal.open();
        this.unitParentSelector.resetValue();

        this.cdRef.detectChanges();
        //Определение вышестоящих офицеров
        let excludeOfficers = [];
        for (let officer of this.rootOfficers.concat(this.getParentUsers(this.modalUnitId, [], "", this.unitsTree, 'officers'))) {
            excludeOfficers.push(officer['id']);
        }
        //Удаление выбранынй в юните элементов (чтобы они были доступны для выбора)
        excludeOfficers = excludeOfficers.slice(0, excludeOfficers.length - unit['officers'].length);

        //Запрет выбора вышестоящих офицеров
        this.unitOfficersSelector.changeExcludeArray(excludeOfficers);
        this.unitOfficersSelector.checkUnitTree();
        //for(let officer of unit['officers']) {
        this.unitOfficersSelector.getOutsideUnits(unit['officers']);
        //}

        //Определение вышестоящих менеджеров
        let excludeManagers = [];
        for (let manager of this.rootManagers.concat(this.getParentUsers(this.modalUnitId, [], "", this.unitsTree, 'managers'))) {
            excludeManagers.push(manager['id']);
        }
        ///Удаление выбранынй в юните элементов (чтобы они были доступны для выбора)
        excludeManagers = excludeManagers.slice(0, excludeManagers.length - unit['managers'].length);

        //Запрет выбора вышестоящих менеджеров
        this.unitManagersSelector.changeExcludeArray(excludeManagers);
        this.unitManagersSelector.checkUnitTree();
        //for(let manager of unit['managers']) {
        this.unitManagersSelector.getOutsideUnits(unit['managers']);
        //}

        this.unitParentSelector.changeExcludeArray([unit['id']]);
        this.unitParentSelector.checkUnitTree();
        if (unit['parent'].length != 0) {
            this.unitParentSelector.getOutsideUnit(unit['parent'][0]);
        }
    }

    saveUnit() {
        this.unitModalError = "";
        let unit: Object = {};
        unit['id'] = this.modalUnitId;
        unit['name'] = this.modalUnitName;

        if (this.modalUnitParent.length != 0) {
            unit['parent'] = [];
            let parent = {};
            parent['id'] = this.modalUnitParent[0]['id'];
            parent['name'] = this.modalUnitParent[0]['name'];
            parent['parent'] = null;
            parent['errors'] = [];
            unit['parent'].push(parent);
        } else {
            unit['parent'] = [];
        }

        unit['officers'] = [];
        unit['managers'] = [];

        for (let officer of this.modalUnitOfficers) {
            let obj = {};
            obj['id'] = officer['id'];
            obj['name'] = officer['name'];
            unit['officers'].push(obj);
        }

        for (let manager of this.modalUnitManagers) {
            let obj = {};
            obj['id'] = manager['id'];
            obj['name'] = manager['name'];
            unit['managers'].push(obj);
        }

        unit['nodes'] = this.modalUnitNodes;
        unit['errors'] = [];

        //Проверка на пустое имя
        if (unit['name'] == "") {
            this.unitModalError = "не заполнено наименование организации";
            return;
        }

        //Проверка существования юнита (api + local)
        this.checkUnitExist(unit['name'], unit['id'], unit['parent'], this.unitsTree)
            .then(() => {
                if (unit['id'] == "") {
                    unit['id'] = UUID.UUID();
                    unit['errors'] = [];

                    if (this.modalUnitParent.length > 0) {
                        this.addChildElement(unit, this.modalUnitParent[0]['id'], this.unitsTree);
                        this.unitParentSelector.changeUnitsArray(this.unitsTree);
                        this.unitsTableFilter.updateUnitsSelector();
                    }
                    else {
                        this.unitsTree.push(unit);
                        this.unitParentSelector.changeUnitsArray(this.unitsTree);
                        this.unitsTableFilter.updateUnitsSelector();
                        this.updateList();

                        let filterParams = this.unitsTableFilter.getFilterParams();
                        if(filterParams['name'] || filterParams['officers'] || filterParams['managers'] || filterParams['parent']) {
                            this.filteredValidItems = [];
                            this.filteredInvalidItems = [];
                            this.filterUnits(this.unitsTableFilter.getFilterParams(), this.unitsTree, true);
                            this.filterUnits(this.unitsTableFilter.getFilterParams(), this.invalidUnits, false);
                        }
                    }
                } else {
                    this.changeUnitValue(unit['id'], unit, this.unitsTree);
                    this.changeUnitValue(unit['id'], unit, this.invalidUnits);

                    if (this.modalUnitParent.length > 0) {
                        //@todo удаление и перемещение элемента
                        let unitObj = JSON.parse(this.removeUnit(unit['id'], this.unitsTree));
                        unitObj['parent']['id'] = this.modalUnitParent[0]['id'];
                        unitObj['parent']['name'] = this.modalUnitParent[0]['name'];
                        this.addChildElement(unitObj, this.modalUnitParent[0]['id'], this.unitsTree);

                        this.changeParentName(unit['name'], unit['id'], this.unitsTree);
                        this.changeParentName(unit['name'], unit['id'], this.invalidUnits);

                        this.unitParentSelector.changeUnitsArray(this.unitsTree);
                        this.unitsTableFilter.updateUnitsSelector();
                        this.updateUnitsList();
                        this.updateList();
                    }
                    else {
                        let unitObj = JSON.parse(this.removeUnit(unit['id'], this.unitsTree));
                        unitObj['parent'] = [];
                        this.unitsTree.push(unitObj);

                        this.changeParentName(unit['name'], unit['id'], this.unitsTree);
                        this.changeParentName(unit['name'], unit['id'], this.invalidUnits);

                        this.unitParentSelector.changeUnitsArray(this.unitsTree);
                        this.unitsTableFilter.updateUnitsSelector();
                        this.updateUnitsList();
                        this.updateList();
                    }
                }
                this.unitModal.close();
                this.checkErrors();
                this.updateList();
            })
            .catch(() => {
                this.unitModalError = "организация с таким наименованием уже существует";
            });
    }

    //Замена имени родителя у элементов
    changeParentName(name: string, parentId: string, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['parent'].length > 0) {
                if (unit['parent'][0]['id'] == parentId) {
                    unit['parent'][0]['name'] = name;
                }
            }
            if (unit['nodes'].length > 0) {
                this.changeParentName(name, parentId, unit['nodes']);
            }
        }
    }

    updateUnitsList() {
        this.filteredInvalidItems = [];
        this.filterUnits(this.unitsTableFilter.getFilterParams(), this.invalidUnits, false);
    }

    updateList() {
        this.checkErrors();
        if (this.loadErrors) {
            this.pagination.totalElements = this.filteredInvalidItems.length;
        } else {
            this.pagination.totalElements = this.filteredValidItems.length;
        }
        this.updatePagination(this.pagination);
        //this.tablePagination.changeParams(this.pagination);
    }

    deleteUnit(unit: Object) {
        this.deleteModalUnit = unit;
        this.deleteModalId = unit['id'];
        this.unitDeleteModal.open();
    }

    delUnit() {
        let id = this.deleteModalId;
        this.removeUnit(id, this.unitsTree);
        this.removeUnit(id, this.invalidUnits);
        this.cdRef.detectChanges();
        this.updateUnitsList();

        //Обновление пэйджинации
        if (this.deleteModalUnit['parent'].length == 0) {
            // this.filteredValidItems = [];
            // this.filterUnits(this.unitsTableFilter.getFilterParams(), this.unitsTree, true);

            /////////////////////////
            this.updateList();

            //Обновление пэйджинации
            if (!this.loadErrors) {
                this.tablePagination.changeParams(this.pagination);
                this.tablePagination.refreshPagination();
            }
            /////////////////////////
        }
    }

    delDomChildrenElements(id: string, unitsTree) {
        for (let item of unitsTree) {
            if (item['id'] == id) {
                if (item['nodes'].length > 0) {
                    this.delDomElements(item['nodes']);
                }
            }
            if (item['nodes'].length > 0) {
                this.delDomChildrenElements(id, item['nodes']);
            }
        }
    }

    delDomElements(unitTree: Array<Object>) {
        for (let unit of unitTree) {
            let parent = document.getElementById('units-valid-table');
            let children = document.getElementById('row-' + unit['id']);
            if (children != null) {
                parent.removeChild(children);
            }

            if (unit['nodes'].length != 0) {
                this.delDomElements(unit['nodes']);
            }
        }
    }

    removeUnit(unitId: string, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['id'] == unitId) {
                let obj_string = JSON.stringify(unit);
                unitsTree.splice(unitsTree.indexOf(unit), 1);
                if (!this.loadErrors) {
                    //Удаление элемента из DOM
                    let parent = document.getElementById('units-valid-table');
                    let children = document.getElementById('row-' + unitId);
                    if (children != null) {
                        parent.removeChild(children);
                    }
                }
                this.unitParentSelector.changeUnitsArray(this.unitsTree);
                this.unitsTableFilter.updateUnitsSelector();

                if (unit['nodes'].length > 0) {
                    this.delDomElements(unit['nodes']);
                }

                return obj_string;
            }
            if (unit['nodes'].length > 0) {
                let res = this.removeUnit(unitId, unit['nodes']);
                if (typeof res != "undefined") {
                    return res;
                }
            }
        }
    }

    changeUnitValue(unitId: string, unitObj: Object, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['id'] == unitId) {
                unit['name'] = unitObj['name'];
                unit['parent'] = JSON.parse(JSON.stringify(unitObj['parent']));
                unit['officers'] = unitObj['officers'];
                unit['managers'] = unitObj['managers'];
                unit['errors'] = [];
                return;
            }
            if (unit['nodes'].length > 0) {
                this.changeUnitValue(unitId, unitObj, unit['nodes']);
            }
        }
    }

    checkUnitExist(checkName: string, unitId: string, parent: any, unitsTree: Array<Object>): Promise<any> {
        return new Promise((resolve, reject) => {
            if (parent.length != 0) {
                for (let unit of unitsTree) {
                    if (unit['id'] == parent[0]['id']) {
                        for (let item of unit['nodes']) {
                            if (item['name'] == checkName && item['id'] != unitId) {
                                reject();
                            }
                        }
                    }
                    if (unit['nodes'].length != 0) {
                        this.checkUnitExist(checkName, unitId, parent, unit['nodes'])
                            .then(() => {
                                resolve();
                            })
                            .catch(() => {
                                reject();
                            });
                    }
                }
                setTimeout(() => {
                    resolve();
                }, 100);
            } else {
                let params = {};

                if (this.parentID != "") {
                    params['filters'] = "'parent.id' EQ '" + this.parentID + "'";
                } else {
                    params['filters'] = "'parent.id' EQ 'null'";
                }
                //params['searchs'] = "'name':'"+checkName+"'";
                params['filters'] += ",'name' EQ '" + checkName + "'";

                this.core.get('api/units/exists', params)
                    .then((res) => {
                        if (res['_embedded'] == true) {
                            reject();
                        } else {
                            for (let unit of unitsTree) {
                                if (unit['name'] == checkName && unit['id'] != unitId) {
                                    reject();
                                }
                            }
                            setTimeout(() => {
                                resolve();
                            }, 100);
                        }
                    });
            }
        });
    }

    addChildElement(addUnit: Object, parentId: string, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['id'] == parentId) {
                unit['nodes'].push(addUnit);
                return;
            }
            if (unit['nodes'].length != 0) {
                this.addChildElement(addUnit, parentId, unit['nodes']);
            }
        }
        return;
    }

    unitsEqual(firstUnits: Array<Object>, secondUnits: Array<Object>) {
        var flag = true;
        if (firstUnits.length == secondUnits.length) {
            for (var unit of firstUnits) {
                for (var unit2 of secondUnits) {
                    if (unit['id'] != unit2['id']) {
                        flag = false;
                    }
                    else {
                        flag = true;
                        break;
                    }
                }
            }
        }
        else {
            flag = false;
        }

        return flag;
    }


    clearModalValues() {
        this.modalUnitName = "";
        this.modalUnitParent = [];
        this.modalUnitOfficers = [];
        this.modalUnitManagers = [];
    }

    toggleModalManagersShow() {
        this.modalManagersShow = !this.modalManagersShow;
    }

    toggleModalOfficersShow() {
        this.modalOfficersShow = !this.modalOfficersShow;
    }

    parentSelectorChange(selected) {
        this.modalUnitParent = selected;
    }

    officersSelectorChange(selected) {
        this.modalUnitOfficers = selected;
    }

    managersSelectorChange(selected) {
        this.modalUnitManagers = selected;
    }

    DBUnitsSelectorChange(selected) {
        if (selected.length > 0) {
            this.parentID = selected[0]['id'];
        } else {
            this.parentID = "";
        }
    }

    getParentUsers(_unitId: string, _usersArray: Array<Object>, _path: string, unitsTree: Array<Object>, type: string): Array<Object> {
        for (let unit of unitsTree) {
            let unitId = _unitId;
            let usersArray = JSON.parse(JSON.stringify(_usersArray));
            let path = _path.slice(0, _path.length);
            if (unitId == unit['id']) {
                return usersArray;
            }
            if (path != "") {
                path += " - " + unit['name'];
            } else {
                path += unit['name'];
            }
            if (type == 'managers') {
                for (let manager of unit['managers']) {
                    usersArray.push({id: manager['id'], name: manager['name'], path: path});
                }
            }
            if (type == 'officers') {
                for (let officer of unit['officers']) {
                    usersArray.push({id: officer['id'], name: officer['name'], path: path});
                }
            }
            if (unit['nodes'].length > 0) {
                let res = this.getParentUsers(unitId, usersArray, path, unit['nodes'], type);
                if (res != null) {
                    return res;
                }
            }
        }
        return null;
    }

    //Проверка на наличие ошибок в юнитах
    checkErrors() {
        let flag = false;
        let count = 0;
        for (let item of this.invalidUnits) {
            if (item['errors'].length != 0) {
                flag = true;
                count++;
            }
        }
        if (!flag) {
            this.updateAllow = true;
        }
        this.errorsCount = count;
    }

    //Нажатие на кнопку "Обновить базу"
    updateUnits() {
        if (this.updateAllow) {
            this.loadErrors = false;
            this.unitsTableFilter.resetFilter();
            this.pagination.number = 1;
            this.pagination.size = 10;
            this.pagination.totalElements = this.unitsTree.length;
            this.updatePagination(this.pagination);
            this.tablePagination.changeParams(this.pagination);
        }
    }

    public filteredInvalidItems: Array<Object>;
    public filteredValidItems: Array<Object>;

    searchUnits(filterParams: Object) {
        this.pagination.number = 1;
        this.applyFilter(filterParams);
        setTimeout(() => {
            this.updatePagination(this.pagination);
            if (this.tablePagination !== undefined) {
                this.tablePagination.changeParams(this.pagination);
            }
        }, 50);
    }

    applyFilter(filterParams: Object) {
        this.filteredInvalidItems = [];
        this.filteredValidItems = [];
        this.filterUnits(filterParams, this.invalidUnits, false);
        this.filterUnits(filterParams, this.unitsTree, true);

        if (this.loadErrors) {
            this.pagination.totalElements = this.filteredInvalidItems.length;
        } else {
            this.pagination.totalElements = this.filteredValidItems.length;
        }
        this.updatePagination(this.pagination);
        if (this.loadErrors) {
            this.updateList();
        }
    }

    filterUnits(filterParams: Object, units: Array<Object>, valid: boolean) {
        if (filterParams['name'] == "" && filterParams['managers'].length == 0
            && filterParams['officers'].length == 0 && filterParams['parent'].length == 0) {
            if (valid) {
                this.filteredValidItems = units;
            } else {
                this.filteredInvalidItems = units;
            }
            return;
        }

        for (let unit of units) {
            let addUnit: boolean = true;

            if (filterParams['name'] != "") {
                if (unit['name'].toString().toLowerCase().indexOf(filterParams['name'].toLowerCase()) == -1) {
                    addUnit = false;
                }
            }

            if (filterParams['parent'].length != 0) {
                if (unit['parent'].length != 0) {
                    if (unit['parent'][0]['id'] != filterParams['parent'][0]['id']) {
                        addUnit = false;
                    }
                } else {
                    addUnit = false;
                }
            }

            if (filterParams['managers'].length != 0) {
                let flag = true;

                for (let item of filterParams['managers']) {
                    if (!this.inArray(item, unit['managers'])) {
                        flag = false;
                    }
                }
                if (flag == false) {
                    addUnit = false;
                }
            }

            if (filterParams['officers'].length != 0) {
                let flag = true;

                for (let item of filterParams['officers']) {
                    if (!this.inArray(item, unit['officers'])) {
                        flag = false;
                    }
                }
                if (flag == false) {
                    addUnit = false;
                }
            }

            if (addUnit) {
                if (valid) {
                    this.filteredValidItems.push(unit);
                } else {
                    this.filteredInvalidItems.push(unit);
                }
            }

            if (unit['nodes'].length != 0) {
                this.filterUnits(filterParams, unit['nodes'], valid);
            }
        }
    }

    inArray(findItem: Object, units: Array<Object>): boolean {
        let flag = false;

        for (let item of units) {
            if (item['id'] == findItem['id']) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    changePagination(reload: boolean) {
        this.updatePagination(this.pagination);
    }

    sortUnits(units: Array<Object>) {
        let sortedUnitsArray: Array<Object> = [];
        let index = 0;

        if (!this.loadErrors) {
            this.sortUnitsTreeByAlphabet(units);
        }

        for (let item of units) {
            if (index < this.pagination.size * this.pagination.number && (index > this.pagination.size * this.pagination.number - this.pagination.size - 1)) {
                sortedUnitsArray.push(item);
            }
            index++;
        }

        return sortedUnitsArray;
    }

    updatePagination(pagination: Pagination) {
        let division = pagination.totalElements / pagination.size;
        pagination.totalPages = Math.floor(pagination.totalElements / pagination.size);
        pagination.totalPages += ((division % 1) != 0) ? 1 : 0;
    }

    //Сортировка массива по алфавиту
    sortArray(unitArray: Array<Object>) {
        unitArray.sort((a, b) => {
            var textA = a['name'].toUpperCase();
            var textB = b['name'].toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
    }

    sortUnitsTreeByAlphabet(unitTree: Array<Object>) {
        this.sortArray(unitTree);
        for (let unit of unitTree) {
            if (unit['nodes'].length > 0) {
                this.sortUnitsTreeByAlphabet(unit['nodes']);
            }
        }
    }

    public loadUnits: Array<Object>;
    public uploadDBError: boolean = false;

    prepareToLoad(units: Array<any>) {
        for (let item of units) {

            if (item['parent'].length == 0) {
                item['parent'] = null;
            } else {
                item['parent'] = item['parent']['id'];
            }

            item['users'] = [];

            for (let user of item['officers']) {
                item['users'].push(user['id']);
            }

            for (let user of item['managers']) {
                item['users'].push(user['id']);
            }

            if (item['nodes'].length > 0) {
                this.prepareToLoad(item['nodes']);
            }
        }
    }

    uploadUnitsDB() {
        this.uploadDBError = false;
        this.loadUnits = JSON.parse(JSON.stringify(this.unitsTree));
        this.prepareToLoad(this.loadUnits);

        let form = {};
        form['units'] = this.loadUnits;
        if (this.parentID != "") {
            form['id'] = this.parentID;
        }

        let postData = JSON.parse('{"form": ' + JSON.stringify(form) + '}');
        this.core.post_json('api/units/import/save', postData)
            .then(() => {
                this.router.navigate(['/units']);
            })
            .catch(() => {
                this.uploadDBError = true;
            });
    }
}