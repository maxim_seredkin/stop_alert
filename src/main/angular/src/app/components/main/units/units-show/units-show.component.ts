/**
 * Created by Baser on 17.03.2017.
 */
import { Component, ViewChild } from '@angular/core';
import { AppCoreService } from "../../../../services/app-core.service";
import { ActivatedRoute, Router } from "@angular/router";
import { UnitsTreeSelectorComponent } from '../../units-tree-selector/units-tree-selector.component';
import { Modal } from 'ngx-modal';
import { UserService } from '../../../../services/user.service';

@Component({
    selector: 'units-show',
    template: require('./units-show.component.html')
})

export class UnitsShowComponent {
    public unit: Object;
    public editUnit: boolean = false;
    public unitsTree: Array<Object>;

    public modalDeleteUnit: Object;
    public modalError: boolean = false;
    public modalErrorText: string = "";
    public modalUnitsTree: Array<Object>;
    public employeeMoveOrg: Array<Object>;
    public moveEmployeeModalError: boolean = false;
    public moveEmployeeError: boolean = false;
    @ViewChild('orgMoveSelector') orgMoveSelector: UnitsTreeSelectorComponent;

    @ViewChild('emptyUnitDeleteModal')
    private emptyUnitDeleteModal: Modal;

    @ViewChild('moveEmployees')
    private moveEmployeesModal: Modal;

    @ViewChild('unitParentSelector')
    private unitParentSelector: UnitsTreeSelectorComponent;

    public managers: Array<Object>;
    public officers: Array<Object>;

    constructor(private core: AppCoreService,
                private route: ActivatedRoute,
                private router: Router,
                public user: UserService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let id = params['id'];
            this.getUnitById(id);
        });
    }

    getUnitById(id: string) {
        this.core.get("api/units/" + id)
            .then((res) => {
                this.unit = res['_embedded'];
            });
    }

    getUnitsTree() {
        return new Promise((resolve, reject) => {
            this.core.get('api/units/tree')
            .then(res => {
                this.unitsTree = res['_embedded'];
                resolve();
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getManagers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_MANAGER'";
        params['filters'] += ",'enabled' EQ 'true'";
        params['filters'] += ",'accountNonLocked' EQ 'true'";

        this.core.get('api/users', params)
            .then((res) => {
                this.managers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    getOfficers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_SECURITY_OFFICER'";
        params['filters'] += ",'enabled' EQ 'true'";
        params['filters'] += ",'accountNonLocked' EQ 'true'";

        this.core.get('api/users', params)
            .then((res) => {
                this.officers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    usersToSelectorItem(users: Array<Object>) {
        let result: Array<Object> = [];

        for (let item of users) {
            let tmp_user = new Object();
            tmp_user['id'] = item['id'];
            tmp_user['name'] = item['shortName'] + " (" + item['username'] + ")";
            tmp_user['nodes'] = [];
            tmp_user['parent'] = null;
            tmp_user['used'] = true;
            result.push(tmp_user);
        }
        return result;
    }

    orgEntityDelete(unit: Object) {
        this.modalDeleteUnit = unit;
        this.modalError = false;
        this.modalErrorText = "";

        this.modalUnitsTree = [];
        this.employeeMoveOrg = [];
        this.moveEmployeeModalError = false;

        let params = new Object();

        params['filters'] = "EXTRA:'units' NE '" + unit['id'] + "'";

        this.core.get('api/units/tree', params)
            .then((res) => {
                this.modalUnitsTree = res['_embedded'];
                this.orgMoveSelector.changeUnitsArray(this.modalUnitsTree);
                if (this.modalUnitsTree.length == 0) {
                    this.modalError = true;
                    this.modalErrorText = "Нет доступных подразделений для переноса";
                }
            });

        if (unit['countEmployees'] != 0) {
            this.moveEmployeesModal.open();
        }
        else {
            this.emptyUnitDeleteModal.open();
        }
    }

    delUnit() {
        if (this.modalError) {
            return;
        }
        this.delUnitQuery().then(() => {
            this.router.navigate(['units']);
        });
    }

    delUnitQuery() {
        return new Promise((resolve, reject) => {
            if (this.modalDeleteUnit['countEmployees'] != 0) {
                if (this.employeeMoveOrg.length != 0) {
                    this.orgMoveSelector.resetValue();

                    let data = new Object();
                    data['successor'] = this.employeeMoveOrg[0]['id'];

                    let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                    this.core.put("api/units/" + this.modalDeleteUnit['id'] + "/delete", putData)
                        .then((res) => {
                            if (res['code'] == 200) {
                                this.moveEmployeesModal.close();
                                resolve();
                                this.emptyUnitDeleteModal.close();
                            }
                            if (res['code'] == 1013) {
                                this.modalError = true;
                                this.modalErrorText = "Данная организация используется в одной из задач, ожидающих выполнения (или приостановленных)";
                                reject();
                            }
                        });
                }
                else {
                    this.moveEmployeeError = true;
                }
            }
            else {
                let putData = JSON.parse('{"form": {}}');
                this.core.put("api/units/" + this.modalDeleteUnit['id'] + "/delete", putData)
                    .then((res) => {
                        if (res['code'] == 200) {
                            this.emptyUnitDeleteModal.close();
                            resolve();
                        }

                        if (res['code'] == 1013) {
                            reject();
                            this.modalError = true;
                            this.modalErrorText = "Данная организация используется в одной из задач, ожидающих выполнения (или приостановленных)";
                        }
                    });
            }
        });
    }

    employeesMoveOrgChange(value) {
        this.employeeMoveOrg = value;
    }

    public editUnitName: string = "";
    public editUnitParent: Array<Object>;
    public editUnitManagers: Array<Object>;
    public editUnitOfficers: Array<Object>;
    public fillFormError: boolean = false;
    public formErrorText: string = "";

    parentSelectorChange(selected) {
        this.editUnitParent = selected;
    }

    officersSelectorChange(selected) {
        this.editUnitOfficers = selected;
    }

    managersSelectorChange(selected) {
        this.editUnitManagers = selected;
    }

    toggleEdit() {
        this.editUnitName = this.unit['name'];
        this.editUnitParent = (this.unit['parent']) ? this.unit['parent'] : [];
        this.editUnitManagers = this.unit['managers'];
        this.editUnitOfficers = this.unit['officers'];
        this.editUnit = !this.editUnit;

        if (this.editUnit) {
            if (!this.managers) {
                this.getManagers();
            }

            if (!this.officers) {
                this.getOfficers();
            }

            this.getUnitsTree()
                .then(() => {
                    if (this.unit['parent']) {
                        setTimeout(() => {
                            this.unitParentSelector.getOutsideUnit(this.editUnitParent);
                        }, 50);
                    }
                });
        } else {
            this.editUnitParent = [];
        }
    }

    saveUnit() {
        this.formErrorText = "";
        this.fillFormError = false;

        let unit: Object = {};
        unit['id'] = this.unit['id'];
        unit['name'] = this.editUnitName;

        if (this.editUnitParent.length != 0) {
            unit['parent'] = [];
            let parent = {};
            parent['id'] = this.editUnitParent[0]['id'];
            parent['name'] = this.editUnitParent[0]['name'];
            unit['parent'].push(parent);
        } else {
            unit['parent'] = [];
        }

        unit['officers'] = [];
        unit['managers'] = [];

        for (let officer of this.editUnitOfficers) {
            let obj = {};
            obj['id'] = officer['id'];
            obj['name'] = officer['name'];
            unit['officers'].push(obj);
        }

        for (let manager of this.editUnitManagers) {
            let obj = {};
            obj['id'] = manager['id'];
            obj['name'] = manager['name'];
            unit['managers'].push(obj);
        }

        //Проверка на пустое имя
        if (unit['name'] == "") {
            this.fillFormError = true;
            this.formErrorText = "не заполнено наименование организации";
            return;
        }
        //Проверка существования юнита (api + local)
        this.checkUnitExist(unit['name'], unit['id'], unit['parent'])
            .then(() => {
                this.editUnitQuery(unit)
                    .then(() => {
                        this.route.params.subscribe(params => {
                            let id = params['id'];
                            this.getUnitById(id);
                        });
                        this.toggleEdit();
                        this.fillFormError = false;
                        this.formErrorText = "";
                    })
                    .catch(() => {
                        this.fillFormError = true;
                        this.formErrorText = "при редактировании организации произошла ошибка";
                    });
            })
            .catch(() => {
                this.fillFormError = true;
                this.formErrorText = "организация с таким наименованием уже существует";
            });
    }

    editUnitQuery(unit: Object) {
        return new Promise((resolve, reject) => {
            let data = this.prepareUnitToSave(unit);
            let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
            this.core.put("api/units/" + data['id'], putData)
                .then((res) => {
                    if (res['code'] == 200) {
                        resolve();
                    }
                })
                .catch(() => {
                    reject();
                })
        });
    }

    prepareUnitToSave(unit: Object) {
        let data = {};
        data['id'] = unit['id'];
        data['name'] = unit['name'];

        if (unit['parent'].length != 0) {
            data['parent'] = unit['parent'][0]['id'];
        }
        else {
            data['parent'] = null;
        }
        data['users'] = [];
        for (let user of unit['officers']) {
            data['users'].push(user['id']);
        }
        for (let user of unit['managers']) {
            data['users'].push(user['id']);
        }
        return data;
    }

    checkUnitExist(checkName: string, unitId: string, parent: Array<Object>): Promise<any> {
        return new Promise((resolve, reject) => {
            let params = {};
            if (parent.length !== 0) {
                params['filters'] = "'parent.id' EQ '" + parent[0]['id'] + "'";
            } else {
                params['filters'] = "'parent.id' EQ 'null'";
            }

            params['filters'] += ",'name' EQ '" + checkName + "'";

            this.core.get('api/units/exists', params)
                .then((res) => {
                    if (res['_embedded'] == true) {
                        if (checkName === this.unit['name']) {
                            resolve();
                        } else {
                            reject();
                        }
                    } else {
                        resolve();
                    }
                });
        });
    }
}