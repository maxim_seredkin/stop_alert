/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 05.06.2017.
 */
import {
    Component, Input, Output, ElementRef, TemplateRef, ChangeDetectorRef,
    EventEmitter
} from '@angular/core';
import {DomSanitizer, SafeStyle} from "@angular/platform-browser";

@Component({
    selector: "units-valid-table-row, [unitsValidTableRow]",
    template: require('./units-valid-table-row.component.html')
})

export class UnitsValidTableRowComponent {
    @Input() unit: Object;
    @Input() index: number;
    @Input() level: number;
    @Input() show: boolean;
    @Input() unitLinks: boolean;
    @Output() delUnit = new EventEmitter();
    @Output() addUnit = new EventEmitter();
    @Output() editUnit = new EventEmitter();

    public linesCount: Array<any>;
    public showChildren: boolean;

    public safeTDwidth: SafeStyle;

    constructor(private el: ElementRef, private tmplRef:TemplateRef<any>, private cdRef:ChangeDetectorRef, private sanitizer: DomSanitizer) {
        this.showChildren = false;
    }

    //wait for the component to render completely
    ngOnInit() {
    }

    ngAfterViewInit() {
        this.linesCount = new Array(this.level);
        this.safeTDwidth = this.sanitizer.bypassSecurityTrustStyle('calc(100% - 16px*'+this.level+')');

        this.cdRef.detectChanges();
    }

    toggleShowChildren() {
        this.showChildren = !this.showChildren;
    }
    

    deleteUnit(unit: Object) {
        this.delUnit.emit(unit);
    }

    deleteUnitPush(unit: Object) {
        this.delUnit.emit(unit);
    }

    addUnitPush(parent: Object) {
        this.addUnit.emit(parent);
    }
    
    editUnitPush(unit: Object) {
        this.editUnit.emit(unit);
    }

}