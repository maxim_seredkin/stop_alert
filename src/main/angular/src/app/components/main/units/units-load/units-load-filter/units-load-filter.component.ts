/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 01.06.2017.
 */
import {Component, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {TreeSelectorComponent} from "../../../tree-selector/tree-selector.controller";
import {UnitsTreeSelectorComponent} from "../../../units-tree-selector/units-tree-selector.component";

@Component({
    selector: 'units-load-filter',
    template: require('./units-load-filter.component.html')
})

export class UnitsLoadFilterComponent {
    @Input() managers: Array<Object>;
    @Input() officers: Array<Object>;
    @Input() unitsTree: Array<Object>;

    public showAdv: boolean = false;

    @ViewChild('filterUnitsSelector')
    private filterUnitsSelector: UnitsTreeSelectorComponent;

    public selectedUnitParent: Array<Object> = [];
    public selectedOfficers: Array<Object> = [];
    public selectedManagers: Array<Object> = [];
    public filteredUnitName: string = "";

    @Output() apply = new EventEmitter();

    toggleShow() {
        if(this.showAdv) {
            this.selectedManagers = [];
            this.selectedOfficers = [];
            this.selectedUnitParent = [];
        }
        this.showAdv = !this.showAdv;
    }
    
    updateUnitsSelector() {
        if(typeof this.filterUnitsSelector != "undefined") {
            //this.filterUnitsSelector.resetValue();
            this.filterUnitsSelector.changeUnitsArray(this.unitsTree);
        }
    }

    applyFilter() {
        this.apply.emit(this.getFilterParams());
    }

    parentSelectorChange(selected) {
        this.selectedUnitParent = selected;
    }

    officersSelectorChange(selected) {
        this.selectedOfficers = selected;
    }

    managersSelectorChange(selected) {
        this.selectedManagers = selected;
    }

    getFilterParams() {
        let params = {};
        params['name'] = this.filteredUnitName;
        params['officers'] = this.selectedOfficers;
        params['managers'] = this.selectedManagers;
        params['parent'] = this.selectedUnitParent;
        return params;
    }

    resetFilter() {
        this.selectedUnitParent = [];
        this.selectedManagers = [];
        this.selectedOfficers = [];
        this.filteredUnitName = "";
    }
}