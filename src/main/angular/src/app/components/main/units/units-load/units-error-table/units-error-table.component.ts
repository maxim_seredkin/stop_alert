/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 02.06.2017.
 */
import {
    Component, Input, Output, TemplateRef, ViewChild,
    ChangeDetectorRef, EventEmitter
} from '@angular/core';
import {Modal} from "ngx-modal/index";
import {TreeSelectorComponent} from "../../../tree-selector/tree-selector.controller";
import {UnitsTreeSelectorComponent} from "../../../units-tree-selector/units-tree-selector.component";

@Component({
    selector: 'units-error-table',
    template: require('./units-error-table.component.html')
})

export class UnitsErrorTableComponent {
    @Input() invalidUnits: Array<any>;
    @Input() unitsTree: Array<any>;
    @Input() managers: Array<any>;
    @Input() officers: Array<any>;
    @Output() editUnit = new EventEmitter();
    @Output() delUnit = new EventEmitter();

    @ViewChild('editUnitModal')
    private editUnitModal:Modal;

    @ViewChild('unitParentSelector')
    private unitParentSelector: UnitsTreeSelectorComponent;

    private modalManagersShow: boolean = false;
    private modalOfficersShow: boolean = false;

    constructor(private templateRef: TemplateRef<any>, private cdr:ChangeDetectorRef) {
    }

    ngAfterViewInit() {
    }

    findError(errorText: string, unit: Object): boolean {
        let flag = false;

        for(let error of unit['errors']) {
            if(error == errorText) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public modalUnitName: string;
    public modalUnitParent: Array<Object>;
    public modalUnitOfficers: Array<Object>;
    public modalUnitManagers: Array<Object>;
    public modalUnitId: string;
    
    // editUnit(unit: Object) {
    //     this.clearModalValues();
    //     this.modalUnitName = unit['name'];
    //     //this.modalUnitParent = [unit['parent']];
    //     this.modalUnitOfficers = unit['officers'];
    //     this.modalUnitManagers = unit['managers'];
    //     this.modalUnitId = unit['id'];
    //
    //     this.editUnitModal.open();
    //
    //     this.unitParentSelector.resetValue();
    //     if(unit['parent'] != null && unit['parent'].length != 0) {
    //         this.unitParentSelector.getOutsideUnit(unit['parent']);
    //     }
    //
    // }

    saveUnit() {
        this.changeUnitValue(this.modalUnitId, this.unitsTree);
        this.changeUnitValue(this.modalUnitId, this.invalidUnits);
        this.editUnitModal.close();
        this.unitParentSelector.changeUnitsArray(this.unitsTree);
    }

    changeUnitValue(id: string, unitTree: Array<Object>) {
        for(let unit of unitTree) {
            if(unit['id'] == id) {
                unit['name'] = this.modalUnitName;
                unit['parent'] = this.modalUnitParent[0];
                unit['errors'] = [];
            }
            if(unit['nodes'] != null) {
                if(unit['nodes'].length > 0) {
                    this.changeUnitValue(id, unit['nodes']);
                }
            }
        }
    }

    clearModalValues() {
        this.modalUnitName = "";
        this.modalUnitParent = [];
        this.modalUnitOfficers = [];
        this.modalUnitManagers = [];
    }

    toggleModalManagersShow() {
        this.modalManagersShow = !this.modalManagersShow;
    }

    toggleModalOfficersShow() {
        this.modalOfficersShow = !this.modalOfficersShow;
    }

    parentSelectorChange(selected) {
        this.modalUnitParent = selected;
    }

    officersSelectorChange(selected) {
        this.modalUnitOfficers = selected;
    }

    managersSelectorChange(selected) {
        this.modalUnitManagers = selected;
    }

}