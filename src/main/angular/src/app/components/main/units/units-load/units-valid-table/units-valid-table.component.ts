/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 02.06.2017.
 */
import {
    Component, Input, Output, EventEmitter
} from '@angular/core';
import {Pagination} from "../../../../../models/pagination";

@Component({
    selector: 'units-valid-table',
    template: require('./units-valid-table.component.html')
})

export class UnitsValidTableComponent {
    @Input() unitsTree: Array<any>;
    @Input() managers: Array<any>;
    @Input() officers: Array<any>;
    @Input() pagination: Pagination;
    @Input() unitLinks: boolean;
    @Output() addUnit = new EventEmitter();
    @Output() editUnit = new EventEmitter();
    @Output() delUnit = new EventEmitter();

    @Input() managersUnitArray: Array<any>;
    @Input() officersUnitArray: Array<any>;
    @Input() unitPath: string;

    constructor() {

    }

    deleteUnit(unit: Object) {
        this.delUnit.emit(unit);
        // setTimeout(() => {
        //     for(let item in this.unitsTree) {
        //         if(item['id'] == id) {
        //             this.unitsTree.splice(this.unitsTree.indexOf(item), 1);
        //         }
        //     }
        // },100);
    }

    addUnitPush(parent: Object) {
        this.addUnit.emit(parent);
    }

    editUnitPush(unit: Object) {
        this.editUnit.emit(unit);
    }
}