/**
 * Created by Baser on 15.03.2017.
 */
import {Component, ChangeDetectorRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UUID} from "angular2-uuid";
import {AppCoreService} from "../../../../services/app-core.service";
import {Pagination} from "../../../../models/pagination";
import {TreeSelectorComponent} from "../../tree-selector/tree-selector.controller";
import {UnitsLoadFilterComponent} from "../units-load/units-load-filter/units-load-filter.component";
import {Modal} from "ngx-modal";
import {UnitsPaginationComponent} from "../units-load/units-pagination/units-pagination.component";
import {UnitsTreeSelectorComponent} from "../../units-tree-selector/units-tree-selector.component";

@Component({
    selector: 'units-edit',
    template: require('./units-edit.component.html')
})

export class UnitsEditComponent {
    public unitsTree: Array<Object>;
    public managers: Array<Object>;
    public officers: Array<Object>;

    public editEntityId: string;
    public pathToEntity: string;
    public isInit: boolean = false;

    public pagination: Pagination;

    @ViewChild('unitDeleteModal')
    private unitDeleteModal: Modal;

    @ViewChild('unitModal')
    private unitModal: Modal;

    @ViewChild('unitParentSelector')
    private unitParentSelector: UnitsTreeSelectorComponent;

    @ViewChild('unitOfficersSelector')
    private unitOfficersSelector: TreeSelectorComponent;

    @ViewChild('unitManagersSelector')
    private unitManagersSelector: TreeSelectorComponent;

    @ViewChild('unitsTableFilter')
    private unitsTableFilter: UnitsLoadFilterComponent;

    @ViewChild('tablePagination')
    private tablePagination: UnitsPaginationComponent;

    @ViewChild('moveEmployees')
    private moveEmployeesModal: Modal;

    @ViewChild('emptyUnitDeleteModal')
    private emptyUnitDeleteModal: Modal;

    @ViewChild('orgMoveSelector') orgMoveSelector: TreeSelectorComponent;

    constructor(private cdRef: ChangeDetectorRef,
                private route: ActivatedRoute,
                private router: Router,
                private core: AppCoreService) {
        this.route.queryParams.subscribe(params => {
            if (params['editEntity']) {
                this.editEntityId = params['editEntity'];
            }
        });
        this.route.queryParams.subscribe(params => {
            if (params['path']) {
                this.pathToEntity = params['path'].split(",");
            }
        });

        this.getUnitsTree();
        this.getManagers();
        this.getOfficers();

        this.pagination = new Pagination();
    }

    successorAccept(unit: Object) {
        this.addEmployeesCount(event['id'], event['count'], this.unitsTree, false);
    }

    addEmployeesCount(id, count, unitsTree, addToParent) {
        for (let unit of unitsTree) {
            if (unit['id'] == id) {
                unit['countEmployees'] += count;
                if (unit['parent'] != null) {
                    this.addEmployeesCount(unit['parent']['id'], count, this.unitsTree, true);
                }
                break;
            }
            if (unit['nodes'] != null) {
                this.addEmployeesCount(id, count, unit['nodes'], addToParent);
            }
        }
    }

    getManagers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_MANAGER'";
        params['filters'] += ",'enabled' EQ 'true'";
        params['filters'] += ",'accountNonLocked' EQ 'true'";

        this.core.get('api/users', params)
            .then((res) => {
                this.managers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    getOfficers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_SECURITY_OFFICER'";
        params['filters'] += ",'enabled' EQ 'true'";
        params['filters'] += ",'accountNonLocked' EQ 'true'";

        this.core.get('api/users', params)
            .then((res) => {
                this.officers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    usersToSelectorItem(users: Array<Object>) {
        let result: Array<Object> = [];

        for (let item of users) {
            let tmp_user = new Object();
            tmp_user['id'] = item['id'];
            tmp_user['name'] = item['shortName'] + " (" + item['username'] + ")";
            tmp_user['nodes'] = [];
            tmp_user['parent'] = null;
            tmp_user['used'] = true;
            result.push(tmp_user);
        }
        return result;
    }

    handleUnitsTree(units) {
        for (let item of units) {
            if (item['officers'].length != 0) {
                let temp_officers = [];

                for (let officer of item['officers']) {
                    let officerObj = new Object();
                    officerObj['id'] = officer['id'];
                    officerObj['name'] = officer['shortName'] + " (" + officer['username'] + ")";
                    officerObj['nodes'] = null;
                    officerObj['parent'] = null;
                    temp_officers.push(officerObj);
                }

                item['officers'] = temp_officers;
            }
            if (item['managers'].length != 0) {
                let temp_managers = [];

                for (let manager of item['managers']) {
                    let managerObj = new Object();
                    managerObj['id'] = manager['id'];
                    managerObj['name'] = manager['shortName'] + " (" + manager['username'] + ")";
                    managerObj['nodes'] = null;
                    managerObj['parent'] = null;
                    temp_managers.push(managerObj);
                }

                item['managers'] = temp_managers;
            }

            if (!item['parent']) {
                item['parent'] = [];
            } else {
                item['parent'] = [item['parent']];
            }

            if (item['nodes'].length != 0) {
                this.handleUnitsTree(item['nodes']);
            }
        }
    }

    getUnitsTree() {
        this.core.get('api/units/tree')
            .then((res) => {
                this.unitsTree = res['_embedded'];
                this.handleUnitsTree(this.unitsTree);
                this.pagination.totalElements = this.unitsTree.length;
                this.filterUnits(this.unitsTableFilter.getFilterParams(), this.unitsTree, true);
                this.updatePagination(this.pagination);
            });

    }

    //UNIT MODAL
    public modalUnitName: string;
    public modalUnitNameOld: string;
    public modalUnitParent: Array<Object>;
    public modalUnitParentOld: Array<Object>;
    public modalUnitOfficers: Array<Object>;
    public modalUnitManagers: Array<Object>;
    public modalUnitId: string;
    public modalUnitNodes: Array<Object>;
    public modalCountEmployees: number;
    public modalUnitParentOfficers: Array<Object>;
    public modalUnitParentManagers: Array<Object>;

    private modalManagersShow: boolean = false;
    private modalOfficersShow: boolean = false;

    public unitModalError: string = "";

    addUnit(parent: Array<Object>) {
        this.unitModalError = "";
        this.modalManagersShow = false;
        this.modalOfficersShow = false;
        this.clearModalValues();

        this.modalUnitId = "";
        if(parent !== null) {
            this.modalUnitParent = [parent];
            this.modalUnitParentOld = [parent];
        } else {
            this.modalUnitParent = [];
            this.modalUnitParentOld = [];
        }
        this.modalUnitOfficers = [];
        this.modalUnitManagers = [];
        this.modalUnitParentOfficers = [];
        this.modalUnitParentManagers = [];
        this.modalUnitName = "";
        this.modalUnitNameOld = "";
        this.modalUnitNodes = [];
        this.modalCountEmployees = 0;

        this.unitModal.open();
        this.unitParentSelector.resetValue();

        //Определение вышестоящих офицеров
        let excludeOfficers = [];
        if(this.modalUnitParent.length != 0) {
            for (let officer of this.getParentUsers(this.modalUnitParent[0]['id'], [], "", this.unitsTree, 'officers', true)) {
                excludeOfficers.push(officer['id']);
                this.modalUnitParentOfficers.push(officer);
            }
        }
        this.unitOfficersSelector.changeExcludeArray(excludeOfficers);
        this.unitOfficersSelector.checkUnitTree();

        //Определение вышестоящих менеджеров
        let excludeManagers = [];
        if(this.modalUnitParent.length != 0) {
            for (let manager of this.getParentUsers(this.modalUnitParent[0]['id'], [], "", this.unitsTree, 'managers', true)) {
                excludeManagers.push(manager['id']);
                this.modalUnitParentManagers.push(manager);
            }
        }
        this.unitManagersSelector.changeExcludeArray(excludeManagers);
        this.unitManagersSelector.checkUnitTree();


        this.unitParentSelector.changeExcludeArray([]);
        this.unitParentSelector.checkUnitTree();
        if (this.modalUnitParent.length != 0) {
            this.unitParentSelector.getOutsideUnit(this.modalUnitParent[0]);
        }

    }

    editUnit(unit: Object) {
        this.unitModalError = "";
        this.modalManagersShow = false;
        this.modalOfficersShow = false;
        this.clearModalValues();

        this.modalUnitId = unit['id'];
        this.modalUnitParent = unit['parent'];
        this.modalUnitParentOld = unit['parent'];
        this.modalUnitOfficers = unit['officers'];
        this.modalUnitManagers = unit['managers'];
        this.modalUnitParentManagers = [];
        this.modalUnitParentOfficers = [];
        this.modalUnitName = unit['name'];
        this.modalUnitNameOld = unit['name'];
        this.modalCountEmployees = unit['countEmployees'];

        this.unitModal.open();
        this.unitParentSelector.resetValue();

        //Определение вышестоящих офицеров
        let excludeOfficers = [];
        for (let officer of this.getParentUsers(this.modalUnitId, [], "", this.unitsTree, 'officers')) {
            excludeOfficers.push(officer['id']);
            this.modalUnitParentOfficers.push(officer);
        }
        //Удаление выбранынй в юните элементов (чтобы они были доступны для выбора)
        excludeOfficers = excludeOfficers.slice(0, excludeOfficers.length - unit['officers'].length);

        //Запрет выбора вышестоящих офицеров
        this.unitOfficersSelector.changeExcludeArray(excludeOfficers);
        this.unitOfficersSelector.checkUnitTree();
        //for(let officer of unit['officers']) {
        this.unitOfficersSelector.getOutsideUnits(unit['officers']);
        //}

        //Определение вышестоящих менеджеров
        let excludeManagers = [];
        for (let manager of this.getParentUsers(this.modalUnitId, [], "", this.unitsTree, 'managers')) {
            excludeManagers.push(manager['id']);
            this.modalUnitParentManagers.push(manager);
        }
        ///Удаление выбранынй в юните элементов (чтобы они были доступны для выбора)
        excludeManagers = excludeManagers.slice(0, excludeManagers.length - unit['managers'].length);

        //Запрет выбора вышестоящих менеджеров
        this.unitManagersSelector.changeExcludeArray(excludeManagers);
        this.unitManagersSelector.checkUnitTree();
        //for(let manager of unit['managers']) {
        this.unitManagersSelector.getOutsideUnits(unit['managers']);
        //}

        this.unitParentSelector.changeExcludeArray([unit['id']]);
        this.unitParentSelector.checkUnitTree();
        if (unit['parent'].length != 0) {
            this.unitParentSelector.getOutsideUnit(unit['parent'][0]);
        }
    }

    saveUnit() {
        this.unitModalError = "";
        let unit: Object = {};
        unit['id'] = this.modalUnitId;
        unit['name'] = this.modalUnitName;
        unit['countEmployees'] = this.modalCountEmployees;

        if (this.modalUnitParent.length != 0) {
            unit['parent'] = [];
            let parent = {};
            parent['id'] = this.modalUnitParent[0]['id'];
            parent['name'] = this.modalUnitParent[0]['name'];
            parent['parent'] = null;
            parent['errors'] = [];
            unit['parent'].push(parent);
        } else {
            unit['parent'] = [];
        }

        unit['officers'] = [];
        unit['managers'] = [];

        for (let officer of this.modalUnitOfficers) {
            let obj = {};
            obj['id'] = officer['id'];
            obj['name'] = officer['name'];
            unit['officers'].push(obj);
        }

        for (let manager of this.modalUnitManagers) {
            let obj = {};
            obj['id'] = manager['id'];
            obj['name'] = manager['name'];
            unit['managers'].push(obj);
        }

        unit['nodes'] = this.modalUnitNodes;
        unit['errors'] = [];

        //Проверка на пустое имя
        if (unit['name'] == "") {
            this.unitModalError = "не заполнено наименование организации";
            return;
        }
        //Проверка существования юнита (api + local)
        this.checkUnitExist(unit['name'], unit['id'], unit['parent'], this.unitsTree)
            .then(() => {
                if (unit['id'] == "") {
                    unit['id'] = UUID.UUID();
                    if (this.modalUnitParent.length > 0) {
                        this.saveUnitQuery(unit)
                            .then(res => {
                                unit['id'] = res['newId'];
                                this.addChildElement(unit, this.modalUnitParent[0]['id'], this.unitsTree);
                                this.unitParentSelector.changeUnitsArray(this.unitsTree);
                                this.unitsTableFilter.updateUnitsSelector();
                                this.updateList();
                                this.unitModal.close();
                            })
                            .catch(() => {
                                this.unitModalError = "при создании организации произошла ошибка";
                            });
                    }
                    else {
                        this.saveUnitQuery(unit)
                            .then(res => {
                                unit['id'] = res['newId'];
                                this.unitsTree.push(unit);
                                this.unitParentSelector.changeUnitsArray(this.unitsTree);
                                this.unitsTableFilter.updateUnitsSelector();
                                this.updateList();
                                this.unitModal.close();
                            })
                            .catch(() => {
                                this.unitModalError = "при создании организации произошла ошибка";
                            });
                    }
                } else {
                    if (this.modalUnitParent.length > 0) {
                        //@todo удаление и перемещение элемента
                        this.editUnitQuery(unit)
                            .then(() => {
                                this.changeUnitValue(unit['id'], unit, this.unitsTree);
                                setTimeout(() => {
                                    let unitObj = JSON.parse(this.removeUnit(unit['id'], this.unitsTree));
                                    unitObj['parent']['id'] = this.modalUnitParent[0]['id'];
                                    unitObj['parent']['name'] = this.modalUnitParent[0]['name'];
                                    this.addChildElement(unitObj, this.modalUnitParent[0]['id'], this.unitsTree);

                                    this.changeParentName(unit['name'], unit['id'], this.unitsTree);

                                    this.unitParentSelector.changeUnitsArray(this.unitsTree);
                                    this.unitsTableFilter.updateUnitsSelector();
                                    this.updateList();

                                    this.unitModal.close();
                                }, 100);
                            })
                            .catch(() => {
                                this.unitModalError = "при редактировании организации произошла ошибка";
                            });

                    }
                    else {
                        this.editUnitQuery(unit)
                            .then(() => {
                                this.changeUnitValue(unit['id'], unit, this.unitsTree);
                                setTimeout(() => {
                                    let unitObj = JSON.parse(this.removeUnit(unit['id'], this.unitsTree));
                                    unitObj['parent'] = [];
                                    this.unitsTree.push(unitObj);

                                    this.changeParentName(unit['name'], unit['id'], this.unitsTree);

                                    this.unitParentSelector.changeUnitsArray(this.unitsTree);
                                    this.unitsTableFilter.updateUnitsSelector();
                                    this.updateList();
                                    this.unitModal.close();
                                }, 100);
                            })
                            .catch(() => {
                                this.unitModalError = "при редактировании организации произошла ошибка";
                            });
                    }
                }
                this.updateList();
            })
            .catch(() => {
                this.unitModalError = "организация с таким наименованием уже существует";
            });
    }

    saveUnitQuery(unit: Object): Promise<any> {
        return new Promise((resolve, reject) => {
            let data = this.prepareUnitToSave(unit);
            let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
            this.core.post_json("api/units", postData)
                .then((res) => {
                    if (res['code'] == 200) {
                        resolve({oldId: unit['id'], newId: res['_embedded']['id']});
                    }
                })
                .catch(() => {
                    reject();
                })
        });
    }

    editUnitQuery(unit: Object) {
        return new Promise((resolve, reject) => {
            let data = this.prepareUnitToSave(unit);
            let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
            this.core.put("api/units/" + data['id'], putData)
                .then((res) => {
                    if (res['code'] == 200) {
                        resolve();
                    }
                })
                .catch(() => {
                    reject();
                })
        });
    }

    delUnitQuery() {
        return new Promise((resolve, reject) => {
            if (this.modalDeleteUnit['countEmployees'] != 0) {
                if (this.employeeMoveOrg.length != 0) {
                    this.orgMoveSelector.resetValue();

                    let data = new Object();
                    data['successor'] = this.employeeMoveOrg[0]['id'];

                    let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
                    this.core.put("api/units/" + this.modalDeleteUnit['id'] + "/delete", putData)
                        .then((res) => {
                            if (res['code'] == 200) {
                                this.moveEmployeesModal.close();
                                this.successorAccept({
                                    "id": this.employeeMoveOrg[0]['id'],
                                    "count": this.modalDeleteUnit['countEmployees'],
                                    "parent": this.modalDeleteUnit['parent']
                                });
                                resolve();
                                this.emptyUnitDeleteModal.close();
                            }
                            if (res['code'] == 1013) {
                                this.modalError = true;
                                this.modalErrorText = "Данная организация используется в одной из задач, ожидающих выполнения (или приостановленных)";
                                reject();
                            }
                        });
                }
                else {
                    this.moveEmployeeError = true;
                }
            }
            else {
                let putData = JSON.parse('{"form": {}}');
                this.core.put("api/units/" + this.modalDeleteUnit['id'] + "/delete", putData)
                    .then((res) => {
                        if (res['code'] == 200) {
                            this.emptyUnitDeleteModal.close();
                            resolve();
                        }

                        if (res['code'] == 1013) {
                            reject();
                            this.modalError = true;
                            this.modalErrorText = "Данная организация используется в одной из задач, ожидающих выполнения (или приостановленных)";
                        }
                    });
            }
        });
    }

    prepareUnitToSave(unit: Object) {
        let data = {};
        data['id'] = unit['id'];
        data['name'] = unit['name'];

        if (unit['parent'].length != 0) {
            data['parent'] = unit['parent'][0]['id'];
        }
        else {
            data['parent'] = null;
        }
        data['users'] = [];
        for (let user of unit['officers']) {
            data['users'].push(user['id']);
        }
        for (let user of unit['managers']) {
            data['users'].push(user['id']);
        }
        return data;
    }

    clearModalValues() {
        this.modalUnitName = "";
        this.modalUnitParent = [];
        this.modalUnitOfficers = [];
        this.modalUnitManagers = [];
    }

    getParentUsers(_unitId: string, _usersArray: Array<Object>, _path: string, unitsTree: Array<Object>, type: string, lastUnitInclude: boolean = false): Array<Object> {
        for (let unit of unitsTree) {
            let unitId = _unitId;
            let usersArray = JSON.parse(JSON.stringify(_usersArray));
            let path = _path.slice(0, _path.length);
            if (unitId == unit['id'] && !lastUnitInclude) {
                return usersArray;
            }
            if (path != "") {
                path += " - " + unit['name'];
            } else {
                path += unit['name'];
            }
            if (type == 'managers') {
                for (let manager of unit['managers']) {
                    usersArray.push({id: manager['id'], name: manager['name'], path: path});
                }
            }
            if (type == 'officers') {
                for (let officer of unit['officers']) {
                    usersArray.push({id: officer['id'], name: officer['name'], path: path});
                }
            }

            if (unitId == unit['id']) {
                return usersArray;
            }

            if (unit['nodes'].length > 0) {
                let res = this.getParentUsers(unitId, usersArray, path, unit['nodes'], type, lastUnitInclude);
                if (res != null) {
                    return res;
                }
            }
        }
        return null;
    }

    addChildElement(addUnit: Object, parentId: string, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['id'] == parentId) {
                unit['nodes'].push(addUnit);
                return;
            }
            if (unit['nodes'].length != 0) {
                this.addChildElement(addUnit, parentId, unit['nodes']);
            }
        }
        return;
    }

    updateList() {
        let filterParams = this.unitsTableFilter.getFilterParams();
        if (filterParams['name'] || filterParams['officers'] || filterParams['managers'] || filterParams['parent']) {
            this.filteredValidItems = [];
            this.filterUnits(this.unitsTableFilter.getFilterParams(), this.unitsTree, true);
        }

        this.pagination.totalElements = this.filteredValidItems.length;
        this.updatePagination(this.pagination);
    }

    updatePagination(pagination: Pagination) {
        let division = pagination.totalElements / pagination.size;
        pagination.totalPages = Math.floor(pagination.totalElements / pagination.size);
        pagination.totalPages += ((division % 1) != 0) ? 1 : 0;
    }

    public filteredValidItems: Array<Object>;

    searchUnits(filterParams: Object) {
        this.pagination.number = 1;
        this.applyFilter(filterParams);
        setTimeout(() => {
            this.updatePagination(this.pagination);
            if (this.tablePagination !== undefined) {
                this.tablePagination.changeParams(this.pagination);
            }
        }, 50);
    }

    applyFilter(filterParams: Object) {
        this.filteredValidItems = [];
        this.filterUnits(filterParams, this.unitsTree, true);

        this.pagination.totalElements = this.filteredValidItems.length;
        this.updatePagination(this.pagination);
        this.updateList();
    }

    filterUnits(filterParams: Object, units: Array<Object>, valid: boolean) {
        if (filterParams['name'] == "" && filterParams['managers'].length == 0
            && filterParams['officers'].length == 0 && filterParams['parent'].length == 0) {
            if (valid) {
                this.filteredValidItems = units;
            }
            return;
        }

        for (let unit of units) {
            let addUnit: boolean = true;

            if (filterParams['name'] != "") {
                if (unit['name'].toString().toLowerCase().indexOf(filterParams['name'].toLowerCase()) == -1) {
                    addUnit = false;
                }
            }

            if (filterParams['parent'].length != 0) {
                if (unit['parent'].length != 0) {
                    if (unit['parent'][0]['id'] != filterParams['parent'][0]['id']) {
                        addUnit = false;
                    }
                } else {
                    addUnit = false;
                }
            }

            if (filterParams['managers'].length != 0) {
                let flag = true;

                for (let item of filterParams['managers']) {
                    if (!this.inArray(item, unit['managers'])) {
                        flag = false;
                    }
                }
                if (flag == false) {
                    addUnit = false;
                }
            }

            if (filterParams['officers'].length != 0) {
                let flag = true;

                for (let item of filterParams['officers']) {
                    if (!this.inArray(item, unit['officers'])) {
                        flag = false;
                    }
                }
                if (flag == false) {
                    addUnit = false;
                }
            }

            if (addUnit) {
                if (valid) {
                    this.filteredValidItems.push(unit);
                }
            }

            if (unit['nodes'].length != 0) {
                this.filterUnits(filterParams, unit['nodes'], valid);
            }
        }
    }

    inArray(findItem: Object, units: Array<Object>): boolean {
        let flag = false;

        for (let item of units) {
            if (item['id'] == findItem['id']) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    //Замена имени родителя у элементов
    changeParentName(name: string, parentId: string, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['parent'].length > 0) {
                if (unit['parent'][0]['id'] == parentId) {
                    unit['parent'][0]['name'] = name;
                }
            }
            if (unit['nodes'].length > 0) {
                this.changeParentName(name, parentId, unit['nodes']);
            }
        }
    }

    changeUnitValue(unitId: string, unitObj: Object, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['id'] == unitId) {
                unit['name'] = unitObj['name'];
                unit['parent'] = JSON.parse(JSON.stringify(unitObj['parent']));
                unit['officers'] = unitObj['officers'];
                unit['managers'] = unitObj['managers'];
                unit['errors'] = [];
                return;
            }
            if (unit['nodes'].length > 0) {
                this.changeUnitValue(unitId, unitObj, unit['nodes']);
            }
        }
    }

    checkUnitExist(checkName: string, unitId: string, parent: any, unitsTree: Array<Object>): Promise<any> {
        return new Promise((resolve, reject) => {
            if (parent.length != 0) {
                for (let unit of unitsTree) {
                    if (unit['id'] == parent[0]['id']) {
                        for (let item of unit['nodes']) {
                            if (item['name'] == checkName && item['id'] != unitId) {
                                reject();
                            }
                        }
                    }
                    if (unit['nodes'].length != 0) {
                        this.checkUnitExist(checkName, unitId, parent, unit['nodes'])
                            .then(() => {
                                resolve();
                            })
                            .catch(() => {
                                reject();
                            });
                    }
                }
                setTimeout(() => {
                    resolve();
                }, 100);
            } else {
                for (let unit of unitsTree) {
                    if (unit['name'] == checkName && unit['id'] != unitId) {
                        reject();
                    }
                }
                setTimeout(() => {
                    resolve();
                }, 100);
            }
        });
    }

    removeUnit(unitId: string, unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if (unit['id'] == unitId) {
                let obj_string = JSON.stringify(unit);
                unitsTree.splice(unitsTree.indexOf(unit), 1);

                this.unitParentSelector.changeUnitsArray(this.unitsTree);
                this.unitsTableFilter.updateUnitsSelector();

                return obj_string;
            }
            if (unit['nodes'].length > 0) {
                let res = this.removeUnit(unitId, unit['nodes']);
                if (typeof res != "undefined") {
                    return res;
                }
            }
        }
    }

    toggleModalManagersShow() {
        this.modalManagersShow = !this.modalManagersShow;
    }

    toggleModalOfficersShow() {
        this.modalOfficersShow = !this.modalOfficersShow;
    }

    parentSelectorChange(selected) {
        this.modalUnitParent = selected;
    }

    officersSelectorChange(selected) {
        this.modalUnitOfficers = selected;
    }

    managersSelectorChange(selected) {
        this.modalUnitManagers = selected;
    }

    changePagination(reload: boolean) {
        this.updatePagination(this.pagination);
    }

    sortUnits(units: Array<Object>) {
        let sortedUnitsArray: Array<Object> = [];
        let index = 0;

        this.sortUnitsTreeByAlphabet(units);

        for (let item of units) {
            if (index < this.pagination.size * this.pagination.number && (index > this.pagination.size * this.pagination.number - this.pagination.size - 1)) {
                sortedUnitsArray.push(item);
            }
            index++;
        }

        return sortedUnitsArray;
    }

    sortUnitsTreeByAlphabet(unitTree: Array<Object>) {
        this.sortArray(unitTree);
        for (let unit of unitTree) {
            if (unit['nodes'].length > 0) {
                this.sortUnitsTreeByAlphabet(unit['nodes']);
            }
        }
    }

    //Сортировка массива по алфавиту
    sortArray(unitArray: Array<Object>) {
        unitArray.sort((a, b) => {
            var textA = a['name'].toUpperCase();
            var textB = b['name'].toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
    }

    public modalError: boolean = false;
    public moveEmployeeModalError: boolean = false;
    public modalErrorText: string = "";
    public modalUnitsTree: Array<Object>;
    public employeeMoveOrg: Array<Object>;
    public modalDeleteUnit: Object;
    public moveEmployeeError: boolean = false;

    orgEntityDelete(unit: Object) {
        this.modalDeleteUnit = unit;
        this.modalError = false;
        this.modalErrorText = "";

        this.modalUnitsTree = [];
        this.employeeMoveOrg = [];
        this.moveEmployeeModalError = false;

        let params = new Object();

        params['filters'] = "EXTRA:'units' NE '" + unit['id'] + "'";

        this.core.get('api/units/tree', params)
            .then((res) => {
                this.modalUnitsTree = res['_embedded'];
                this.orgMoveSelector.changeUnitsArray(this.modalUnitsTree);
                if (this.modalUnitsTree.length == 0) {
                    this.modalError = true;
                    this.modalErrorText = "Нет доступных подразделений для переноса";
                }
            });

        if (unit['countEmployees'] != 0) {
            this.moveEmployeesModal.open();
        }
        else {
            this.emptyUnitDeleteModal.open();
        }
    }

    delUnit() {
        this.delUnitQuery()
            .then(() => {
                let id = this.modalDeleteUnit['id'];
                this.removeUnit(id, this.unitsTree);

                //Обновление пэйджинации
                if (this.modalDeleteUnit['parent'].length == 0) {
                    this.updateList();
                    this.tablePagination.changeParams(this.pagination);
                    this.tablePagination.refreshPagination();
                }
            })
            .catch(() => {});
    }

    employeesMoveOrgChange(value) {
        this.employeeMoveOrg = value;
    }
}
