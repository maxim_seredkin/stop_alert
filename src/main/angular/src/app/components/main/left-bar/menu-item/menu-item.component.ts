/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 20.01.2017.
 */
import {Component, OnInit, Input, Output} from '@angular/core';
import {Router} from "@angular/router";
import {EventEmitter} from "@angular/common/src/facade/async";
import {UserService} from "../../../../services/user.service";

@Component({
    selector: 'menu-item',
    template: require('./menu-item.component.html'),
    inputs: ['linkText', 'childrens']
})
export class MenuItemComponent implements OnInit {
    @Input() linkText: string = "";
    @Input() icon: string;
    @Input() childrens: any;
    @Input() menuHeaderId: string;
    @Input() activeHeader: string;
    @Input() link: string;
    @Input() roles: Array<string>;
    @Input() needLicense: boolean;
    private active: boolean = false;
    //private changeActive: boolean = true;
    private showChildrensAllow: boolean = false;
    private showChildrens: boolean = false;


    @Output() headerClick: EventEmitter<any> = new EventEmitter();

    constructor(
        private router: Router,
        private user: UserService
    ) {
        
    }

    ngOnInit() {
        this.initializeMenu(this.childrens);
    }

    private initializeMenu(childrens) {
        this.showChildrensAllow = (childrens.length > 0) ? true : false;
        
        if(childrens.length != 0) {
            for (var item of childrens)
            {
                if(this.router.isActive(item.link, true)) {
                    this.headerClick.emit(this.menuHeaderId);
                    break;
                }
            }
        }
        else {
            if(this.router.isActive(this.link, true)) {
                this.headerClick.emit(this.menuHeaderId);
            }
        }
    }

    private menuHeaderClick() {
        this.headerClick.emit(this.menuHeaderId);
        // this.active = !this.active;
        // if(this.showChildrensAllow) {this.showChildrens = !this.showChildrens;}
        if(this.childrens.length == 0) {
            this.router.navigate([this.link]);
        }
    }

    private needLicenseCheck(needLicense: boolean): boolean {
        if(!needLicense) {
            return true;
        }

        if(this.user.licenseActive) {
            return true;
        }

        return false;
    }
}
