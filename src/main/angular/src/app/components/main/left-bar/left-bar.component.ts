import {Component, OnInit} from "@angular/core";
import {UserService} from "../../../services/user.service";

@Component({
    selector: 'left-bar',
    template: require('./left-bar.component.html'),
    styleUrls: ['./left-bar.component.scss']
})
export class LeftBarComponent implements OnInit {
    private links;
    public activeHeader:string;

    constructor(private user: UserService) {
        this.links = [
            {
                id: "9",
                linkText: "Главная",
                icon: "browser-icon",
                link: "/main",
                childrens: [],
                roles: [],
                needLicense: false
            },
            {
                id: "3",
                linkText: "Управление задачами",
                icon: "task-icon",
                roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                needLicense: true,
                childrens: [
                    {
                        linkText: "Создание задачи",
                        link: "/tasks/new",
                        roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                        needLicense: true
                    },
                    {
                        linkText: "История задач",
                        link: "/tasks",
                        roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                        needLicense: true
                    }
                ]
            },
            {
                id: "6",
                linkText: "Управление организациями",
                icon: "unit-icon",
                roles: ["ROLE_ADMINISTRATOR"],
                needLicense: true,
                childrens: [
                    {
                        linkText: "Список организаций",
                        link: "/units",
                        roles: ["ROLE_ADMINISTRATOR"],
                        needLicense: true
                    },
                    {
                        linkText: "Загрузка базы",
                        link: "/units/load",
                        roles: ["ROLE_ADMINISTRATOR"],
                        needLicense: true
                    }
                ]
            },
            {
                id: "5",
                linkText: "Управление тегами",
                icon: "tag-icon",
                link: "/employee-tags",
                childrens: [],
                roles: ["ROLE_ADMINISTRATOR"],
                needLicense: true
            },
            {
                id: "4",
                linkText: "Управление сотрудниками",
                icon: "employee-icon",
                roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                needLicense: true,
                childrens: [
                    {
                        linkText: "Создание сотрудника",
                        link: "/employees/new",
                        roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                        needLicense: true
                    },
                    {
                        linkText: "Список сотрудников",
                        link: "/employees",
                        roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                        needLicense: true
                    },
                    {
                        linkText: "Загрузка базы",
                        link: "/employees/load",
                        roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                        needLicense: true
                    }
                ]
            },
            {
                id: "7",
                linkText: "Отчеты проверок",
                icon: "report-icon",
                roles: [],
                needLicense: false,
                childrens: [
                    {
                        linkText: "Отчет по тестированию",
                        link: "/reports/tasks",
                        roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                        needLicense: false
                    },
                    {
                        linkText: "Отчет по подразделению",
                        link: "/reports/units",
                        roles: [],
                        needLicense: false
                    },
                    {
                        linkText: "Отчет по сотруднику",
                        link: "/reports/employees",
                        roles: [],
                        needLicense: false
                    }
                ]
            },
            {
                id: "10",
                linkText: "Управление шаблонами писем",
                icon: "mail-icon",
                roles: ["ROLE_ADMINISTRATOR"],
                needLicense: true,
                childrens: [
                    {
                        linkText: "Создание шаблона",
                        link: "/mail-templates/new",
                        roles: ["ROLE_ADMINISTRATOR", "ROLE_SECURITY_OFFICER"],
                        needLicense: true
                    },
                    {
                        linkText: "Список шаблонов",
                        link: "/mail-templates",
                        roles: [],
                        needLicense: true
                    }
                ]
            },
            {
                id: "1",
                linkText: "Управление пользователями",
                icon: "user-icon",
                roles: ["ROLE_ADMINISTRATOR"],
                needLicense: false,
                childrens: [
                    {
                        linkText: "Создание пользователя",
                        link: "/users/new",
                        roles: [],
                        needLicense: false
                    },
                    {
                        linkText: "Список пользователей",
                        link: "/users",
                        roles: [],
                        needLicense: false
                    }
                ]
            },
            {
                id: "2",
                linkText: "Аккаунт",
                icon: "account-icon",
                link: "/account",
                needLicense: false,
                childrens: [],
                roles: []
            },
            {
                id: "8",
                linkText: "Настройка системы",
                icon: "settings-icon",
                needLicense: false,
                childrens: [
                    {
                        linkText: "Настройка системы",
                        link: "/settings/system-settings",
                        needLicense: false,
                        roles: []
                    },
                    {
                        linkText: "Почтовый сервер",
                        link: "/settings/mail-server",
                        needLicense: false,
                        roles: []
                    },
                    {
                        linkText: "Подключение к LDAP",
                        link: "/settings/ldap-settings",
                        needLicense: false,
                        roles: []
                    },
                    {
                        linkText: "Лицензия",
                        link: "/settings/license",
                        needLicense: false,
                        roles: []
                    }
                ],
                roles: ["ROLE_ADMINISTRATOR"]
            },
        ];
    }

    ngOnInit() {
    }

    public changeActiveHeader(id) {
        this.activeHeader = id;
    }
}
