import {Component, OnInit} from "@angular/core";
import {AppCoreService} from "../../../services/app-core.service";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'nav-bar',
  template: require('./nav-bar.component.html'),
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(private core: AppCoreService, private user: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  logOut() {
    this.user.logOut();
  }
}
