/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 29.03.2017.
 */
import { Component } from '@angular/core';
import {AppCoreService} from "../../../services/app-core.service";


@Component({
    selector: 'mail-server',
    template: require('./mail-server-settings.component.html')
})
export class MailServerSettingsComponent {
    public selectedValue: string = 'val1';
    public selectedValues: string[] = [];

    public connectionStatus: boolean = false;

    public settings: Object;
    public settingsCopied: Object;

    public formError: boolean = false;
    public formErrorText: string = "";
    public formInfoText: string = "";

    public addressError: boolean = false;
    
    public checkBoxState: Array<any> = [];
    public checkBoxAnonymousState: Array<any> = [];

    public loading: boolean = false;

    constructor(
        private core: AppCoreService
    ) {
        this.getConnectionSettings();
    }

    getConnectionSettings() {
        this.core.get("api/settings/server")
            .then((res) => {
                this.settings = res['_embedded'];
                this.settingsCopied = JSON.parse(JSON.stringify(this.settings));
                if(this.settings['useSSL']) {
                    this.checkBoxState.push("useSSL");
                }
                if(this.settings['anonymous']) {
                    this.checkBoxAnonymousState.push("useAnonymous");
                }
                this.checkConnection(this.settings);
            });
    }

    checkConnection(settings: Object) {
        this.formError = false;
        this.addressError = false;
        this.formErrorText = "";
        this.formInfoText = "";

        if(this.checkBoxState[0] == "useSSL") {
            this.settings['useSSL'] = true;
        }
        else {
            this.settings['useSSL'] = false;
        }

        if(this.checkBoxAnonymousState[0] == "useAnonymous") {
            this.settings['anonymous'] = true;
        }
        else {
            this.settings['anonymous'] = false;
        }
        
        this.loading = true;
        let putData = JSON.parse('{"form": ' + JSON.stringify(settings) + '}');

        this.core.put("api/settings/server/verify", putData)
            .then((res) => {
                this.connectionStatus = res['_embedded'];
                this.loading = false;
            })
            .catch(() => {
                this.connectionStatus = false;
                this.loading = false;
            });
    }
    
    saveSettings(): void {
        this.formError = false;
        this.addressError = false;
        this.formErrorText = "";
        this.formInfoText = "";

        if(this.checkBoxState[0] == "useSSL") {
            this.settings['useSSL'] = true;
        }
        else {
            this.settings['useSSL'] = false;
        }

        if(this.checkBoxAnonymousState[0] == "useAnonymous") {
            this.settings['anonymous'] = true;
        }
        else {
            this.settings['anonymous'] = false;
        }

        if(this.isEqual(this.settings, this.settingsCopied)) {
            return;
        }

        if(!this.isValid(this.settings)) {
            this.formError = true;
            this.formErrorText = "Не все поля заполнены!";
            return;
        }

        if(!this.validPort(this.settings['port'])) {
            this.formError = true;
            this.addressError = true;
            this.formErrorText = "Некорректный адрес подключения!";
            return;
        }

        let putData = JSON.parse('{"form": ' + JSON.stringify(this.settings) + '}');

        this.core.put("api/settings/server", putData)
            .then(() => {
                this.settingsCopied = JSON.parse(JSON.stringify(this.settings));
                this.formInfoText = "Настройки сохранены";
                this.checkConnection(this.settings);
            })
            .catch(() => {
                this.formError = true;
                this.formErrorText = "При выполнении запроса произошла ошибка";
            });
    }

    isValid(settings: Object) {
        return (
            (settings['host'] != "") &&
            (settings['port'] != "")
        );
    }

    validPort(port: number) {
        return ((port >= 0) && (port <= 65535));
    }
    
    isEqual(settings: Object, settingsCopied: Object) {
        return (
            (settings['host'] == settingsCopied['host']) &&
            (settings['port'] == settingsCopied['port']) &&
            (settings['username'] == settingsCopied['username']) &&
            (settings['password'] == settingsCopied['password']) &&
            (settings['useSSL'] == settingsCopied['useSSL']) &&
            (settings['anonymous'] == settingsCopied['anonymous'])
        );
    }

    checkConnect() {
        this.checkConnection(this.settings);
    }
}