/**
 * Created by Baser on 27.03.2017.
 */
import { Component, ViewChild } from '@angular/core';
import { DatePickerSettingsService } from "../../../../../services/datePickerSettings";
import { Pagination } from "../../../../../models/pagination";
import { AppCoreService } from "../../../../../services/app-core.service";
import { MailTemplateContext } from "../../../../../models/template/template-context";
import { Modal } from "ngx-modal/index";

@Component({
    selector: 'task-reports-list',
    template: require('./task-reports-list.component.html')
})

export class TaskReportsListComponent {
    public selectedDateFirst: Date = null;
    public selectedDateSecond: Date = null;
    public pagination: Pagination;
    public reports: Array<Object>;

    public templatesItems: Array<Object>;
    public templatesSelectorValue: Array<Object> = [{ id: "ALL" }];

    public showAdvFilter: boolean = false;
    public filteredTaskName: string = "";
    public filteredAuthorFIO: string = "";

    constructor(private datePicker: DatePickerSettingsService,
        private core: AppCoreService,
        private templateContext: MailTemplateContext) {
        this.pagination = new Pagination();
        this.getMailTemplates();
        let params = this.getRequestParams();
        this.getReportsList(params);
    }

    applyFilter() {
        this.pagination.number = 1;
        let params = this.getRequestParams();
        this.getReportsList(params);
    }

    changePagination() {
        let params = this.getRequestParams();
        this.getReportsList(params);
    }

    getReportsList(params) {
        this.core.get("api/reports/tasks", params)
            .then((res) => {
                this.reports = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    getRequestParams(): Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;

        let second: Date;

        params['searchs'] = "";

        if (this.filteredTaskName != "") {
            params['searchs'] = "'name':'" + this.filteredTaskName + "'";
        }

        if (this.selectedDateSecond) {
            second = new Date(this.selectedDateSecond.getTime());
        }

        params['filters'] = "";

        if (this.filteredAuthorFIO) {
            params['filters'] += `${params['filters'] ? ',' : ''}EXTRA:'author' LIKE '${this.filteredAuthorFIO}'`;
        }

        if (this.selectedDateFirst) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' GE '${this.selectedDateFirst.getTime()}'`;
        }

        if (this.selectedDateSecond) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' LT '${second.setDate(second.getDate() + 1)}'`;
        }

        if (this.templatesSelectorValue[0]['id'] !== 'ALL') {
            params['filters'] += `${params['filters'] ? ',' : ''}'template.id' EQ '${this.templatesSelectorValue[0]['id']}'`;
        }

        params['sorts'] = ['startDate:DESC', 'endDate:DESC'];
        return params;
    }

    getMailTemplates() {
        this.templateContext.getMailTemplateList()
            .then((data) => {
                let mailTemplates = data;
                this.templatesItems = this.getTemplatesItems(mailTemplates);
            });
    }

    public getTemplatesItems(templatesList: Array<Object>) {
        var items = [];
        for (var item of templatesList) {
            items.push({ "id": item['id'], "text": item['name'] });
        }
        items.unshift({ 'id': "ALL", "text": "Все шаблоны" });
        return items;
    }

    public refreshTemplateValue(value: any): void {
        if (value.length == 0) {
            this.templatesSelectorValue = [{ id: "" }];
        }
        else {
            this.templatesSelectorValue = [value];
        }
    }

    public templateName: string = "";
    public templateHtml: string = "";
    @ViewChild('mailTemplateShowModal')
    private mailTemplateShowModal: Modal;

    templatePreviewLoad(templateId: string) {
        let id: string = templateId;

        if (id != '') {
            this.core.get("api/mail-templates/" + id + "/example")
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }

    toggleShowFilter() {
        if (this.showAdvFilter) {
            this.templatesSelectorValue = [{ id: "ALL" }];
            this.selectedDateFirst = null;
            this.selectedDateSecond = null;
            this.filteredAuthorFIO = "";
        }
        this.showAdvFilter = !this.showAdvFilter;
    }
}
