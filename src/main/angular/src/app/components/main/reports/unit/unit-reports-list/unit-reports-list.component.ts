/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 30.03.2017.
 */
import {Component} from '@angular/core';
import {Pagination} from "../../../../../models/pagination";
import {AppCoreService} from "../../../../../services/app-core.service";
import {UserService} from "../../../../../services/user.service";

@Component({
    selector: 'unit-reports-list',
    template: require('./unit-reports-list.component.html')
})

export class UnitReportsListComponent {
    public pagination:Pagination;
    public reports:Array<Object>;

    public unitsTree: Array<Object>;
    public managers: Array<Object>;
    public officers: Array<Object>;

    private selectedOrgs:Array<Object>;
    private selectedManagers: Array<Object>;
    private selectedOfficers: Array<Object>;

    public showAdvFilter:boolean = false;

    constructor(
                private core:AppCoreService,
                private user: UserService) {
        this.pagination = new Pagination();
        let params = this.getRequestParams();
        this.getReportsList(params);
        this.getUnitsTree();

        if(this.user.getAuthority() == 'ROLE_ADMINISTRATOR') {
            this.getManagers();
            this.getOfficers();
        }
    }

    orgSelectorChange(selected) {
        this.selectedOrgs = selected;
    }

    officerSelectorChange(selected) {
        this.selectedOfficers = selected;
    }

    managerSelectorChange(selected) {
        this.selectedManagers = selected;
    }

    applyFilter() {
        this.pagination.number = 1;
        let params = this.getRequestParams();
        this.getReportsList(params);
    }


    getReportsList(params) {
        this.core.get("api/reports/units", params)
            .then((res) => {
                this.reports = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    getRequestParams():Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;

        params['filters'] = "";

        if (this.selectedOrgs) {
            this.selectedOrgs.forEach(unit => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'units' EQ '${unit['id']}'`;
            });
        }

        if (this.selectedManagers) {
            this.selectedManagers.forEach(manager => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'managers' EQ '${manager['id']}'`;
            });
        }

        if (this.selectedOfficers) {
            this.selectedOfficers.forEach(officer => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'officers' EQ '${officer['id']}'`;
            });
        }
        
        params['sorts'] = ['name:ASC'];
        return params;
    }

    changePagination(event:Pagination) {
        let params = this.getRequestParams();
        this.getReportsList(params);
    }

    getManagers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_MANAGER'";

        this.core.get('api/users', params)
            .then((res) => {
                this.managers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    getOfficers() {
        let params = new Object();
        params['filters'] = "'authority.authority' EQ 'ROLE_SECURITY_OFFICER'";

        this.core.get('api/users', params)
            .then((res) => {
                this.officers = this.usersToSelectorItem(res['_embedded']);
            });
    }

    getUnitsTree() {
        this.core.get('api/units/tree')
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });

    }

    usersToSelectorItem(users: Array<Object>) {
        let result: Array<Object> = [];

        for(let item of users) {
            let tmp_user = new Object();
            tmp_user['id'] = item['id'];
            tmp_user['name'] = item['shortName'] + " (" + item['username'] + ")";
            tmp_user['nodes'] = [];
            tmp_user['parent'] = null;
            tmp_user['used'] = true;
            result.push(tmp_user);
        }
        return result;
    }

    toggleShowFilter() {
        if(this.showAdvFilter) {
            this.selectedManagers = [];
            this.selectedOfficers = [];
        }
        this.showAdvFilter = !this.showAdvFilter;
    }
}