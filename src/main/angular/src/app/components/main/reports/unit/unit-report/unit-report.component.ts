/**
 * Created by Baser on 30.03.2017.
 */
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Pagination } from "../../../../../models/pagination";
import { DatePickerSettingsService } from "../../../../../services/datePickerSettings";
import { ActivatedRoute } from "@angular/router";
import { MailTemplateContext } from "../../../../../models/template/template-context";
import { AppCoreService } from "../../../../../services/app-core.service";
import { UserService } from "../../../../../services/user.service";
import { Modal } from "ngx-modal/index";
import { UnitsTreeSelectorComponent } from "../../../units-tree-selector/units-tree-selector.component";
import { TreeSelectorComponent } from "../../../tree-selector/tree-selector.controller";

@Component({
    selector: 'unit-reports',
    template: require('./unit-report.component.html')
})

export class UnitReportComponent {
    private pagination: Pagination;
    private paginationEmployee: Pagination;
    public selectedDateFirst: Date = null;
    public selectedDateSecond: Date = null;

    public tableSelectedDateFirst: Date = null;
    public tableSelectedDateSecond: Date = null;

    public reportEntity: Object;

    public templatesItems: Array<Object>;
    public templatesSelectorValue: Array<Object> = [{ id: "ALL" }];

    public unitsTree: Array<Object>;
    public tagsTree: Array<Object>;

    private selectedOrgs: Array<Object> = [];

    private selectedTags: Array<Object> = [];

    @ViewChild('mailTemplateShowModal')
    private mailTemplateShowModal: Modal;

    public showAdvFilter: boolean = false;
    public showAdvFilterTask: boolean = false;
    public filteredEmail: string = "";
    public filteredFullName: string = "";

    public filteredTaskName: string = "";
    public filteredAuthorFIO: string = "";

    public unitReportTasks: Array<Object>;
    public unitReportEmployees: Array<Object>;

    public diagramDataFromApi: Array<Object> = [];

    @ViewChild('orgSelector') orgSelector: UnitsTreeSelectorComponent;
    @ViewChild('tagSelector') tagSelector: TreeSelectorComponent;

    constructor(private datePicker: DatePickerSettingsService,
        private route: ActivatedRoute,
        private templateContext: MailTemplateContext,
        private core: AppCoreService,
        private user: UserService,
        private domElement: ElementRef) {
        this.pagination = new Pagination();
        this.paginationEmployee = new Pagination();

        this.getReportInfo(this.route.snapshot.params['id']);
        this.getMailTemplates();
        this.getUnitsTree();
        this.getTagsTree();
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.getReportInfo(params['id']);
        });
    }

    getReportInfo(id: string) {
        this.getUnitReport(id)
            .then(() => {
                this.getUnitReportTasks(id)
                    .then(res => {
                        this.unitReportTasks = res['_embedded'];
                        this.pagination.totalElements = res['page']['totalElements'];
                        this.pagination.totalPages = res['page']['totalPages'];
                    });
                this.getUnitReportEmployees(id)
                    .then(res => {
                        this.unitReportEmployees = res['_embedded'];
                        this.paginationEmployee.totalElements = res['page']['totalElements'];
                        this.paginationEmployee.totalPages = res['page']['totalPages'];
                    });
            });
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    orgSelectorChange(selected) {
        this.selectedOrgs = selected;
    }

    tagSelectorChange(selected) {
        this.selectedTags = selected;
    }

    applyFilterEmployees() {
        this.pagination.number = 1;
        this.getUnitReportEmployees(this.route.snapshot.params['id'])
            .then(res => {
                this.unitReportEmployees = res['_embedded'];
                this.paginationEmployee.totalElements = res['page']['totalElements'];
                this.paginationEmployee.totalPages = res['page']['totalPages'];
            });
    }

    getUnitReport(id: string) {
        return new Promise((resolve, reject) => {
            this.core.get("api/reports/units/" + id)
                .then((res) => {
                    this.reportEntity = res['_embedded'];
                    this.rebuildDiagram();
                    resolve();
                })
                .catch(error => reject());
        });
    }

    getUnitReportTasks(id, filterParams?) {
        return new Promise((resolve, reject) => {
            let params = (filterParams) ? filterParams : this.getTasksRequestParams();
            this.core.get(`api/reports/units/${id}/tasks`, params)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }

    getUnitReportEmployees(id, filterParams?) {
        return new Promise((resolve, reject) => {
            let params = (filterParams) ? filterParams : this.getEmployeesRequestParams();
            this.core.get(`api/reports/units/${id}/employees`, params)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }

    getTasksRequestParams(): Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;
        params['searchs'] = "";

        if (this.filteredTaskName != "") {
            params['searchs'] = "'name':'" + this.filteredTaskName + "'";
        }

        let second: Date;

        if (this.tableSelectedDateSecond) {
            second = new Date(this.tableSelectedDateSecond.getTime());
        }

        params['filters'] = "";

        if (this.filteredAuthorFIO) {
            params['filters'] += `${params['filters'] ? ',' : ''}EXTRA:'author' LIKE '${this.filteredAuthorFIO}'`;
        }

        if (this.tableSelectedDateFirst) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' GE '${this.tableSelectedDateFirst.getTime()}'`;
        }

        if (this.tableSelectedDateSecond) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' LT '${second.setDate(second.getDate() + 1)}'`;
        }

        if (this.templatesSelectorValue[0]['id'] !== 'ALL') {
            params['filters'] += `${params['filters'] ? ',' : ''}'template.id' EQ '${this.templatesSelectorValue[0]['id']}'`;
        }

        params['sorts'] = ['name:ASC'];
        return params;
    }

    getEmployeesRequestParams(): Object {
        let params = new Object();

        params['page'] = this.paginationEmployee.number;
        params['size'] = this.paginationEmployee.size;
        params['searchs'] = "";

        if (this.filteredEmail) {
            params['searchs'] = "'email':'" + this.filteredEmail + "'";
        }

        params['filters'] = "";

        if (this.filteredFullName) {
            params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'fullName' LIKE '${this.filteredFullName}'`;
        }

        if (this.selectedOrgs) {
            this.selectedOrgs.forEach(unit => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'units' EQ '${unit['id']}'`;
            });
        }

        if (this.selectedTags) {
            this.selectedTags.forEach(tag => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'employeeTags' EQ '${tag['id']}'`;
            });
        }

        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        return params;
    }

    changePagination(event) {
        this.getUnitReportTasks(this.route.snapshot.params['id'])
            .then(res => {
                this.unitReportTasks = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    changePaginationEmployee(event) {
        this.getUnitReportEmployees(this.route.snapshot.params['id'])
            .then(res => {
                this.unitReportEmployees = res['_embedded'];
                this.paginationEmployee.totalElements = res['page']['totalElements'];
                this.paginationEmployee.totalPages = res['page']['totalPages'];
            });
    }

    public barChartLabels: string[] = ['20.01.17', '10.02.17', '14.03.17', '14.04.17', '15.05.17', '15.06.17', '15.07.17'];

    public barChartData: any[] = [
        { data: [], label: 'Уязвимые сотрудники' },
        { data: [], label: 'Компетентные сотрудники' }
    ];

    rebuildDiagram(limit?: number) {
        let second: Date;

        if (this.selectedDateSecond) {
            second = new Date(this.selectedDateSecond.getTime());
        }

        let params = {};
        params['filters'] = "";

        if (this.selectedDateFirst) {
            params['filters'] += `${(params['filters']) ? ',' : ''}'startDate' GE '${this.selectedDateFirst.getTime()}'`;
        }

        if (this.selectedDateSecond) {
            params['filters'] += `${(params['filters']) ? ',' : ''}'startDate' LT '${second.setDate(second.getDate() + 1)}'`;
        }

        this.getUnitReportTasks(this.route.snapshot.params['id'], params)
            .then(res => {
                this.barChartData[0]['data'] = [];
                this.barChartData[1]['data'] = [];
                this.barChartLabels = [];

                this.diagramDataFromApi = res['_embedded'];

                let diagramElement = this.domElement.nativeElement.querySelector('#unitReportTasksDiagram')
                let limit = this.calculateDiagramColumnNumber(diagramElement.clientWidth);

                while (this.barChartLabels.length < (limit - this.diagramDataFromApi.length)) {
                    this.barChartData[0]['data'].push(0);
                    this.barChartData[1]['data'].push(0);
                    this.barChartLabels.push('');
                }

                for (let task of this.diagramDataFromApi) {
                    this.barChartData[0]['data'].push(task['taskResult']['badPercent']);
                    this.barChartData[1]['data'].push(task['taskResult']['goodPercent']);
                    this.barChartLabels.push(task['name']);

                    if (this.barChartLabels.length === limit) {
                        break;
                    }
                }
                /** Для того чтобы в тултипе в диаграмме выводились правильные значения */
                this.diagramDataFromApi.reverse();
            });
    }

    calculateDiagramColumnNumber(disagramWidth: number): number {
        const COLUMN_WIDTH_PX = 20;
        return Math.floor(disagramWidth / COLUMN_WIDTH_PX);
    }

    filterTaskData(firstDate, secondDate) {
        let temp: Array<Object> = [];

        let second: Date;

        if (secondDate != null) {
            second = new Date(secondDate.getTime());
            second.setDate(second.getDate() + 1);
        }

        for (let task of this.reportEntity['resultByTask']) {
            if (secondDate == null && firstDate != null) {
                if (task['task']['task']['startDate'] >= firstDate) {
                    temp.push(task);
                }
            }
            if (firstDate == null && secondDate != null) {
                if (task['task']['task']['startDate'] < second) {
                    temp.push(task);
                }
            }
            if (firstDate != null && secondDate != null) {
                if (task['task']['task']['startDate'] >= firstDate && task['task']['task']['startDate'] < second) {
                    temp.push(task);
                }
            }
            if (firstDate == null && secondDate == null) {
                temp.push(task);
            }
        }

        return temp;
    }

    sortTask(items: Array<Object>, pagination?: Pagination): Array<Object> {
        let sortedItems = [];
        for (let item of items) {
            if (this.itemDateEqual(this.tableSelectedDateFirst, this.tableSelectedDateSecond, item) &&
                this.checkTemplateValue(item) &&
                item['task']['task']['name'].toUpperCase().indexOf(this.filteredTaskName.toUpperCase()) !== -1 &&
                item['task']['task']['shortName'].toUpperCase().indexOf(this.filteredAuthorFIO.toUpperCase()) !== -1) {
                sortedItems.push(item);
            }
        }
        return sortedItems;
    }

    checkTemplateValue(item) {
        let flag = false;
        if (item['task']['task']['template']['id'] == this.templatesSelectorValue[0]['id']) {
            flag = true;
        }
        if (this.templatesSelectorValue[0]['id'] == 'ALL') {
            flag = true;
        }
        return flag;
    }

    itemDateEqual(firstDate, secondDate, item) {
        let flag = false;

        let second: Date;

        if (secondDate != null) {
            second = new Date(secondDate.getTime());
            second.setDate(second.getDate() + 1);
        }

        if (secondDate == null && firstDate != null) {
            if (item['task']['task']['startDate'] >= firstDate) {
                flag = true;
            }
        }
        if (firstDate == null && secondDate != null) {
            if (item['task']['task']['startDate'] < second) {
                flag = true;
            }
        }
        if (firstDate != null && secondDate != null) {
            if (item['task']['task']['startDate'] >= firstDate && item['task']['task']['startDate'] < second) {
                flag = true;
            }
        }
        if (firstDate == null && secondDate == null) {
            flag = true;
        }
        return flag
    }

    getMailTemplates() {
        this.templateContext.getMailTemplateList()
            .then((data) => {
                let mailTemplates = data;
                this.templatesItems = this.getTemplatesItems(mailTemplates);
            });
    }

    public getTemplatesItems(templatesList: Array<Object>) {
        var items = [];
        for (var item of templatesList) {
            items.push({ "id": item['id'], "text": item['name'] });
        }
        items.unshift({ 'id': "ALL", "text": "Все шаблоны" });
        return items;
    }

    public refreshTemplateValue(value: any): void {
        if (value.length == 0) {
            this.templatesSelectorValue = [{ id: "" }];
        }
        else {
            this.templatesSelectorValue = [value];
        }
    }

    applyFilter() {
        this.pagination.number = 1;
        this.getUnitReportTasks(this.route.snapshot.params['id'])
            .then(res => {
                this.unitReportTasks = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    getStatusName(status) {
        let statusName = "";
        switch (status) {
            case "ENABLED":
                statusName = "активное";
                break;
            case "DELETED":
                statusName = "удален";
                break;
        }
        return statusName;
    }

    public barColors: Array<any> = [
        {
            backgroundColor: '#d94b44',
            hoverBackgroundColor: '#d94b44'
        },
        {
            backgroundColor: '#53cf9c',
            hoverBackgroundColor: '#53cf9c'
        }
    ];

    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        maintainAspectRatio: false,
        scales:
        {
            xAxes: [{
                scaleShowLabels: false,
                ticks: {
                    display: false
                }
            }],
            yAxes: [{
                stacked: true,
                scaleShowLabels: false,
                ticks: {
                    max: 100
                }
            }]
        },
        tooltips: {
            callbacks: {
                label: (tooltipItem, data) => {
                    let label = this.barChartData[tooltipItem.datasetIndex]['label'];
                    let count = this.diagramDataFromApi[data['labels'].length - tooltipItem.index - 1]['taskResult'][(tooltipItem.datasetIndex) ? 'goodCount' : 'badCount'];
                    let percent = this.barChartData[tooltipItem.datasetIndex]['data'][tooltipItem.index];
                    let tooltip_string = `${label} - ${count} (${percent}%)`;
                    return tooltip_string;
                }
            }
        }
    };

    public templateName: string = "";
    public templateHtml: string = "";

    templatePreviewLoad(templateId: string) {
        let id: string = templateId;

        if (id != '') {
            this.core.get("api/mail-templates/" + id + "/example")
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }

    toggleShowFilter() {
        if (this.showAdvFilter) {
            this.filteredEmail = "";
            this.orgSelector.resetValue();
            this.tagSelector.resetValue();
            this.selectedOrgs = [];
            this.selectedTags = [];
        }
        this.showAdvFilter = !this.showAdvFilter;
    }

    toggleShowFilterTask() {
        if (this.showAdvFilterTask) {
            this.templatesSelectorValue = [{ id: "ALL" }];
            this.tableSelectedDateFirst = null;
            this.tableSelectedDateSecond = null;
            this.filteredAuthorFIO = "";
        }
        this.showAdvFilterTask = !this.showAdvFilterTask;
    }
}