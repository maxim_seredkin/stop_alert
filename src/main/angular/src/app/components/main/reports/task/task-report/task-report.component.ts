/**
 * Created by Baser on 20.03.2017.
 */
import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { Pagination } from "../../../../../models/pagination";
import { ActivatedRoute } from "@angular/router";
import { AppCoreService } from "../../../../../services/app-core.service";
import { Unit } from "../../../../../models/unit";
import { Tag } from "../../../../../models/tag/tag";
import { Modal } from "ngx-modal/index";
import { UserService } from "../../../../../services/user.service";
import { Headers, Http, ResponseContentType, Response } from "@angular/http";
import * as FileSaver from 'file-saver';
import { Config } from "../../../../../services/config";
import { UnitsTreeSelectorComponent } from "../../../units-tree-selector/units-tree-selector.component";
import { TreeSelectorComponent } from "../../../tree-selector/tree-selector.controller";

@Component({
    selector: 'task-reports',
    template: require('./task-report.component.html')
})

export class TaskReportComponent {
    private pagination: Pagination;
    public unitsTree: Array<Object>;
    public tagsTree: Array<Object>;
    public reportEntity: Object;

    private selectedOrgs: Array<Unit> = [];
    private selectedTags: Array<Tag> = [];

    public testingDateToString: string;

    public filterResultValue: Array<Object> = [{ "id": "ALL", "text": "Все записи" }];

    @ViewChild('mailTemplateShowModal')
    private mailTemplateShowModal: Modal;

    @ViewChild('orgSelector') orgSelector: UnitsTreeSelectorComponent;
    @ViewChild('tagSelector') tagSelector: TreeSelectorComponent;

    public reportEmployees: Array<Object>;

    constructor(
        private route: ActivatedRoute,
        private core: AppCoreService,
        private user: UserService,
        private http: Http,
        private config: Config,
        private cdRef: ChangeDetectorRef
    ) {
        this.pagination = new Pagination();
        this.getUnitsTree();
        this.getTagsTree();
        this.getTaskReport(this.route.snapshot.params['id'])
            .then(res => {
                this.getTaskReportEmployees();
            });
    }

    public pieChartData;
    public colors = [
        "#d94b44",
        "#45dba3"
    ];

    public pieColors: Array<any> = [{
        backgroundColor: ['#53cf9c', '#d94b44'],
        hoverBackgroundColor: ['#53cf9c', '#d94b44']
    }];

    public pieOptions = {
        tooltips: {
            enabled: false
        }
    };

    getTaskReport(id: string) {
        return new Promise((resolve, reject) => {
            this.core.get("api/reports/tasks/" + id)
                .then((res) => {
                    this.reportEntity = res['_embedded'];

                    this.testingDateToString = new Date(this.reportEntity['startDate']).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' }) +
                        ' - ' + new Date(this.reportEntity['endDate']).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });

                    this.pieChartData = [{
                        data: [
                            this.reportEntity['taskResult']['goodCount'],
                            this.reportEntity['taskResult']['badCount']
                        ]
                    }];
                    resolve();
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    getTaskReportEmployees() {
        return new Promise((resolve, reject) => {
            this.core.get(`api/reports/tasks/${this.route.snapshot.params['id']}/employees`, this.getRequestParams())
                .then((res) => {
                    this.reportEmployees = res['_embedded'];
                    this.pagination.totalElements = res['page']['totalElements'];
                    this.pagination.totalPages = res['page']['totalPages'];
                    resolve();
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    getRequestParams(): Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;
        params['searchs'] = "";

        if (this.filteredEmail) {
            params['searchs'] = "'email':'" + this.filteredEmail + "'";
        }

        params['filters'] = "";

        if (this.filteredFullName) {
            params['filters'] += (params['filters']) ? ',' : '' + `EXTRA:'fullName' LIKE '${this.filteredFullName}'`;
        }

        if (this.selectedOrgs) {
            this.selectedOrgs.forEach(unit => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'units' EQ '${unit['id']}'`;
            });
        }

        if (this.selectedTags) {
            this.selectedTags.forEach(tag => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'employeeTags' EQ '${tag['id']}'`;
            });
        }

        if (this.filterResultValue[0]['id'] !== 'ALL') {
            let operator = (this.filterResultValue[0]['id'] === 'SUCCESS') ? 'NE' : 'EQ';
            params['filters'] += `${params['filters'] ? ',' : ''}EXTRA:'result' ${operator} '${this.route.snapshot.params['id']}'`;
        }

        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        return params;
    }

    applyFilter() {
        this.pagination.number = 1;
        this.getTaskReportEmployees();
    }

    changePagination(event) {
        this.getTaskReportEmployees();
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    statusToString(status) {
        let res = "";
        switch (status) {
            case "WAITING":
                res = "в ожидании";
                break;
            case "SUSPENDED":
                res = "приостановлено";
                break;
            case "EXECUTION":
                res = "выполняется";
                break;
            case "COMPLETED":
                res = "завершено";
                break;
        }
        return res;
    }

    orgSelectorChange(selected) {
        this.selectedOrgs = selected;
    }

    tagSelectorChange(selected) {
        this.selectedTags = selected;
    }

    public templateName: string = "";
    public templateHtml: string = "";

    templatePreviewLoad(templateId: string) {
        if (templateId) {
            this.core.get(`api/mail-templates/${templateId}/example`)
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }

    getReportFile() {
        let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';

        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + token_header);
        let responseType = ResponseContentType.ArrayBuffer;

        this.http.get(this.config.get('backendUrl') + `api/reports/tasks/${this.route.snapshot.params['id']}/document`, { headers: headers, responseType: responseType })
            .subscribe((data: Response) => {
                var blob = new Blob([data.arrayBuffer()], {
                    type: 'application/octet-stream'
                });
                FileSaver.saveAs(blob, "Официальный отчет.docx");
            }),
            error => console.log("Error downloading the file."),
            () => console.info("OK");
    }

    public showAdvFilter: boolean = false;
    public filteredEmail: string = "";
    public filteredFullName: string = "";

    toggleShowFilter() {
        if (this.showAdvFilter) {
            this.filteredEmail = "";
            this.orgSelector.resetValue();
            this.tagSelector.resetValue();
            this.selectedOrgs = [];
            this.selectedTags = [];
            this.filterResultValue = [{ "id": "ALL", "text": "Все" }];
        }
        this.showAdvFilter = !this.showAdvFilter;
        this.cdRef.detectChanges();
    }

    public resultItems: Array<Object> = [
        { "id": "ALL", "text": "Все" },
        { "id": "FAIL", "text": "Перешел" },
        { "id": "SUCCESS", "text": "Не перешел" }
    ];

    public refreshFilterResultValue(value: any): void {
        this.filterResultValue = [value];
    }
}