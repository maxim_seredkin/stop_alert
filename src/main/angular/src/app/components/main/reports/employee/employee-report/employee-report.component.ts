/**
 * Created by Baser on 30.03.2017.
 */
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Pagination } from "../../../../../models/pagination";
import { DatePickerSettingsService } from "../../../../../services/datePickerSettings";
import { ActivatedRoute } from "@angular/router";
import { MailTemplateContext } from "../../../../../models/template/template-context";
import { AppCoreService } from "../../../../../services/app-core.service";
import { UserService } from "../../../../../services/user.service";
import { Modal } from "ngx-modal/index";

@Component({
    selector: 'employee-reports',
    template: require('./employee-report.component.html')
})

export class EmployeeReportComponent {
    private pagination: Pagination;
    public selectedDateFirst: Date = null;
    public selectedDateSecond: Date = null;

    public tableSelectedDateFirst: Date = null;
    public tableSelectedDateSecond: Date = null;

    public reportEntity: Object;

    public templatesItems: Array<Object>;
    public templatesSelectorValue: Array<Object> = [{ id: "ALL" }];

    @ViewChild('mailTemplateShowModal')
    private mailTemplateShowModal: Modal;

    public showAdvFilter: boolean = false;
    public filteredTaskName: string = "";
    public filteredAuthorFIO: string = "";

    public employeeReportTasks: Array<Object> = [];

    constructor(private datePicker: DatePickerSettingsService,
        private route: ActivatedRoute,
        private templateContext: MailTemplateContext,
        private core: AppCoreService,
        private user: UserService,
        private domElement: ElementRef) {
        this.pagination = new Pagination();
        this.getMailTemplates();
        this.getTaskReport(this.route.snapshot.params['id'])
            .then(res => this.getEmployeeReportTasks()
                .then(res => {
                    this.employeeReportTasks = res['_embedded'];
                    this.pagination.totalElements = res['page']['totalElements'];
                    this.pagination.totalPages = res['page']['totalPages'];
                })
            );
    }

    public barChartLabels: string[] = ['20.01.17', '10.02.17', '14.03.17', '14.04.17', '15.05.17', '15.06.17', '15.07.17'];

    public barChartData: any[] = [
        { data: [100, 100, 0, 0, 100, 0, 0], label: 'Перешел по ссылке' },
        { data: [0, 0, 100, 100, 0, 100, 100], label: 'Не перешел по ссылке' }
    ];

    applyFilter() {
        this.pagination.number = 1;
        this.getEmployeeReportTasks()
            .then(res => {
                this.employeeReportTasks = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    getTaskReport(id: string) {
        return new Promise((resolve, reject) => {
            this.core.get("api/reports/employees/" + id)
                .then((res) => {
                    this.reportEntity = res['_embedded'];
                    this.rebuildDiagram();
                    resolve();
                })
                .catch(error => reject(error));
        });
    }

    getEmployeeReportTasks(requestParams?) {
        let params = (requestParams) ? requestParams : this.getRequestParams();
        return new Promise((resolve, reject) => {
            this.core.get(`api/reports/employees/${this.route.snapshot.params['id']}/tasks`, params)
                .then(res => { resolve(res) })
                .catch(error => reject(error));
        });
    }

    getRequestParams(): Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;
        params['searchs'] = "";

        let second: Date;

        params['searchs'] = "";

        if (this.filteredTaskName != "") {
            params['searchs'] = "'name':'" + this.filteredTaskName + "'";
        }

        if (this.tableSelectedDateSecond) {
            second = new Date(this.tableSelectedDateSecond.getTime());
        }

        params['filters'] = "";

        if (this.filteredAuthorFIO) {
            params['filters'] += `${params['filters'] ? ',' : ''}EXTRA:'author' LIKE '${this.filteredAuthorFIO}'`;
        }

        if (this.tableSelectedDateFirst) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' GE '${this.tableSelectedDateFirst.getTime()}'`;
        }

        if (this.tableSelectedDateSecond) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' LT '${second.setDate(second.getDate() + 1)}'`;
        }

        if (this.templatesSelectorValue[0]['id'] !== 'ALL') {
            params['filters'] += `${params['filters'] ? ',' : ''}'template.id' EQ '${this.templatesSelectorValue[0]['id']}'`;
        }

        params['sorts'] = ['author:ASC'];
        return params;
    }

    updatePagination(pagination: Pagination) {
        let division = pagination.totalElements / pagination.size;
        pagination.totalPages = Math.floor(pagination.totalElements / pagination.size);
        pagination.totalPages += ((division % 1) != 0) ? 1 : 0;
    }

    changePagination(event) {
        this.getEmployeeReportTasks()
            .then(res => {
                this.employeeReportTasks = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
                this.updatePagination(this.pagination);
            });
    }

    rebuildDiagram() {
        let second: Date;

        if (this.selectedDateSecond) {
            second = new Date(this.selectedDateSecond.getTime());
        }

        let params = {};
        params['filters'] = "";

        if (this.selectedDateFirst) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' GE '${this.selectedDateFirst.getTime()}'`;
        }

        if (this.selectedDateSecond) {
            params['filters'] += `${params['filters'] ? ',' : ''}'startDate' LT '${second.setDate(second.getDate() + 1)}'`;
        }

        this.getEmployeeReportTasks(params)
            .then(res => {
                this.barChartData[0]['data'] = [];
                this.barChartData[1]['data'] = [];
                this.barChartLabels = [];

                let newData = res['_embedded'];
                let diagramElement = this.domElement.nativeElement.querySelector('#employeeTasksResultDiagram')
                let limit = this.calculateDiagramColumnNumber(diagramElement.clientWidth);

                while (this.barChartLabels.length < (limit - newData.length)) {
                    this.barChartData[0]['data'].push(0);
                    this.barChartData[1]['data'].push(0);
                    this.barChartLabels.push('');
                }

                for (let task of newData) {
                    this.barChartData[0]['data'].push((task['result'] === 'FAIL') ? 1 : 0);
                    this.barChartData[1]['data'].push((task['result'] !== 'FAIL') ? 1 : 0);
                    this.barChartLabels.push(task['name']);

                    if (this.barChartLabels.length === limit) {
                        break;
                    }
                }
            });
    }

    calculateDiagramColumnNumber(disagramWidth: number): number {
        const COLUMN_WIDTH_PX = 20;
        return Math.floor(disagramWidth / COLUMN_WIDTH_PX);
    }

    filterTaskData(firstDate, secondDate) {
        let temp: Array<Object> = [];

        let second: Date;

        if (secondDate) {
            second = new Date(secondDate.getTime());
            second.setDate(second.getDate() + 1);
        }

        for (let task of this.reportEntity['resultByTask']) {
            if (secondDate == null && firstDate != null) {
                if (task['task']['startDate'] >= firstDate) {
                    temp.push(task);
                }
            }
            if (firstDate == null && secondDate != null) {
                if (task['task']['startDate'] < second) {
                    temp.push(task);
                }
            }
            if (firstDate != null && secondDate != null) {
                if (task['task']['startDate'] >= firstDate && task['task']['startDate'] < second) {
                    temp.push(task);
                }
            }
            if (firstDate == null && secondDate == null) {
                temp.push(task);
            }
        }

        return temp;
    }

    sortTask(items: Array<Object>, pagination?: Pagination): Array<Object> {
        let sortedItems = [];
        for (let item of items) {
            if (this.itemDateEqual(this.tableSelectedDateFirst, this.tableSelectedDateSecond, item) &&
                this.checkTemplateValue(item) &&
                item['task']['name'].toUpperCase().indexOf(this.filteredTaskName.toUpperCase()) !== -1 &&
                item['task']['shortName'].toUpperCase().indexOf(this.filteredAuthorFIO.toUpperCase()) !== -1) {
                sortedItems.push(item);
            }
        }
        return sortedItems;
    }

    checkTemplateValue(item) {
        let flag = false;
        if (item['task']['template']['id'] == this.templatesSelectorValue[0]['id']) {
            flag = true;
        }
        if (this.templatesSelectorValue[0]['id'] == 'ALL') {
            flag = true;
        }
        return flag;
    }

    itemDateEqual(firstDate, secondDate, item) {
        let flag = false;

        let second: Date;

        if (secondDate != null) {
            second = new Date(secondDate.getTime());
            second.setDate(second.getDate() + 1);
        }

        if (secondDate == null && firstDate != null) {
            if (item['task']['startDate'] >= firstDate) {
                flag = true;
            }
        }
        if (firstDate == null && secondDate != null) {
            if (item['task']['startDate'] < second) {
                flag = true;
            }
        }
        if (firstDate != null && secondDate != null) {
            if (item['task']['startDate'] >= firstDate && item['task']['startDate'] < second) {
                flag = true;
            }
        }
        if (firstDate == null && secondDate == null) {
            flag = true;
        }
        return flag
    }

    public barColors: Array<any> = [
        {
            backgroundColor: '#d94b44',
            hoverBackgroundColor: '#d94b44'
        },
        {
            backgroundColor: '#53cf9c',
            hoverBackgroundColor: '#53cf9c'
        }
    ];

    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true,
        maintainAspectRatio: false,
        scales:
        {
            xAxes: [{
                scaleShowLabels: false,
                ticks: {
                    display: false
                }
            }],
            yAxes: [{
                stacked: true,
                scaleShowLabels: false,
                ticks: {
                    display: false
                }
            }]
        },
        tooltips: {
            callbacks: {
                label: (tooltipItem, data) => {
                    return this.barChartData[tooltipItem.datasetIndex]['label'];
                }
            }
        }
    };

    getMailTemplates() {
        this.templateContext.getMailTemplateList()
            .then((data) => {
                let mailTemplates = data;
                this.templatesItems = this.getTemplatesItems(mailTemplates);
            });
    }

    public getTemplatesItems(templatesList: Array<Object>) {
        var items = [];
        for (var item of templatesList) {
            items.push({ "id": item['id'], "text": item['name'] });
        }
        items.unshift({ 'id': "ALL", "text": "Все шаблоны" });
        return items;
    }

    public refreshTemplateValue(value: any): void {
        if (value.length == 0) {
            this.templatesSelectorValue = [{ id: "" }];
        }
        else {
            this.templatesSelectorValue = [value];
        }
    }

    public templateName: string = "";
    public templateHtml: string = "";

    templatePreviewLoad(templateId: string) {
        let id: string = templateId;

        if (id != '') {
            this.core.get("api/mail-templates/" + id + "/example")
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }

    toggleShowFilter() {
        if (this.showAdvFilter) {
            this.templatesSelectorValue = [{ id: "ALL" }];
            this.tableSelectedDateFirst = null;
            this.tableSelectedDateSecond = null;
            this.filteredAuthorFIO = "";
        }
        this.showAdvFilter = !this.showAdvFilter;
    }

}