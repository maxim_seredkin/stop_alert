/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 30.03.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {Pagination} from "../../../../../models/pagination";
import {AppCoreService} from "../../../../../services/app-core.service";
import {UserService} from "../../../../../services/user.service";
import {UnitsTreeSelectorComponent} from "../../../units-tree-selector/units-tree-selector.component";

@Component({
    selector: 'employee-reports-list',
    template: require('./employee-reports-list.component.html')
})

export class EmployeeReportsListComponent {
    public pagination:Pagination;
    public reports:Array<Object>;

    public unitsTree: Array<Object>;
    public tagsTree: Array<Object>;

    private selectedOrgs:Array<Object> = [];
    private selectedTags:Array<Object> = [];

    public showAdvFilter:boolean = false;

    public filteredFullName: string = "";
    public filteredEmail: string = "";

    @ViewChild('orgSelector') orgSelector: UnitsTreeSelectorComponent;

    constructor(
        private core:AppCoreService,
        private user: UserService) {
        this.pagination = new Pagination();
        let params = this.getRequestParams();
        this.getReportsList(params);
        this.getUnitsTree();
        this.getTagsTree();
    }

    orgSelectorChange(selected) {
        this.selectedOrgs = selected;
    }

    tagSelectorChange(selected) {
        this.selectedTags = selected;
    }


    applyFilter() {
        this.pagination.number = 1;
        let params = this.getRequestParams();
        this.getReportsList(params);
    }


    getReportsList(params) {
        this.core.get("api/reports/employees", params)
            .then((res) => {
                this.reports = res['_embedded'];
                this.pagination.totalElements = res['page']['totalElements'];
                this.pagination.totalPages = res['page']['totalPages'];
            });
    }

    getRequestParams():Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;
        params['searchs'] = "";

        if (this.filteredEmail) {
            params['searchs'] = "'email':'" + this.filteredEmail + "'";
        }

        params['filters'] = "";

        if (this.filteredFullName) {
            params['filters'] += (params['filters']) ? ',' : '' + `EXTRA:'fullName' LIKE '${this.filteredFullName}'`;
        }
    
        if (this.selectedOrgs) {
            this.selectedOrgs.forEach(unit => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'units' EQ '${unit['id']}'`;
            });
        }

        if (this.selectedTags) {
            this.selectedTags.forEach(tag => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'employeeTags' EQ '${tag['id']}'`;
            });
        }

        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        return params;
    }

    changePagination(event:Pagination) {
        let params = this.getRequestParams();
        this.getReportsList(params);
    }

    getUnitsTree() {
        this.core.get('api/units/tree')
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });

    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    toggleShowFilter() {
        if(this.showAdvFilter) {
            this.orgSelector.resetValue();
            this.selectedTags = [];
            this.filteredEmail = "";
            this.selectedOrgs = [];
        }
        this.showAdvFilter = !this.showAdvFilter;
    }
}