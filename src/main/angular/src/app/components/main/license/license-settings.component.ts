/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 29.03.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {AppCoreService} from "../../../services/app-core.service";
import {Modal} from "ngx-modal/index";
import {UserService} from "../../../services/user.service";


@Component({
    selector: 'license',
    template: require('./license-settings.component.html')
})
export class LicenseSettingsComponent {
    public settings: Object;
    public licenseKey: string;

    public newSettings: Object;

    @ViewChild('myModal')
    public activateStatusModal: Modal;

    public fileLoaderPlaceHolder: string = "Переместите файл сюда или выберите из файла";
    public employeesDB: File;

    constructor(
        private core: AppCoreService,
        private user: UserService
    ) {
        this.getLicenseSettings();
    }

    getLicenseSettings() {
        this.core.get("api/settings/license")
            .then((res) => {
               this.settings = res['_embedded'];
            });
    }

    onUpload(file:File) {
        this.employeesDB = file;
        this.fileLoaderPlaceHolder = file.name;

        var fileReader = new FileReader();
        fileReader.onload = () => {
            this.licenseKey = fileReader.result;
        };
        fileReader.readAsText(file);
    }

    licenseActivate() {
        this.newSettings = null;
        this.activateStatusModal.open();

        let activateForm = new Object();
        activateForm['code'] = this.licenseKey;

        let putData = JSON.parse('{"form": ' + JSON.stringify(activateForm) + '}');
        this.core.put("api/settings/license", putData)
            .then((res) => {
                this.newSettings = res['_embedded'];
                if(res['_embedded']['status'] == 'SUCCESSFUL') {
                    this.settings = this.newSettings;
                    this.user.licenseActive = true;
                }
            });
    }
}