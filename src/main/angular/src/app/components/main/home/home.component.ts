/**
 * Created by Nikolay Gusev <n.gusev@white-soft.ru> on 29.03.2017.
 */
import {Component, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../../services/user.service";
import {AppCoreService} from "../../../services/app-core.service";
import {Modal} from "ngx-modal/index";
import {Task} from "../../../models/task/task";

@Component({
    selector: 'home',
    template: require('./home.component.html')
})
export class HomeComponent {
    public mailStats:Object;
    private tasks: Array<Task>;

    @ViewChild('mailTemplateShowModal')
    private mailTemplateShowModal:Modal;

    @ViewChild('licenseFail')
    private licenseFailModal:Modal;

    public licenseSettings: Object;

    constructor(private router:Router,
                private user:UserService,
                private core:AppCoreService) {
        this.getMailStats();
        if (this.user.getAuthority() != 'ROLE_MANAGER') {
            this.getTasks();
        }

        if(this.user.load == false) {
            this.getLicenseSettings();
            this.user.load = true;
        }
    }

    getLicenseSettings() {
        this.core.get("api/settings/license")
            .then((res) => {
                this.licenseSettings = res['_embedded'];
                if(this.licenseSettings['status'] == "FAILURE" || this.licenseSettings['status'] == "EXPIRED") {
                    if(this.user.getAuthority() == 'ROLE_ADMINISTRATOR') {
                        this.licenseFailModal.open();
                    }
                    this.user.licenseActive = false;
                }
                else {
                    this.user.licenseActive = true;
                }
            });
    }

    getTasks() {
        let params = new Object();

        params['page'] = 1;
        params['size'] = 4;

        if (params['filters'] == "") {
            params['filters'] += "'status' EQ 'EXECUTION'";
        }
        else {
            params['filters'] += ",'status' EQ 'EXECUTION'";
        }

        if (params['filters'] == "") {
            params['filters'] += "'status' EQ 'WAITING'";
        }
        else {
            params['filters'] += ",'status' EQ 'WAITING'";
        }

        params['sorts'] = ['startDate:DESC', 'name:ASC'];

        this.core.get("api/tasks", params)
            .then((res) => {
                this.tasks = res['_embedded'];
            })
    }

    getMailStats() {
        this.core.get("api/stats/mails")
            .then((res) => {
                this.mailStats = res['_embedded'];
                this.pieChartData = [
                    {
                        data: [
                            this.mailStats['percentMailCorrect'],
                            this.mailStats['percentMailIncorrect']
                        ]
                    }];

                this.pieChartLabels = ['Компетентные', 'Некомпетентные'];
            });
    }

    public colors = [
        "#d94b44",
        "#45dba3"
    ];

    public pieColors:Array<any> = [
        {
            backgroundColor: ['#53cf9c', '#d94b44'],
            hoverBackgroundColor: ['#53cf9c', '#d94b44']
        }
    ];

    public pieOptions = {
        tooltips: {
            callbacks: {
                label: (tooltipItem, data) => {
                    return ` ${data.labels[tooltipItem.datasetIndex]}: ${data.datasets[tooltipItem.datasetIndex]['data'][tooltipItem.index]}%`;
                }
            }
        }
    };

    public pieChartData:any = [
        {
            data: [
                20,
                30
            ]
        }];

    public pieChartLabels;

    toReports() {
        if(this.user.getAuthority() == 'ROLE_MANAGER') {
            this.router.navigate(['reports/units']);
        }
        else {
            this.router.navigate(['reports/tasks']);
        }
    }

    public templateName:string = "";
    public templateHtml:string = "";

    templatePreviewLoad(templateId:string) {
        let id:string = templateId;

        if (id != '') {
            this.core.get("api/mail-templates/" + id + "/example")
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }
}