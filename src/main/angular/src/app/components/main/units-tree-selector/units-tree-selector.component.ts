/**
 * Created by Baser on 04.02.2017.
 */
import {Component, Input, Output, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import {EventEmitter} from "@angular/common/src/facade/async";
import {Unit} from "../../../models/unit";
import {SelectorItem} from "../../../models/selector-item";
import {setTimeout} from "timers";

@Component({
    selector: 'units-tree-selector',
    template: require('./units-tree-selector.component.html')
})

export class UnitsTreeSelectorComponent {
    public selectedItems:Array<any> = [];
    public treeShow: boolean = false;

    @ViewChild('orgSelectorForm') el:ElementRef;
    @Output() change: EventEmitter<any> = new EventEmitter();
    @Output() initComplete: EventEmitter<any> = new EventEmitter();

    @Input() startValue: Array<Object>;
    @Input() inputUnitsArray: Array<Object>;
    @Input() placeHolderText: string;
    @Input() iconClass: string;

    @Input() singleSelect: boolean = false;
    @Input() error: boolean = false;
    @Input() exclude: Array<string> = [];

    @Input() showToolTip: boolean;

    public errorColor: boolean;

    public unitArray:Array<SelectorItem>;

    public inputUnitsArrayCopy: Array<Object>;
    public selectorValue: Array<Object> = [];

    public totalSelectedUnitsCount: number = 0;

    public filteredUnitName: string = "";
    public unitsFilterValue: string = "";

    @Input() type: string = "";

    constructor(private cdRef: ChangeDetectorRef) {
    }

    ngOnInit() {
        if(typeof this.showToolTip == 'undefined') {
            this.showToolTip = true;
        }
        
        this.inputUnitsArrayCopy = JSON.parse(JSON.stringify(this.inputUnitsArray));
        this.unitArray = this.itemToUnitArray(this.inputUnitsArray);

        if(this.startValue != null) {
            for (var item of this.startValue) {
                this.initStartValue(item, this.unitArray);
            }
        }
        this.normalizeUnitsTree(this.inputUnitsArrayCopy);
    }

    //Добавление поля состояния юнита для модального окна с чекбоксами
    normalizeUnitsTree(unitsArray: Array<Object>) {
        for(let unit of unitsArray) {
            unit['state'] = null;
            if (this.inArray(unit['id'], this.exclude)) {
                unitsArray.splice(unitsArray.indexOf(unit), 1);
                continue;
            }
            if(this.startValue) {
                for (let start of this.startValue) {
                    if (this.inArray(unit['id'], start['id'])) {
                        unit['state'] = true;
                    }
                }
            }
            if(unit['nodes']) {
                this.normalizeUnitsTree(unit['nodes']);
            }
        }
    }

    //Заполнение селектора начальными значениями
    initStartValue(unit: Object, unitArray: Array<SelectorItem>) {
        for(var item of unitArray) {
            if(item['id'] == unit['id']) {
                this.deleteItem(unitArray, item);
                this.deleteSelectedItemChildren(this.selectedItems, item);
                this.selectorValue.push(item);
            }
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.initStartValue(unit, item.nodes);
                }
            }
        }
        this.initComplete.emit(this.selectorValue);
    }

    itemToUnitArray(items:Array<Object>) {
        //Заполнение массива юнитов
        var unitsArray:Array<SelectorItem> = [];
        for (var item of items) {
            if(!this.inArray(item['id'], this.exclude)) {
                unitsArray.push(new SelectorItem(item['id'], item['name'], this.itemToUnitArray(item['nodes']), item['parent'], item['used'], item['errors']));
            }
        }
        //Сортировка по алфавиту
        this.firstSort(unitsArray);
        return unitsArray;
    }

    firstSort(unitArray: Array<SelectorItem>) {
        for (var item of unitArray) {
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.firstSort(item.nodes);
                }
            }
        }
        this.sortArray(unitArray);
    }

    toggleTreeShow(status) {
        this.treeShow = status;
    }



    //Получение выбранного элемента из стороннего компонента с параметрами id и name.
    //Поиск этого компонента в дереве.
    public tempFindedItem: SelectorItem;

    //Смена основного дерева юнитов
    changeUnitsArray(unitsArray: Array<Object>) {
        this.inputUnitsArrayCopy = JSON.parse(JSON.stringify(unitsArray));
        this.unitArray = this.itemToUnitArray(unitsArray);
    }

    //Получение значения из внешнего компонента
    getOutsideUnit(unit: Object) {
        this.unitArray = this.itemToUnitArray(this.inputUnitsArray);
        this.selectorValue = [];
        this.findUnit(unit, this.unitArray);
        this.getSelectedItem(this.tempFindedItem);
        // Добавляем элемент в массив значений
        this.selectorValue.push(this.tempFindedItem);
        this.tempFindedItem = null;
        this.change.emit(this.selectorValue);
    }

    //Получение значениий из внешнего компонента без сброса значения
    getOutsideUnits(units: Array<Object>) {
        for(let unit of units) {
            this.findUnit(unit, this.unitArray);
            this.getSelectedItem(this.tempFindedItem);
            // Добавляем элемент в массив значений
            this.selectorValue.push(this.tempFindedItem);
            this.tempFindedItem = null;
            this.change.emit(this.selectorValue);
        }
    }

    //Сброс значения селектора
    resetValue() {
        this.unitArray = this.itemToUnitArray(this.inputUnitsArrayCopy);
        this.selectorValue = [];
        this.selectedItems = [];
    }

    //Установить значение селектора
    setValue(items: Array<any>) {
        for(var item of items) {
            this.findUnit(item, this.unitArray);
            if(this.tempFindedItem != null && this.tempFindedItem != undefined) {
                this.getSelectedItem(this.tempFindedItem);
            }
        }
    }

    //Поиск элемента в дереве юнитов
    findUnit(unit: Object, unitArray: Array<SelectorItem>) {
        for (var item of unitArray) {
            if(item.id == unit['id']){
                this.tempFindedItem =  new Unit(item.id, item.name, item['nodes'], item['parent']);
            }
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.findUnit(unit, item.nodes);
                }
            }
        }
    }

    //Получение выбранного отдела и перемещение в массив
    getSelectedItem(item:SelectorItem) {
        if(this.singleSelect == false) {
            this.deleteItem(this.unitArray, item);
        }
        else {
            this.resetValue();
            this.singleSelectResetTreeValue(this.inputUnitsArrayCopy, item['id']);
            this.deleteItem(this.unitArray, item);
        }
        this.resetValue();
        this.updateSelectedArray(this.inputUnitsArrayCopy);
        this.updateSelectedUnitsCount();
    }

    //Удаление подотделов выбранного отдела и перемещение их в свойство nodes выбранного отдела
    deleteSelectedItemChildren(selectedItems: Array<SelectorItem>, selectItem:SelectorItem) {
        var deleteItems: Array<SelectorItem> = [];
        for (var selected of selectedItems) {

            if(selected.parent != null) {

                if (selected.parent.id == selectItem.id) {
                    selectItem.nodes.push(selected);
                    deleteItems.push(selected);
                } else {
                    for (var item of selectItem.nodes) {
                        this.deleteSelectedItemChildren(selectedItems, item);
                    }
                }
            }
        }
        for(var item of deleteItems) {
            selectedItems.splice(selectedItems.indexOf(item), 1);
        }
    }

    //Удаление выбранного элемента из дерева
    deleteItem(items:Array<SelectorItem>, deleteItem:SelectorItem) {
        for (var item of items) {
            if (item.id == deleteItem.id) {
                items.splice(items.indexOf(item), 1);
                return true;
            }
            if(item.nodes != null && (typeof item.nodes != "undefined")){
                if (item.nodes.length != 0) {
                    this.deleteItem(item.nodes, deleteItem);
                }
            }
        }
    }

    //Удаление отдела из списка выбранных отделов и перемещение обратно в дерево отделов
    deleteSelectedItem(unitArray: Array<SelectorItem>, selectedItems: Array<SelectorItem>, item:SelectorItem) {
        this.resetValue();
        this.updateSelectedArray(this.inputUnitsArrayCopy);
        this.updateSelectedUnitsCount();
    }

    // Удаление элемента их лэйбла нажатием на крестик
    deleteSelectedItemFromLabel(item: Object) {
        this.selectorValue.splice(this.selectorValue.indexOf(item), 1);
        this.change.emit(this.selectorValue);
    }

    //Обноавление значения селектора (элемент выбирается, если у него поле state == true - помечен галочкой)
    updateSelectedArray(unitsTree: Array<Object>) {
        for(let unit of unitsTree) {
            if(unit['state'] == true) {
                this.selectedItems.push(unit);
                continue;
            }
            if(this.checkChildrensState(unit['nodes']) === true) {
                unit['state'] = false;
            }
            if(unit['nodes']) {
                this.updateSelectedArray(unit['nodes']);
            }
        }
    }

    //Сортировка массива по алфавиту
    sortArray(unitArray: Array<SelectorItem>) {
        unitArray.sort((a, b) => {
            var textA = a['name'].toUpperCase();
            var textB = b['name'].toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
    }

    //Существоание элемента с указанным id массиве
    inArray(unitId: string, unitTree: Array<string>) {
        let flag = false;
        for(let unit of unitTree) {
            if(unit == unitId) {
                flag = true;
            }
        }
        return flag;
    }

    //Модификация дерева (удаление запрещенных элементов)
    checkUnitTree() {
        this.resetValue();
        this.unitArray = this.itemToUnitArray(this.inputUnitsArray);
    }

    //Редактирование массива запрещенных элементовв
    changeExcludeArray(exclude: Array<string>) {
        this.exclude = exclude;
    }

    // Скрытие дерева
    hideTree() {
        this.treeShow = false;
        this.selectedItems = [];
        this.unitsFilterValue = "";
        this.filteredUnitName = "";
        this.cdRef.detectChanges();
    }

    // Открытие дереве
    showTree() {
        this.selectedItems = this.selectorValue.slice();
        this.updateItemsTree(this.inputUnitsArrayCopy);
        this.checkHalfStateItems(this.inputUnitsArrayCopy);
        this.treeShow = true;
        this.updateSelectedUnitsCount();
    }

    // Сохранение дерева
    saveSelectorItems() {
        this.selectorValue = this.selectedItems.slice();
        this.change.emit(this.selectorValue);
    }

    // Обновляет состояния - state элементов, в соответствии с выбранными элементами
    updateItemsTree(unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            let checked: boolean = false;
            for (let selected of this.selectedItems) {
                if(unit['id'] === selected['id']) {
                    checked = true;
                }
            }
            if(checked) {
                unit['state'] = true;
            } else {
                unit['state'] = null;
            }
            if(unit['nodes']) {
                this.updateItemsTree(unit['nodes']);
            }
        }
    }

    // Обновление счетчика выбранных организаций
    updateSelectedUnitsCount() {
        this.totalSelectedUnitsCount = 0;
        this.countSelectedUnits(this.selectedItems);
    }

    // Подсчет выбранных организаций
    countSelectedUnits(unitsArray: Array<Object>) {
        for(let unit of unitsArray) {
            this.totalSelectedUnitsCount++;

            if(unit['nodes'] && !this.singleSelect) {
                this.countSelectedUnits(unit['nodes']);
            }
        }
    }

    // Проверка на наличие потомков с состоянием true или false
    checkChildrensState(childrens: Array<Object>): boolean {
        let flag = false;

        for(let unit of childrens) {
            if (unit['state'] === true || unit['state'] === false) {
                return true;
            }
            if(unit['nodes']) {
                if(this.checkChildrensState(unit['nodes']) === true) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    // Обновляет состояния - state элементов, в соответствии с выбранными элементами
    checkHalfStateItems(unitsTree: Array<Object>) {
        for (let unit of unitsTree) {
            if(unit['state'] !== true) {
                if(this.checkChildrensState(unit['nodes']) === true) {
                    unit['state'] = false;
                }
            }
            if(unit['nodes']) {
                this.checkHalfStateItems(unit['nodes']);
            }
        }
    }

    // Сброс значений элементов дерева с чкбоксами
    singleSelectResetTreeValue(unitArray: Array<Object>, excludeUnitId: string) {
        for (let item of unitArray) {
            if (item['id'] != excludeUnitId) {
                item['state'] = null;
            }
            if(item['nodes']) {
                this.singleSelectResetTreeValue(item['nodes'], excludeUnitId);
            }
        }
    }

    // Применение фильтра
    applyFilterUnits() {
        this.treeShow = false;
        this.unitsFilterValue = this.filteredUnitName;
        setTimeout(() => {
            this.treeShow = true;
        }, 50);
    }

    // Проверка существования дочерних элементов, которые удовлетворяют условиям фильтра
    findChildrensByName(childrens: Array<Object>) {
        let exists: boolean = false;
        if(!this.unitsFilterValue) {
            return true;
        }
        for (let children of childrens) {
            if (children['name'].toUpperCase().includes(this.unitsFilterValue.toUpperCase())) {
                exists = true;
                break;
            }
            if(children['nodes']) {
                exists = this.findChildrensByName(children['nodes']);
            }
        }
        return exists;
    }
}