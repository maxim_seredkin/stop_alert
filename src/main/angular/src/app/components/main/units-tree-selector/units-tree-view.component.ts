/**
 * Created by Baser on 04.02.2017.
 */
import {ChangeDetectorRef, Component, Input, Output} from '@angular/core';
import {EventEmitter} from "@angular/common/src/facade/async";
import {SelectorItem} from "../../../models/selector-item";
import {Unit} from "../../../models/unit";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";

@Component({
    selector: "units-tree-view, [unitsTreeView]",
    template: require('./units-tree-view.component.html')
})

export class UnitsTreeViewComponent {
    @Input('items') item: Object;
    @Input('child') child: boolean;
    @Input('type') type: string;
    @Input('inputUnitsArrayCopy') inputUnitsArrayCopy: Array<Object>;
    @Input('singleSelect') singleSelect: boolean;
    @Input('filterByName') filterByName: string;

    public showChildrenItems: boolean = false;

    @Output() select: EventEmitter<any> = new EventEmitter();
    @Output() delete: EventEmitter<any> = new EventEmitter();
    @Output() updatingState: EventEmitter<any> = new EventEmitter();

    public tempFindedItem: SelectorItem;

    public unitItemState: any = null;

    public showUnitName: SafeHtml;

    checkUnitItem() {
        if(this.item['state'] == true) {
            if(this.singleSelect === false) {
                this.childrenCheck(this.item['nodes']);
            }
            this.selectItem(this.item);
            return;
        }
        if(this.item['state'] == false) {
            this.item['state'] = null;
            this.childrenUncheck(this.item['nodes']);
            this.deleteItem(this.item);
            return;
        }
        if(this.item['state'] === null) {
            this.item['state'] = true;
            if(this.singleSelect === false) {
                this.childrenCheck(this.item['nodes']);
            }
            this.selectItem(this.item);
            return;
        }
    }

    childrenCheck(childrens: Array<Object>) {
        for(let unit of childrens) {
            unit['state'] = true;
            if(unit['nodes']) {
                this.childrenCheck(unit['nodes']);
            }
        }
    }

    childrenUncheck(childrens: Array<Object>) {
        for(let unit of childrens) {
            unit['state'] = null;
            if(unit['nodes']) {
                this.childrenUncheck(unit['nodes']);
            }
        }
    }

    constructor(private cdRef: ChangeDetectorRef, private sanitazer: DomSanitizer) {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        if(this.item['state'] === true) {
            if(!this.singleSelect) {
                this.childrenCheck(this.item['nodes']);
            }
        }
        this.showChildrenItems = this.findChildrensByName(this.item['nodes']);
        this.checkChildrens(this.item['nodes']);
        this.generateUnitShowName();
        this.cdRef.detectChanges();
    }

    toggleShowChildren() {
        this.showChildrenItems = !this.showChildrenItems;
    }

    // Генерация HTML-кода для отображения имени
    generateUnitShowName() {
        let filterNameLength = this.filterByName.length;
        let unitNameLength = this.item['name'].length;
        let filterNamePosition = this.item['name'].toUpperCase().indexOf(this.filterByName.toUpperCase());

        if(filterNamePosition === -1 || this.filterByName == "") {
            this.showUnitName = this.sanitazer.bypassSecurityTrustHtml(this.item['name']);
            return;
        }

        let resultString = `${this.item['name'].substr(0, filterNamePosition)}<b>${this.item['name'].substr(filterNamePosition, filterNameLength)}</b>${this.item['name'].substr(filterNamePosition+filterNameLength, this.item['name'].length)}`;

        this.showUnitName = this.sanitazer.bypassSecurityTrustHtml(resultString);
    }
    
    isUsed(item: Object) {
        if(item['used'] != null && item['used'] != undefined) {
            return item['used'];
        }
        else {
            return true;
        }
    }

    isShow(item: Object) {
        if(item['errors'] != null) {
            if(item['errors'].length > 0) {
                return false;
            }
        }
        return true;
    }

    selectItem(item: Object) {
        if(this.type == 'tag') {
            if(this.isEnable(item)) {
                this.select.emit(item);
            }
        }
        else {
            this.select.emit(item);
        }
    }

    deleteItem(item: Object) {
        if(this.type == 'tag') {
            if(this.isEnable(item)) {
                this.select.emit(item);
            }
        }
        else {
            this.delete.emit(item);
        }
    }

    pushItemUp(item: Object) {
        this.checkChildrens(this.item['nodes']);
        this.select.emit(item);
    }

    deleteItemPush(item: Object) {
        this.checkChildrens(this.item['nodes']);
        this.delete.emit(item);
    }

    updatingStatePush() {
        this.checkChildrens(this.item['nodes']);
    }

    checkChildrens(childrens: Array<Object>) {
        let checked: number = 0;
        let halfChecked: number = 0;
        let count: number = 0;
        if(childrens.length == 0) {
            return;
        }
        for(let unit of childrens) {
            if(unit['state'] === false) {
                halfChecked++;
            }
            if(unit['state'] === true) {
                checked++;
            }
            count++;
        }
        if((checked != 0) && (checked == count)) {
            if(!this.item['state'] === true) {
                this.item['state'] = false;
            }
            return;
        }
        if((halfChecked != 0) && (halfChecked == count)) {
            this.item['state'] = false;
            return;
        }
        if(checked < count && checked != 0) {
            this.item['state'] = false;
            return;
        }
        if(halfChecked < count && halfChecked != 0) {
            this.item['state'] = false;
            return;
        }
        if (this.singleSelect === true && this.item['state'] === true) {
            return;
        }
        this.item['state'] = null;
    }

    isEnable(unit: Object) {
        this.findUnit(unit, this.inputUnitsArrayCopy);
        return (this.tempFindedItem.nodes.length == 0);
    }

    //Поиск элемента в дереве юнитов
    findUnit(unit: Object, unitArray: Array<Object>) {
        for (var item of unitArray) {
            if(item['id'] == unit['id']){
                this.tempFindedItem =  new Unit(item['id'], item['name'], item['nodes'], item['parent']);
            }
            if(item['nodes'] != null && (typeof item['nodes'] != "undefined")){
                if (item['nodes'].length != 0) {
                    this.findUnit(unit, item['nodes']);
                }
            }
        }
    }

    // Проверка существования дочерних элементов, которые удовлетворяют условиям фильтра
    findChildrensByName(childrens: Array<Object>) {
        let exists: boolean = false;
        if(!this.filterByName) {
            // Чтобы элементы не раскрывались при инициализации
            return false;
        }
        for (let children of childrens) {
            if (children['name'].toUpperCase().includes(this.filterByName.toUpperCase())) {
                exists = true;
                break;
            }
            if(children['nodes']) {
                exists = this.findChildrensByName(children['nodes']);
            }
        }
        return exists;
    }
}
