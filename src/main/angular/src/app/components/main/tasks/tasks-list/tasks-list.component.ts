/**
 * Created by Baser on 17.02.2017.
 */
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Pagination } from "../../../../models/pagination";
import { Unit } from "../../../../models/unit";
import { AppCoreService } from "../../../../services/app-core.service";
import { Tag } from "../../../../models/tag/tag";
import { DatePickerSettingsService } from "../../../../services/datePickerSettings";
import { Task } from "../../../../models/task/task";
import { NavigationExtras, Router } from "@angular/router";
import { TaskContext } from "../../../../models/task/task-context";
import { Calendar } from "primeng/primeng";
import { Modal } from "ngx-modal/index";
import { SelectComponent } from "ng2-select/index";
import { UserService } from "../../../../services/user.service";

@Component({
    selector: 'tasks-list',
    template: require('./tasks-list.component.html'),
    styles: [require('./tasks-list.component.scss')]
})

export class TasksListComponent {
    private pagination: Pagination;
    private unitsTree: Array<Unit>;
    private tagsTree: Array<Tag>;

    private selectedOrgs: Array<Unit>;
    private selectedTags: Array<Tag>;

    private filteredTaskName: string = "";
    private filteredCreatorName: string = "";

    public selectedDate: Date = null;

    private tasks: Array<Task>;

    public modalHeader: string = "";
    public modalText: string = "";
    public modalTaskName: string = "";
    public modalSubmitButtonText: string = "";
    public modalDelete: boolean = false;
    public modalRecover: boolean = false;
    public modalAction: string = "";

    public modalError: boolean = false;
    public modalErrorText: string = "";

    public selectedTaskId: string = "";
    public selectedTask: Task;

    public selectedStartTime: Date = null;

    public showAdvFilter: boolean = false;

    @ViewChild('orgSelector') orgSelector: ElementRef;
    @ViewChild('tagSelector') tagSelector: ElementRef;

    @ViewChild('startDateSelector') startDateSelector: Calendar;

    @ViewChild('orgSelectorForm') el: ElementRef;

    @ViewChild('myModal')
    private modal: Modal;

    @ViewChild('changeStartDate')
    private changeStartDateModal: Modal;

    public filterStatusValue: Array<Object> = [{ "id": "ALL", "text": "Все записи" }];

    @ViewChild(SelectComponent)
    private select: SelectComponent;

    constructor(private core: AppCoreService,
        private datePicker: DatePickerSettingsService,
        private router: Router,
        private taskContext: TaskContext,
        public user: UserService) {
        this.pagination = new Pagination();
        let params = this.getRequestParams();
        this.getTasksList(params)
            .then(() => {
                this.getTagsTree();
                this.getUnitsTree();
            });
    }

    ngAfterViewInit() {
    }

    public statusItems: Array<Object> = [
        { "id": "ALL", "text": "Все записи" },
        { "id": "WAITING", "text": "В ожидании" },
        { "id": "SUSPENDED", "text": "Приостановлена" },
        { "id": "EXECUTION", "text": "Выполняется" },
        { "id": "COMPLETED", "text": "Завершена" },
    ];

    public refreshFilterStatusValue(value: any): void {
        this.filterStatusValue = (value.length === 0) ? [{ id: "" }] : [value];
    }

    getTasksList(params) {
        return new Promise((resolve, reject) => {
            this.core.get("api/tasks", params)
                .then((res) => {
                    this.tasks = res['_embedded'];
                    this.pagination.totalElements = res['page']['totalElements'];
                    this.pagination.totalPages = res['page']['totalPages'];
                    resolve();
                })
                .catch(() => {
                    reject();
                });
        });
    }

    getRequestParams(): Object {
        let params = new Object();

        params['page'] = this.pagination.number;
        params['size'] = this.pagination.size;
        params['searchs'] = "";

        if (this.filteredTaskName) {
            params['searchs'] = "'name':'" + this.filteredTaskName + "'";
        }

        params['filters'] = "";

        if (this.filteredCreatorName) {
            params['filters'] += (params['filters']) ? ',' : '' + `EXTRA:'author' LIKE '${this.filteredCreatorName}'`;
        }

        if (this.selectedOrgs) {
            this.selectedOrgs.forEach(unit => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'units' EQ '${unit['id']}'`;
            });
        }

        if (this.selectedTags) {
            this.selectedTags.forEach(tag => {
                params['filters'] += `${(params['filters']) ? ',' : ''}EXTRA:'employeeTags' EQ '${tag['id']}'`;
            });
        }

        if (this.selectedDate) {
            params['filters'] += `${(params['filters']) ? ',' : ''}'startDate' GE '${this.selectedDate.getTime()}','startDate' LT '${this.selectedDate.getTime() + 86400000}'`;
        }

        if (this.filterStatusValue[0]['id'] !== 'ALL') {
            params['filters'] += `${params['filters'] ? ',' : ''}'status' EQ '${this.filterStatusValue[0]['id']}'`;
        }

        params['sorts'] = ['startDate:DESC', 'name:ASC'];
        return params;
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    orgSelectorChange(selected) {
        this.selectedOrgs = selected;
    }

    tagSelectorChange(selected) {
        this.selectedTags = selected;
    }

    applyFilter() {
        this.pagination.number = 1;
        let params = this.getRequestParams();
        this.getTasksList(params);
    }

    changePagination(event: Pagination) {
        let params = this.getRequestParams();
        this.getTasksList(params);
    }

    editTask(id: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                "action": "edit",
            }
        };
        this.router.navigate(['tasks/', id], navigationExtras);
    }

    changeTask(task: Task, action) {
        this.modalErrorText = "";
        this.modalError = false;

        this.selectedTaskId = task.id;

        this.modalTaskName = task.name;
        switch (action) {
            case "pause":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = true;
                this.modalHeader = "Приостановка задачи";
                this.modalText = "Вы уверены, что хотите приостановить задачу ";
                this.modalSubmitButtonText = "Приостановить";
                break;
            case "resume":
                this.modalAction = action;
                this.modalRecover = true;
                this.modalDelete = false;
                this.modalHeader = "Возобновление задачи";
                this.modalText = "Вы уверены, что хотите возобновить выполнение задачи ";
                this.modalSubmitButtonText = "Возобновить";
                break;
            case "cancel":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = true;
                this.modalHeader = "Отмена задачи";
                this.modalText = "Вы уверены, что хотите отменить задачу ";
                this.modalSubmitButtonText = "Отменить";
                break;
        }
    }

    changeTaskAction() {
        this.taskContext.changeTaskStatus(this.selectedTaskId, this.modalAction)
            .then((res) => {
                if (res['code'] === 200) {
                    let params = this.getRequestParams();
                    this.getTasksList(params);
                    this.modal.close();
                }
                if (res['code'] === 1011) {
                    this.modalError = true;
                    this.modalErrorText = res['_embedded'];
                }
                if (res['code'] === 1012) {
                    this.modalError = true;
                    this.modalErrorText = "Невозможно приостановить задачу, так как она уже выполняется.";
                }
            });
    }

    changeTaskStartTime(task: Task) {
        this.modalError = false;
        this.modalErrorText = "";

        this.selectedTask = task;
        this.selectedStartTime = new Date(task.startDate);
    }

    changeTaskStartDate() {
        var oldDate = new Date(this.selectedTask.startDate);
        if (this.selectedStartTime.getTime() !== oldDate.getTime()) {
            this.taskContext.changeTaskStartDate(this.selectedTask.id, this.selectedStartTime.getTime())
                .then((res) => {
                    let params = this.getRequestParams();
                    this.getTasksList(params);
                    if (res['code'] === 200) {
                        this.changeStartDateModal.close();
                    }
                    if (res['code'] === 1011) {
                        this.modalError = true;
                        this.modalErrorText = res['_embedded'];
                    }
                });
        } else {
            this.changeStartDateModal.close();
        }
    }


    public dateSelectorOverlayView = false;
    toggleStartDateSelectorShow(status: boolean) {
        if (status) {
            this.dateSelectorOverlayView = true;
            this.startDateSelector.overlayVisible = status;
        }
        else {
            if (this.dateSelectorOverlayView) {
                this.dateSelectorOverlayView = false;
            }
            else {
                this.startDateSelector.overlayVisible = status;
            }
        }
    }

    toggleShowFilter() {
        if (this.showAdvFilter) {
            this.filterStatusValue = [{ "id": "ALL", "text": "Все записи" }];
            this.filteredCreatorName = "";
            this.selectedDate = null;
            this.selectedTags = [];
            this.selectedOrgs = [];
        }
        this.showAdvFilter = !this.showAdvFilter;
    }
}
