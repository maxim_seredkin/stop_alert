/**
 * Created by Baser on 14.02.2017.
 */
import { Component, ElementRef, ViewChild } from '@angular/core';
import { DatePickerSettingsService } from "../../../../services/datePickerSettings";
import { TaskContext } from "../../../../models/task/task-context";
import { Task } from "../../../../models/task/task";
import { Unit } from "../../../../models/unit";
import { AppCoreService } from "../../../../services/app-core.service";
import { SelectorItem } from "../../../../models/selector-item";
import { MailTemplate } from "../../../../models/template/template";
import { MailTemplateContext } from "../../../../models/template/template-context";
import { Tag } from "../../../../models/tag/tag";
import { Employee } from "../../../../models/employee/employee";
import { EmployeeContext } from "../../../../models/employee/employee-context";
import { UserService } from "../../../../services/user.service";
import { Router } from "@angular/router";
import { TreeSelectorComponent } from "../../tree-selector/tree-selector.controller";
import { SelectComponent } from "ng2-select/index";
import { Modal } from "ngx-modal/index";
import { Calendar } from "primeng/primeng";
import { Pagination } from '../../../../models/pagination';
import { EmployeesListPaginationComponent } from '../employees-list-pagination/employees-list-pagination.component';

@Component({
    selector: 'task-create',
    template: require('./task-create.component.html')
})

export class TaskCreateComponent {

    private URL_REGEXP: RegExp = new RegExp(
        "^" +
        // protocol identifier
        "(?:(?:https?|ftp)://)" +
        // user:pass authentication
        "(?:\\S+(?::\\S*)?@)?" +
        "(?:" +
        // IP address exclusion
        // private & local networks
        //"(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
        //"(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
        //"(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
        // IP address dotted notation octets
        // excludes loopback network 0.0.0.0
        // excludes reserved space >= 224.0.0.0
        // excludes network & broacast addresses
        // (first & last IP address of each class)
        "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
        "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
        "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
        "|" +
        // host name
        "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
        // domain name
        "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
        // TLD identifier
        "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
        // TLD may end with dot
        "\\.?" +
        ")" +
        // port number
        "(?::\\d{2,5})?" +
        // resource path
        "(?:[/?#]\\S*)?" +
        "$", "i"
    );

    public currentDateToString = new Date().toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });

    public newTask: Task;
    public selectedStartDate: Date = null;
    public selectedEndDate: Date = null;

    public minEndDate: Date = new Date();

    public fillFormError: boolean = false;
    public formErrorText: string = "";

    public unitsTree: Array<Unit>;
    public tagsTree: Array<Tag>;

    public selectedUnits: Array<Unit> = [];
    public selectedTags: Array<Tag> = [];

    public mailTemplates: Array<MailTemplate>;
    public mailTemplatesItems: Array<any>;

    private templatesSelectorValue: any = { id: "" };

    public mockPages: Array<Object>;
    private mockPageSelectorValue: any = { id: "" };
    public mockPageItems: Array<Object>;

    private periodSelectorValue: any = [{ id: "" }];

    public employeesList: Array<Employee> = [];

    public countSelectedEmployees: number = 0;

    public employeeFilter: string = "";

    public taskTemplateSelectorValue: Array<any> = [{ id: "" }];

    public tasksList: Array<Task>;
    public tasksListItems: Array<any>;

    public periods: Array<any>;
    public periodsItems: Array<any>;

    public validLink: boolean = false;

    public customUrl: string = "";

    public selectedRedirectType: string = "TEMPLATE_LINK";

    public employeesPagination: Pagination;

    public showLoadingSpinner: boolean = false;

    @ViewChild('orgSelector') orgSelector: TreeSelectorComponent;
    @ViewChild('tagSelector') tagSelector: TreeSelectorComponent;

    @ViewChild('templateSelector') templateSelector: SelectComponent;
    @ViewChild('periodSelector') periodSelector: SelectComponent;

    @ViewChild('mailTemplateShowModal')
    private mailTemplateShowModal: Modal;

    @ViewChild('startDateErrorModal')
    private startDateErrorModal: Modal;

    // список сотрудников
    @ViewChild('employeesList') employeesListElement: ElementRef;
    // список сотрудников (просмотр)
    @ViewChild('employeesListView') employeesListViewElement: ElementRef;
    // Пейджинация
    @ViewChild('employeesPaginationElement') employeesPaginationElement: EmployeesListPaginationComponent;
    @ViewChild('employeesListViewPagination') employeesListViewPagination: EmployeesListPaginationComponent;

    /* возможность открыть модальное окно (используется при обновлении списка пользователей
       при изменении тега или организации)
     */
    public allowOpenEmployeesModal: boolean = true;

    private viewEmployeesList: Array<Employee> = [];

    constructor(private employeeContext: EmployeeContext,
                private templateContext: MailTemplateContext,
                private datePicker: DatePickerSettingsService,
                private taskContext: TaskContext,
                private core: AppCoreService,
                private userService: UserService,
                private router: Router) {
        this.newTask = this.taskContext.newTask();
        this.getUnitsTree();
        this.getTagsTree();
        this.getTasksList();
        this.templateContext.getMailTemplateList()
            .then((data) => {
                this.mailTemplates = data;
                this.mailTemplatesItems = this.getTemplatesItems(this.mailTemplates);
            });

        //Задание начального типа редиректа
        this.newTask.redirect = { redirectType: 'TEMPLATE_LINK', mailTemplate: "" };

        let currentDate: Date = new Date();
        currentDate.setMinutes(currentDate.getMinutes() + 30);
        this.selectedStartDate = currentDate;
        currentDate = new Date(this.selectedStartDate);
        currentDate.setDate(currentDate.getDate() + 7);
        this.selectedEndDate = currentDate;

        this.employeesPagination = new Pagination();
        this.employeesPagination.size = 100;

        this.getMockPagesList();
    }

    getMockPagesList() {
        this.core.get('api/mock-pages')
            .then((res) => {
                this.mockPages = res['_embedded'];
                this.mockPageItems = TaskCreateComponent.getMockPageItems(this.mockPages);
            })
    }

    static getMockPageItems(mockPages: Array<Object>) {
        let mockPagesItems: Array<Object> = [];
        for (let item of mockPages) {
            mockPagesItems.push({ id: item['id'], text: item['name'] })
        }
        return mockPagesItems;
    }

    static getPeriodsItems(periods: Array<any>) {
        if (periods == null || periods.length <= 0)
            return [{ "id": "", "text": "" }];

        return periods.map(period => {
            return {
                "id": period['value'],
                "text": period['name']
            };
        });
    }

    loadTaskParams() {
        if (this.taskTemplateSelectorValue == null
            || this.taskTemplateSelectorValue.length <= 0
            || this.taskTemplateSelectorValue[0]['id'] == "")
            return;

        this.selectedUnits = [];
        this.selectedTags = [];

        this.taskContext.getTaskById(this.taskTemplateSelectorValue[0]['id'])
            .then((task: Task) => {
                this.orgSelector.resetValue();
                this.orgSelector.setValue(task.units);
                this.tagSelector.resetValue();
                this.tagSelector.setValue(task.employeeTags);
                this.templatesSelectorValue = {
                    "id": task.template.id,
                    "text": task.template.name
                };
                this.templateSelector.active = [this.templatesSelectorValue];
            })
    }

    getTasksList() {
        let params = {
            'sorts': ['name:ASC']
        };

        this.core.get("api/tasks", params)
            .then((res) => {
                this.tasksList = res['_embedded'];
                this.tasksListItems = TaskCreateComponent.getTasksListItems(this.tasksList);
            });
    }

    static getTasksListItems(tasksList: Array<any>) {
        if (tasksList == null)
            return [];

        return tasksList.map(item => {
            return {
                "id": item.id,
                "text": item.name
            };
        });
    }

    getEmployeesList(edit: boolean): void {
        this.showLoadingSpinner = true;
        if (edit) {
            this.employeeContext.getEmployeeList(this.getSimpleRequestParams()).then((data) => {
                // выбираем сотрудников, которые были выбраны вручную
                this.selectEmployees(data, this.newTask.employeesByHand, false);
                // выбираем сотрудников, которые попадают под действие фильтра
                this.getEmployeesListByFilters().then(selectedEmployees => {
                    this.selectEmployees(data, selectedEmployees, true);
                    this.countSelectedEmployees = this.getSelectedEmployeeList(this.employeesList).length;
                    this.updateEmployeeList();
                    // убираем спиннер
                    this.showLoadingSpinner = false;
                });
                // в режиме редактирования получаем всех сотрудников
                this.employeesList = data;
            });
        } else {
            // в режиме чтения получаем только выбранных сотрудников
            this.employeesList = this.newTask.employeesByHand.concat(this.newTask.employeesByFilter);
            this.countSelectedEmployees = this.employeesList.length;
            this.updateEmployeeList();
            // убираем спиннер
            this.showLoadingSpinner = false;
        }
    }

    private selectEmployees(employeesList: Array<Employee>, selectedEmployeesList: Array<Employee>, disabled: boolean) {
        employeesList = employeesList.sort((e1, e2) => e1.id.localeCompare(e2.id));
        selectedEmployeesList = selectedEmployeesList.sort((e1, e2) => e1.id.localeCompare(e2.id));

        let currentIndex: number = 0;
        let currentEmployee: Employee = employeesList[currentIndex];

        selectedEmployeesList.forEach(selectEmployee => {
            while (true) {
                if (selectEmployee.id == currentEmployee.id) {
                    currentEmployee.disabled = disabled;
                    currentEmployee.selected = true;
                    break;
                } else {
                    if ((++currentIndex) == employeesList.length)
                        break;
                    currentEmployee = employeesList[currentIndex];
                }
            }
        });
    }

    getEmployeesListByFilters(): Promise<Array<Employee>> {
        return new Promise((resolve, cancel) => {
            if ((this.selectedUnits.length == 0) && (this.selectedTags.length == 0))
                resolve([]);
            else
                this.employeeContext.getEmployeeList(this.getFilteredRequestParams())
                    .then((data) => {
                        resolve(data);
                    });
        });
    }

    getSelectedEmployeeList(employees: Array<Employee>): Array<Employee> {
        return employees.filter(employee => employee.selected);
    }

    resetFilterBySecondName() {
        this.employeeFilter = "";
    }

    getSimpleRequestParams() {
        let params = new Object();
        if (this.employeeFilter != "" && this.employeeFilter != undefined) {
            params['searchs'] = "'secondName':'" + this.employeeFilter + "'";
        }
        params['filters'] = "";
        if (params['filters'] == "") {
            params['filters'] += "'status' EQ 'ENABLED'";
        }
        else {
            params['filters'] += ",'status' EQ 'ENABLED'";
        }
        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        params['projection'] = 'SELECTOR';
        return params;
    }

    getFilteredRequestParams(): Object {
        let params = new Object();

        params['filters'] = "";
        if (this.selectedUnits != null) {
            for (var unit of this.selectedUnits) {
                if (params['filters'] == "") {
                    params['filters'] += "EXTRA:'units' EQ '" + unit['id'] + "'";
                }
                else {
                    params['filters'] += ",EXTRA:'units' EQ '" + unit['id'] + "'";
                }
            }
        }
        if (this.selectedTags != null) {
            for (var tag of this.selectedTags) {
                if (params['filters'] == "") {
                    params['filters'] += "EXTRA:'employeeTags' EQ '" + tag['id'] + "'";
                }
                else {
                    params['filters'] += ",EXTRA:'employeeTags' EQ '" + tag['id'] + "'";
                }
            }
        }
        if (params['filters'] == "") {
            params['filters'] += "'status' EQ 'ENABLED'";
        }
        else {
            params['filters'] += ",'status' EQ 'ENABLED'";
        }
        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        params['projection'] = 'SELECTOR';
        return params;
    }

    saveSelectedEmployees() {
        this.newTask.employeesByFilter = this.employeesList.filter(employee => employee.selected && employee.disabled);
        this.newTask.employeesByHand = this.employeesList.filter(employee => employee.selected && !employee.disabled);
        this.employeesList = [];
    }

    cancelSelectedEmployees() {
    }

    public getTemplatesItems(templatesList: Array<Object>) {
        let templates = templatesList;

        let templateGroups: Array<Object> = [];
        let templateItems: Array<Object> = [];

        for (let item of templates) {
            if (item['group'] === null) {
                item['group'] = { id: '1', text: ' ' };
                if (!this.inArray(templateGroups, '1')) {
                    templateGroups.push(item['group']);
                }
                continue;
            }
            if (!this.inArray(templateGroups, item['group']['id'])) {
                templateGroups.push({ id: item['group']['id'], text: item['group']['name'] });
            }
        }

        this.sortTemplateArray(templateGroups);

        for (let group of templateGroups) {
            let groupItem = {};
            groupItem['id'] = group['id'];
            groupItem['text'] = group['text'];
            groupItem['children'] = [];
            for (let template of templates) {
                if (String(template['group']) == "null") {
                    continue;
                }
                if (template['group']['id'] == group['id']) {
                    groupItem['children'].push({ id: template['id'], text: template['name'] });
                }
            }
            templateItems.push(groupItem);
        }

        return templateItems;
    }

    inArray(array: Array<Object>, id: string) {
        return array.some(item => item['id'] == id);
    }

    //Сортировка массива по алфавиту
    sortTemplateArray(unitArray: Array<Object>) {
        unitArray.sort((a, b) => {
            var textA = a['text'].toUpperCase();
            var textB = b['text'].toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    orgSelectorChange(selected) {
        // Дизэйблим кнопки открытия модалки сотрудников
        this.allowOpenEmployeesModal = false;
        // При фильтрации элементов в селекторе выбора организаций киадется событие change, игнорим этот кейс
        if (selected.target)
            return;

        this.selectedUnits = selected;
        this.orgItemsToModel(this.selectedUnits);

        this.getEmployeesListByFilters().then(selectedEmployees => {
            this.newTask.employeesByFilter = selectedEmployees.map(employee => {
                employee.selected = employee.disabled = true;
                return employee;
            });
            this.newTask.employeesByHand = [];
            // Активируем кнопки обратно
            this.allowOpenEmployeesModal = true;
        });
    }

    tagSelectorChange(selected) {
        // Дизэйблим кнопки открытия модалки сотрудников
        this.allowOpenEmployeesModal = false;
        this.selectedTags = selected;
        this.tagItemsItemsToModel(this.selectedTags);

        this.getEmployeesListByFilters().then(selectedEmployees => {
            this.newTask.employeesByFilter = selectedEmployees.map(employee => {
                employee.selected = employee.disabled = true;
                return employee;
            });
            this.newTask.employeesByHand = [];
            // Активируем кнопки обратно
            this.allowOpenEmployeesModal = true;
        });
    }

    tagItemsItemsToModel(selectedArray: Array<SelectorItem>) {
        this.newTask.employeeTags = selectedArray;
    }

    orgItemsToModel(selectedArray: Array<any>) {
        this.newTask.units = selectedArray;
    }

    convertTimeToModel() {
        if (this.selectedStartDate != null)
            this.newTask.startDate = this.selectedStartDate.getTime();

        if (this.selectedEndDate != null)
            this.newTask.endDate = this.selectedEndDate.getTime();
    }

    public templatesItemsToModel(item) {
        if (item['id'] != "")
            this.newTask.template.id = item['id'];
        else
            this.newTask.template.id = "";
    }

    public startDateModalError: boolean = false;

    changeTaskStartDate() {
        this.startDateModalError = false;
        if (this.startDateModal < new Date()) {
            this.startDateModalError = true;
        } else {
            this.selectedStartDate = this.startDateModal;
            this.startDateErrorModal.close();
            this.createTask();
        }
    }

    fastStart() {
        let currentDate: Date = new Date();
        currentDate.setMinutes(currentDate.getMinutes() + 3);
        this.selectedStartDate = currentDate;
        currentDate = new Date(this.selectedStartDate);
        currentDate.setDate(currentDate.getDate() + 7);
        this.selectedEndDate = currentDate;
        this.createTask();
    }

    createTask() {
        if (this.startDateError || this.endDateError)
            return;

        if (this.newTask.redirect['redirectType'] == 'CUSTOM_LINK')
            this.newTask.redirect['customUri'] = this.customUrl;

        this.fillFormError = false;
        this.formErrorText = "";

        this.templatesItemsToModel(this.templatesSelectorValue);
        this.convertTimeToModel();

        if (this.newTask.valid()) {

            if (this.selectedStartDate < new Date()) {
                let currentDate = new Date();
                currentDate.setMinutes(currentDate.getMinutes() + 5);
                this.startDateModal = currentDate;
                this.startDateErrorModal.open();
                return;
            }

            this.taskContext.createTask(this.newTask)
                .then((resp) => {
                    if (resp['code'] == 200) {
                        this.router.navigate(['/tasks']);
                    }
                    if (resp['code'] == 1010) {
                        this.fillFormError = true;
                        this.formErrorText = "Сотрудники не выбраны!";
                    }
                    if (resp['code'] == 1011) {
                        this.fillFormError = true;
                        this.formErrorText = "Дата запуска задачи меньше даты создания задачи!";
                        this.newTask.startDate = 0;
                    }
                    if (resp['code'] == 1017) {
                        this.fillFormError = true;
                        this.formErrorText = "Дата завершения меньше даты начала!";
                        this.newTask.startDate = 0;
                    }
                })
        }
        else {
            this.fillFormError = true;
            this.formErrorText = "Не все поля заполнены!";
        }
    }

    public refreshTemplateValue(value: any): void {
        if (value.length == 0) {
            this.templatesSelectorValue = { id: "" };
        }
        else {
            this.templatesSelectorValue = value;
            if (this.newTask.redirect['redirectType'] == 'TEMPLATE_LINK') {
                this.newTask.redirect['mailTemplate'] = value['id'];
            }
        }
    }

    public refreshMockPageValue(value: any): void {
        if (value.length == 0) {
            this.validLink = false;
            this.mockPageSelectorValue = { id: "" };
        }
        else {
            this.validLink = true;
            this.mockPageSelectorValue = value;
            if (this.newTask.redirect['redirectType'] == 'MOCK_PAGE_LINK') {
                this.newTask.redirect['mockPage'] = value['id'];
            }
        }
    }

    public refreshTaskTemplateValue(value: any) {
        if (value.length == 0) {
            this.taskTemplateSelectorValue = [{ id: "" }];
        }
        else {
            this.taskTemplateSelectorValue = [value];
        }
    }

    public templateName: string = "";
    public templateHtml: string = "";

    templatePreviewLoad() {
        if (this.templatesSelectorValue['id'] != '') {
            this.core.get("api/mail-templates/" + this.templatesSelectorValue['id'] + "/example")
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }

    public startDateError: boolean = false;
    public endDateError: boolean = false;
    public startDateModal: Date = null;

    changedStartDate() {
        this.minEndDate = this.selectedStartDate;

        if (this.selectedStartDate > this.selectedEndDate) {
            this.selectedEndDate = this.selectedStartDate;
        }

        this.startDateError = (this.selectedStartDate < new Date());
    }

    changedEndDate() {
        this.endDateError = (this.selectedEndDate < new Date());
    }


    @ViewChild('startDateSelectorModal')
    public startDateSelectorModal: Calendar;

    public dateSelectorOverlayView = false;

    toggleStartDateSelectorShow(status: boolean) {
        if (status) {
            this.dateSelectorOverlayView = true;
            this.startDateSelectorModal.overlayVisible = status;
        }
        else {
            if (this.dateSelectorOverlayView) {
                this.dateSelectorOverlayView = false;
            }
            else {
                this.startDateSelectorModal.overlayVisible = status;
            }
        }
    }

    validCustomUrl() {
        this.validLink = this.URL_REGEXP.test(this.customUrl);
    }

    redirectTypeChanged() {
        setTimeout(() => {
            if (this.selectedRedirectType == 'TEMPLATE_LINK') {
                this.validLink = true;
                this.newTask.redirect = {};
                this.newTask.redirect['redirectType'] = 'TEMPLATE_LINK';
                this.newTask.redirect['mailTemplate'] = (this.templatesSelectorValue['id'] != '') ? this.templatesSelectorValue['id'] : '';
            }
            if (this.selectedRedirectType == 'MOCK_PAGE_LINK') {
                this.validLink = false;
                this.newTask.redirect = {};
                this.newTask.redirect['redirectType'] = 'MOCK_PAGE_LINK';
                this.newTask.redirect['mockPage'] = '';
                this.mockPageSelectorValue = { id: '' };
            }
            if (this.selectedRedirectType == 'CUSTOM_LINK') {
                this.validLink = true;
                this.newTask.redirect = {};
                this.newTask.redirect['redirectType'] = 'CUSTOM_LINK';
                this.newTask.redirect['customUri'] = this.customUrl;
            }
        }, 50);
    }

    goToPage(): void {
        switch (this.selectedRedirectType) {
            case 'TEMPLATE_LINK':
                let template: MailTemplate = this.mailTemplates.find(template => template['id'] == this.templatesSelectorValue['id']);
                if (template)
                    window.open(template.redirectUri);
                break;

            case 'MOCK_PAGE_LINK':
                let mockPage: Object = this.mockPages.find(mockPage => mockPage['id'] == this.mockPageSelectorValue['id']);
                if (mockPage)
                    window.open(mockPage['redirectUri']);
                break;

            case 'CUSTOM_LINK':
                window.open(this.customUrl);
                break;
        }
    }

    trackById(index, item): string {
        return item.id;
    }

    employeesListModalClose() {
        this.employeesList = [];
        this.viewEmployeesList = [];
        // Задаем начальное значение пэйджинации и обновляем ее
        this.employeesPagination.totalElements = 0;
        this.employeesPagination.number = 1;
        this.employeesPagination.updatePagination();
        // Обнуляем фильтр по фамилии + обнуляем количество выбранных сотрудников
        this.resetFilterBySecondName();
        this.countSelectedEmployees = 0;
    }

    employeesListModalOpen() {
    }

    selectEmployee(employee: Employee): void {
        if (employee.selected)
            this.countSelectedEmployees++;
        else
            this.countSelectedEmployees--;
    }

    updateEmployeeList(): void {
        this.viewEmployeesList = (this.employeeFilter)
            ? this.employeesList.filter(employee => employee.fullName
                .match(new RegExp(this.employeeFilter, "gi")))
            : this.employeesList;

        this.employeesPagination.totalElements = this.viewEmployeesList.length;
        this.employeesPagination.number = 1;
        this.employeesPagination.updatePagination();

        // При изменении списка проскроливаем его в начало
        this.employeesListElement.nativeElement.scrollTop = 0;
        this.employeesListViewElement.nativeElement.scrollTop = 0;

        // Обнуляем счетчик на пейджинации
        if (this.employeesPagination.totalPages > 1) {
            setTimeout(() => { this.employeesPaginationElement.updateNewPageCount(); }, 100);
        }

        if (this.employeesPagination.totalPages > 1) {
            setTimeout(() => { this.employeesListViewPagination.updateNewPageCount(); }, 100);
        }
    }

    changeEmployeesListPagination() {
        // При изменении пэйджинации проскроливаем список пользователей в начало
        this.employeesListElement.nativeElement.scrollTop = 0;
        this.employeesListViewElement.nativeElement.scrollTop = 0;
    }

}
