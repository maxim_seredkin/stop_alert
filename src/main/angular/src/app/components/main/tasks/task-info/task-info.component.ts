/**
 * Created by Baser on 25.02.2017.
 */
import { Component, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AppCoreService } from "../../../../services/app-core.service";
import { Task } from "../../../../models/task/task";
import { TaskContext } from "../../../../models/task/task-context";
import { EmployeeContext } from "../../../../models/employee/employee-context";
import { Unit } from "../../../../models/unit";
import { Tag } from "../../../../models/tag/tag";
import { Employee } from "../../../../models/employee/employee";
import { MailTemplate } from "../../../../models/template/template";
import { MailTemplateContext } from "../../../../models/template/template-context";
import { DatePickerSettingsService } from "../../../../services/datePickerSettings";
import { SelectorItem } from "../../../../models/selector-item";
import { SelectComponent } from "ng2-select/index";
import { TreeSelectorComponent } from "../../tree-selector/tree-selector.controller";
import { Modal } from "ngx-modal/index";
import { UserService } from "../../../../services/user.service";
import { Calendar } from "primeng/primeng";
import { Pagination } from '../../../../models/pagination';
import { EmployeesListPaginationComponent } from '../employees-list-pagination/employees-list-pagination.component';

@Component({
    selector: 'task-info',
    template: require('./task-info.component.html')
})

export class TaskInfoComponent {
    public taskEditParam = false;
    public taskEdit = false;

    public selectedStartDate: Date = null;
    public selectedEndDate: Date = null;

    public minEndDate: Date = new Date();

    public creationDateToString: string = "";
    public executeDateToString: string = "";
    public endDateToString: string = "";

    public task: Task;
    public changedTask: Task;

    public editEmployeeList: boolean = false;

    public modalHeader: string = "";
    public modalText: string = "";
    public modalTaskName: string = "";
    public modalSubmitButtonText: string = "";
    public modalDelete: boolean = false;
    public modalRecover: boolean = false;
    public modalAction: string = "";

    public selectedEmployees: string[] = [];
    public selectedEmployeesTemp: string[] = [];

    public selectedUnits: Array<Unit> = [];
    public selectedTags: Array<Tag> = [];

    public filteredSecondName: string = "";

    public employeesList: Array<Employee> = [];
    public selectedEmployeesList: Array<Employee> = [];

    public unitsTree: Array<Unit>;
    public tagsTree: Array<Tag>;

    public mailTemplates: Array<MailTemplate>;
    public mailTemplatesItems: Array<any>;

    public mockPages: Array<Object>;
    private mockPageSelectorValue: any = { id: "" };
    public mockPageItems: Array<Object>;

    private templatesSelectorValue: any = { id: "" };

    public fillFormError: boolean = false;
    public formErrorText: string = "";

    public tasksList: Array<Task>;
    public tasksListItems: Array<any>;

    public taskTemplateSelectorValue: any = [{ id: "" }];

    public vulnerableEmployees: number;

    public taskShowEmployees: Array<Employee> = [];

    public modalError: boolean = false;
    public modalErrorText: string = "";

    @ViewChild('templateSelector') templateSelector: SelectComponent;
    @ViewChild('mockPageSelector') mockPageSelector: SelectComponent;

    @ViewChild('orgSelector') orgSelector: TreeSelectorComponent;
    @ViewChild('tagSelector') tagSelector: TreeSelectorComponent;

    @ViewChild('mailTemplateShowModal')
    private mailTemplateShowModal: Modal;

    @ViewChild('myModal')
    private changeActionModal: Modal;

    @ViewChild('startDateErrorModal')
    private startDateErrorModal: Modal;

    public validLink: boolean = false;

    public customUrl: string = "";

    public selectedRedirectType: string;

    public employeesPagination: Pagination;

    public showLoadingSpinner: boolean = false;

    public countSelectedEmployees: number = 0;

    public countSelectedEmployeesLabel: number = 0;

    public employeeFilter: string = "";

    // список сотрудников
    @ViewChild('employeesList') employeesListElement: ElementRef;
    // список сотрудников (просмотр)
    @ViewChild('employeesListView') employeesListViewElement: ElementRef;
    // Пейджинация
    @ViewChild('employeesPaginationElement') employeesPaginationElement: EmployeesListPaginationComponent;
    @ViewChild('employeesListViewPagination') employeesListViewPagination: EmployeesListPaginationComponent;

    /* возможность открыть модальное окно (используется при обновлении списка пользователей
     при изменении тега или организации)
     */
    public allowOpenEmployeesModal: boolean = true;

    private viewEmployeesList: Array<Employee> = [];

    constructor(
        private core: AppCoreService,
        private route: ActivatedRoute,
        private router: Router,
        private taskContext: TaskContext,
        private employeeContext: EmployeeContext,
        private templateContext: MailTemplateContext,
        private datePicker: DatePickerSettingsService,
        private user: UserService,
        private cdRef: ChangeDetectorRef
    ) {
        this.route.queryParams.subscribe(params => {
            if (params['action'] === 'edit') {
                this.taskEdit = true;
                this.taskEditParam = true;
            }
        });
        this.getUnitsTree();
        this.getTagsTree();
        this.getMockPagesList();
        this.employeesPagination = new Pagination();
    }

    ngAfterViewInit() {
        this.taskContext.getTaskById(this.route.snapshot.params['id'])
            .then((res: Task) => {
                //Зарпет редактирования задачи со статусом отличным от "SUSPENDED"
                if (res.status !== "SUSPENDED" && this.taskEditParam) {
                    this.router.navigate(['/tasks']);
                }

                this.task = res;
                this.changedTask = this.task.clone();

                this.creationDateToString = new Date(this.task.createdDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
                this.executeDateToString = new Date(this.task.startDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
                this.endDateToString = new Date(this.task.endDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
                this.selectedUnits = this.task.units.slice();

                this.selectedTags = this.task.employeeTags.slice();
                this.templatesSelectorValue = this.normalizeTemplateItem(this.task.template);
                this.selectedStartDate = new Date(this.task.startDate);
                this.selectedEndDate = new Date(this.task.endDate);

                this.getTasksList();

                this.taskContext.getVulnerableEmployees(this.task.id)
                    .then((res) => { this.vulnerableEmployees = res; });

                this.updateEmployeesInfo(this.route.snapshot.params['id']);

                if (this.taskEditParam) {
                    //Установка параметров редиректа
                    this.selectedRedirectType = this.task.redirect['redirectType'];

                    if (this.selectedRedirectType === 'MOCK_PAGE_LINK') {
                        setTimeout(() => {
                            for (let mockPage of this.mockPageItems) {
                                if (mockPage['id'] === this.task.redirect['mockPage']) {
                                    this.mockPageSelector.active = [mockPage];
                                    this.mockPageSelectorValue = mockPage;
                                    break;
                                }
                            }
                        }, 100);
                    }

                    if (this.selectedRedirectType === 'CUSTOM_LINK') {
                        this.customUrl = this.task.redirect['customUri'];
                        this.validCustomUrl();
                    }

                    setTimeout(() => {
                        this.templateSelector.active = [this.normalizeTemplateItem(this.task.template)];
                        this.templatesSelectorValue = this.normalizeTemplateItem(this.task.template);
                    }, 100);
                }
            });

        this.templateContext.getMailTemplateList()
            .then((data) => {
                this.mailTemplates = data;
                this.mailTemplatesItems = this.getTemplatesItems(this.mailTemplates);
            });
    }

    updateEmployeesInfo(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.taskContext.getEmployeesByhand(this.route.snapshot.params['id'])
                .then((res: Employee[]) => {
                    this.task.employeesByHand = res;
                    return this.taskContext.getEmployeesByFilter(this.route.snapshot.params['id']);
                })
                .then((res: Employee[]) => {
                    this.task.employeesByFilter = res;
                    return Promise.resolve();
                })
                .then(() => {
                    this.task.employeesByHand.forEach(employee => this.selectedEmployees.push(employee.id));

                    this.taskShowEmployees = this.task.employeesByHand.concat(this.task.employeesByFilter);
                    this.taskShowEmployees.sort((a, b) => {
                        if (a.fullName < b.fullName) return -1;
                        if (a.fullName > b.fullName) return 1;
                        return 0;
                    });

                    this.changedTask = this.task.clone();
                    
                    this.getEmployeesList(false);
                    resolve();
                });
        });
    }

    getMockPagesList() {
        this.core.get('api/mock-pages')
            .then((res) => {
                this.mockPages = res['_embedded'];
                this.mockPageItems = this.getMockPageItems(this.mockPages);
            })
    }

    getMockPageItems(mockPages: Array<Object>) {
        return mockPages.map(item => { return { id: item['id'], text: item['name'] } });
    }

    loadTaskParams() {
        if (this.taskTemplateSelectorValue[0]['id']) {
            this.selectedUnits = [];
            this.selectedTags = [];
            this.taskContext.getTaskById(this.taskTemplateSelectorValue[0]['id'])
                .then((task: Task) => {
                    this.orgSelector.resetValue();
                    this.orgSelector.setValue(task.units);
                    this.tagSelector.resetValue();
                    this.tagSelector.setValue(task.employeeTags);
                    this.templatesSelectorValue = this.normalizeTemplateItem(task.template);
                    this.templateSelector.active = [this.normalizeTemplateItem(task.template)];
                });
        }
    }

    getTasksList() {
        let params = new Object();
        params['sorts'] = ['name:ASC'];

        this.core.get("api/tasks", params)
            .then((res) => {
                this.tasksList = res['_embedded'];
                this.tasksListItems = this.getTasksListItems();
            });
    }

    getTasksListItems() {
        return this.tasksList.map(item => { return { "id": item.id, "text": item.name } });
    }

    public startDateModalError: boolean = false;

    changeTaskStartDate() {
        this.startDateModalError = false;
        if (this.startDateModal < new Date()) {
            this.startDateModalError = true;
        } else {
            this.selectedStartDate = this.startDateModal;
            this.startDateErrorModal.close();
            this.saveTask();
        }
    }

    public startDateError: boolean = false;
    public endDateError: boolean = false;
    public startDateModal: Date = null;

    changedStartDate() {
        this.minEndDate = this.selectedStartDate;

        if (this.selectedStartDate > this.selectedEndDate) {
            this.selectedEndDate = this.selectedStartDate;
        }

        this.startDateError = (this.selectedStartDate < new Date());
    }

    changedEndDate() {
        this.endDateError = (this.selectedEndDate < new Date());
    }


    @ViewChild('startDateSelectorModal')
    public startDateSelectorModal: Calendar;

    public dateSelectorOverlayView = false;
    toggleStartDateSelectorShow(status: boolean) {
        if (status) {
            this.dateSelectorOverlayView = true;
            this.startDateSelectorModal.overlayVisible = status;
        }
        else {
            if (this.dateSelectorOverlayView) {
                this.dateSelectorOverlayView = false;
            }
            else {
                this.startDateSelectorModal.overlayVisible = status;
            }
        }
    }

    saveTask() {
        if (this.startDateError || this.endDateError) {
            return;
        }

        if (this.changedTask.redirect['redirectType'] === 'CUSTOM_LINK') {
            this.changedTask.redirect['customUri'] = this.customUrl;
        }

        this.fillFormError = false;
        this.formErrorText = "";

        this.templatesItemsToModel(this.templatesSelectorValue);
        this.orgItemsItemsToModel(this.selectedUnits);
        this.tagItemsItemsToModel(this.selectedTags);
        this.convertTimeToModel();

        if (this.changedTask.equal(this.task)) {
            if (this.taskEditParam) {
                this.router.navigate(['/tasks']);
            }
            else {
                this.taskEdit = !this.taskEdit;
            }
            return;
        }

        if (!this.changedTask.valid()) {
            this.fillFormError = true;
            this.formErrorText = "Не все поля заполнены!";
            return;
        }

        if (this.selectedStartDate < new Date()) {
            let currentDate = new Date();
            currentDate.setMinutes(currentDate.getMinutes() + 5);
            this.startDateModal = currentDate;
            this.startDateErrorModal.open();
            return;
        }

        this.taskContext.editTask(this.changedTask)
            .then((resp) => {
                if (resp['code'] === 200) {
                    if (this.taskEditParam) {
                        this.router.navigate(['/tasks']);
                        return;
                    }

                    this.taskContext.getTaskById(this.route.snapshot.params['id'])
                        .then((res) => {
                            this.task = res;
                            this.changedTask = this.task.clone();
                            this.creationDateToString = new Date(this.task.createdDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
                            this.executeDateToString = new Date(this.task.startDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
                            this.endDateToString = new Date(this.task.endDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
                            this.taskContext.getVulnerableEmployees(this.task.id)
                                .then((res) => { this.vulnerableEmployees = res; });

                            this.updateEmployeesInfo(this.route.snapshot.params['id']);

                            this.taskEdit = !this.taskEdit;
                        });
                }
                if (resp['code'] === 1010) {
                    this.fillFormError = true;
                    this.formErrorText = "Сотрудники не выбраны!";
                }
                if (resp['code'] === 1011) {
                    this.fillFormError = true;
                    this.formErrorText = "Дата запуска задачи меньше даты создания задачи!";
                    this.changedTask.startDate = null;
                }
                if (resp['code'] === 1012) {
                    this.fillFormError = true;
                    this.formErrorText = "Неверный статус задачи!";
                }
            });
    }


    convertTimeToModel() {
        this.changedTask.startDate = (this.selectedStartDate != null) ? this.selectedStartDate.getTime() : null;
        this.changedTask.endDate = (this.selectedEndDate != null) ? this.selectedEndDate.getTime() : null;
    }

    public getTemplatesItems(templatesList: Array<Object>) {
        let templates = templatesList;

        let templateGroups: Array<Object> = [];
        let templateItems: Array<Object> = [];

        for (let item of templates) {
            if (!item['group']) {
                item['group'] = { id: '1', text: ' ' };
                if (!this.inArray(templateGroups, '1')) {
                    templateGroups.push(item['group']);
                }
                continue;
            }
            if (!this.inArray(templateGroups, item['group']['id'])) {
                templateGroups.push({ id: item['group']['id'], text: item['group']['name'] });
            }
        }

        this.sortTemplateArray(templateGroups);

        for (let group of templateGroups) {
            let groupItem = {};
            groupItem['id'] = group['id'];
            groupItem['text'] = group['text'];
            groupItem['children'] = [];
            for (let template of templates) {
                if (String(template['group']) == "null") {
                    continue;
                }
                if (template['group']['id'] == group['id']) {
                    groupItem['children'].push({ id: template['id'], text: template['name'] });
                }
            }
            templateItems.push(groupItem);
        }

        return templateItems;
    }

    inArray(array: Array<Object>, id: string) {
        return array.some(item => item['id'] === id);
    }

    //Сортировка массива по алфавиту
    sortTemplateArray(unitArray: Array<Object>) {
        unitArray.sort((a, b) => {
            var textA = a['text'].toUpperCase();
            var textB = b['text'].toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
    }

    public templatesItemsToModel(item) {
        this.changedTask.template.id = item['id'];
    }

    toggleEdit() {
        this.creationDateToString = new Date(this.changedTask.createdDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
        this.executeDateToString = new Date(this.changedTask.startDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
        this.endDateToString = new Date(this.changedTask.endDate).toLocaleString("ru", { day: 'numeric', month: 'long', year: 'numeric' });
        this.selectedUnits = this.changedTask.units.slice();
        this.selectedTags = this.changedTask.employeeTags.slice();
        this.selectedEmployeesTemp = [];
        this.selectedEmployees = [];

        this.changedTask.employeesByFilter.forEach(employee => this.selectedEmployees.push(employee.id));

        this.changedTask.employeesByHand.forEach(employee => {
            if (this.selectedEmployees.indexOf(employee.id) === -1) {
                this.selectedEmployeesTemp.push(employee.id)
            }
        });

        this.selectedStartDate = new Date(this.changedTask.startDate);
        this.selectedEndDate = new Date(this.changedTask.endDate);

        setTimeout(() => {
            this.templateSelector.active = [this.normalizeTemplateItem(this.task.template)];
            this.templatesSelectorValue = this.normalizeTemplateItem(this.task.template);
        }, 100);

        //Установка параметров редиректа
        this.selectedRedirectType = this.changedTask.redirect['redirectType'];

        if (this.selectedRedirectType === 'MOCK_PAGE_LINK') {
            setTimeout(() => {
                for (let mockPage of this.mockPageItems) {
                    if (mockPage['id'] === this.changedTask.redirect['mockPage']) {
                        this.mockPageSelector.active = [mockPage];
                        this.mockPageSelectorValue = mockPage;
                    }
                }
            }, 100);
        }

        if (this.selectedRedirectType == 'CUSTOM_LINK') {
            this.customUrl = this.changedTask.redirect['customUri'];
            this.validCustomUrl();
        }

        this.taskEdit = !this.taskEdit;
    }

    normalizeTemplateItem(template: Object) {
        return {
            id: template['id'],
            text: template['name'],
            parent: template['group']
        };
    }

    getUnitsTree() {
        this.core.get("api/units/tree")
            .then((res) => {
                this.unitsTree = res['_embedded'];
            });
    }

    getTagsTree() {
        this.core.get("api/employee-tags/tree")
            .then((res) => {
                this.tagsTree = res['_embedded'];
            });
    }

    changeTask(id, action) {
        this.modalTaskName = this.task.name;
        this.modalErrorText = "";
        this.modalError = false;

        switch (action) {
            case "pause":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = true;
                this.modalHeader = "Приостановка задачи";
                this.modalText = "Вы уверены, что хотите приостановить задачу ";
                this.modalSubmitButtonText = "Приостановить";
                break;
            case "resume":
                this.modalAction = action;
                this.modalRecover = true;
                this.modalDelete = false;
                this.modalHeader = "Возобновление задачи";
                this.modalText = "Вы уверены, что хотите возобновить выполнение задачи ";
                this.modalSubmitButtonText = "Возобновить";
                break;
            case "cancel":
                this.modalAction = action;
                this.modalRecover = false;
                this.modalDelete = true;
                this.modalHeader = "Отмена задачи";
                this.modalText = "Вы уверены, что хотите отменить задачу ";
                this.modalSubmitButtonText = "Отменить";
                break;
        }
    }

    getEmployeesList(edit: boolean): void {
        this.showLoadingSpinner = true;
        if (edit) {
            this.employeeContext.getEmployeeList(this.getSimpleRequestParams()).then((data) => {
                // выбираем сотрудников, которые были выбраны вручную
                this.selectEmployees(data, this.changedTask.employeesByHand, false);
                // выбираем сотрудников, которые попадают под действие фильтра
                this.getEmployeesListByFilters().then(selectedEmployees => {
                    this.selectEmployees(data, selectedEmployees, true);
                    this.countSelectedEmployees = this.getSelectedEmployeeList(this.employeesList).length;
                    this.updateEmployeeList();
                    // убираем спиннер
                    this.showLoadingSpinner = false;
                });
                // в режиме редактирования получаем всех сотрудников
                this.employeesList = data;
            });
        } else {
            // в режиме чтения получаем только выбранных сотрудников
            this.employeesList = this.changedTask.employeesByHand.concat(this.changedTask.employeesByFilter);
            this.countSelectedEmployees = this.employeesList.length;
            this.countSelectedEmployeesLabel = this.countSelectedEmployees;
            this.updateEmployeeList();
            // убираем спиннер
            this.showLoadingSpinner = false;
        }
    }

    private selectEmployees(employeesList: Array<Employee>, selectedEmployeesList: Array<Employee>, disabled: boolean) {
        employeesList = employeesList.sort((e1, e2) => e1.id.localeCompare(e2.id));
        selectedEmployeesList = selectedEmployeesList.sort((e1, e2) => e1.id.localeCompare(e2.id));

        let currentIndex: number = 0;
        let currentEmployee: Employee = employeesList[currentIndex];

        selectedEmployeesList.forEach(selectEmployee => {
            while (true) {
                if (selectEmployee.id == currentEmployee.id) {
                    currentEmployee.disabled = disabled;
                    currentEmployee.selected = true;
                    break;
                } else {
                    if ((++currentIndex) == employeesList.length)
                        break;
                    currentEmployee = employeesList[currentIndex];
                }
            }
        });
    }

    getEmployeesListByFilters(): Promise<Array<Employee>> {
        return new Promise((resolve, cancel) => {
            if ((this.selectedUnits.length == 0) && (this.selectedTags.length == 0))
                resolve([]);
            else
                this.employeeContext.getEmployeeList(this.getFilteredRequestParams())
                    .then((data) => {
                        resolve(data);
                    });
        });
    }

    getSelectedEmployeeList(employees: Array<Employee>): Array<Employee> {
        return employees.filter(employee => employee.selected);
    }

    resetFilterBySecondName() {
        this.employeeFilter = "";
    }

    getSimpleRequestParams() {
        let params = new Object();
        if (this.filteredSecondName != "" && this.filteredSecondName != undefined) {
            params['searchs'] = "'secondName':'" + this.filteredSecondName + "'";
        }
        params['filters'] = "";
        if (params['filters'] === "") {
            params['filters'] += "'status' EQ 'ENABLED'";
        }
        else {
            params['filters'] += ",'status' EQ 'ENABLED'";
        }
        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        params['projection'] = 'SELECTOR';
        return params;
    }

    getFilteredRequestParams(): Object {
        let params = new Object();

        params['filters'] = "";
        if (this.selectedUnits) {
            for (var unit of this.selectedUnits) {
                if (params['filters'] === "") {
                    params['filters'] += "EXTRA:'units' EQ '" + unit['id'] + "'";
                }
                else {
                    params['filters'] += ",EXTRA:'units' EQ '" + unit['id'] + "'";
                }
            }
        }
        if (this.selectedTags) {
            for (var tag of this.selectedTags) {
                if (params['filters'] === "") {
                    params['filters'] += "EXTRA:'employeeTags' EQ '" + tag['id'] + "'";
                }
                else {
                    params['filters'] += ",EXTRA:'employeeTags' EQ '" + tag['id'] + "'";
                }
            }
        }
        if (params['filters'] === "") {
            params['filters'] += "'status' EQ 'ENABLED'";
        }
        else {
            params['filters'] += ",'status' EQ 'ENABLED'";
        }
        params['sorts'] = ['secondName:ASC', 'firstName:ASC', 'middleName:ASC'];
        params['projection'] = 'SELECTOR';
        return params;
    }

    selectEmployee(employee: Employee): void {
        if (employee.selected)
            this.countSelectedEmployees++;
        else
            this.countSelectedEmployees--;
    }

    updateEmployeeList(): void {
        this.viewEmployeesList = (this.employeeFilter)
            ? this.employeesList.filter(employee => employee.fullName
                .match(new RegExp(this.employeeFilter, "gi")))
            : this.employeesList;

        this.employeesPagination.totalElements = this.viewEmployeesList.length;
        this.employeesPagination.number = 1;
        this.employeesPagination.updatePagination();

        // При изменении списка проскроливаем его в начало
        this.employeesListElement.nativeElement.scrollTop = 0;
        this.employeesListViewElement.nativeElement.scrollTop = 0;

        // Обнуляем счетчик на пейджинации
        if (this.employeesPagination.totalPages > 1) {
            setTimeout(() => { this.employeesPaginationElement.updateNewPageCount(); }, 100);
        }

        if (this.employeesPagination.totalPages > 1) {
            setTimeout(() => { this.employeesListViewPagination.updateNewPageCount(); }, 100);
        }
    }

    changeEmployeesListPagination() {
        // При изменении пэйджинации проскроливаем список пользователей в начало
        this.employeesListElement.nativeElement.scrollTop = 0;
        this.employeesListViewElement.nativeElement.scrollTop = 0;
    }

    public refreshTemplateValue(value: any): void {
        if (value.length === 0) {
            this.templatesSelectorValue = { id: "" };
        }
        else {
            this.templatesSelectorValue = value;
            if (this.changedTask.redirect['redirectType'] == 'TEMPLATE_LINK') {
                this.changedTask.redirect['mailTemplate'] = value['id'];
            }
        }
    }

    public refreshMockPageValue(value: any): void {
        if (value.length == 0) {
            this.validLink = false;
            this.mockPageSelectorValue = { id: "" };
        }
        else {
            this.validLink = true;
            this.mockPageSelectorValue = value;
            if (this.changedTask.redirect['redirectType'] == 'MOCK_PAGE_LINK') {
                this.changedTask.redirect['mockPage'] = value['id'];
            }
        }
    }

    public refreshTaskTemplateValue(value: any) {
        if (value.length == 0) {
            this.taskTemplateSelectorValue = [{ id: "" }];
        }
        else {
            this.taskTemplateSelectorValue = [value];
        }
    }

    orgSelectorChange(selected) {
        // Дизэйблим кнопки открытия модалки сотрудников
        this.allowOpenEmployeesModal = false;
        // При фильтрации элементов в селекторе выбора организаций киадется событие change, игнорим этот кейс
        if (selected.target)
            return;

        this.selectedUnits = selected;
        this.orgItemsToModel(this.selectedUnits);

        this.getEmployeesListByFilters().then(selectedEmployees => {
            this.changedTask.employeesByFilter = selectedEmployees.map(employee => {
                employee.selected = employee.disabled = true;
                return employee;
            });
            this.changedTask.employeesByHand = [];
            // Активируем кнопки обратно
            this.allowOpenEmployeesModal = true;
        });
    }

    tagSelectorChange(selected) {
        // Дизэйблим кнопки открытия модалки сотрудников
        this.allowOpenEmployeesModal = false;
        this.selectedTags = selected;
        this.tagItemsItemsToModel(this.selectedTags);

        this.getEmployeesListByFilters().then(selectedEmployees => {
            this.changedTask.employeesByFilter = selectedEmployees.map(employee => {
                employee.selected = employee.disabled = true;
                return employee;
            });
            this.changedTask.employeesByHand = [];
            // Активируем кнопки обратно
            this.allowOpenEmployeesModal = true;
        });
    }

    orgItemsToModel(selectedArray: Array<any>) {
        this.changedTask.units = selectedArray;
    }

    onOrgSelectorInit(value) {
        this.selectedUnits = value;
    }

    onTagSelectorInit(value) {
        this.selectedTags = value;
    }

    saveSelectedEmployees() {
        this.changedTask.employeesByFilter = this.employeesList.filter(employee => employee.selected && employee.disabled);
        this.changedTask.employeesByHand = this.employeesList.filter(employee => employee.selected && !employee.disabled);
        this.employeesList = [];
    }

    cancelSelectedEmployees() {
    }

    tagItemsItemsToModel(selectedArray: Array<SelectorItem>) {
        this.changedTask.employeeTags = [];
        if (selectedArray.length != 0) {
            for (var item of selectedArray) {
                this.changedTask.employeeTags.push(item);
            }
        }
    }

    orgItemsItemsToModel(selectedArray: Array<any>) {
        this.changedTask.units = [];
        if (selectedArray.length != 0) {
            for (var item of selectedArray) {
                this.changedTask.units.push(item);
            }
        }
    }


    changeTaskAction() {
        this.taskContext.changeTaskStatus(this.task.id, this.modalAction)
            .then((res) => {
                if (res['code'] == 1011) {
                    this.modalError = true;
                    this.modalErrorText = res['_embedded'];
                }
                if (res['code'] == 200) {
                    this.changeActionModal.close();
                }
                if (this.modalAction == 'cancel') {
                    this.router.navigate(['tasks']);
                }
                else {
                    this.taskContext.getTaskById(this.task.id)
                        .then((res: Task) => {
                            this.task = res;
                            if (this.task) {
                                this.changedTask = this.task.clone();
                            }
                        });
                }
            });
    }

    public templateName: string = "";
    public templateHtml: string = "";

    templatePreviewLoad(templateId?: string) {
        let id: string;
        if (templateId != null) {
            id = templateId;
        }
        else {
            id = this.templatesSelectorValue['id'];
        }

        if (id != '') {
            this.core.get("api/mail-templates/" + id + "/example")
                .then((res) => {
                    this.templateHtml = res['_embedded']['body'];
                    this.templateName = res['_embedded']['name'];
                    this.mailTemplateShowModal.open();
                })
        }
    }
    validCustomUrl() {
        let URL_REGEXP = new RegExp(
            "^" +
            // protocol identifier
            "(?:(?:https?|ftp)://)" +
            // user:pass authentication
            "(?:\\S+(?::\\S*)?@)?" +
            "(?:" +
            // IP address exclusion
            // private & local networks
            //"(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
            //"(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
            //"(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
            // IP address dotted notation octets
            // excludes loopback network 0.0.0.0
            // excludes reserved space >= 224.0.0.0
            // excludes network & broacast addresses
            // (first & last IP address of each class)
            "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
            "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
            "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
            "|" +
            // host name
            "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
            // domain name
            "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
            // TLD identifier
            "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
            // TLD may end with dot
            "\\.?" +
            ")" +
            // port number
            "(?::\\d{2,5})?" +
            // resource path
            "(?:[/?#]\\S*)?" +
            "$", "i"
        );

        if (this.customUrl.match(URL_REGEXP)) {
            this.validLink = true;
        } else {
            this.validLink = false;
        }
    }

    redirectTypeChanged() {
        setTimeout(() => {
            if (this.selectedRedirectType == 'TEMPLATE_LINK') {
                this.validLink = true;
                this.changedTask.redirect = {};
                this.changedTask.redirect['redirectType'] = 'TEMPLATE_LINK';
                this.changedTask.redirect['mailTemplate'] = (this.templatesSelectorValue['id'] != '') ? this.templatesSelectorValue['id'] : '';
            }
            if (this.selectedRedirectType == 'MOCK_PAGE_LINK') {
                this.validLink = false;
                this.changedTask.redirect = {};
                this.changedTask.redirect['redirectType'] = 'MOCK_PAGE_LINK';
                this.changedTask.redirect['mockPage'] = '';
                this.mockPageSelectorValue = { id: '' };

            }
            if (this.selectedRedirectType == 'CUSTOM_LINK') {
                this.validLink = true;
                this.changedTask.redirect = {};
                this.changedTask.redirect['redirectType'] = 'CUSTOM_LINK';
                this.changedTask.redirect['customUri'] = this.customUrl;
            }
        }, 50);
    }

    goToPage() {
        if (this.selectedRedirectType == 'TEMPLATE_LINK') {
            for (let template of this.mailTemplates) {
                if (template['id'] == this.templatesSelectorValue['id']) {
                    window.open(template['redirectUri']);
                    break;
                }
            }
        }
        if (this.selectedRedirectType == 'MOCK_PAGE_LINK') {
            for (let mockPage of this.mockPages) {
                if (mockPage['id'] == this.mockPageSelectorValue['id']) {
                    window.open(mockPage['redirectUri']);
                    break;
                }
            }
        }
        if (this.selectedRedirectType == 'CUSTOM_LINK') {
            window.open(this.customUrl);
        }
    }

    public employeesListShow: boolean = false;

    employeesListModalOpen() {
        this.employeesList = [];
        this.viewEmployeesList = [];
        this.employeesListShow = true;
        this.employeesPagination.number = 1;
        if (!this.taskEdit) {
            this.employeesPagination.totalElements = this.taskShowEmployees.length;
        }
        this.employeesPagination.updatePagination();
    }

    employeesListModalClose() {
        this.employeesList = [];
        this.viewEmployeesList = [];
        // Задаем начальное значение пэйджинации и обновляем ее
        this.employeesPagination.totalElements = 0;
        this.employeesPagination.number = 1;
        this.employeesPagination.updatePagination();
        // Обнуляем фильтр по фамилии + обнуляем количество выбранных сотрудников
        this.resetFilterBySecondName();
        this.countSelectedEmployeesLabel = this.countSelectedEmployees;
        this.countSelectedEmployees = 0;
    }
}