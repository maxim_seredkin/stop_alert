/**
 * Created by Baser on 26.03.2017.
 */
import { Component } from '@angular/core';


@Component({
    selector: 'not-found',
    template: require('./not-found.component.html'),
    styles: [require('../../../../style/login.scss')],
})
export class NotFoundComponent {

}