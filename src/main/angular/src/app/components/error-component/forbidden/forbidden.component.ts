/**
 * Created by Baser on 27.03.2017.
 */
import { Component } from '@angular/core';


@Component({
    selector: 'not-found',
    template: require('./forbidden.component.html'),
    styles: [require('../../../../style/login.scss')],
})
export class ForbiddenComponent {

}