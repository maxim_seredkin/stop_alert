import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';

import {AppCoreService} from './services/app-core.service';

import {MainModule} from './components/main/main.module';
import {LoginModule} from './components/login/login.module';
import {LoginComponent} from './components/login/login.component';
import {MainComponent} from './components/main/main.component';
import {AuthGuardService} from './services/auth-guard.service';
import {UserService} from './services/user.service';
import {MyAccountComponent} from './components/main/my-account/my-account.component';
import {UsersListComponent} from './components/main/users/users-list/users-list.component';
import {UserInfoComponent} from './components/main/users/user-info/user-info.component';
import {UserContext} from './models/user/user-context';
import {UserCreateComponent} from './components/main/users/user-create/user-create.component';
import {AuthorityContext} from './models/user/authority-context';
import {TaskCreateComponent} from './components/main/tasks/task-create/task-create.component';
import {DatePickerSettingsService} from './services/datePickerSettings';
import {TaskContext} from './models/task/task-context';
import {MailTemplateContext} from './models/template/template-context';
import {EmployeeContext} from './models/employee/employee-context';
import {TasksListComponent} from './components/main/tasks/tasks-list/tasks-list.component';
import {LoginPageGuardService} from './services/login-page-guard.service';
import {TaskInfoComponent} from './components/main/tasks/task-info/task-info.component';
import {EmployeesListComponent} from './components/main/employees/employees-list/employees-list.component';
import {EmployeeCreateComponent} from './components/main/employees/employee-create/employee-create.component';
import {EmployeeInfoComponent} from './components/main/employees/employee-info/employee-info.component';
import {EmployeeTagsEditComponent} from './components/main/employee-tags/employee-tags-edit/employee-tags-edit.component';
import {EmployeeTagsShowComponent} from './components/main/employee-tags/employee-tags-show/employee-tags-show.component';
import {EmployeesLoadComponent} from './components/main/employees/employees-load/employees-load.component';
import {UnitsEditComponent} from './components/main/units/units-edit/units-edit.component';
import {UnitsShowComponent} from './components/main/units/units-show/units-show.component';
import {TaskReportComponent} from './components/main/reports/task/task-report/task-report.component';
import {NotFoundComponent} from './components/error-component/not-found/not-found.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {ForbiddenComponent} from './components/error-component/forbidden/forbidden.component';
import {TaskReportsListComponent} from './components/main/reports/task/task-reports-list/task-reports-list.component';
import {HomeComponent} from './components/main/home/home.component';
import {MailServerSettingsComponent} from './components/main/mail-server/mail-server-settings.component';
import {LicenseSettingsComponent} from './components/main/license/license-settings.component';
import {UnitReportsListComponent} from './components/main/reports/unit/unit-reports-list/unit-reports-list.component';
import {EmployeeReportsListComponent} from './components/main/reports/employee/employee-reports-list/employee-reports-list.component';
import {EmployeeReportComponent} from './components/main/reports/employee/employee-report/employee-report.component';
import {UnitReportComponent} from './components/main/reports/unit/unit-report/unit-report.component';
import {UnitsLoadComponent} from "./components/main/units/units-load/units-load.component";
import {Config} from "./services/config";
import {MailTemplatesListComponent} from "./components/main/mail-templates/mail-templates-list/mail-templates-list.component";
import {MailTemplateCreateComponent} from "./components/main/mail-templates/mail-template-create/mail-template-create.component";
import {MailTemplateInfoComponent} from "./components/main/mail-templates/mail-template-info/mail-template-info.component";
import {SystemSettingsComponent} from "./components/main/system-settings/system-settings.component";
import {LdapSettingsComponent} from './components/main/ldap-settings/ldap-settings.component';

export const routes:Routes = [
    {path: '', component: HomeComponent, canActivate: [AuthGuardService]},
    {
        path: '', component: MainComponent, canActivate: [AuthGuardService], children: [
        {path: 'account', component: MyAccountComponent},
        {
            path: 'users', canActivateChild: [AuthGuardService], children: [
            {path: '', component: UsersListComponent, data: {roles: ['ROLE_ADMINISTRATOR']}},
            {path: 'new', component: UserCreateComponent, data: {roles: ['ROLE_ADMINISTRATOR']}},
            {path: ':id', component: UserInfoComponent, data: {roles: ['ROLE_ADMINISTRATOR']}}
        ]
        },
        {
            path: 'tasks', canActivateChild: [AuthGuardService], children: [
            {
                path: '',
                component: TasksListComponent,
                data: {roles: ['ROLE_ADMINISTRATOR', 'ROLE_SECURITY_OFFICER'], needLicense: true}
            },
            {
                path: 'new',
                component: TaskCreateComponent,
                data: {roles: ['ROLE_ADMINISTRATOR', 'ROLE_SECURITY_OFFICER'], needLicense: true}
            },
            {
                path: ':id',
                component: TaskInfoComponent,
                data: {roles: ['ROLE_ADMINISTRATOR', 'ROLE_SECURITY_OFFICER'], needLicense: true}
            }
        ]
        },
        {
            path: 'employees', canActivateChild: [AuthGuardService], children: [
            {
                path: '',
                component: EmployeesListComponent,
                data: {roles: ['ROLE_ADMINISTRATOR', 'ROLE_SECURITY_OFFICER'], needLicense: true}
            },
            {
                path: 'new',
                component: EmployeeCreateComponent,
                data: {roles: ['ROLE_ADMINISTRATOR', 'ROLE_SECURITY_OFFICER'], needLicense: true}
            },
            {
                path: 'load',
                component: EmployeesLoadComponent,
                data: {roles: ['ROLE_ADMINISTRATOR', 'ROLE_SECURITY_OFFICER'], needLicense: true}
            },
            {
                path: ':id',
                component: EmployeeInfoComponent,
                data: {roles: ['ROLE_ADMINISTRATOR', 'ROLE_SECURITY_OFFICER'], needLicense: true}
            }
        ]
        },
        {
            path: 'employee-tags', canActivateChild: [AuthGuardService], children: [
            {path: '', component: EmployeeTagsEditComponent, data: {roles: ['ROLE_ADMINISTRATOR'], needLicense: true}},
            {path: ':id', component: EmployeeTagsShowComponent, data: {roles: [], needLicense: true}}
        ]
        },
        {
            path: 'units', canActivateChild: [AuthGuardService], children: [
            {path: '', component: UnitsEditComponent, data: {roles: ['ROLE_ADMINISTRATOR'], needLicense: true}},
            {path: 'load', component: UnitsLoadComponent, data: {roles: ['ROLE_ADMINISTRATOR'], needLicense: true}},
            {path: ':id', component: UnitsShowComponent, data: {roles: [], needLicense: true}}
        ]
        },
        {
            path: 'reports', children: [
            {path: '', pathMatch: 'full', redirectTo: '/'},
            {
                path: 'tasks', canActivateChild: [AuthGuardService], children: [
                {path: '', component: TaskReportsListComponent, data: {roles: []}},
                {path: ':id', component: TaskReportComponent, data: {roles: []}}

            ]
            },
            {
                path: 'units', canActivateChild: [AuthGuardService], children: [
                {path: '', component: UnitReportsListComponent, data: {roles: []}},
                {path: ':id', component: UnitReportComponent, data: {roles: []}}

            ]
            },
            {
                path: 'employees', canActivateChild: [AuthGuardService], children: [
                {path: '', component: EmployeeReportsListComponent, data: {roles: []}},
                {path: ':id', component: EmployeeReportComponent, data: {roles: []}}

            ]
            },
        ]
        },
        {
            path: 'settings', canActivateChild: [AuthGuardService], children: [
            {path: 'mail-server', component: MailServerSettingsComponent, data: {roles: ['ROLE_ADMINISTRATOR']}},
            {path: 'license', component: LicenseSettingsComponent, data: {roles: ['ROLE_ADMINISTRATOR']}},
            {path: 'system-settings', component: SystemSettingsComponent, data: {roles: ['ROLE_ADMINISTRATOR']}},
            {path: 'ldap-settings', component: LdapSettingsComponent, data: {roles: ['ROLE_ADMINISTRATOR']}}
        ]
        },
        {
            path: 'mail-templates', canActivateChild: [AuthGuardService], children: [
            {path: '', component: MailTemplatesListComponent, data: {roles: ['ROLE_ADMINISTRATOR'], needLicense: true}},
            {
                path: 'new',
                component: MailTemplateCreateComponent,
                data: {roles: ['ROLE_ADMINISTRATOR'], needLicense: true}
            },
            {path: ':id', component: MailTemplateInfoComponent, data: {roles: ['ROLE_ADMINISTRATOR'], needLicense: true}},
        ]
        }
    ]
    },
    {path: 'main', component: HomeComponent, canActivate: [AuthGuardService]},
    {path: 'login', component: LoginComponent, canActivate: [LoginPageGuardService]},
    {path: 'forgot-password', component: ForgotPasswordComponent}, /*, canActivate: [LoginPageGuardService]*/
    {path: '404', component: NotFoundComponent},
    {path: '403', component: ForbiddenComponent},
    {path: '**', redirectTo: '404'}
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routes),
        MainModule,
        LoginModule
    ],
    providers: [
        AppCoreService,
        Config,
        UserService,
        AuthGuardService,
        LoginPageGuardService,
        UserContext,
        AuthorityContext,
        DatePickerSettingsService,
        TaskContext,
        MailTemplateContext,
        EmployeeContext
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}
