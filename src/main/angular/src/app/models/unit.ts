import {SelectorItem} from "./selector-item";
/**
 * Created by Baser on 03.02.2017.
 */
export class Unit extends  SelectorItem{
    constructor(
        public id: string,
        public name: string,
        public nodes: Array<Unit>,
        public parent: Unit,
        public used?: boolean,
        public operations?: Array<any>
    ) {
        super(id, name, nodes, parent);
    }
}