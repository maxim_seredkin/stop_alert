import {SelectorItem} from "../selector-item";
/**
 * Created by Baser on 14.02.2017.
 */
export class Tag extends SelectorItem{
    constructor(
        public id: string,
        public name: string,
        public nodes: Array<Tag>,
        public parent: Tag,
        public used?: boolean
    ) {
        super(id, name, nodes, parent);
    }
}