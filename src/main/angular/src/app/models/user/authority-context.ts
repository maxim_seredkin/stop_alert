/**
 * Created by Baser on 03.02.2017.
 */
import {Injectable} from "@angular/core";
import {AppCoreService} from "../../services/app-core.service";
import {Authority} from "./authority";

@Injectable()
export class AuthorityContext {
    constructor(private core: AppCoreService) {

    }

    getAuthoritiesList() {
        return this.core.get('api/authorities/')
            .then((res: Array<any>) => {
                var data = res['_embedded'];
                var authorities = [];

                for (var item of data) {
                    authorities.push(new Authority(item['id'], item['authority'], item['displayName']));
                }
                return Promise.resolve(authorities);
            });
    }
}