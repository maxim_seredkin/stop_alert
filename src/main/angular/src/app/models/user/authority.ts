/**
 * Created by Baser on 02.02.2017.
 */
export class Authority {
    constructor(
        public id: string,
        public authority: string,
        public displayName: string,
        public description?: string
    ) {

    }

    equal(model: Authority) {
        return (this.id == model.id);
    }

    valid() {
        return (this.id != "" && this.id != null);
    }
}