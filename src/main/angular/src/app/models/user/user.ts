import {Authority} from "./authority";
import {Unit} from "../unit";
/**
 * Created by Baser on 02.02.2017.
 */

export class User {
    constructor(public id:string,
                public username:string,
                public secondName:string,
                public firstName:string,
                public middleName:string,
                public email:string,
                public authority: Authority,
                public units:Array<Unit>,
                public status: string,
                public registerDate: number,
                public fullName: string,
                public shortName: string,
                private type: string = null,
                public password: any
    ) {
        if (type) {
            switch (type) {
                case 'new':
                    this.password = "";
                    break;
                case "edit":
                    this.password = {
                        "old": "",
                        "renewed": ""
                    };
                    break;
            }
        }
    }

    clone() {
        return new User(
            this.id,
            this.username,
            this.secondName,
            this.firstName,
            this.middleName,
            this.email,
            new Authority(this.authority.id, this.authority.authority, this.authority.displayName, this.authority.description),
            this.units,
            this.status,
            this.registerDate,
            this.fullName,
            this.shortName,
            this.type,
            this.password
        );
    }

    valid() {
        return !!(
        this.username
        && this.email
        && this.authority.valid());
    }

    validEmail() {
        var EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return (this.email.length >= 5 && EMAIL_REGEXP.test(this.email));
    }

    validPassword(password) {
        var PASSWORD_REGEXP = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,64}$/;
        return (PASSWORD_REGEXP.test(password));
    }

    equal(model:User) {
        return (this.id == model.id) &&
            (this.username == model.username) &&
            (this.secondName == model.secondName) &&
            (this.firstName == model.firstName) &&
            (this.middleName == model.middleName) &&
            (this.email == model.email) &&
            (this.authority.equal(model.authority)) &&
            (this.unitsEqual(this.units, model.units));
    }

    unitsEqual(firstUnits: Array<Unit>, secondUnits: Array<Unit>) {
        var flag = true;
        if(firstUnits.length == secondUnits.length) {
            for (var unit of firstUnits) {
                for( var unit2 of secondUnits) {
                    if(unit.id != unit2.id) {
                        flag = false;
                    }
                    else {
                        flag = true;
                        break;
                    }
                }
            }
        }
        else {
            flag = false;
        }

        return flag;
    }
}