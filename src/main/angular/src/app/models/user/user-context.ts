import {User} from "./user";
import {Authority} from "./authority";
import {AppCoreService} from "../../services/app-core.service";
import {Injectable} from "@angular/core";
import {Unit} from "../unit";
/**
 * Created by Baser on 02.02.2017.
 */
@Injectable()
export class UserContext {

    constructor(private core: AppCoreService) {

    }

    getUserById(id: string, type: string) {
        var user = {};
        return this.core.get("api/users/"+id)
            .then((res) => {
                user = res['_embedded'];

                var units = [];
                for (let unit of user['units']) {
                    units.push(new Unit(unit['id'], unit['name'], unit['nodes'], unit['parent']));
                }
                return Promise.resolve(new User(
                    user['id'], user['username'], user['secondName'],
                    user['firstName'], user['middleName'], user['email'],
                    new Authority(
                        user['authority']['id'], user['authority']['authority'],
                        user['authority']['displayName'], user['authority']['description']
                    ), units, user['status'], user['registerDate'], user['fullName'], user['shortName'], type, null));
            });
    }

    createUser(user: User) {
        let authorityId = user.authority.id;
        let data = JSON.parse(JSON.stringify(user));
        data['authority'] = authorityId;
        let postData = JSON.parse('{"form": '+JSON.stringify(data)+'}');
        return this.core.post_json("api/users/"+user.id, postData);
    }

    editUser(user: User) {
        let authorityId = user.authority.id;
        let data = JSON.parse(JSON.stringify(user));
        data['authority'] = authorityId;
        data['units'] = [];
        if(user.units.length != 0) {
            for(var unit of user.units) {
                data['units'].push(unit.id);
            }
        }
        else {
            data['units'] = [];
        }
        if (!data['password']) {
            data['password'] = null;
        }
        let postData = JSON.parse('{"form": '+JSON.stringify(data)+'}');
        return this.core.put("api/users/"+user.id, postData);
    }

    changeUserStatus(id: string, action: string) {
        return this.core.put("api/users/" + id + "/" + action);
    }

    getUsersList() {

    }

    userExist(username: string) {
        let filters = new Object();
        filters['filters'] = "'username' EQ '" + username + "'";

        //let filters = {"filters": "'username' EQ '"+username+"'"};
        return this.core.get('api/users/exists/', filters);
    }

    newUser() {
        return new User("", "", "", "", "", "", new Authority("", "", "", ""), [], "", 0, "", "", "", "");
    }

}