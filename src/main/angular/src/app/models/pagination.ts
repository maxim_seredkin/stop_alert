/**
 * Created by Baser on 30.01.2017.
 */
export class Pagination {
    public size: number;
    public totalElements: number;
    public totalPages: number;
    public number: number;

    constructor() {
        this.size = 10;
        this.totalElements = 0;
        this.totalPages = 0;
        this.number = 1;
    }

    updatePagination() {
        let division = this.totalElements / this.size;
        this.totalPages = Math.floor(this.totalElements / this.size);
        this.totalPages += ((division % 1) != 0) ? 1 : 0;
    }
}