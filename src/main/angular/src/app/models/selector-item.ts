/**
 * Created by Baser on 10.02.2017.
 */
export class SelectorItem {
    constructor(
        public id: string,
        public name: string,
        public nodes: Array<SelectorItem>,
        public parent: SelectorItem,
        public used?: boolean,
        public errors?: Array<any>
    ) {
        
    }
}