import {Unit} from "../unit";
import {Tag} from "../tag/tag";
import {Employee} from "../employee/employee";
import {MailTemplate} from "../template/template";

/**
 * Created by Baser on 14.02.2017.
 */
export class Task {

  constructor(public id: string,
              public name: string,
              public units: Array<Unit>,
              public employeeTags: Array<Tag>,
              public employeesByHand: Array<Employee>,
              public template: MailTemplate,
              public startDate: number,
              public endDate: number,
              public redirect: Object,
              //public period: string,
              public author?: string,
              public createdDate?: number,
              public status?: string,
              public employeesByFilter?: Array<Employee>) {
    if (employeesByHand == null)
      this.employeesByHand = [];

    if (employeesByFilter == null)
      this.employeesByFilter = [];
  }

  valid() {
    return !!(
      this.name &&
      this.template.id &&
      this.startDate &&
      this.endDate &&
      this.validRedirect()
    );
  }

  validRedirect() {
    if (this.redirect['redirectType'] == 'TEMPLATE_LINK') {
      return (this.redirect['mailTemplate'] != '');
    }
    if (this.redirect['redirectType'] == 'MOCK_PAGE_LINK') {
      return (this.redirect['mockPage'] != '');
    }
    if (this.redirect['redirectType'] == 'CUSTOM_LINK') {
      return (this.redirect['customUri'] != '');
    }
  }

  clone() {
    return new Task(
      this.id,
      this.name,
      this.units.slice(),
      this.employeeTags.slice(),
      this.employeesByHand.slice(),
      new MailTemplate(this.template.id, this.template.name, this.template.status, this.template.group, this.template.description, this.template.subject, this.template.fromPersonal, this.template.fromAddress, this.template.redirectUri),
      this.startDate,
      this.endDate,
      this.redirect,
      this.author,
      this.createdDate,
      this.status,
      this.employeesByFilter.slice()
    );
  }

  equal(model: Task) {
    return (this.id == model.id) &&
      (this.name == model.name) &&
      (this.unitsEqual(this.units, model.units)) &&
      (this.unitsEqual(this.employeeTags, model.employeeTags)) &&
      (this.unitsEqual(this.employeesByHand, model.employeesByHand)) &&
      (this.template.equal(model.template)) &&
      (this.startDate == model.startDate) &&
      (this.endDate == model.endDate) &&
      (this.author == model.author) &&
      (this.createdDate == model.createdDate) &&
      (this.status == model.status) &&
      (this.unitsEqual(this.employeesByFilter, model.employeesByFilter)) &&
      this.redirectTypeEqual(this.redirect, model.redirect);
  }

  redirectTypeEqual(redirect: Object, redirectModel: Object) {
    if (redirect['redirectType'] != redirectModel['redirectType']) {
      return false;
    }
    if (redirect['redirectType'] == 'MOCK_PAGE_LINK') {
      if (redirect['mockPage'] != redirectModel['mockPage']) {
        return false;
      }
    }
    if (redirect['redirectType'] == 'CUSTOM_LINK') {
      if (redirect['customUri'] != redirectModel['customUri']) {
        return false;
      }
    }
    return true;
  }

  unitsEqual(firstUnits: Array<any>, secondUnits: Array<any>) {
    var flag = true;
    if (firstUnits.length == secondUnits.length) {
      for (var unit of firstUnits) {
        for (var unit2 of secondUnits) {
          if (unit['id'] != unit2['id']) {
            flag = false;
          }
          else {
            flag = true;
            break;
          }
        }
      }
    }
    else {
      flag = false;
    }

    return flag;
  }

  statusToString(status) {
    let res = "";
    switch (status) {
      case "WAITING":
        res = "в ожидании";
        break;
      case "SUSPENDED":
        res = "приостановлена";
        break;
      case "EXECUTION":
        res = "выполняется";
        break;
      case "COMPLETED":
        res = "завершена";
        break;
      case "EXPIRED":
        res = "завершена с ошибкой";
        break;
    }
    return res;
  }

  periodToString(period) {
    let res = "";
    switch (period) {
      case "DAY":
        res = "День";
        break;
      case "THREE_DAYS":
        res = "Три дня";
        break;
      case "WEEK":
        res = "Неделя";
        break;
    }
    return res;
  }
}
