import { AppCoreService } from "../../services/app-core.service";
import { Task } from "./task";
import { Injectable } from "@angular/core";
import { Unit } from "../unit";
import { Tag } from "../tag/tag";
import { Employee } from "../employee/employee";
import { MailTemplate } from "../template/template";
/**
 * Created by Baser on 16.02.2017.
 */
@Injectable()
export class TaskContext {
    constructor(private core: AppCoreService) {

    }

    newTask() {
        return new Task("", "", [], [], [], new MailTemplate("", "", "", {}, "", "", "", "", ""), 0, 0, {});
    }

    createTask(task: Task) {
        let units = [];
        for (let unit of task.units) {
            units.push(unit.id);
        }
        let tags = [];
        for (let tag of task.employeeTags) {
            tags.push(tag.id);
        }

        let data = JSON.parse(JSON.stringify(task));
        data['units'] = units;
        data['employeeTags'] = tags;
        data['template'] = task.template.id;
        data['employeesByHand'] = task.employeesByHand.map(item => item.id);
        data['employeesByFilter'] = task.employeesByFilter.map(item => item.id);
        let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');

        return this.core.post_json("api/tasks", postData);
    }

    editTask(task: Task) {
        let units = [];
        for (let unit of task.units) {
            units.push(unit.id);
        }
        let tags = [];
        for (let tag of task.employeeTags) {
            tags.push(tag.id);
        }
        let data = JSON.parse(JSON.stringify(task));
        data['units'] = units;
        data['employeeTags'] = tags;
        data['employeesByHand'] = task.employeesByHand.map(item => item.id);
        data['employeesByFilter'] = task.employeesByFilter.map(item => item.id);
        data['template'] = task.template.id;
        let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');

        return this.core.put("api/tasks/" + task.id, postData);
    }

    getTaskById(id: string) {
        var task = {};
        return this.core.get("api/tasks/" + id)
            .then((res) => {
                task = res['_embedded'];

                var units = [];
                for (let unit of task['units']) {
                    units.push(new Unit(unit['id'], unit['name'], null, null, false, unit['operations']));
                }

                var employeeTags = [];
                for (let tag of task['employeeTags']) {
                    employeeTags.push(new Tag(tag['id'], tag['name'], null, null, true));
                }

                var employees = [];
                var filteredEmployees = [];

                return Promise.resolve(new Task(task['id'], task['name'], units,
                    employeeTags, employees, new MailTemplate(task['template']['id'], task['template']['name'], task['template']['status'], task['template']['group'],
                        task['template']['description'], task['template']['subject'], task['template']['fromPersonal'], task['template']['fromAddress'], task['template']['redirectUri']), task['startDate'],
                    task['endDate'], task['redirect'], task['author'], task['createdDate'], task['status'], filteredEmployees));
            });
    }

    changeTaskStatus(id: string, action: string) {
        return this.core.put("api/tasks/" + id + "/" + action);
    }

    changeTaskStartDate(id: string, time: number) {
        let data = {};
        data['startDate'] = time;
        let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
        return this.core.put("api/tasks/" + id + "/update/start-date", putData);
    }

    getVulnerableEmployees(taskId: string) {
        return this.core.get("api/tasks/" + taskId + "/employees/count-attackable")
            .then((res) => {
                return Promise.resolve(res['_embedded']);
            });
    }

    getEmployeesByhand(taskId: string) {
        return new Promise((resolve, reject) => {
            let employeesByHand = [];
            this.core.get(`api/tasks/${taskId}/employees/hand`)
                .then(res => {
                    res['_embedded'].forEach(employee => {
                        employeesByHand.push(new Employee(employee['id'], employee['fullName'], employee['unitName'], "", [], [], "", "", "", "", "", employee['position']));
                    });
                    resolve(employeesByHand);
                })
                .catch(error => reject(error));
        });
    }

    getEmployeesByFilter(taskId: string) {
        return new Promise((resolve, reject) => {
            let employeesByFilter = [];
            this.core.get(`api/tasks/${taskId}/employees/filter`)
                .then(res => {
                    res['_embedded'].forEach(employee => {
                        employeesByFilter.push(new Employee(employee['id'], employee['fullName'], employee['unitName'], "", [], [], "", "", "", "", "", employee['position']));
                    });
                    resolve(employeesByFilter);
                })
                .catch(error => reject(error));
        });
    }
}