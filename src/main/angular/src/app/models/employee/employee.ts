import {Unit} from "../unit";
import {Tag} from "../tag/tag";

/**
 * Created by Baser on 16.02.2017.
 */
export class Employee {
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = value;
  }

  private static EMAIL_REGEXP: RegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  private _selected: boolean = false;
  private _disabled: boolean = false;

  constructor(public id: string,
              public fullName?: string,
              public unitName?: string,
              public email?: string,
              public unit?: Array<Unit>,
              public employeeTags?: Array<Tag>,
              public status?: string,
              public shortName?: string,
              public secondName?: string,
              public firstName?: string,
              public middleName?: string,
              public position?: string) {
  }

  clone() {
    return new Employee(this.id, this.fullName, this.unitName, this.email, this.unit, this.employeeTags,
      this.status, this.shortName, this.secondName, this.firstName, this.middleName, this.position);
  }

  validEmail() {
    return (this.email.length >= 5 && Employee.EMAIL_REGEXP.test(this.email));
  }

  valid() {
    return !!(
      this.secondName &&
      this.firstName &&
      this.email &&
      (this.unit.length > 0));
  }

  equal(model: Employee) {
    return (this.id == model.id) &&
      (this.fullName == model.fullName) &&
      (this.unitName == model.unitName) &&
      (this.email == model.email) &&
      this.unitsEqual(this.unit, model.unit) &&
      this.unitsEqual(this.employeeTags, model.employeeTags) &&
      (this.status == model.status) &&
      (this.shortName == model.shortName) &&
      (this.secondName == model.secondName) &&
      (this.firstName == model.firstName) &&
      (this.middleName == model.middleName) &&
      (this.position == model.position);
  }

  unitsEqual(firstUnits: Array<any>, secondUnits: Array<any>) {
    if (firstUnits.length != secondUnits.length)
      return false;

    for (let unit of firstUnits) {
      if (!secondUnits.some(unit2 => unit['id'] == unit2['id']))
        return false;
    }

    return true;
  }

  get selected(): boolean {
    return this._selected;
  }

  set selected(value: boolean) {
    this._selected = value;
  }

}
