/**
 * Created by Baser on 16.02.2017.
 */

import {Injectable} from "@angular/core";
import {AppCoreService} from "../../services/app-core.service";
import {Employee} from "./employee";
import {Unit} from "../unit";
import {Tag} from "../tag/tag";

@Injectable()
export class EmployeeContext {

  constructor(private core: AppCoreService) {

  }

  getEmployeeList(params?: Object) {
    return this.core.get("api/employees", params)
      .then((res) => {
        return res['_embedded'].map(employee => new Employee(employee['id'],
          employee['fullName'],
          employee['unitName']));
      });
  }

  changeEmployeeStatus(id: string, action: string) {
    return this.core.put("api/employees/" + id + "/" + action);
  }

  createEmployee(employee: Employee) {
    let tags = [];
    for (let tag of employee.employeeTags) {
      tags.push(tag.id);
    }
    let data = JSON.parse(JSON.stringify(employee));
    data['unit'] = employee.unit[0].id;
    data['employeeTags'] = tags;
    let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');

    return this.core.post_json("api/employees", postData);
  }

  saveEmployee(employee: Employee) {
    let tags = [];
    for (let tag of employee.employeeTags) {
      tags.push(tag.id);
    }
    let data = JSON.parse(JSON.stringify(employee));
    data['unit'] = employee.unit[0].id;
    data['employeeTags'] = tags;
    let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');

    return this.core.put("api/employees/" + employee.id, postData);
  }

  getEmployeeById(id: string) {
    let params = new Object();
    params['projection'] = 'FULL';

    var employee = {};
    return this.core.get("api/employees/" + id, params)
      .then((res) => {
        employee = res['_embedded'];

        var unit = [];
        if (employee['unit'] != null) {
          unit.push(new Unit(employee['unit']['id'], employee['unit']['name'], null, null));
        }

        var employeeTags = [];
        for (let tag of employee['employeeTags']) {
          employeeTags.push(new Tag(tag['id'], tag['name'], null, null));
        }

        return Promise.resolve(new Employee(employee['id'], "", "", employee['email'],
          unit, employeeTags, employee['status'], employee['shortName'], employee['secondName'],
          employee['firstName'], employee['middleName'], employee['position']));
      });
  }

  uploadEmployeesDB(employees: Array<Object>) {
    let saveEmployees = [];

    for (let item of employees) {
      let tempEmployee = new Object();
      tempEmployee['secondName'] = item['secondName'];
      tempEmployee['firstName'] = item['firstName'];
      tempEmployee['middleName'] = item['middleName'];
      tempEmployee['email'] = item['email'];
      tempEmployee['position'] = item['position'];
      tempEmployee['unit'] = item['selectedUnit'][0]['id'];

      tempEmployee['employeeTags'] = [];
      for (let tag of item['selectedTags']) {
        tempEmployee['employeeTags'].push(tag['id']);
      }
      if (tempEmployee['employeeTags'].length == 0) {
        tempEmployee['employeeTags'] = null
      }
      saveEmployees.push(tempEmployee);
    }

    let postData = JSON.parse('{"form": ' + JSON.stringify(saveEmployees) + '}');
    return this.core.post_json("api/employees/import/save", postData);
  }
}
