/**
 * Created by Baser on 16.02.2017.
 */
export class MailTemplate {
    constructor(
        public id: string,
        public name: string,
        public status: string,
        public group: Object,
        public description: string,
        public subject: string,
        public fromPersonal: string,
        public fromAddress: string,
        public redirectUri: string
    ) {}

    equal(model: MailTemplate) {
        return (this.id == model.id);
    }
}