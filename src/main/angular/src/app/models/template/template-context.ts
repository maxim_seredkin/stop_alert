/**
 * Created by Baser on 16.02.2017.
 */
import {Injectable} from "@angular/core";
import {MailTemplate} from "./template";
import {AppCoreService} from "../../services/app-core.service";

@Injectable()
export class MailTemplateContext {
    constructor(private core: AppCoreService) {}

    getMailTemplateList() {
        let params = new Object();
        params['filters'] = "'status' EQ 'ENABLED'";

        return this.core.get('api/mail-templates', params)
            .then((res: Array<any>) => {
                var data = res['_embedded'];
                var templates = [];

                for (var item of data) {
                    templates.push(new MailTemplate(item['id'], item['name'], item['status'], item['group'], item['description'], item['subject'], item['fromPersonal'],
                    item['fromAddress'], item['redirectUri']));
                }
                return Promise.resolve(templates);
            });
    }
}