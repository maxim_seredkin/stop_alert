/**
 * Created by Baser on 14.02.2017.
 */
export class Tag {
    constructor(
        public id: string,
        public name: string,
        public nodes: Array<Tag>,
        public parent: Tag
    ) {

    }
}