/**
 * Created by Baser on 28.12.2016.
 */
import { Injectable } from "@angular/core";
import { AppCoreService } from "./app-core.service";
import { Router } from "@angular/router";
import { Unit } from "../models/unit";
import { Authority } from "../models/user/authority";
import { User } from "../models/user/user";
import { Headers, Http, Response } from "@angular/http";
import { Config } from "./config";

@Injectable()
export class UserService {
    private isGuest: boolean = true;
    private authority: string;

    public username: string = "";
    public fullName: string = "";

    //флаг проверки первой загрузки приложения
    public load: boolean = false;

    public licenseActive: boolean = true;

    constructor(private core: AppCoreService,
        private router: Router,
        private http: Http,
        private config: Config) {
        //this.getCurrentUser();
    }

    public login(login, password) {
        let data =
            {
                "username": login,
                "password": password,
                "grant_type": 'password',
                "scope": 'read write',
                "client_secret": 'my-secret-token-to-change-in-production',
                "client_id": 'RiskGuardapp'
            };

        let headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa('RiskGuardapp' + ':' + data.client_secret));
        headers.append('content-type', 'application/x-www-form-urlencoded');

        return this.http.post(this.config.get('backendUrl') + 'oauth/token', this.core.makePostBody(data), { headers: headers })
            .toPromise()
            .then((data) => {
                localStorage.setItem('access_token', data.json()['access_token']);
                localStorage.setItem('refresh_token', data.json()['refresh_token']);
                this.isGuest = false;
                this.router.navigate(['']);
            });
    }

    public passwordRecover(login: string) {
        let data =
            {
                "username": login
            };

        let putData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');

        return this.http.put('api/users/password/reset', putData).toPromise();
    }

    public logOut() {
        this.core.post("oauth/logout")
            .then(() => {
                this.load = false;
                localStorage.clear();
                this.router.navigate(['login']);
            })
    }

    public getCurrentUser(): Promise<any> {
        return new Promise((resolve, reject) => {

            var headers = new Headers();
            headers.append('accept', 'application/json');
            headers.append('x-requested-with', 'XMLHttpRequest');
            let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';
            if (token_header) {
                headers.append('Authorization', 'Bearer ' + token_header);
            }

            this.http.get(this.config.get('backendUrl') + 'api/account', { headers: headers }).toPromise()
                .then((resp) => {
                    let data = resp.json();
                    this.isGuest = (data['_embedded'] == null);
                    if (data['_embedded'] != null) {
                        this.username = data['_embedded']['username'];
                        this.fullName = data['_embedded']['fullName'];
                        this.authority = data['_embedded']['authority']['authority'];
                    }
                })
                .then(() => {
                    resolve();
                })
                .catch(() => {
                    this.core.refreshToken()
                        .then((res: Response) => {
                            localStorage.setItem('access_token', res.json()['access_token']);
                            localStorage.setItem('refresh_token', res.json()['refresh_token']);
                            resolve();
                        })
                        .catch(() => {
                            localStorage.clear();
                            reject();
                        });
                });
        });
    }

    getAccountData() {
        var user = {};
        return this.core.get("api/account/")
            .then((res) => {
                user = res['_embedded'];

                var units = [];
                for (let unit of user['units']) {
                    units.push(new Unit(unit['id'], unit['name'], unit['nodes'], unit['parent']));
                }

                return Promise.resolve(new User(
                    user['id'], user['username'], user['secondName'],
                    user['firstName'], user['middleName'], user['email'],
                    new Authority(
                        user['authority']['id'], user['authority']['authority'],
                        user['authority']['displayName'], user['authority']['description']
                    ), units, user['status'], user['registerDate'], user['fullName'], user['shortName'], "edit", null));
            });
    }

    editUser(user: User) {
        let authorityId = user.authority.id;
        let data = JSON.parse(JSON.stringify(user));
        data['authority'] = authorityId;
        data['units'] = [];
        if (user.units.length != 0) {
            for (var unit of user.units) {
                data['units'].push(unit.id);
            }
        }
        else {
            data['units'] = [];
        }
        if (!data['password']['renewed']) {
            data['password'] = null;
        }
        //data['password'] = null;
        let postData = JSON.parse('{"form": ' + JSON.stringify(data) + '}');
        return this.core.put("api/account/", postData);
    }

    saveUser(renewed: string, old: string) {
        var passwordForm = new Object(
            {
                "form": {
                    "password": (renewed) ? { "old": old, "renewed": renewed } : null
                }

            });

        return this.core.put("api/account/", passwordForm);
    }

    public isActiveRoute(roles: Array<string>) {
        let userAuthority = this.authority;
        let active = false;

        let authorities: Array<string> = roles.slice();

        if (authorities.length == 0) {
            return true;
        }

        for (let item of authorities) {
            if (item == userAuthority) {
                active = true;
                break;
            }
        }
        return active;
    }

    hasAccess(access_object: Object, operation: string) {
        return access_object['operations'].some(item => item === operation);
    }

    checkLicense() {
        return this.core.get("api/settings/license")
            .then((res) => {
                if (res['_embedded']['status'] == "FAILURE" || res['_embedded']['status'] == "EXPIRED") {
                    this.licenseActive = false;
                }
                else {
                    this.licenseActive = true;
                }
            });
    }

    public isLogged(): boolean {
        return !this.isGuest;
    }

    public getAuthority() {
        return this.authority;
    }
}
