import {Injectable} from '@angular/core';
import {Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

import {UserService} from './user.service';

@Injectable()
export class AuthGuardService {

    constructor(private router:Router, private user:UserService) {
    }

    canActivate() {
        return this.user.getCurrentUser()
            .then(() => {
                if (this.user.isLogged()) {
                    return this.user.isLogged()
                }
                else {
                    this.router.navigate(['login']);
                }
            })
            .catch(() => {
                this.router.navigate(['/login']);
                return false;
            });
    }

    canActivateChild(next:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
        return this.user.getCurrentUser()
            .then(() => {
                if (!this.user.isLogged()) {
                    this.router.navigate(['/login']);
                    return false;
                }

                if(!this.user.isActiveRoute(next.data['roles'])) {
                    this.router.navigate(["/main"]);
                    return false;
                }

                if(next.data['needLicense']) {
                    if(this.user.load == true) {
                        if(!this.user.licenseActive) {
                            this.router.navigate(["/main"]);
                        }
                        return true;
                    }
                    else {
                        return this.user.checkLicense()
                            .then(() => {
                                if(!this.user.licenseActive) {
                                    this.router.navigate(["/main"]);
                                }
                                return true;
                            });
                    }
                }

                return true;
            })
            .catch(() => {
                this.router.navigate(['/login']);
                return false;
            });
    }
}
