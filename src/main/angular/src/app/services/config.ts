/**
 * Created by Baser on 28.12.2016.
 */
import {Injectable} from '@angular/core';

@Injectable()
export class Config {
    private _config: Object;

    constructor() {
        this._config = {};

        /*
         * DEV SERVERS
         */
        // this._config['backendUrl'] = 'http://192.168.0.39:8080/';
        // this._config['backendUrl'] = 'http://192.168.0.95:8090/';
        // this._config['backendUrl'] = 'http://192.168.0.123:8090/';

        /*
        * PRODUCTION
        */
        this._config['backendUrl'] = '';
    }


    get(key: any) {
        return this._config[key];
    }
}
