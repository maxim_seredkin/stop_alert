/**
 * Created by Baser on 29.12.2016.
 */
import {Injectable} from "@angular/core";
import {Headers, Http, Request, RequestMethod, Response} from "@angular/http";
import {Router} from "@angular/router";
import "rxjs/add/operator/map";
import "rxjs/Rx";
import {Config} from "./config";

@Injectable()
export class AppCoreService {
  private authFailCallback: Function;

  constructor(private http: Http, private router: Router, private config: Config) {

  }

  public get(endpoint: string, params: any = null, headers: any = null): Promise<Object> {
    return this.send('GET', endpoint, params, headers);
  }

  public post(endpoint: string, data?: any, headers: any = null): Promise<Object> {
    return this.send('POST', endpoint, data, headers);
  }

  public put(endpoint: string, data?: any, headers: any = null): Promise<Object> {
    return this.send('PUT', endpoint, data, headers);
  }

  public put_file(endpoint: string, data?: any, headers: any = null): Promise<Object> {
    return this.send('PUT_FILE', endpoint, data, headers);
  }

  public post_json(endpoint: string, data?: any, headers: any = null): Promise<Object> {
    return this.send('POST_JSON', endpoint, data, headers);
  }

  public post_file(endpoint: string, data?: any, headers: any = null): Promise<Object> {
    return this.send('POST_FILE', endpoint, data, headers);
  }

  public delete(endpoint: string, data: any = null, headers: any = null): Promise<Object> {
    return this.send('DELETE', endpoint, data, headers);
  }

  public send(method: string, endpoint: string, params?: Object, custom_headers?: Object): Promise<Object> {

    let url = this.makeUrl(endpoint, ['GET', 'HEAD', 'OPTIONS'].indexOf(method) >= 0 ? params : null);
    let headers = this.getRequestHeaders(custom_headers);

    let request: Request;
    let requestOptions: any = {
      url: url,
    };

    switch (method) {
      case 'GET':
        requestOptions.method = RequestMethod.Get;
        break;
      case 'POST':
        requestOptions.method = RequestMethod.Post;
        requestOptions.body = params ? this.makePostBody(params) : '';
        headers.append('content-type', 'application/x-www-form-urlencoded');
        break;
      case 'PUT':
        requestOptions.method = RequestMethod.Put;
        requestOptions.body = params ? JSON.stringify(params) : '';
        headers.append('content-type', 'application/json');
        break;
      case 'PUT_FILE':
        requestOptions.method = RequestMethod.Put;
        requestOptions.body = params;
        break;
      case 'POST_JSON':
        requestOptions.method = RequestMethod.Post;
        requestOptions.body = params ? JSON.stringify(params) : '';
        headers.append('content-type', 'application/json');
        break;
      case 'POST_FILE':
        requestOptions.method = RequestMethod.Post;
        requestOptions.body = params;
        break;
      case 'DELETE':
        requestOptions.method = RequestMethod.Delete;
        break;

      default:
        throw new Error('Method not implemented');
    }

    requestOptions.headers = headers;
    request = new Request(requestOptions);

    return new Promise((resolve, reject) => {
      this.sendRequest(request)
        .then((data) => {
          if (data != null) {
            if (data['code'] == 404) {
              this.router.navigate(['404']);
            }
            if (data['code'] == 403) {
              this.router.navigate(['403']);
            }
          }
          resolve(data);
        })
        .catch((err: any) => {

          if (err instanceof Response) {
            if (err.ok == false && err.status == 0) {
              this.router.navigate(['login']);
            }
            if (err.status == 401 || err.status == 403) {
              if (localStorage.getItem("refresh_token")) {
                localStorage.removeItem("access_token");
                this.refreshToken()
                  .then((res: Response) => {
                    localStorage.setItem('access_token', res.json()['access_token']);
                    localStorage.setItem('refresh_token', res.json()['refresh_token']);

                    let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';
                    request.headers.delete('Authorization');
                    request.headers.append('Authorization', 'Bearer ' + token_header);

                    return this.sendRequest(request)
                      .then((data) => {
                        resolve(data);
                      }).catch((err) => {
                        reject(err);
                      });
                  })
                  .catch(() => {
                    localStorage.removeItem("refresh_token");
                    this.router.navigate(['login']);
                  });
              } else {
                this.router.navigate(['login']);
              }
            } else {
              reject(err);
            }
          } else {
            reject(err);
          }
        });


      // promise.reject(err);

    });
  }

  public refreshToken() {
    let data =
      {
        "grant_type": 'refresh_token',
        "client_id": 'RiskGuardapp',
        "refresh_token": (localStorage.getItem("refresh_token")) ? localStorage.getItem("refresh_token").toString() : ''
      };

    // let headers = {
    //     'Authorization': 'Basic ' + btoa('RiskGuardapp' + ':' + 'my-secret-token-to-change-in-production')
    // };

    let headers = new Headers();
    headers.append('Authorization', 'Basic ' + btoa('RiskGuardapp' + ':' + 'my-secret-token-to-change-in-production'));
    headers.append('content-type', 'application/x-www-form-urlencoded');

    return this.http.post(this.config.get('backendUrl')+"oauth/token", this.makePostBody(data), {headers: headers}).toPromise();

    //return this.post("oauth/token", data, headers);
  }


  private sendRequest(request: Request): Promise<any> {
    return this.http.request(request)
      .toPromise()
      .then((resp: Response) => {
        return resp.json();
      });
  }

  private getRequestHeaders(custom_headers?: Object): Headers {
    var headers = new Headers();
    headers.append('accept', 'application/json');
    headers.append('x-requested-with', 'XMLHttpRequest');
    let token_header = (localStorage.getItem("access_token")) ? localStorage.getItem("access_token").toString() : '';
    if (token_header) {
      headers.append('Authorization', 'Bearer ' + token_header);
    }
    if (custom_headers) {
      for (let header in custom_headers) {
        headers.append(header, custom_headers[header]);
      }
    }

    return headers;
  }

  private makeUrl(endpoint: string, params: Object = null): string {
    let ret = this.config.get('backendUrl') + endpoint;
    if (params) {
      let queryParams = this.encodeQueryData(params);
      if (queryParams)
        ret += '?' + queryParams;
    }
    return ret;
  }

  private encodeQueryData(data) {
    let ret = [];
    for (let item in data)
      ret.push(encodeURIComponent(item) + '=' + encodeURIComponent(data[item]));
    return ret.join('&');
  }

  public makePostBody(params: any): string {
    return this.encodeQueryData(params);
  }

}
