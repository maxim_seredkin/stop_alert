/**
 * Created by Baser on 23.02.2017.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './user.service';

@Injectable()
export class LoginPageGuardService {

  constructor(private router: Router, private user: UserService) {
  }

  canActivate() {
    return this.user.getCurrentUser()
      .then(() => {
        if (this.user.isLogged()) {
          this.router.navigate(['/account']);
          return false;
        }
        else {
          return true;
        }
      })
      .catch(() => {
        return true;
      });
  }
}
