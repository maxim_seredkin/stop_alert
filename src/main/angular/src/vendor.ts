/**
 * Created by Baser on 25.01.2017.
 */
import './app_config.json';
import '@angular/common';
import '@angular/compiler';
import '@angular/core';
import '@angular/forms';
import '@angular/http';
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/router';

import 'angular2-template-loader';
import 'angular2-uuid';
import 'chart.js/dist/Chart.js';
import 'core-js';
import 'file-saver';

import 'ng2-charts';
import 'ng2-file-upload';
import 'ng2-page-scroll';
import 'ng2-select';

import 'ngx-modal';
import 'primeng/primeng';
import 'rxjs';

// import 'zone.js';
import 'angular2-loaders-css';
import 'angular2-text-mask';
