package com.ws.sa.services.user_details;

import com.ws.sa.services.base.BaseService;
import com.ws.sa.repositories.user.UserRepository;
import com.ws.sa.entities.user.User;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * <h1>Сервис для работы с пользователем</h1>
 *
 * @author Maxim Seredkin
 */
@Service
public class UserServiceImpl extends BaseService<User> implements UserService {

    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @Transactional(readOnly = true)
    public User findOneByUsername(final String username) {
        return repository.findOneByUsername(username);
    }

    @Override
    public boolean existsByUsername(final String username) {
        return repository.existsByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<User> findActiveUsersByUsernameOrEmail(String username) {
        return repository.findActiveUsersByUsernameOrEmail(username);
    }

}
