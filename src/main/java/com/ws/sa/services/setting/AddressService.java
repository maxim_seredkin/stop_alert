package com.ws.sa.services.setting;

import com.ws.sa.dtos.setting.TransitionDTO;
import com.ws.sa.dtos.setting.AddressDTO;
import com.ws.sa.forms.setting.TransitionForm;
import com.ws.sa.forms.setting.AddressForm;

/**
 * Created by Vladislav Shelengovskiy on 14.07.2017.
 */
public interface AddressService {

    TransitionDTO getTransitionAddress();

    void setTransitionAddress(TransitionForm form);

    AddressDTO getExternalAddress();

    void setExternalAddress(AddressForm form);
}
