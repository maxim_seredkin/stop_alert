package com.ws.sa.services.setting;

import com.ws.sa.dtos.setting.LicenseDTO;
import com.ws.sa.forms.setting.LicenseForm;

/**
 * Created by Vladislav Shelengovskiy on 02.04.2017.
 */
public interface LicenseService {

    LicenseDTO activateLicense(LicenseForm form);

    LicenseDTO getLicense();
}
