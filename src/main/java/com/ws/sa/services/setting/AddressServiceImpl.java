package com.ws.sa.services.setting;

import com.ws.sa.dtos.setting.TransitionDTO;
import com.ws.sa.dtos.setting.AddressDTO;
import com.ws.sa.forms.setting.TransitionForm;
import com.ws.sa.forms.setting.AddressForm;
import com.ws.sa.configs.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Vladislav Shelengovskiy on 14.07.2017.
 */
@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AppConfig appConfig;

    @Override
    public TransitionDTO getTransitionAddress(){
        return new TransitionDTO(appConfig.getTransitionPath());
    }

    @Override
    public void setTransitionAddress(TransitionForm form){
        appConfig.setTransitionPath(form.getUri());
    }

    @Override
    public AddressDTO getExternalAddress(){
        return new AddressDTO(appConfig.getExternalAddress());
    }

    @Override
    public void setExternalAddress(AddressForm form){
        appConfig.setExternalAddress(form.getUri());
    }
}
