package com.ws.sa.services.setting;

import com.ws.sa.enums.ResourceType;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.services.base.ResourceService;
import com.ws.sa.enums.setting.AppSettingProperty;
import com.ws.sa.enums.setting.JavaMailProperty;
import com.ws.sa.enums.setting.SpringMailProperty;
import com.ws.sa.dtos.setting.ServerSettingDTO;
import com.ws.sa.forms.setting.ServerSettingForm;
import com.ws.sa.configs.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.mail.Session;
import javax.mail.Transport;
import java.io.*;
import java.util.Properties;

/**
 * Created by Vladislav Shelengovskiy on 30.03.2017.
 */
@Service
public class ServerServiceImpl implements ServerService {

    private final JavaMailSender javaMailSender;
    private final AppConfig config;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    public ServerServiceImpl(JavaMailSender javaMailSender, AppConfig config) {
        this.javaMailSender = javaMailSender;
        this.config = config;
    }

    @Override
    public boolean verifyConnectionState(ServerSettingForm form) {
        return verifyConnectionState(form.getHost(), form.getPort(), form.getUsername(), form.getPassword(), form.getUseSSL());
    }

    @Override
    public boolean verifyConnectionState() throws IOException {
        ServerSettingDTO dto = getSettings();
        return verifyConnectionState(dto.getHost(), dto.getPort(), dto.getUsername(), dto.getPassword(), dto.getUseSSL());
    }

    @Override
    public ServerSettingDTO getSettings() throws IOException {
        Properties prop = getProperties();

        String strPort = prop.getProperty(SpringMailProperty.PORT.getPropName());
        Guard.checkEntityExists(strPort, ApiStatusCode.EMPTY_PARAM);
        Guard.checkEntityArgument(tryParsePort(strPort), ApiStatusCode.INVALID_PARAM);

        String protocol = prop.getProperty(SpringMailProperty.PROTOCOL.getPropName());
        Guard.checkEntityExists(protocol, ApiStatusCode.EMPTY_PARAM);
        Guard.checkEntityArgument(tryParseProtocol(protocol), ApiStatusCode.INVALID_PARAM);

        String host = prop.getProperty(SpringMailProperty.HOST.getPropName());
        Guard.checkEntityExists(host, ApiStatusCode.EMPTY_PARAM);

        String username = prop.getProperty(SpringMailProperty.USERNAME.getPropName());

        String password = prop.getProperty(SpringMailProperty.PASSWORD.getPropName());

        String strAnonymous = prop.getProperty(AppSettingProperty.MAIL_ANONYMOUS.getPropName());

        Integer port = Integer.valueOf(strPort);
        Boolean useSSL = isUseSSL(protocol);
        Boolean anonymous = Boolean.parseBoolean(strAnonymous);

        return ServerSettingDTO.builder()
                .host(host)
                .port(port)
                .username(username)
                .password(password)
                .useSSL(useSSL)
                .anonymous(anonymous).build();
    }

    @Override
    public void setSettings(ServerSettingForm form) throws IOException {
        Properties prop = getProperties();

        if (form.getAnonymous()) {
            prop.setProperty(SpringMailProperty.USERNAME.getPropName(), config.getDefaultMailUsername());
            prop.setProperty(SpringMailProperty.PASSWORD.getPropName(), config.getDefaultMailPassword());
        } else {
            prop.setProperty(SpringMailProperty.USERNAME.getPropName(), form.getUsername());
            prop.setProperty(SpringMailProperty.PASSWORD.getPropName(), form.getPassword());
        }

        config.setMailUsername(prop.getProperty(SpringMailProperty.USERNAME.getPropName()));

        config.setAnonymous(form.getAnonymous());
        prop.setProperty(AppSettingProperty.MAIL_ANONYMOUS.getPropName(), form.getAnonymous().toString());

        prop.setProperty(SpringMailProperty.PROTOCOL.getPropName(), getProtocol(form.getUseSSL()));
        prop.setProperty(SpringMailProperty.PORT.getPropName(), form.getPort().toString());
        prop.setProperty(SpringMailProperty.HOST.getPropName(), form.getHost());

        OutputStream outputStream = new FileOutputStream(getFileProperties());
        Guard.checkEntityExists(outputStream, ApiStatusCode.NOT_FOUND);

        prop.store(outputStream, null);
        outputStream.close();

        applySettings(form.getHost(), form.getPort(), form.getUsername(), form.getPassword(), form.getUseSSL());
    }

    private Properties getProperties() throws IOException {
        Properties prop = new Properties();
        InputStream inputStream = new FileInputStream(getFileProperties());
        Guard.checkEntityExists(inputStream, ApiStatusCode.NOT_FOUND);

        prop.load(inputStream);
        inputStream.close();

        return prop;
    }

    private File getFileProperties(){
        return resourceService.get(config.getServerProperties(), ResourceType.PROPERTY);
    }

    private boolean tryParsePort(String port) {
        try {
            Integer value = Integer.parseInt(port);
            return value > 0 && value < 65536;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean tryParseProtocol(String protocol) {
        return protocol.equalsIgnoreCase("smtp") || protocol.equalsIgnoreCase("smtps");
    }

    private boolean verifyConnectionState(String host, Integer port, String username, String password, Boolean useSSL) {
        Properties prop = new Properties();
        prop.put(JavaMailProperty.PROTOCOL.getPropName(), "smtp");
        prop.put(JavaMailProperty.HOST.getPropName(), host);
        prop.put(JavaMailProperty.PORT.getPropName(), port.toString());
        prop.put(JavaMailProperty.AUTH.getPropName(), "true");
        prop.put(JavaMailProperty.SSL.getPropName(), useSSL.toString());
        prop.put("mail.smtp.connectiontimeout", "1500");
        try {
            Transport transport = Session.getInstance(prop).getTransport();
            transport.connect(username, password);
            transport.close();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private void applySettings(String host, Integer port, String username, String password, Boolean useSSL) {
        JavaMailSenderImpl jms = (JavaMailSenderImpl) javaMailSender;
        Properties prop = new Properties();
        prop.put(JavaMailProperty.AUTH.getPropName(), "true");
        jms.setProtocol(getProtocol(useSSL));
        jms.setHost(host);
        jms.setPort(port);
        jms.setUsername(username);
        jms.setPassword(password);
        jms.setJavaMailProperties(prop);
    }

    private String getProtocol(Boolean useSSL) {
        return useSSL ? "smtps" : "smtp";
    }

    private boolean isUseSSL(String protocol) {
        return protocol.equalsIgnoreCase("smtps");
    }
}
