package com.ws.sa.services.setting;

import com.ws.sa.configs.AppConfig;
import com.ws.sa.dtos.setting.LdapDTO;
import com.ws.sa.forms.setting.LdapForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Vladislav Shelengovskiy on 30.10.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Service
public class LdapServiceImpl implements LdapService {

    @Autowired
    private AppConfig appConfig;

    @Override
    public LdapDTO getLdapSetting(){
        return new LdapDTO(appConfig.getLdapUrl(),
                           appConfig.getLdapPort(),
                           appConfig.getLdapDn(),
                           appConfig.getLdapUseSSL(),
                           appConfig.getLdapUsername(),
                           appConfig.getLdapPassword());
    }

    @Override
    public void setLdapSetting(LdapForm form){
        appConfig.setLdapUrl(form.getUrl());
        appConfig.setLdapPort(form.getPort());
        appConfig.setLdapDn(form.getDn());
        appConfig.setLdapUseSSL(form.getUseSSL());
        appConfig.setLdapUsername(form.getUsername());
        appConfig.setLdapPassword(form.getPassword());
    }
}
