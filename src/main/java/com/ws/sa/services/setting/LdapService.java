package com.ws.sa.services.setting;

import com.ws.sa.dtos.setting.LdapDTO;
import com.ws.sa.forms.setting.LdapForm;

/**
 * Created by Vladislav Shelengovskiy on 30.10.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public interface LdapService {

    LdapDTO getLdapSetting();

    void setLdapSetting(LdapForm form);
}
