package com.ws.sa.services.setting;

import com.ws.sa.dtos.setting.ServerSettingDTO;
import com.ws.sa.forms.setting.ServerSettingForm;

import java.io.IOException;

/**
 * Created by Vladislav Shelengovskiy on 30.03.2017.
 */
public interface ServerService {

    ServerSettingDTO getSettings() throws IOException;

    void setSettings(ServerSettingForm form) throws IOException;

    boolean verifyConnectionState(ServerSettingForm form);

    boolean verifyConnectionState() throws IOException;
}
