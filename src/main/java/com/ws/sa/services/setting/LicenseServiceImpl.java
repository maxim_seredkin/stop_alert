package com.ws.sa.services.setting;

import com.ws.sa.enums.ResourceType;
import com.ws.sa.services.base.ResourceService;
import com.ws.sa.enums.setting.ActivateStatus;
import com.ws.sa.dtos.setting.LicenseDTO;
import com.ws.sa.forms.setting.LicenseForm;
import com.ws.sa.configs.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Scanner;

/**
 * Created by Vladislav Shelengovskiy on 02.04.2017.
 */
@Service
public class LicenseServiceImpl implements LicenseService {

    @Autowired
    private AppConfig config;

    @Autowired
    private ResourceService resourceService;

    @Override
    public LicenseDTO activateLicense(LicenseForm form) {
        try {
            String license = transformLicense(form.getCode());
            LicenseDTO dto = parseLicense(license);
            if (dto.isSuccessful()) {
                writeLicense(form.getCode());
            }
            return dto;
        } catch (Exception e) {
            LicenseDTO dto = new LicenseDTO();
            dto.setStatus(ActivateStatus.FAILURE);
            return dto;
        }
    }

    @Override
    public LicenseDTO getLicense() {
        try {
            String license = transformLicense(readLicense());
            return parseLicense(license);
        } catch (Exception e) {
            LicenseDTO dto = new LicenseDTO();
            dto.setStatus(ActivateStatus.FAILURE);
            return dto;
        }
    }

    private String transformLicense(String code) throws Exception {
        byte[] data = decode(code);
        return new String(decrypt(data, getKey()), StandardCharsets.UTF_8);
    }

    private String readLicense() throws Exception {
        Scanner in = new Scanner(getLicenseFile(), "UTF-8");
        String temp = "";
        while (in.hasNextLine()) {
            temp = temp.concat(in.nextLine());
        }
        return temp;
    }

    private void writeLicense(String code) throws Exception {
        PrintWriter writer = new PrintWriter(getLicenseFile(), "UTF-8");
        writer.println(code);
        writer.close();
    }

    private LicenseDTO parseLicense(String str) {
        LicenseDTO dto = new LicenseDTO();
        String[] array = str.split(";");
        if (!str.contains("Risk Guard license file") || array.length < 4) {
            dto.setStatus(ActivateStatus.FAILURE);
            return dto;
        }
        dto.setOrgName(array[1]);
        dto.setDateEnd(array[2]);
        if (LocalDate.now().isAfter(
                LocalDate.parse(array[2], DateTimeFormatter.ofPattern("dd.MM.yyyy")))) {
            dto.setStatus(ActivateStatus.EXPIRED);
            return dto;
        }
        dto.setStatus(ActivateStatus.SUCCESSFUL);
        return dto;
    }

    private byte[] decode(String str) throws Exception {
        return Base64.getDecoder().decode(str.trim());
    }

    private byte[] decrypt(byte[] data, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        return cipher.doFinal(data);
    }

    private PublicKey getKey() throws Exception {
        InputStream in = new FileInputStream(resourceService.get("public.key", ResourceType.KEY));
        ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
        BigInteger m = (BigInteger) oin.readObject();
        BigInteger e = (BigInteger) oin.readObject();
        return KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(m, e));
    }

    private File getLicenseFile(){
        return resourceService.get("license.lic", ResourceType.LICENSE);
    }
}
