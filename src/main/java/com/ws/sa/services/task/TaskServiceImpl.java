package com.ws.sa.services.task;


import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.task.Task;
import com.ws.sa.enums.TaskStatus;
import com.ws.sa.repositories.task.TaskRepository;
import com.ws.sa.services.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Stream;

import static com.ws.sa.utils.PageUtils.createPageRequest;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@Service
public class TaskServiceImpl extends BaseService<Task> implements TaskService {

    private final TaskRepository repository;

    @Autowired
    public TaskServiceImpl(TaskRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Collection<Task> findActive() {
        return repository.findActive();
    }

    @Override
    @Transactional
    public Task addMails(UUID taskId) {
        Task task = findOneExisting(taskId);
        task.getMails().clear();
        Stream.concat(task.getEmployeesByFilter().stream(), task.getEmployeesByHand().stream()).forEach(employee -> {
            Mail mail = Mail.builder().task(task).employee(employee).build();
            task.getMails().add(mail);
        });
        return save(task);
    }

    @Override
    public Page<Task> findAllTaskByUnit(UUID unitId, int pageNo, int pageSize) {
        return repository.findAllTaskByUnitId(unitId, createPageRequest(pageNo, pageSize));
    }

    @Override
    @Transactional
    public void complete(UUID taskId) {
        updateStatus(taskId, TaskStatus.COMPLETED);
    }

    @Override
    public void execution(UUID taskId) {
        updateStatus(taskId, TaskStatus.EXECUTION);
    }

    @Override
    public void expired(UUID taskId) {
        updateStatus(taskId, TaskStatus.EXPIRED);
    }

    private void updateStatus(UUID taskId, TaskStatus status) {
        Task task = findOneExisting(taskId);
        task.setStatus(status);
    }

    @Override
    public boolean hasAccess(UUID taskId, UUID userId) {
        return repository.hasAccess(taskId, userId);
    }

    @Override
    public Page<Task> findAllByEmployee(UUID employeeId, int page, int size) {
        return repository.findAllTaskByEmployeeId(employeeId, createPageRequest(page, size));
    }
}
