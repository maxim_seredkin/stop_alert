package com.ws.sa.services.task;

import com.ws.sa.services.base.Service;
import com.ws.sa.entities.task.Task;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
public interface TaskService extends Service<Task> {

    Collection<Task> findActive();

    Task addMails(UUID taskId);

    void complete(UUID taskId);

    void execution(UUID taskId);

    void expired(UUID taskId);

    boolean hasAccess(UUID taskId, UUID userId);

    Page<Task> findAllTaskByUnit(UUID unitId, int page, int size);

    Page<Task> findAllByEmployee(UUID employeeId, int page, int size);
}
