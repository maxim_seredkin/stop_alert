package com.ws.sa.services.unit;

import com.ws.sa.enums.Role;
import com.ws.sa.services.base.Service;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 31.01.2017.
 */
public interface UnitService extends Service<Unit> {

    Unit findByNameLike(String name);

    boolean existsByNameAndParentId(String name, UUID parentId);

    Collection<Unit> findBranchNodes(Collection<UUID> parentId);

    Boolean notExistsWaitingTasks(UUID unitId);

    Collection<Unit> findByUser(UUID uuid);

    void updatePath(String oldPath, String path);

    Collection<Unit> findBranchNodes(UUID parentId);

    int countBranchEmployees(UUID id);

    Collection<User> findUsersByPathAndRole(UUID id, Role role);

    List<Unit> getUnitPath(String path);

    boolean hasAccess(UUID unitId, UUID userId);

    void deleteBranchFromUsers(UUID id);

    void setBranchDeleted(UUID id);

    void transferBranchEmployees(UUID unitId, UUID successorId);

}
