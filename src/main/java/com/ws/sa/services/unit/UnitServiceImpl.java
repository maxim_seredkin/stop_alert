package com.ws.sa.services.unit;

import com.ws.sa.enums.Role;
import com.ws.sa.services.base.BaseService;
import com.ws.sa.repositories.unit.UnitRepository;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Created by Vladislav Shelengovskiy on 31.01.2017.
 */
@Service
public class UnitServiceImpl extends BaseService<Unit> implements UnitService {

    private final UnitRepository repository;

    @Autowired
    public UnitServiceImpl(UnitRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Collection<Unit> findAll() {
        return repository.findAll();
    }

    @Override
    public Unit save(Unit unitId) {
        return repository.save(unitId);
    }

    @Override
    public void delete(Unit unitId) {
        repository.delete(unitId);
    }

    @Override
    public Unit findByNameLike(String name) {
        return repository.findByNameContainingIgnoreCase(name);
    }

    @Override
    public boolean existsByNameAndParentId(String name, UUID parentId) {
        return Objects.nonNull(parentId) ? repository.existsByNameAndParent_Id(name, parentId) : repository.existsByNameAndParentIsNull(name);
    }

    @Override
    public Collection<Unit> findBranchNodes(UUID parentId) {
        return repository.findByPathContaining(parentId.toString());
    }

    @Override
    public Collection<Unit> findBranchNodes(Collection<UUID> parentIds) {
        return parentIds.stream()
                        .map(this::findBranchNodes)
                        .map(Collection::stream)
                        .reduce(Stream::concat)
                        .orElse(Stream.empty())
                        .collect(toList());
    }

    @Override
    public Boolean notExistsWaitingTasks(UUID unitId) {
        return repository.notExistsWaitingTasks(unitId);
    }

    @Override
    public Collection<Unit> findByUser(UUID id) {
        return repository.findByUsers_Id(id);
    }

    @Override
    public void updatePath(String oldPath, String path) {
        repository.updateBranchPath(oldPath, path);
    }

    @Override
    public int countBranchEmployees(UUID id) {
        return repository.countBranchEmployees(id);
    }

    @Override
    public Collection<User> findUsersByPathAndRole(UUID id, Role role) {
        return repository.findUsersByPathAndAuthority(id, role.getAuthority());
    }

    @Override
    public List<Unit> getUnitPath(String path) {
        if (path.isEmpty()) return new ArrayList<>();

        return Stream.of(path.split("/"))
                     .filter(unit -> !unit.trim().isEmpty())
                     .map(unit -> repository.findOne(UUID.fromString(unit)))
                     .collect(toList());
    }

    @Override
    public boolean hasAccess(UUID unitId, UUID userId) {
        return repository.hasAccess(unitId, userId);
    }

    @Override
    public void deleteBranchFromUsers(UUID id) {
        repository.deleteBranchFromUsers(id.toString());
    }

    @Override
    public void setBranchDeleted(UUID id) {
        repository.setBranchDeleted(id.toString());
    }

    @Override
    public void transferBranchEmployees(UUID unitId, UUID successorId) {
        repository.transferBranchEmployees(unitId.toString(), successorId.toString());
    }
}
