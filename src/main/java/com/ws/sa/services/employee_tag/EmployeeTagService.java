package com.ws.sa.services.employee_tag;

import com.ws.sa.services.base.Service;
import com.ws.sa.entities.employee_tag.EmployeeTag;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface EmployeeTagService extends Service<EmployeeTag> {

    Collection<EmployeeTag> findNodes(UUID parentId);

    Boolean notExistsWaitingTasks(UUID employeeTagId);

    void updatePath(String oldPath, String path);

    List<EmployeeTag> getEmployeeTagPath(String path);

    void deleteBranchFromEmployees(UUID id);

    void setBranchDeleted(UUID id);

}
