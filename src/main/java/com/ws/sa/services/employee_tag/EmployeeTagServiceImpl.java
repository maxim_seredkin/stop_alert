package com.ws.sa.services.employee_tag;

import com.ws.sa.services.base.BaseService;
import com.ws.sa.repositories.employee_tag.EmployeeTagRepository;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Service
public class EmployeeTagServiceImpl extends BaseService<EmployeeTag> implements EmployeeTagService {

    private final EmployeeTagRepository repository;

    @Autowired
    public EmployeeTagServiceImpl(EmployeeTagRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Collection<EmployeeTag> findNodes(UUID parentId) {
        return repository.findByPathContaining(parentId.toString());
    }

    @Override
    public void updatePath(String oldPath, String path) {
        repository.updateBranchPath(oldPath, path);
    }

    @Override
    public Boolean notExistsWaitingTasks(UUID employeeTagId) {
        return repository.notExistsWaitingTasks(employeeTagId);
    }

    @Override
    public List<EmployeeTag> getEmployeeTagPath(String path) {
        if (path.isEmpty()) return new ArrayList<>();

        return Stream.of(path.split("/"))
                     .filter(unit -> !unit.isEmpty())
                     .map(unit -> repository.findOne(UUID.fromString(unit)))
                     .collect(Collectors.toList());
    }

    @Override
    public void deleteBranchFromEmployees(UUID id) {
        repository.deleteBranchFromEmployees(id.toString());
    }

    @Override
    public void setBranchDeleted(UUID id) {
        repository.setBranchDeleted(id.toString());
    }
}
