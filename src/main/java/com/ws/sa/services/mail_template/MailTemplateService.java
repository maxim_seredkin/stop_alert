package com.ws.sa.services.mail_template;


import com.ws.sa.services.base.Service;
import com.ws.sa.entities.mail_template.MailTemplate;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface MailTemplateService extends Service<MailTemplate> {

    boolean existsByName(String name);

}
