package com.ws.sa.services.mail_template;

import com.ws.sa.services.base.BaseService;
import com.ws.sa.repositories.mail_template.MailTemplateRepository;
import com.ws.sa.entities.mail_template.MailTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Service
public class MailTemplateServiceImpl extends BaseService<MailTemplate> implements MailTemplateService {

    private final MailTemplateRepository repository;

    @Autowired
    public MailTemplateServiceImpl(MailTemplateRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public boolean existsByName(String name) {
        return repository.existsByName(name.trim());
    }

}
