package com.ws.sa.services.user;


import com.ws.sa.services.base.Service;
import com.ws.sa.entities.user.User;

import java.util.Collection;

/**
 * @author Maxim Seredkin
 */
public interface UserService extends Service<User> {

    User findOneByUsername(String username);

    boolean existsByUsername(String username);

    Collection<User> findActiveUsersByUsernameOrEmail(String username);

}
