package com.ws.sa.services.user;

import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.entities.user.User;
import com.ws.sa.security.user.LoginAttemptService;
import com.ws.sa.security.user.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Maxim Seredkin
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService service;
    private final LoginAttemptService loginAttemptService;

    @Autowired
    public UserDetailsServiceImpl(UserService service,
                                  LoginAttemptService loginAttemptService) {
        this.service = service;
        this.loginAttemptService = loginAttemptService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = service.findOneByUsername(username);
        Guard.checkEntityExists(user, ApiStatusCode.INVALID_AUTH);
        Guard.checkEntitySecurity(loginAttemptService.nonBlocked(username), ApiStatusCode.USER_LOCKED);
        return UserDetailsImpl.create(user);
    }

}
