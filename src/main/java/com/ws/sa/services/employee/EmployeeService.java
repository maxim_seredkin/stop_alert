package com.ws.sa.services.employee;

import com.ws.sa.services.base.Service;
import com.ws.sa.entities.employee.Employee;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface EmployeeService extends Service<Employee> {

    Integer countAllByTask(UUID taskId);

    Integer countAttackedEmployeesByTask(UUID taskId);

    Boolean existsById(UUID employeeId);

    Boolean notExistsWaitingTasks(UUID employeeId);

    boolean hasAccess(UUID employeeId, UUID userId);

    List<Employee> getFilterEmployeesByTask(UUID taskId);

    List<Employee> getHandEmployeesByTask(UUID taskId);

    Page<Employee> getEmployeesByTask(UUID taskId, int page, int size);

    Integer countAllTasks(UUID id);

    Integer countAttackedTasks(UUID id);

    Integer countAllByUnit(UUID unitId);

    Integer countAttackedByUnit(UUID unitId);

    Integer countAllByUnitAndTask(UUID unitId, UUID taskId);

    Integer countAttackedByUnitAndTask(UUID unitId, UUID taskId);

    Page<Employee> findAllByUnit(UUID unitId, int page, int size);

    List<Employee> getAttackedEmployeeByTask(UUID taskId);
}
