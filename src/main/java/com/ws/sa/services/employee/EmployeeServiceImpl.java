package com.ws.sa.services.employee;

import com.ws.sa.entities.employee.Employee;
import com.ws.sa.repositories.employee.EmployeeRepository;
import com.ws.sa.services.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static com.ws.sa.utils.PageUtils.createPageRequest;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Service
public class EmployeeServiceImpl extends BaseService<Employee> implements EmployeeService {

    private final EmployeeRepository repository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Integer countAttackedEmployeesByTask(UUID taskId) {
        return repository.countAttackedByTaskId(taskId);
    }

    @Override
    public Integer countAllByTask(UUID taskId) {
        return repository.countAllByTaskId(taskId);
    }

    @Override
    public Boolean existsById(UUID employeeId) {
        return repository.existsById(employeeId);
    }

    @Override
    public Boolean notExistsWaitingTasks(UUID employeeId) {
        return repository.notExistsWaitingTasks(employeeId);
    }

    @Override
    public boolean hasAccess(UUID employeeId, UUID userId) {
        return repository.hasAccess(employeeId, userId);
    }


    @Override
    public List<Employee> getFilterEmployeesByTask(UUID taskId) {
        return repository.findFilterEmployeesByTask(taskId);
    }

    @Override
    public List<Employee> getHandEmployeesByTask(UUID taskId) {
        return repository.findHandEmployeesByTask(taskId);
    }

    @Override
    public Page<Employee> getEmployeesByTask(UUID taskId, int page, int size) {
        return repository.findAllByTaskId(taskId, createPageRequest(page, size));
    }

    @Override
    public Integer countAllTasks(UUID id) {
        return repository.countAllByEmployeeId(id);
    }

    @Override
    public Integer countAttackedTasks(UUID id) {
        return repository.countAttackedByEmployeeId(id);
    }

    @Override
    public Integer countAllByUnit(UUID unitId) {
        return repository.countAllByUnitId(unitId);
    }

    public Integer countAttackedByUnit(UUID unitId) {
        return repository.countAttackedByUnitId(unitId);
    }

    @Override
    public Integer countAllByUnitAndTask(UUID unitId, UUID taskId) {
        return repository.countAllByTaskIdAndUnitId(taskId, unitId);
    }

    @Override
    public Integer countAttackedByUnitAndTask(UUID unitId, UUID taskId) {
        return repository.countAttackedByTaskIdAndUnitId(taskId, unitId);
    }

    @Override
    public Page<Employee> findAllByUnit(UUID unitId, int page, int size) {
        return repository.findAllByUnitId(unitId, createPageRequest(page, size));
    }

    @Override
    public List<Employee> getAttackedEmployeeByTask(UUID taskId) {
        return repository.findAttackedByTaskId(taskId);
    }
}
