package com.ws.sa.services.transition;

import com.ws.sa.repositories.transition.TransitionRepository;
import com.ws.sa.entities.transition.Transition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 27.02.2017.
 */
@Service
public class TransitionServiceImpl implements TransitionService {

    private final TransitionRepository repository;

    @Autowired
    public TransitionServiceImpl(TransitionRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Transition> findAll() {
        return repository.findAll();
    }

    @Override
    public Transition findOne(UUID transitionId) {
        return repository.findOne(transitionId);
    }

    @Override
    public Transition save(Transition transition) {
        return repository.saveAndFlush(transition);
    }

    @Override
    public void delete(Transition transition) {
        repository.delete(transition);
    }

    @Override
    public Collection<Transition> findAllByTask(UUID id) {
        return repository.findAllByMail_Task_Id(id);
    }

    @Override
    public Collection<Transition> findAllByTaskAndEmployee(UUID taskId, UUID employeeId) {
        return repository.findAllByMail_Task_IdAndMail_Employee_Id(taskId, employeeId);
    }
}
