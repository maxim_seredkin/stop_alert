package com.ws.sa.services.transition;

import com.ws.sa.entities.transition.Transition;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 27.02.2017.
 */
public interface TransitionService {

    List<Transition> findAll();

    Transition findOne(UUID transitionId);

    Transition save(Transition transition);

    void delete(Transition transition);

    Collection<Transition> findAllByTask(UUID id);

    Collection<Transition> findAllByTaskAndEmployee(UUID taskId, UUID employeeId);
}
