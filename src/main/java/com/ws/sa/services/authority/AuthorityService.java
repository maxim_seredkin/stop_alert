package com.ws.sa.services.authority;


import com.ws.sa.services.base.Service;
import com.ws.sa.entities.authority.Authority;

/**
 * Created by vlad_shelengovskiy on 24.11.2016.
 */
public interface AuthorityService extends Service<Authority> {

}
