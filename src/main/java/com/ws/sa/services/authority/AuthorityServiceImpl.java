package com.ws.sa.services.authority;

import com.ws.sa.services.base.BaseService;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.entities.authority.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by vlad_shelengovskiy on 24.11.2016.
 */
@Service
public class AuthorityServiceImpl extends BaseService<Authority> implements AuthorityService {

    @Autowired
    public AuthorityServiceImpl(AuthorityRepository repository) {
        super(repository);
    }

}