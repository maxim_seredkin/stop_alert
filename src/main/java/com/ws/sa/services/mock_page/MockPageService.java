package com.ws.sa.services.mock_page;


import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.services.base.Service;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
public interface MockPageService extends Service<MockPage> {

    boolean existsByName(String name);

}
