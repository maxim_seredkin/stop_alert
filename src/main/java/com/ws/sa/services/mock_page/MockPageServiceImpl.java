package com.ws.sa.services.mock_page;

import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.repositories.mock_page.MockPageRepository;
import com.ws.sa.services.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Service
public class MockPageServiceImpl extends BaseService<MockPage> implements MockPageService {

    private final MockPageRepository repository;

    @Autowired
    public MockPageServiceImpl(MockPageRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public boolean existsByName(String name) {
        return repository.existsByName(name);
    }

}
