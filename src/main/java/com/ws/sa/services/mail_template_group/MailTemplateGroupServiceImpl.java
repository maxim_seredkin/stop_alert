package com.ws.sa.services.mail_template_group;

import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.repositories.mail_template.MailTemplateRepository;
import com.ws.sa.repositories.mail_template_group.MailTemplateGroupRepository;
import com.ws.sa.services.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Service
public class MailTemplateGroupServiceImpl extends BaseService<MailTemplateGroup> implements MailTemplateGroupService {

    private final MailTemplateGroupRepository repository;
    private final MailTemplateRepository mailTemplateRepository;

    @Autowired
    public MailTemplateGroupServiceImpl(MailTemplateGroupRepository repository, MailTemplateRepository mailTemplateRepository) {
        super(repository);
        this.repository = repository;
        this.mailTemplateRepository = mailTemplateRepository;
    }

    @Override
    public void delete(MailTemplateGroup mailTemplateGroup) {
        mailTemplateRepository.setNullValueGroupByGroup(mailTemplateGroup);
        repository.delete(mailTemplateGroup);
    }

    @Override
    public boolean existsByName(String name) {
        return repository.existsByName(name);
    }

}
