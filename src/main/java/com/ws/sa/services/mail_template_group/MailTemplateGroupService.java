package com.ws.sa.services.mail_template_group;


import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.services.base.Service;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface MailTemplateGroupService extends Service<MailTemplateGroup> {

    boolean existsByName(String name);

}
