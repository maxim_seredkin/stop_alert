package com.ws.sa.services.statistic;

import com.ws.sa.dtos.statistic.MailStatDTO;
import com.ws.sa.dtos.statistic.TemplateStatDTO;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.mail.MailService;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.services.unit.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;

import static java.util.stream.Collectors.toList;


/**
 * Created by Vladislav Shelengovskiy on 29.03.2017.
 */
@Service
public class StatisticServiceImpl implements StatisticService {

    private final TaskService taskService;
    private final EmployeeService employeeService;
    private final UnitService unitService;
    private MailService mailService;
    private MailTemplateService templateService;
    private MailTemplateMapper templateMapper;

    @Autowired
    public StatisticServiceImpl(MailService mailService, MailTemplateService templateService,
                                MailTemplateMapper templateMapper, TaskService taskService,
                                EmployeeService employeeService, UnitService unitService) {
        this.mailService = mailService;
        this.templateService = templateService;
        this.templateMapper = templateMapper;
        this.taskService = taskService;
        this.employeeService = employeeService;
        this.unitService = unitService;
    }

    public MailStatDTO getMailStat() {
        return new MailStatDTO(mailService.count(),
                               mailService.countCorrect(),
                               templateService.findAll()
                                              .stream()
                                              .map(template -> new TemplateStatDTO(templateMapper.toLight(template),
                                                                                   mailService.countWrongByTemplate(template.getId())))
                                              .sorted(Comparator.comparing(TemplateStatDTO::getCountMailIncorrect, Comparator.reverseOrder()))
                                              .limit(5)
                                              .collect(toList()));
    }
}


