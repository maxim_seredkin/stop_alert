package com.ws.sa.services.statistic;

import com.ws.sa.dtos.statistic.MailStatDTO;
import com.ws.sa.entities.task.Task;

import org.springframework.transaction.annotation.Transactional;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 29.03.2017.
 */
public interface StatisticService {
    MailStatDTO getMailStat();
}
