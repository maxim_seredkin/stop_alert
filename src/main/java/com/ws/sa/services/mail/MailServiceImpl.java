package com.ws.sa.services.mail;

import com.google.common.collect.Lists;
import com.ws.sa.repositories.mail.MailRepository;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
@Service
public class MailServiceImpl implements MailService {

    private final MailRepository repository;

    @Value("${transition.redirect.uri.default}")
    private String DEFAULT_REDIRECT_URI;

    @Autowired
    public MailServiceImpl(MailRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Mail> findAll() {
        return repository.findAll();
    }

    @Override
    public Mail findOne(UUID mailId) {
        return repository.findOne(mailId);
    }

    @Override
    public Mail save(Mail mail) {
        return repository.saveAndFlush(mail);
    }

    @Override
    public void delete(Mail mail) {
        repository.delete(mail);
    }

    @Override
    public void deleteByTask(UUID taskId) {
        repository.deleteByTaskId(taskId);
    }

    @Override
    public Collection<Mail> findAll(Task task) {
        return task != null ? repository.findByTaskId(task.getId()) : Lists.newArrayList();
    }

    @Override
    public Collection<Mail> findAll(UUID id) {
        return repository.findByTaskId(id);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public long countCorrect() {
        return repository.countCorrect();
    }

    @Override
    public long countWrongByTemplate(UUID templateId) {
        return repository.countWrongByTemplate(templateId);
    }

    @Override
    public String findMailRedirectUriOrGetDefault(UUID mailId) {
        Mail mail = findOne(mailId);

        if (Objects.isNull(mail))
            return DEFAULT_REDIRECT_URI;

        return mail.getTask().getRedirect().getRedirectUri();
    }
}
