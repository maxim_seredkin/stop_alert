package com.ws.sa.services.mail;

import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.task.Task;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
public interface MailService {

    List<Mail> findAll();

    Mail findOne(UUID mailId);

    Mail save(Mail mail);

    void delete(Mail mail);

    void deleteByTask(UUID taskId);

    Collection<Mail> findAll(Task task);

    Collection<Mail> findAll(UUID id);

    long count();

    long countCorrect();

    long countWrongByTemplate(UUID templateId);

    String findMailRedirectUriOrGetDefault(UUID mailId);
}
