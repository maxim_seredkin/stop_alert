package com.ws.sa.services.base;

import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.exceptions.ApiException;
import com.ws.sa.repositories.base.BaseRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Seredkin M. on 07.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseService<EntityT extends BaseEntity> implements Service<EntityT> {

    protected final BaseRepository<EntityT> repository;

    protected BaseService(BaseRepository<EntityT> repository) {this.repository = repository;}

    @Override
    @Transactional(readOnly = true)
    public Collection<EntityT> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<EntityT> findAll(Specification<EntityT> specification) {
        return repository.findAll(specification);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EntityT> findOne(UUID id) {
        return repository.findOneOptionalById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public EntityT findOneExisting(UUID id) {
        return repository.findOneOptionalById(id)
                         .orElseThrow(ApiException::notFound);
    }

    @Override
    @Transactional
    public EntityT save(EntityT entity) {
        return repository.save(entity);
    }

    @Override
    @Transactional
    public void delete(EntityT entity) {
        repository.delete(entity);
    }

}
