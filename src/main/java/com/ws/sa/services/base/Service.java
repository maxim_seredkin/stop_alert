package com.ws.sa.services.base;

import com.ws.sa.entities.base.BaseEntity;
import org.springframework.data.jpa.domain.Specification;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Seredkin M. on 07.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public interface Service<EntityT extends BaseEntity> {

    Collection<EntityT> findAll();

    Collection<EntityT> findAll(Specification<EntityT> specification);

    Optional<EntityT> findOne(UUID id);

    EntityT findOneExisting(UUID id);

    EntityT save(EntityT entity);

    void delete(EntityT entity);

}
