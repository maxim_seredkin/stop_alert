package com.ws.sa.services.base;

import com.ws.sa.enums.ResourceType;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.EntityNotFoundException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 17.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Service
public class ResourceServiceImpl implements ResourceService {

    @Override
    public File get(String name, ResourceType type) {
        Resource resource = new ClassPathResource(Paths.get(type.getFolder(), name).toString());
        try {
            return resource.getFile();
        } catch (IOException e) {
            throw new EntityNotFoundException(ApiStatusCode.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public String create(MultipartFile file, ResourceType type) {
        try {
            String fileName = UUID.randomUUID().toString().concat(type.getExtension());
            Path path = Paths.get(new ClassPathResource(type.getFolder()).getFile().getAbsolutePath(), fileName);
            Files.copy(file.getInputStream(), path);
            return fileName;
        } catch (IOException e) {
            throw new EntityNotFoundException(ApiStatusCode.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public void update(String name, MultipartFile file, ResourceType type) {
        try {
            Path path = Paths.get(new ClassPathResource(type.getFolder()).getFile().getAbsolutePath(), name);
            if (file != null)
                Files.copy(file.getInputStream(),
                           path,
                           StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new EntityNotFoundException(ApiStatusCode.RESOURCE_NOT_FOUND);
        }
    }

    @Override
    public void delete(String name, ResourceType type) {
        try {
            Files.delete(new ClassPathResource(Paths.get(type.getFolder(), name).toString()).getFile().toPath());
        } catch (IOException e) {
            throw new EntityNotFoundException(ApiStatusCode.RESOURCE_NOT_FOUND);
        }
    }

}
