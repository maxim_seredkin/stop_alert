package com.ws.sa.services.base;

import com.ws.sa.enums.ResourceType;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;


/**
 * Created by Vladislav Shelengovskiy on 17.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public interface ResourceService {

    File get(String name, ResourceType type);

    String create(MultipartFile file, ResourceType type);

    void update(String name, MultipartFile file, ResourceType type);

    void delete(String name, ResourceType type);
}
