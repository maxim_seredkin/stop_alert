package com.ws.sa.services.app_context;

import org.springframework.context.ApplicationContextAware;
import org.springframework.data.repository.Repository;

/**
 * @author Evgeny Albinets
 */
public interface AppContextService extends ApplicationContextAware {
    <T> T getBean(Class<T> clazz);


    public Repository getRepository(Class clazz);
}
