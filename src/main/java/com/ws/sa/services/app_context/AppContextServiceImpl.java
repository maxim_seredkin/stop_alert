package com.ws.sa.services.app_context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Service;

/**
 * @author Evgeny Albinets
 */
@Service
public class AppContextServiceImpl implements AppContextService {

    private static ApplicationContext applicationContext;

    private static Repositories repositories;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        repositories = new Repositories(applicationContext);
    }

    /**
     * Возвращает бин для соответствуещего класса
     *
     * @param clazz Класс, объект которго необходимо вернуть
     * @return Бин
     */
    public <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }


    /**
     * Возвращает репозиторий для сущности
     *
     * @param clazz Класс сущности для которой нужно вернуть репозиторий
     * @return Репозитори для сущности
     */
    @Override
    public Repository getRepository(Class clazz) {
        return (JpaRepository) repositories.getRepositoryFor(clazz);
    }

}
