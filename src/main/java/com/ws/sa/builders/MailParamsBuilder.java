package com.ws.sa.builders;

import com.google.common.collect.Lists;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.configs.AppConfig;
import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Maxim Seredkin
 * @since 28.02.2017
 */
@Log
@Component
public class MailParamsBuilder {

    private final AppConfig appConfig;

    @Autowired
    public MailParamsBuilder(AppConfig appConfig) {
        this.appConfig = appConfig;
    }


    public Map<String, Object> build(Mail mail) {

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("employee", mail.getEmployee());
        params.put("link", StringUtils.join(Lists.newArrayList(appConfig.getTransitionAddress(), mail.getId()), ""));

        return params;
    }

}
