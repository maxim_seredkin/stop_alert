package com.ws.sa.builders;

import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.renders.MailRender;
import com.ws.sa.entities.mail_template.MailTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Objects;

/**
 * @author Maxim Seredkin
 * @since 28.02.2017
 */
@Component
public class MailBuilder {

    private static final Logger log = LoggerFactory.getLogger(MailBuilder.class.getName());

    private final JavaMailSender javaMailSender;
    private final MailParamsBuilder paramsBuilder;
    private final MailRender mailRender;

    @Autowired
    public MailBuilder(JavaMailSender javaMailSender, MailParamsBuilder paramsBuilder, MailRender mailRender) {
        this.javaMailSender = javaMailSender;
        this.paramsBuilder = paramsBuilder;
        this.mailRender = mailRender;
    }

    public MimeMessage build(Mail mail) {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {
            MailTemplate template = mail.getTask().getTemplate();
            Employee employee = mail.getEmployee();
            InternetAddress to = new InternetAddress(employee.getEmail(), employee.getFullName());
            Map<String, Object> params = paramsBuilder.build(mail);

            String body = mailRender.render(template.getPath(), params);
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom(new InternetAddress(getFromAddress(template.getFromAddress()), template.getFromPersonal()));
            mimeMessageHelper.setTo(new InternetAddress[]{to});
            mimeMessageHelper.setSubject(template.getSubject());
            mimeMessageHelper.setText(body, true);
        } catch (MessagingException ex) {
            log.debug("Письмо не создано");
        } catch (UnsupportedEncodingException ex) {
            log.debug("Неподдерживаемый тип кодировки письма");
        }

        return mimeMessage;
    }

    public MimeMessage build(String toAddress, String toPersonal, String fromPersonal, String fromAddress, String subject, String templatePath, Map<String, Object> params) {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {
            InternetAddress to = new InternetAddress(toAddress, toPersonal);

            //  рендеринг тела письма
            String body = mailRender.render(templatePath, params);
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setFrom(new InternetAddress(getFromAddress(fromAddress), fromPersonal));
            mimeMessageHelper.setTo(new InternetAddress[]{to});
            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.setText(body, true);
        } catch (MessagingException ex) {
            log.debug("Письмо не создано");
        } catch (UnsupportedEncodingException ex) {
            log.debug("Неподдерживаемый тип кодировки письма");
        }

        return mimeMessage;
    }

    private String getFromAddress(String fromAddress) {
        if (Objects.isNull(fromAddress))
            return getSenderAddress();

        return fromAddress;
    }

    private String getSenderAddress() {
        return ((JavaMailSenderImpl) javaMailSender).getUsername();
    }
}
