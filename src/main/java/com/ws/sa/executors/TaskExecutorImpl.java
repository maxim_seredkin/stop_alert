package com.ws.sa.executors;

import com.google.common.collect.Queues;
import com.ws.sa.builders.MailParamsBuilder;
import com.ws.sa.configs.AppConfig;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.entities.task.Task;
import com.ws.sa.listeners.mail.events.SendMailEvent;
import com.ws.sa.managers.TaskManager;
import com.ws.sa.services.mail.MailService;
import com.ws.sa.services.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * @author Maxim Seredkin
 * @since 02.04.2017
 */
@Service
public class TaskExecutorImpl implements TaskExecutor {

    @Qualifier("customMailSender")
    private final MailService mailService;
    private final TaskService taskService;
    private final ApplicationEventPublisher publisher;
    private final MailParamsBuilder mailParamsBuilder;
    private final AppConfig config;

    @Autowired
    private TaskManager taskManager;

    public TaskExecutorImpl(MailService mailService, TaskService taskService,
                            ApplicationEventPublisher publisher, MailParamsBuilder mailParamsBuilder,
                            AppConfig config) {
        this.mailService = mailService;
        this.taskService = taskService;
        this.publisher = publisher;
        this.mailParamsBuilder = mailParamsBuilder;
        this.config = config;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void run(Task task) {
        task = runTask(task);
        sendMails(task.getMails().stream().filter(Mail::isNotSend).collect(Collectors.toList()));
        taskManager.addToWaitingCompleteQueue(task);
    }

    private Task runTask(Task task) {
        if (!task.isWaiting())
            return task;

        taskService.execution(task.getId());
        task = taskService.addMails(task.getId());
        return task;
    }

    private void sendMails(Collection<Mail> mails) {
        Queue<Mail> queueMails = Queues.newConcurrentLinkedQueue(mails);
        Mail mail;
        while (Objects.nonNull(mail = queueMails.poll())) {
            final MailTemplate mailTemplate = mail.getTask().getTemplate();

            // TODO починить адрес отправителя

            publisher.publishEvent(SendMailEvent.builder()
                                                .toAddress(mail.getEmployee().getEmail())
                                                .toPersonal(mail.getEmployee().getFullName())
                                                .fromPersonal(mailTemplate.getFromPersonal())
                                                .fromAddress((config.getAnonymous()) ? mailTemplate.getFromAddress() : config.getMailUsername())
                                                .params(mailParamsBuilder.build(mail))
                                                .subject(mailTemplate.getSubject())
                                                .templatePath(mailTemplate.getPath()).build());

            mail.setSendDate(new Date());
            mail.setSend(true);
            mailService.save(mail);
        }
    }
}
