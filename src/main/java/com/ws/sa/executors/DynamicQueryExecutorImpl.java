package com.ws.sa.executors;

import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.specifications.base.JpaQuerySpecification;
import com.ws.sa.services.app_context.AppContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;

/**
 * @author Evgeny Albinets
 */
@Service
public class DynamicQueryExecutorImpl implements DynamicQueryExecutor {

    private final AppContextService appContextService;

    @Autowired
    public DynamicQueryExecutorImpl(AppContextService appContextService) {this.appContextService = appContextService;}

    @Override
    public <EntityT extends BaseEntity> Page<EntityT> findAll(JpaQuerySpecification<EntityT> querySpecification) {
        JpaSpecificationExecutor repository = (JpaSpecificationExecutor) appContextService.getRepository(querySpecification.getEntityClass());

        PageRequest pageRequest = querySpecification.getPageRequest();
        if (pageRequest != null)
            return repository.<EntityT>findAll(querySpecification.getQueryPredicate(), pageRequest);

        return new PageImpl<EntityT>(repository.<EntityT>findAll(querySpecification.getQueryPredicate(), querySpecification.getSort()));
    }

    @Override
    public <EntityT extends BaseEntity> Boolean exists(JpaQuerySpecification<EntityT> querySpecification) {
        return count(querySpecification) > 0;
    }

    @Override
    public <EntityT extends BaseEntity> Long count(JpaQuerySpecification<EntityT> querySpecification) {
        JpaSpecificationExecutor repository = (JpaSpecificationExecutor) appContextService.getRepository(querySpecification.getEntityClass());
        return repository.count(querySpecification.getQueryPredicate());
    }

}
