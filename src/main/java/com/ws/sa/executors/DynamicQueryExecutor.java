package com.ws.sa.executors;

import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.specifications.base.JpaQuerySpecification;
import org.springframework.data.domain.Page;

/**
 * @author Evgeny Albinets
 */
public interface DynamicQueryExecutor {

    <EntityT extends BaseEntity> Page<EntityT> findAll(JpaQuerySpecification<EntityT> querySpecification);

    <EntityT extends BaseEntity> Boolean exists(JpaQuerySpecification<EntityT> querySpecification);

    <EntityT extends BaseEntity> Long count(JpaQuerySpecification<EntityT> querySpecification);

}
