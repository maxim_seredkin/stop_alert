package com.ws.sa.executors;

import com.ws.sa.entities.task.Task;

/**
 * @author Maxim Seredkin
 * @since 02.04.2017
 */
public interface TaskExecutor {
    void run(Task task);
}
