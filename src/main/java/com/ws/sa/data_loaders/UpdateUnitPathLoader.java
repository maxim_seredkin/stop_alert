package com.ws.sa.data_loaders;

import com.ws.sa.entities.unit.Unit;
import com.ws.sa.services.unit.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Objects;

/**
 * Created by Vladislav Shelengovskiy on 20.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Component
public class UpdateUnitPathLoader {

    @Autowired
    private UnitService service;

    public void update(){
       Collection<Unit> roots =  service.findAll((root, criteriaQuery, criteriaBuilder) -> root.get("parent").isNull());
       roots.forEach(this::updateWithRecursive);
    }

    private void updateWithRecursive(Unit unit) {
        Unit parent = unit.getParent();
        String path = Objects.isNull(parent) ? ""
                                             : String.format("%s/%s",
                                                             parent.getPath(),
                                                             parent.getId().toString());
        unit.setPath(path);
        service.save(unit);

        service.findAll((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("parent").get("id"), unit.getId()))
               .forEach(this::updateWithRecursive);
    }
}
