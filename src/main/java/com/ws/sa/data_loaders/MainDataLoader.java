package com.ws.sa.data_loaders;

import com.github.matek2305.dataloader.DataLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim Seredkin
 */
@Component
public class MainDataLoader implements DataLoader {

    private final UserDataLoader userDataLoader;
    private final AuthorityDataLoader authorityDataLoader;
    private final MailTemplateDataLoader mailTemplateDataLoader;
    private final MailTemplateGroupDataLoader mailTemplateGroupDataLoader;
    private final MockPageDataLoader mockPageDataLoader;
    private final LoadTestingDataLoader loadTestingDataLoader;

    @Autowired
    public MainDataLoader(UserDataLoader userDataLoader, AuthorityDataLoader authorityDataLoader,
                          MailTemplateDataLoader mailTemplateDataLoader, MailTemplateGroupDataLoader mailTemplateGroupDataLoader,
                          MockPageDataLoader mockPageDataLoader, LoadTestingDataLoader loadTestingDataLoader) {
        this.userDataLoader = userDataLoader;
        this.authorityDataLoader = authorityDataLoader;
        this.mailTemplateDataLoader = mailTemplateDataLoader;
        this.mailTemplateGroupDataLoader = mailTemplateGroupDataLoader;
        this.mockPageDataLoader = mockPageDataLoader;
        this.loadTestingDataLoader = loadTestingDataLoader;
    }

    @Override
    public void load() {
        authorityDataLoader.load();
        userDataLoader.load();
        mailTemplateGroupDataLoader.load();
        mailTemplateDataLoader.load();
        mockPageDataLoader.load();
        loadTestingDataLoader.load();
    }
}
