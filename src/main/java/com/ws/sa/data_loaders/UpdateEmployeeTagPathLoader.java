package com.ws.sa.data_loaders;

import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Objects;

/**
 * Created by Vladislav Shelengovskiy on 21.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Component
public class UpdateEmployeeTagPathLoader {

    @Autowired
    private EmployeeTagService service;

    public void update(){
        Collection<EmployeeTag> roots =  service.findAll((root, criteriaQuery, criteriaBuilder) -> root.get("parent").isNull());
        roots.forEach(this::updateWithRecursive);
    }

    private void updateWithRecursive(EmployeeTag tag) {
        EmployeeTag parent = tag.getParent();
        String path = Objects.isNull(parent) ? ""
                                             : String.format("%s/%s",
                                                             parent.getPath(),
                                                             parent.getId().toString());
        tag.setPath(path);
        service.save(tag);

        service.findAll((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("parent").get("id"), tag.getId()))
               .forEach(this::updateWithRecursive);
    }
}
