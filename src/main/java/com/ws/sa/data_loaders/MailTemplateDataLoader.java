package com.ws.sa.data_loaders;

import com.ws.sa.repositories.mail_template.MailTemplateRepository;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.repositories.mail_template_group.MailTemplateGroupRepository;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@Component
public class MailTemplateDataLoader {

    private final MailTemplateRepository repository;
    private final MailTemplateGroupRepository mailTemplateGroupRepository;

    @Autowired
    public MailTemplateDataLoader(MailTemplateRepository repository, MailTemplateGroupRepository mailTemplateGroupRepository) {
        this.repository = repository;
        this.mailTemplateGroupRepository = mailTemplateGroupRepository;
    }

    public void load() {
        if (repository.count() > 0)
            return;

        List<MailTemplateGroup> groups = mailTemplateGroupRepository.findAll();

        addMailTemplate("Сбербанк", "Шаблон письма Сбербанка о просроченном платеже по кредиту", "sberbank.vm", "Просроченный платеж по кредиту", "Сбербанк", "sberbank@sberbank.ru", "http://www.sberbank.ru/ru/person", groups.get(0));
        addMailTemplate("Налоговая", "Уведомление о начале судебного рабирательства", "law.vm", "Уведомление о начале судебного рабирательства", "Налоговая", "support@nalog.ru", "https://www.nalog.ru/", groups.get(0));
        addMailTemplate("Почта России", "Шаблон письма Почта России", "russianpost.vm", "Сервис подготовки посылок ФГУП \"ПочтаРоссия\"", "ФГУП \"ПочтаРоссии\"", "support@russianpost.ru", "https://www.pochta.ru/", groups.get(1));
        addMailTemplate("PayPal", "Информационное сообщение", "paypal.vm", "Информационное сообщение PayPal", "PayPal", "support@paypal.com", "https://www.paypal.com/us/home", groups.get(1));
        addMailTemplate("Яндекс", "Бонус", "yandex.vm", "Яндекс.Деньги бонус", "Яндекс.Деньги", "bonus@money.yandex.ru", "https://money.yandex.ru/new", groups.get(2));
        addMailTemplate("Сбербанк-Онлайн", "Шаблон с уведомлением о вирусе-шифровальщике", "yandex.vm", "Защита от мошеничества", "Сбербанк", "sberbank@sberbank.ru", "https://online.sberbank.ru/CSAFront/index.do", groups.get(2));
    }

    private void addMailTemplate(String name, String description, String path, String subject, String fromPersonal, String fromAddress, String redirectUri, MailTemplateGroup group) {
        MailTemplate mailTemplate = new MailTemplate();
        mailTemplate.setName(name);
        mailTemplate.setDescription(description);
        mailTemplate.setPath(path);
        mailTemplate.setSubject(subject);
        mailTemplate.setFromAddress(fromAddress);
        mailTemplate.setFromPersonal(fromPersonal);
        mailTemplate.setRedirectUri(redirectUri);
        mailTemplate.setGroup(group);
        repository.saveAndFlush(mailTemplate);
    }
}
