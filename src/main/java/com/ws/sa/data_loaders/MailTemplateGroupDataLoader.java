package com.ws.sa.data_loaders;

import com.ws.sa.repositories.mail_template_group.MailTemplateGroupRepository;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@Component
public class MailTemplateGroupDataLoader {

    private final MailTemplateGroupRepository repository;

    @Autowired
    public MailTemplateGroupDataLoader(MailTemplateGroupRepository repository) {
        this.repository = repository;
    }

    public void load() {
        if (repository.count() > 0)
            return;

        addMailTemplate("Базовый");
        addMailTemplate("Средний");
        addMailTemplate("Продвинутый");
    }

    private void addMailTemplate(String name) {
        MailTemplateGroup mailTemplateGroup = new MailTemplateGroup();
        mailTemplateGroup.setName(name);

        repository.saveAndFlush(mailTemplateGroup);
    }
}
