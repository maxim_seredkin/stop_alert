package com.ws.sa.data_loaders;

import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.repositories.user.UserRepository;
import com.ws.sa.entities.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Maxim Seredkin
 */
@Component
public class UserDataLoader {

    private final AuthorityRepository authorityRepository;
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserDataLoader(AuthorityRepository authorityRepository, UserRepository repository,
                          PasswordEncoder passwordEncoder) {
        this.authorityRepository = authorityRepository;
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    public void load() {
        if (repository.count() > 0)
            return;

        loadAdminUser();
    }

    private void loadAdminUser() {
        User user = new User();
        user.setUsername("admin");
        user.setPassword(passwordEncoder.encode("@ynbAbibyu"));
        user.setAuthority(authorityRepository.findByAuthority("ROLE_ADMINISTRATOR"));
        user.setEmail("admin@risk-guard.com");
        repository.saveAndFlush(user);
    }
}
