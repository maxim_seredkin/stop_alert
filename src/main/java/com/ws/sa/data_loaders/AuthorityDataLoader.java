package com.ws.sa.data_loaders;

import com.ws.sa.enums.Role;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.entities.authority.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim Seredkin
 */
@Component
public class AuthorityDataLoader {

    private final AuthorityRepository repository;

    @Autowired
    public AuthorityDataLoader(AuthorityRepository repository) {
        this.repository = repository;
    }

    public void load() {
        if (repository.count() > 0)
            return;

        Role[] roles = Role.values();
        for (Role role : roles) {
            repository.saveAndFlush(Authority.builder().authority(role.getAuthority()).displayName(role.getDisplayName()).build());
        }
    }
}
