package com.ws.sa.data_loaders;

import com.ws.sa.entities.authority.Authority;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.entities.task.Task;
import com.ws.sa.entities.transition.Transition;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.repositories.employee.EmployeeRepository;
import com.ws.sa.repositories.mail.MailRepository;
import com.ws.sa.repositories.mail_template.MailTemplateRepository;
import com.ws.sa.repositories.task.TaskRepository;
import com.ws.sa.repositories.transition.TransitionRepository;
import com.ws.sa.repositories.unit.UnitRepository;
import com.ws.sa.repositories.user.UserRepository;
import com.ws.sa.utils.CalendarUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Vladislav Shelengovskiy on 12.09.2017.
 */
@Component
public class LoadTestingDataLoader {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UnitRepository unitRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private MailRepository mailRepository;

    @Autowired
    private TransitionRepository transitionRepository;

    @Autowired
    private MailTemplateRepository mailTemplateRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void load() {
        if(taskRepository.count() > 0) return;

        Authority authority = new Authority();
        authority = authorityRepository.save(authority);

        User user = new User();
        user.setUsername("user");
        user.setPassword(passwordEncoder.encode("user"));
        user.setEmail("user@e-mail.com");
        user.setAuthority(authority);
        user = userRepository.save(user);

        MailTemplate mailTemplate = new MailTemplate();
        mailTemplate.setName("some name");
        mailTemplate.setSubject("some subject");
        mailTemplate.setFromPersonal("from personal");
        mailTemplate.setFromAddress("from address");
        mailTemplate.setRedirectUri("some uri");
        mailTemplate = mailTemplateRepository.save(mailTemplate);

        Unit unit = new Unit();
        unit.setName("root");
        unit = unitRepository.save(unit);

        int countEmployees = 30;

        List<Employee> employees = new ArrayList<>();

        for (int i = 0; i < countEmployees; ++i) {
            employees.add(employeeRepository.save(new Employee(UUID.randomUUID().toString(),
                                                               UUID.randomUUID().toString(),
                                                               UUID.randomUUID().toString(),
                                                               "some".concat(String.valueOf(i)).concat("@mail.com"),
                                                               "big boss",
                                                               unit)));
        }

        int countTasks = 3;

        for (int i = 0; i < countTasks; ++i) {
            Task task = new Task();
            task.setName(UUID.randomUUID().toString());
            task.setAuthor(user);
            task.setUnits(Stream.of(unit).collect(Collectors.toList()));
            task.setTemplate(mailTemplate);
            task.setStartDate(CalendarUtil.addDays(new Date(), 180));
            task.setEndDate(CalendarUtil.addDays(new Date(), 187));
            task.setEmployeesByFilter(employees);
            task = taskRepository.save(task);

            Task finalTask = task;
            employees.forEach(employee -> {
                Mail mail = mailRepository.save(new Mail(finalTask, employee));

                int countTransition = new Random().nextInt(5);

                for (int j = 0; j < countTransition; ++j) {
                    transitionRepository.save(new Transition(mail,
                                                             new Date(),
                                                             "127.0.0.1",
                                                             "super browser",
                                                             "MS-DOS",
                                                             "no info".concat(String.valueOf(j))));
                }
            });
        }
    }
}

