package com.ws.sa.data_loaders;

import com.ws.sa.repositories.mock_page.MockPageRepository;
import com.ws.sa.entities.mock_page.MockPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@Component
public class MockPageDataLoader {

    private final MockPageRepository repository;

    @Autowired
    public MockPageDataLoader(MockPageRepository repository) {
        this.repository = repository;
    }

    public void load() {
        if (repository.count() > 0)
            return;

        addMockPage("ПХК", "info/goverment.html", "/info/goverment.html");
    }

    private void addMockPage(String name, String path, String redirectUri) {
        MockPage mockPage = new MockPage();
        mockPage.setName(name);
        mockPage.setPath(path);
        mockPage.setRedirectUri(redirectUri);
        repository.saveAndFlush(mockPage);
    }
}
