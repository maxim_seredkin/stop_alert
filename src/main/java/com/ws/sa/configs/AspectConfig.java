package com.ws.sa.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author Maxim Seredkin
 * @since 09.02.2017
 */
@Configuration
@EnableAspectJAutoProxy
public class AspectConfig {
}
