package com.ws.sa.configs;

import com.github.matek2305.dataloader.annotations.EnableDataLoader;
import com.github.matek2305.dataloader.config.DataLoaderConfiguration;
import org.springframework.context.annotation.Configuration;

/**
 * @author Maxim Seredkin
 */
@Configuration
@EnableDataLoader
public class DataLoaderConfig extends DataLoaderConfiguration {
}
