package com.ws.sa.configs;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Seredkin M. on 24.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@EnableCaching
@Configuration
public class CacheConfig {
}
