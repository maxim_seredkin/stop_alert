package com.ws.sa.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.expression.SecurityExpressionOperations;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;
import org.springframework.security.oauth2.provider.vote.ClientScopeVoter;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.access.expression.WebSecurityExpressionRoot;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Maxim Seredkin
 */
@Configuration(value = "commonOAuth2Config")
public class OAuth2Config {

    @Bean
    public DefaultMethodSecurityExpressionHandler defaultMethodSecurityExpressionHandler() {
        DefaultMethodSecurityExpressionHandler handler = new DefaultMethodSecurityExpressionHandler();
        handler.setDefaultRolePrefix("");
        return handler;
    }

    @Bean
    public AccessDecisionVoter roleVoter() {
        RoleVoter roleVoter = new RoleVoter();
//        roleVoter.setRolePrefix("");
        return roleVoter;
    }

    @Bean
    public AccessDecisionVoter webExpressionVoter() {
        WebExpressionVoter webExpressionVoter = new WebExpressionVoter();
        webExpressionVoter.setExpressionHandler(new OAuth2WebSecurityExpressionHandler() {
            @Override
            protected SecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication, FilterInvocation fi) {
                WebSecurityExpressionRoot root = (WebSecurityExpressionRoot) super.createSecurityExpressionRoot(authentication, fi);
//                root.setDefaultRolePrefix(""); //remove the prefix ROLE_
                return root;
            }
        });
        return webExpressionVoter;
    }

    @Bean
    public AccessDecisionVoter authenticatedVoter() {
        AuthenticatedVoter authenticatedVoter = new AuthenticatedVoter();
        return authenticatedVoter;
    }

    @Bean
    public AccessDecisionVoter clientScopeVoter() {
        ClientScopeVoter clientScopeVoter = new ClientScopeVoter();
        return clientScopeVoter;
    }

    @Bean
    public AccessDecisionManager accessDecisionManager() {
        List<AccessDecisionVoter<?>> decisionVoters = new ArrayList<>();

        decisionVoters.add(roleVoter());
        decisionVoters.add(webExpressionVoter());
        decisionVoters.add(authenticatedVoter());
        decisionVoters.add(clientScopeVoter());

        return new AffirmativeBased(decisionVoters);
    }
}