package com.ws.sa.configs.docker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

/**
 * @author Maxim Seredkin
 */
@Configuration
@Profile("docker")
public class OAuth2Config {

    @Configuration
    @Profile("docker")
    @EnableResourceServer
    protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

        @Autowired
        private AccessDecisionManager accessDecisionManager;

        @Bean
        public TokenStore tokenStore() {
            return new InMemoryTokenStore();
        }

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            resources.tokenStore(tokenStore());
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/**")
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/resources/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/account").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/users/password/reset").permitAll()
                .antMatchers(HttpMethod.GET, "/api/settings/license").permitAll()
                .antMatchers(HttpMethod.GET, "/api/transitions/**").permitAll()

                .antMatchers(HttpMethod.GET, "/").permitAll()
                .antMatchers(HttpMethod.GET, "/index.html").permitAll()
                .antMatchers(HttpMethod.GET, "/login").permitAll()
                .antMatchers(HttpMethod.GET, "/account").permitAll()
                .antMatchers(HttpMethod.GET, "/main").permitAll()
                .antMatchers(HttpMethod.GET, "/forgot-password").permitAll()
                .antMatchers(HttpMethod.GET, "/users/**").permitAll()
                .antMatchers(HttpMethod.GET, "/tasks/**").permitAll()
                .antMatchers(HttpMethod.GET, "/employees/**").permitAll()
                .antMatchers(HttpMethod.GET, "/employee-tags/**").permitAll()
                .antMatchers(HttpMethod.GET, "/units/**").permitAll()
                .antMatchers(HttpMethod.GET, "/reports/**").permitAll()
                .antMatchers(HttpMethod.GET, "/settings/**").permitAll()
                .antMatchers(HttpMethod.GET, "/license/**").permitAll()
                .antMatchers(HttpMethod.GET, "/info/**").permitAll()
                .antMatchers(HttpMethod.GET, "/documents").permitAll()
                .antMatchers(HttpMethod.GET, "/mail-templates/**").permitAll()
                .anyRequest().fullyAuthenticated()
                .accessDecisionManager(accessDecisionManager)
                .and().anonymous()
                .and().csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .and().httpBasic().disable();
        }
    }

    @Configuration
    @Profile("docker")
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Autowired
        @Qualifier("authenticationManagerBean")
        private AuthenticationManager authenticationManager;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @Autowired
        private TokenStore tokenStore;

        @Autowired
        private UserDetailsService userDetailsService;

        @Override
        public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
            security.tokenKeyAccess("permitAll()")
                    .checkTokenAccess("isAuthenticated()");
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.inMemory()
                   .withClient("RiskGuardapp")
                   .authorities("ROLE_WEB_CLIENT")
                   .secret("my-secret-token-to-change-in-production")
                   .authorizedGrantTypes("authorization_code", "refresh_token", "password", "client_credentials", "implicit")
                   .scopes("read", "write");
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.tokenStore(tokenStore)
                     .authenticationManager(authenticationManager)
                     .userDetailsService(userDetailsService);
        }

    }
}