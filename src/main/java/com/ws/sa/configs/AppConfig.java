package com.ws.sa.configs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Vladislav Shelengovskiy on 02.04.2017.
 */
@Component
public class AppConfig {
    @Getter
    @Value("${server.properties.name}")
    private String serverProperties;

    @Setter
    @Getter
    @Value("${server.transition.address}")
    private String transitionPath;

    @Getter
    @Setter
    @Value("${server.external.address}")
    private String externalAddress;

    @Setter
    @Getter
    @Value("${app.setting.mail.anonymous}")
    private Boolean anonymous;

    @Getter
    @Value("${app.setting.mail.username}")
    private String defaultMailUsername;

    @Setter
    @Getter
    @Value("${spring.mail.username}")
    private String mailUsername;

    @Getter
    @Value("${app.setting.mail.password}")
    private String defaultMailPassword;

    //region LDAP
    @Getter
    @Setter
    @Value("${app.setting.ldap.url}")
    private String ldapUrl;

    @Getter
    @Setter
    @Value("${app.setting.ldap.port}")
    private Integer ldapPort;

    @Getter
    @Setter
    @Value("${app.setting.ldap.dn}")
    private String ldapDn;

    @Getter
    @Setter
    @Value("${app.setting.ldap.useSSL}")
    private Boolean ldapUseSSL;

    @Getter
    @Setter
    @Value("${app.setting.ldap.username}")
    private String ldapUsername;

    @Getter
    @Setter
    @Value("${app.setting.ldap.password}")
    private String ldapPassword;

    //endregion

    public String getTransitionAddress() {
        return externalAddress.concat(transitionPath);
    }
}
