package com.ws.sa.exceptions;

import com.ws.sa.errors.ApiStatusCode;

/**
 * @author Evgeny Albintes
 */
public class MediaTypeNotSupportedException extends ApiException {
    public MediaTypeNotSupportedException(ApiStatusCode errorCode) {
        super(errorCode);
    }
}
