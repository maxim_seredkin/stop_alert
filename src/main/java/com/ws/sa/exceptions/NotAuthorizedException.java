package com.ws.sa.exceptions;

import com.ws.sa.errors.ApiStatusCode;

/**
 *
 *
 * @author Evgeny Albinets
 */
public class NotAuthorizedException extends ApiException {
    public NotAuthorizedException() {
        super(ApiStatusCode.NOT_AUTHORIZED);
    }
}
