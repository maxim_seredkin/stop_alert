package com.ws.sa.exceptions;

import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.security.base.SecurityHelper;

import java.util.Objects;

/**
 * Утилитный класс для проверки сущностей
 *
 * @author Denis Pakhomov
 */
public class Guard {

    /**
     * Проверка сущестования сущности
     *
     * @param entity проверяемая сущность
     * @param code   код ошибки
     * @throws EntityNotFoundException если сущость равна null
     */
    public static void checkEntityExists(Object entity, ApiStatusCode code) {
        if (Objects.isNull(entity)) throw new EntityNotFoundException(code);
    }

    /**
     * Проверяем свойство сущности
     *
     * @param check проверяемое условие для продолжения
     * @param code  код ошибки
     * @throws EntityArgumentException если проверяемое условие не выполняется
     */
    public static void checkEntityArgument(boolean check, ApiStatusCode code) {
        if (!check) throw new EntityArgumentException(code);
    }

    /**
     * Проверяем наличие свойства сущности
     *
     * @param argument свойство сущности
     * @param code     код ошибки
     * @throws EntityArgumentException если сущость равна null
     */
    public static void checkEntityArgumentExists(Object argument, ApiStatusCode code) {
        if (Objects.isNull(argument)) throw new EntityArgumentException(code);
    }

    /**
     * Проверяем статус сущности
     *
     * @param stateCheck условие для продолжения
     * @param code       код ошибки
     * @throws EntityStateException если проверяемое условие не выполняется
     */
    public static void checkEntityState(boolean stateCheck, ApiStatusCode code) {
        if (!stateCheck) throw new EntityStateException(code);
    }

    /**
     * Проверяем требования безопаности для сущности
     *
     * @param securityCheck условие для продолжения
     * @param code          код ошибки
     * @throws EntitySecurityException если проверяемое условие не выполняется
     */
    public static void checkEntitySecurity(boolean securityCheck, ApiStatusCode code) {
        if (!securityCheck) throw new EntitySecurityException(code);
    }

    public static void checkAuthorization() {
        if (!SecurityHelper.isAuthenticated()) throw new NotAuthorizedException();
    }

    public static void checkDocumentMediaType(boolean isMediaTypeSupported, ApiStatusCode code) {
        if (!isMediaTypeSupported) throw new MediaTypeNotSupportedException(code);
    }
}
