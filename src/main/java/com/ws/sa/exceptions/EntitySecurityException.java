package com.ws.sa.exceptions;

import com.ws.sa.errors.ApiStatusCode;

/**
 * Исключение при нарушении безопасности работы с доменным объектом
 *
 * @author Denis Pakhomov
 */
public class EntitySecurityException extends ApiException {

    public EntitySecurityException(ApiStatusCode errorCode) {
        super(errorCode);
    }
}
