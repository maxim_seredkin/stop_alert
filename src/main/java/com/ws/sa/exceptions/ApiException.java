package com.ws.sa.exceptions;


import com.ws.sa.errors.ApiStatusCode;

import static com.ws.sa.errors.ApiStatusCode.NOT_FOUND;

/**
 * Базовый класс исключений API
 *
 * @author Evgeny Albinets
 */
public class ApiException extends RuntimeException {

    /**
     * Код ошибки
     */
    private final ApiStatusCode errorCode;

    public ApiException(ApiStatusCode errorCode) {
        super(errorCode.getDescription());
        this.errorCode = errorCode;
    }

    public ApiStatusCode getErrorCode() {
        return errorCode;
    }

    public static ApiException notFound() {
        return new ApiException(NOT_FOUND);
    }
}
