package com.ws.sa.exceptions;

public class InitializationException extends Exception {
    public InitializationException(Throwable cause) {
        super(cause);
    }

    public InitializationException(String message, Throwable cause) {
        super(message, cause);
    }
}
