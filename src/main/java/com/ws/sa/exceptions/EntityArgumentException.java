package com.ws.sa.exceptions;

import com.ws.sa.errors.ApiStatusCode;

/**
 * Ошибка инициализации поля сущности
 *
 * @author Denis Pakhomov
 */
public class EntityArgumentException extends ApiException {

    public EntityArgumentException(ApiStatusCode errorCode) {
        super(errorCode);
    }
}
