package com.ws.sa.exceptions;

import com.ws.sa.errors.ApiStatusCode;

/**
 * Исключение при отсутствии доменного объекта
 *
 * @author Denis Pakhomov
 */
public class EntityNotFoundException extends ApiException {

    public EntityNotFoundException(ApiStatusCode errorCode) {
        super(errorCode);
    }
}
