package com.ws.sa.exceptions;

import com.ws.sa.errors.ApiStatusCode;

/**
 * Ошибка состояния исключения
 *
 * @author Denis Pakhomov
 */
public class EntityStateException extends ApiException {

    public EntityStateException(ApiStatusCode errorCode) {
        super(errorCode);
    }
}
