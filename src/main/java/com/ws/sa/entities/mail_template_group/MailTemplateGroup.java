package com.ws.sa.entities.mail_template_group;

import com.ws.sa.entities.base.StatefulEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * @author Vladislav Shelengovskiy
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "MAIL_TEMPLATE_GROUPS")
public class MailTemplateGroup extends StatefulEntity {

    @Column(name = "NAME")
    private String name;

}
