package com.ws.sa.entities.user;

import com.google.common.collect.Lists;
import com.ws.sa.entities.authority.Authority;
import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.enums.Role;
import com.ws.sa.enums.UserStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static org.hibernate.annotations.LazyCollectionOption.FALSE;

/**
 * @author Maxim Seredkin
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "USERS")
public class User extends BaseEntity {

    @Column(name = "USERNAME", nullable = false)
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "REGISTER_DATE")
    private Date registerDate;

    @Column(name = "SECOND_NAME")
    private String secondName;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Email
    @Column(name = "EMAIL", nullable = false)
    private String email;

    @ManyToOne(optional = false)
    @JoinColumn(name = "AUTHORITY_ID", nullable = false)
    private Authority authority;

    @Column(name = "ACCOUNT_NON_LOCKED")
    private boolean accountNonLocked = true;

    @Column(name = "ACCOUNT_NON_EXPIRED")
    private boolean accountNonExpired = true;

    @Column(name = "CREDENTIALS_NON_EXPIRED")
    private boolean credentialsNonExpired = true;

    @Column(name = "ENABLED")
    private boolean enabled = true;

    @LazyCollection(FALSE)
    @ManyToMany(targetEntity = Unit.class, cascade = {PERSIST, MERGE})
    @JoinTable(name = "USER_UNITS",
               joinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "ID")},
               inverseJoinColumns = {@JoinColumn(name = "UNIT_ID", referencedColumnName = "ID")})
    private Set<Unit> units = new HashSet<>();

    /** фамилия имя отчество (полностью) - "SecondName FirstName MiddleName" */
    public String getFullName() {
        return Stream.of(secondName, firstName, middleName)
                     .filter(StringUtils::isNotBlank)
                     .map(String::trim)
                     .collect(Collectors.joining(" "));
    }

    /** фамилия и инициалы - "SecondName F. M." */
    public String getShortName() {
        return Stream.concat(Stream.of(secondName)
                                   .filter(StringUtils::isNotBlank)
                                   .map(String::trim),
                             Stream.of(firstName, middleName)
                                   .filter(StringUtils::isNotBlank)
                                   .map(String::trim)
                                   .map(str -> str.substring(0, 1))
                                   .map(String::toUpperCase)
                                   .map(str -> str.concat(".")))
                     .collect(Collectors.joining(" "));
    }

    public boolean isSecurityOfficer() {
        return isRole(Role.SECURITY_OFFICER);
    }

    public boolean isManager() {
        return isRole(Role.MANAGER);
    }

    public boolean isAdministrator() {
        return isRole(Role.ADMINISTRATOR);
    }

    private boolean isRole(Role role) {
        return authority.getAuthority().equals(role.getAuthority());
    }

    public boolean isDisabled() {
        return !enabled;
    }

    public boolean isAccountLocked() {
        return !accountNonLocked;
    }

    public UserStatus getStatus() {
        return UserStatus.getStatus(isDisabled(), isAccountLocked());
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Lists.newArrayList(authority);
    }

    public void block() {
        this.setAccountNonLocked(false);
    }

    public void setBlocked(boolean blocked) {
        this.setAccountNonLocked(!blocked);
    }

    @PrePersist
    protected void onCreate() {
        registerDate = new Date();
    }

}
