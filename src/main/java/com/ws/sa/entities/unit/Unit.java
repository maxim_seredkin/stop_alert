package com.ws.sa.entities.unit;

import com.ws.sa.entities.base.StatefulEntity;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.LAZY;
import static org.hibernate.annotations.FetchMode.SUBSELECT;

/**
 * @author Vladislav Shelengovskiy
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "UNITS")
public class Unit extends StatefulEntity {

    @Column(name = "NAME")
    private String name;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "PARENT_ID")
    private Unit parent;

    @Fetch(value = SUBSELECT)
    @ManyToMany(targetEntity = User.class,
                mappedBy = "units",
                fetch = LAZY,
                cascade = {PERSIST, MERGE})
    private Set<User> users = new HashSet<>();

    @Fetch(value = SUBSELECT)
    @OneToMany(mappedBy = "unit", fetch = LAZY)
    private Set<Employee> employees = new HashSet<>();

    @Fetch(value = SUBSELECT)
    @OneToMany(mappedBy = "parent", fetch = LAZY)
    private Set<Unit> childs = new HashSet<>();

    @Column(name = "PATH", length = 2048)
    private String path = "";

    public void addUser(User user) {
        user.getUnits().add(this);
    }

    private void removeUsers(User user) {
        user.getUnits().remove(this);
    }

    public void removeUsers() {
        users.forEach(this::removeUsers);
    }

    public Collection<Unit> getEnabledChilds() {
        return childs.stream().filter(StatefulEntity::isEnabled).collect(Collectors.toSet());
    }

    public Collection<User> getManagers() {
        return users.stream().filter(User::isManager).collect(Collectors.toSet());
    }

    public Collection<User> getOfficers() {
        return users.stream().filter(User::isSecurityOfficer).collect(Collectors.toSet());
    }
}
