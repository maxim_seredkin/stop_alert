package com.ws.sa.entities.base;

import com.ws.sa.enums.EntityStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

/**
 * Логичекие удаляемая сущность
 *
 * @author Maxim Seredkin
 */
@Getter
@Setter
@MappedSuperclass
public class StatefulEntity extends BaseEntity {

    @Column(name = "STATUS", columnDefinition = "VARCHAR(255) DEFAULT 'ENABLED'")
    @Enumerated(EnumType.STRING)
    private EntityStatus status = EntityStatus.ENABLED;

    /**
     * Восстановить
     */
    public void enable() {
        this.setStatus(EntityStatus.ENABLED);
    }

    /**
     * Удалить
     */
    public void delete() {
        this.setStatus(EntityStatus.DELETED);
    }

    /**
     * Активна?
     *
     * @return boolean
     */
    public boolean isEnabled() {
        return this.getStatus().equals(EntityStatus.ENABLED);
    }

    /**
     * Удалена?
     *
     * @return boolean
     */
    public boolean isDeleted() {
        return this.getStatus().equals(EntityStatus.DELETED);
    }
}
