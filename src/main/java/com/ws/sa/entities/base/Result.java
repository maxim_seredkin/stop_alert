package com.ws.sa.entities.base;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Результат
 *
 * @author Maxim Seredkin
 * @since 29.03.2017
 */
@Getter
@Setter
@Embeddable
@NoArgsConstructor
public class Result implements Serializable {

    public Result(long totalCount ,long badCount ) {
        this.totalCount = totalCount;
        this.badCount = badCount;
    }

    @Column(name = "RESULT_TOTAL_COUNT")
    private long totalCount = 0L;

    @Column(name = "RESULT_BAD_COUNT")
    private long badCount = 0L;

    @JsonProperty("countTrueEmployees")
    public long getGoodCount() {
        return totalCount - badCount;
    }

    @JsonProperty("badPercent")
    public double getBadPercent() {
        return totalCount != 0 ? new BigDecimal(badCount / (double) totalCount * 100.0).setScale(2, RoundingMode.UP).doubleValue() : 0.0;
    }

    @JsonProperty("goodPercent")
    public double getGoodPercent() {
        return totalCount != 0 ? new BigDecimal(getGoodCount() / (double) totalCount * 100.0).setScale(2, RoundingMode.UP).doubleValue() : 0.0;
    }
}
