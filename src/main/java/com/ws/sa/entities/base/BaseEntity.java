package com.ws.sa.entities.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

/**
 * Базовая сущность
 *
 * @author Maxim Seredkin
 */
@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(of = "id", doNotUseGetters = true, callSuper = false)
public class BaseEntity implements Entity, Serializable {

    @Id
    @GeneratedValue
    @Type(type = "uuid-char")
    @Column(name = "ID", nullable = false)
    private UUID id;

}
