package com.ws.sa.entities.base;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.base.DTO;

import java.util.UUID;

import static java.util.Objects.isNull;

/**
 * Иерархичная сущность
 *
 * @author Maxim Seredkin
 */
public interface HierarchyDTO<ParentT extends BaseDTO> extends DTO {

    /**
     * Получить родителя
     *
     * @return ParentT
     */
    @JsonIgnore
    ParentT getParent();

    /**
     * Задать родителя
     *
     * @param parent родитель
     */
    void setParent(ParentT parent);

    /**
     * Получить идентификатор родителя
     *
     * @return UUID
     */
    default UUID getParentId() {
        ParentT parentT = getParent();

        if (isNull(parentT))
            return null;

        return parentT.getId();
    }

}
