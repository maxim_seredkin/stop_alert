package com.ws.sa.entities.redirect;

import com.ws.sa.enums.RedirectType;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.entities.mock_page.MockPage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Getter
@Setter
@Embeddable
@NoArgsConstructor
public class Redirect implements Serializable {

    @Column(name = "REDIRECT_TYPE")
    private RedirectType redirectType;

    @ManyToOne
    @JoinColumn(name = "REDIRECT_TEMPLATE_ID")
    private MailTemplate mailTemplate;

    @ManyToOne
    @JoinColumn(name = "REDIRECT_MOCK_PAGE_ID")
    private MockPage mockPage;

    @Column(name = "REDIRECT_URI")
    private String redirectUri;
}
