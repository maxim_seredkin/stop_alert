package com.ws.sa.entities.mail;

import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.task.Task;
import com.ws.sa.entities.unit.Unit;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Vladislav Shelengovskiy
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "MAILS",
       indexes = {@Index(columnList = "employee_id", name = "employee_id_b_tree_idx"),
                  @Index(columnList = "task_id, unit_id", name = "task_id_unit_id_b_tree_idx"),
                  @Index(columnList = "task_id, employee_id", name = "task_id_employee_id_b_tree_idx")})

public class Mail extends BaseEntity {

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "TASK_ID")
    private Task task;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "EMPLOYEE_ID")
    private Employee employee;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "UNIT_ID")
    private Unit unit;

    @Column(name = "UNIT_PATH")
    private String unitPath;

    @Column(name = "SEND_DATE")
    private Date sendDate = new Date();

    @Column(name = "IS_SEND")
    private boolean send;

    @Column(name = "COUNT_TRANSITIONS", columnDefinition = "BIGINT default 0")
    private Long countTransitions = 0L;

    public synchronized void addCount() {
        ++countTransitions;
    }

    @Builder
    public Mail(Task task, Employee employee) {
        this.task = task;
        this.employee = employee;
        this.unit = employee.getUnit();
        this.unitPath = employee.getUnit().getPath();
    }

    public boolean isNotSend() {
        return !this.send;
    }
}
