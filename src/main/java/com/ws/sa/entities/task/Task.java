package com.ws.sa.entities.task;

import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.entities.redirect.Redirect;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import com.ws.sa.enums.TaskStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Vladislav Shelengovskiy
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "TASKS")
public class Task extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User author;

    @Column(name = "TASK_NAME")
    private String name;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE", nullable = false)
    private Date createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEMPLATE_ID", nullable = false)
    private MailTemplate template;

    @Embedded
    private Redirect redirect;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "TASK_UNITS",
            joinColumns = {@JoinColumn(name = "TASK_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "UNIT_ID", referencedColumnName = "ID")}
    )
    private List<Unit> units = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "TASK_EMPLOYEE_TAGS",
            joinColumns = {@JoinColumn(name = "TASK_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "TAG_ID", referencedColumnName = "ID")}
    )
    private List<EmployeeTag> employeeTags = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "TASK_EMPLOYEES_BY_HAND",
            joinColumns = {@JoinColumn(name = "TASK_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID")}
    )
    private List<Employee> employeesByHand = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "TASK_EMPLOYEES_BY_FILTER",
            joinColumns = {@JoinColumn(name = "TASK_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID")}
    )
    private List<Employee> employeesByFilter = new ArrayList<>();

    @OneToMany(mappedBy = "task", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Mail> mails = new ArrayList<>();

    @Column(name = "STATUS", columnDefinition = "INT DEFAULT 0")
    @Enumerated(EnumType.ORDINAL)
    private TaskStatus status = TaskStatus.WAITING;

    @PrePersist
    protected void onCreate() {
        createdDate = new Date();
    }

    public boolean isWaiting() {
        return status.equals(TaskStatus.WAITING);
    }

    public boolean isSuspended() {
        return status.equals(TaskStatus.SUSPENDED);
    }

    public boolean isCompleted() {
        return status.equals(TaskStatus.COMPLETED);
    }

    public boolean isExecution() {
        return status.equals(TaskStatus.EXECUTION);
    }

    public boolean isExpired() {
        return status.equals(TaskStatus.EXPIRED);
    }
}