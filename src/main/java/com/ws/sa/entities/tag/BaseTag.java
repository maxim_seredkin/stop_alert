package com.ws.sa.entities.tag;

import com.ws.sa.entities.base.StatefulEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * @author Vladislav Shelengovskiy
 */
@Getter
@Setter
@MappedSuperclass
public class BaseTag extends StatefulEntity {

    @Column(name = "NAME", nullable = false)
    private String name;
}
