package com.ws.sa.entities.authority;

import com.ws.sa.entities.base.BaseEntity;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Maxim Seredkin
 */
@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "AUTHORITIES")
public class Authority extends BaseEntity implements GrantedAuthority {

    @Column(name = "AUTHORITY")
    private String authority;

    @Column(name = "DISPLAY_NAME")
    private String displayName;

    @Column(name = "DESCRIPTION")
    private String description;

}
