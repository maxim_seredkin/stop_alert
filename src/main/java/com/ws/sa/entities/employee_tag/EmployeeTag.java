package com.ws.sa.entities.employee_tag;

import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.tag.BaseTag;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.*;

/**
 * @author Vladislav Shelengovskiy
 */

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "EMPLOYEE_TAGS")
public class EmployeeTag extends BaseTag {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID")
    private EmployeeTag parent;

    @Column(name = "PATH", nullable = false, length = 2048)
    private String path = "";

    @Column(name = "IS_USED")
    private boolean used = true;

    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    private Set<EmployeeTag> nodes = new HashSet<>();

    @ManyToMany(mappedBy = "employeeTags", fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Employee> employees = new ArrayList<>();
}
