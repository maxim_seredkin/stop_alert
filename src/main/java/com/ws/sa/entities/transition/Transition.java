package com.ws.sa.entities.transition;

import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.entities.mail.Mail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.REFRESH;

/**
 * @author Vladislav Shelengovskiy
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TRANSITIONS",
       indexes = {@Index(columnList = "mail_id", name = "mail_id_b_tree_idx")})
public class Transition extends BaseEntity {

    @ManyToOne(cascade = {MERGE, REFRESH})
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "MAIL_ID")
    private Mail mail;

    @Column(name = "TRANSITION_DATE")
    private Date transitionDate;

    @Column(name = "IP")
    private String ip;

    @Column(name = "BROWSER")
    private String browser;

    @Column(name = "OS")
    private String os;

    @Column(name = "ADDITIONAL_INFO")
    private String additionalInfo;
}
