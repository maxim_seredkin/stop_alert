package com.ws.sa.entities.mail_template;

import com.ws.sa.entities.base.StatefulEntity;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


/**
 * @author Vladislav Shelengovskiy
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "MAIL_TEMPLATES")
public class MailTemplate extends StatefulEntity {

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    @Column(name = "PATH")
    private String path;

    @Column(name = "FROM_PERSONAL", nullable = false)
    private String fromPersonal;

    @Column(name = "FROM_ADDRESS", nullable = false)
    private String fromAddress;

    @Column(name = "REDIRECT_URI", nullable = false)
    private String redirectUri;

    @ManyToOne(targetEntity = MailTemplateGroup.class)
    @JoinColumn(name = "GROUP_ID")
    private MailTemplateGroup group;

}
