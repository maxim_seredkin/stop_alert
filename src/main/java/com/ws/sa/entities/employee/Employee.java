package com.ws.sa.entities.employee;

import com.ws.sa.entities.base.StatefulEntity;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.entities.unit.Unit;
import lombok.*;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Vladislav Shelengovskiy
 */
@Setter
@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EMPLOYEES")
public class Employee extends StatefulEntity {

    @Column(name = "SECOND_NAME")
    private String secondName;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Email
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "POSITION")
    private String position;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UNIT_ID", nullable = false)
    private Unit unit;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "EMPLOYEE_EMPLOYEE_TAGS",
            joinColumns = {@JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "TAG_ID", referencedColumnName = "ID")}
    )
    private List<EmployeeTag> employeeTags = new ArrayList<>();

    @Builder
    public Employee(String secondName, String firstName, String middleName, String email, String position, Unit unit) {
        this.secondName = secondName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.email = email;
        this.position = position;
        this.unit = unit;
    }

    public String getFullName() {
        return ((secondName != null ? secondName : "") + (firstName != null ? " " + firstName : "") + (middleName != null ? " " + middleName : "")).trim();
    }

    public String getShortName() {
        return ((secondName != null ? secondName : "")
                + ((firstName != null && !firstName.isEmpty()) ? " " + firstName.substring(0, 1).toUpperCase() + "." : "")
                + ((middleName != null && !middleName.isEmpty()) ? " " + middleName.substring(0, 1).toUpperCase() + "." : "")).trim();
    }

    public String getUnitName() {
        return unit.getName();
    }
}
