package com.ws.sa.entities.mock_page;

import com.ws.sa.entities.base.StatefulEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "MOCK_PAGES")
public class MockPage extends StatefulEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "PATH")
    private String path;

    @Column(name = "REDIRECT_URI")
    private String redirectUri;

}
