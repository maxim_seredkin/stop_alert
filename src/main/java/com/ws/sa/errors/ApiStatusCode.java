package com.ws.sa.errors;

/**
 * Коды ошибок REST API
 *
 * @author Evgeny Albinets
 * @version 1.0
 */
public enum ApiStatusCode {

    // region Common
    SUCCESS(200, "Операция выполнена успешна"),

    NOT_AUTHORIZED(403, "Пользователь не авторизован"),

    NOT_FOUND(404, "Объект не найден"),

    INVALID_PARAM(412, "Неверное значение параметра"),

    EMPTY_PARAM(406, "Пустое значение параметра"),

    NO_CONTENT(204, "Нет содержимого"),

    FORBIDDEN(403, "Доступ запрещен"),

    UNSUPPORTED_MEDIA_TYPE(415, "Неподдерживаемый тип данных"),

    INTERNAL_SERVER_ERROR(500, "Внутреняя ошибка сервера"),
    // endregion
    // region User
    INVALID_PASSWORD(1000, "Неправильный пароль"),
    // endregion
    // region Task
    EMPTY_EMPLOYEES(1010, "Сотрудники не выбраны"),
    INVALID_DATE(1011, "Дата запуска задачи меньше текущей даты"),
    INVALID_STATUS(1012, "Неверный статус задачи"),
    ENTITY_IS_USED(1013, "Объект используется в задаче ожидающей выполнения"),
    INVALID_EMAIL(1014, "Не задан электронный адрес"),
    USER_LOCKED(1015, "Пользователь заблокирован"),
    INVALID_AUTH(1016, "Неверное имя пользователя или пароль"),
    END_DATE_BEFORE_START_DATE(1017, "Дата окончания меньше даты начала"),
    ALREADY_EXISTS(1018, "Такой объект уже существует уже существует."),
    NOT_VELOCITY_TEMPLATE(1019, "Файл не является Velocity-шаблоном."),
    // endregion

    // region Resource
    RESOURCE_NOT_FOUND(1030, "Запрашиваемый ресурс не найден"),
    ILLEGAL_PROJECTION(1031, "Неподдерживаямая проекция.");
    // end region

    private final int value;

    private final String description;

    ApiStatusCode(int value, String description) {
        this.value = value;
        this.description = description;

    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
