package com.ws.sa.errors;

import lombok.Getter;

/**
 * @author Maxim Seredkin
 * @since 22.06.2017
 */
public enum EmployeeError {

    FIRST_NAME_NOT_SET("не задано имя"),
    SECOND_NAME_NOT_SET("не задана фамилия"),
    EMAIL_NOT_SET("не задана элеетронная почта"),
    UNIT_NOT_SET("не задана организация");

    @Getter
    private String description;

    EmployeeError(String description) {
        this.description = description;
    }

}
