package com.ws.sa.errors;

import lombok.Getter;

/**
 * Created by Vladislav Shelengovskiy on 01.06.2017.
 */
public enum UnitError {

    EMPTY_NAME("нет названия"),
    UNIT_EXISTS("организация уже существует"),
    PARENT_UNIT_NOT_EXISTS("родительская организация не существует"),
    UNIT_NOT_EXISTS("организация не существует");

    @Getter
    private String description;

    UnitError(String description) {
        this.description = description;
    }
}
