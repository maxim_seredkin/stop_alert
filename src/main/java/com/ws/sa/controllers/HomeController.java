package com.ws.sa.controllers;

import com.ws.sa.beans.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * @author Maxim Seredkin
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private TokenStore tokenStore;
    @Autowired
    @Qualifier("defaultAuthorizationServerTokenServices")
    private AuthorizationServerTokenServices authorizationServerTokenServices;
    @Autowired
    private ConsumerTokenServices consumerTokenServices;

    @RequestMapping({"/",
                     "login",
                     "account",
                     "main",
                     "forgot-password",
                     "users/**",
                     "tasks/**",
                     "employees/**",
                     "employee-tags/**",
                     "units/**",
                     "reports/**",
                     "settings/**",
                     "license/**",
                     "mail-templates/**",
                     "documents"})
    public String redirect(HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        return "forward:/index.html";
    }

    @RequestMapping("oauth/logout")
    public @ResponseBody
    ApiResponse logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        if (Objects.isNull(authentication))
            return ApiResponse.ok();

        if (authentication instanceof OAuth2Authentication) {

            Object principal = authentication.getPrincipal();

            if (Objects.isNull(principal))
                return ApiResponse.ok();

            OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
            OAuth2AccessToken accessToken = authorizationServerTokenServices.getAccessToken(oAuth2Authentication);
            consumerTokenServices.revokeToken(accessToken.getValue());

            new SecurityContextLogoutHandler().logout(request, response, authentication);
            SecurityContextHolder.getContext().setAuthentication(null);
        }

        return ApiResponse.ok();
    }

}
