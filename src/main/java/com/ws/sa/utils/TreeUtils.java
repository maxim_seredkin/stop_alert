package com.ws.sa.utils;

import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.base.TreeNode;
import com.ws.sa.entities.base.HierarchyDTO;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Maxim Seredkin
 */
public class TreeUtils {

    public static <HierarchyT extends HierarchyDTO<ParentT>, ParentT extends BaseDTO> Collection<TreeNode<HierarchyT, ParentT>> buildTree(Collection<HierarchyT> items) {
        return items.stream()
                    .filter(root -> isRoot(root, items))
                    .map(item -> {
                        item.setParent(null);
                        return createTreeNode(items, item);
                    }).collect(Collectors.toList());
    }

    private static <HierarchyT extends HierarchyDTO> boolean isRoot(HierarchyDTO root, Collection<HierarchyT> items) {
        return Objects.isNull(root.getParent()) || items.stream().noneMatch(item -> Objects.equals(root.getParent(), item));
    }

    private static <HierarchyT extends HierarchyDTO<ParentT>, ParentT extends BaseDTO> TreeNode<HierarchyT, ParentT> createTreeNode(Collection<HierarchyT> items, HierarchyT item) {
        TreeNode<HierarchyT, ParentT> node = new TreeNode<>(item);
        node.addNodes(buildNodes(node, items));
        return node;
    }

    private static <HierarchyT extends HierarchyDTO<ParentT>, ParentT extends BaseDTO> Collection<TreeNode<HierarchyT, ParentT>> buildNodes(TreeNode<HierarchyT, ParentT> parent, Collection<HierarchyT> items) {
        return items.stream()
                    .filter(item -> Objects.equals(parent.getItem(), item.getParent()))
                    .map(item -> createTreeNode(items, item)).collect(Collectors.toList());
    }

}
