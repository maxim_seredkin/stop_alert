package com.ws.sa.utils;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Maxim Seredkin
 * @since 02.04.2017
 */
public class MapUtils {

    public static Collection<String> UUIDtoString(Collection<UUID> list) {
        return list.stream().map(UUID::toString).collect(Collectors.toList());
    }
}
