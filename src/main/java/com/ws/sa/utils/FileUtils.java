package com.ws.sa.utils;

import com.google.common.io.Files;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import org.h2.util.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Set;

import static com.google.common.collect.ImmutableSet.of;

/**
 * Created by Vladislav Shelengovskiy on 01.06.2017.
 */
public class FileUtils {

    public static void fillResponse(File file, String contentType, HttpServletResponse servletResponse) throws IOException {
        Guard.checkEntityExists(servletResponse, ApiStatusCode.NOT_FOUND);
        Guard.checkEntityExists(file, ApiStatusCode.NOT_FOUND);
        servletResponse.setContentType(contentType);
        servletResponse.setContentLength((int) file.length());
        servletResponse.setCharacterEncoding("UTF-8");
        servletResponse.setHeader("Content-Disposition", "attachment; filename=".concat(file.getName()));
        servletResponse.getOutputStream().flush();

        try (InputStream is = new FileInputStream(file);
             OutputStream os = servletResponse.getOutputStream()) {
            IOUtils.copy(is, os);
        }
    }

    public static boolean validCSVFormat(MultipartFile file) {
        if (Files.getFileExtension(file.getName()).equals(".csv")) return true;
        String type = file.getContentType();
        return type != null && contentTypes.stream().anyMatch(type::contains);
    }

    private static Set<String> contentTypes = of("text/comma-separated-values",
                                                 "text/csv",
                                                 "application/csv",
                                                 "application/excel",
                                                 "application/vnd.ms-excel",
                                                 "application/vnd.msexcel",
                                                 "application/octet-stream");

}
