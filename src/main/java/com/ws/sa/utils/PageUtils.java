package com.ws.sa.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * Created by Seredkin M. on 18.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public class PageUtils {

    public static PageRequest createPageRequest(Integer page, Integer size, Sort sort) {
        if (sort == null) {
            return createPageRequest(page, size);
        }

        return new PageRequest(page < 1 ? 0 : page - 1, size, sort);
    }

    public static PageRequest createPageRequest(Integer page, Integer size) {
        return new PageRequest(page < 1 ? 0 : page - 1, size);
    }

}
