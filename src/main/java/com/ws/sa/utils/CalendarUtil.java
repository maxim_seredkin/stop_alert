package com.ws.sa.utils;

import com.ws.sa.beans.DatePeriod;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;
import java.util.Calendar;
import java.util.Date;

/**
 * Утилита для работы с календарем
 *
 * @author Evgeny Albinets
 */
public class CalendarUtil {


    private final static int QUARTER_MONTH[] = new int[]{1, 4, 7, 10};

    public static Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }

    public static Date getNextDate(Date date) {
        Calendar calendar = getCalendar(date);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    public static boolean isLastDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        return date.equals(cal.getTime());
    }

    public static boolean isFirstDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return date.equals(cal.getTime());
    }

    public static boolean isWeekend(Date date) {
        int day = getDayOfWeek(date);
        return (day == Calendar.SUNDAY || day == Calendar.SATURDAY);
    }

    public static long getDateDiffInDays(Date date) {
        long diff = getCurrentDate().getTime() - date.getTime();
        return diff / (1000 * 60 * 60 * 24);
    }

    public static long getDateDiffInDays(Date date1, Date date2) {
        long diff = date1.getTime() - date2.getTime();
        return diff / (1000 * 60 * 60 * 24);
    }

    public static int getDayOfWeek(Date date) {
        return getCalendar(date).get(Calendar.DAY_OF_WEEK);
    }

    public static Calendar getCalendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static Date addDays(Date startDate, int days) {

        Calendar calendar = getCalendar(startDate);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    public static Date addDays(int days) {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    public static Date getDateFromMilliseconds(Long milliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        return calendar.getTime();
    }

    public static Long getDateInMilliseconds(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTimeInMillis();
    }

    public static Date dateFromLong(Long longDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(longDate);
        return calendar.getTime();
    }

    public static Integer getCurrentQuarter() {
        return LocalDate.now().query(new Quarter());
    }

    public static DatePeriod getCurrentQuarterPeriod() {

        Integer month = QUARTER_MONTH[getCurrentQuarter() - 1];
        LocalDate now = LocalDate.now();
        //  Дата начала кваратала
        LocalDate quarterStart = LocalDate.of(now.getYear(), month, 1);
        return new DatePeriod(quarterStart, quarterStart.plusMonths(3).minusDays(1));

    }

    public static Integer getNextQuarter() {
        Integer quarter = getCurrentQuarter();
        return quarter == 4 ? 1 : quarter + 1; // quarter % 4 + 1;
    }

    public static int getQuarterFromMonth(int month) {
        if (month <= 3) {
            return new Integer(1);
        } else if (month <= 6) {
            return new Integer(2);
        } else if (month <= 9) {
            return new Integer(3);
        } else {
            return new Integer(4);
        }
    }

    public static DatePeriod getQuarterPeriod(int quarter) {
        return getQuarterPeriod(quarter, LocalDate.now().getYear());
    }

    public static DatePeriod getQuarterPeriod(int quarter, int year) {
        Integer month = QUARTER_MONTH[quarter - 1];
        //  Дата начала кваратала
        LocalDate quarterStart = LocalDate.of(year, month, 1);
        return new DatePeriod(quarterStart, quarterStart.plusMonths(3).minusDays(1));
    }

    public static DatePeriod getCurrentYearPeriod() {
        LocalDate now = LocalDate.now();
        return new DatePeriod(LocalDate.of(now.getYear(), 1, 1), LocalDate.of(now.getYear(), 12, 31));
    }

    public static DatePeriod getYearPeriod(Integer year) {
        //
        if (year == null) {
            LocalDate now = LocalDate.now();
            return new DatePeriod(LocalDate.of(now.getYear(), 1, 1), LocalDate.of(now.getYear(), 12, 31));
        }
        return new DatePeriod(LocalDate.of(year, 1, 1), LocalDate.of(year, 12, 31));
    }

    public static Date toDate(LocalDate date) {
        return Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static boolean isNoneDateExpired(Date date) {
        return date.after(new Date());
    }

    private static class Quarter implements TemporalQuery<Integer> {
        @Override
        public Integer queryFrom(TemporalAccessor date) {
            int month = date.get(ChronoField.MONTH_OF_YEAR);
            return getQuarterFromMonth(month);
        }
    }

}
