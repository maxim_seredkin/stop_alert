package com.ws.sa.beans;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.dtos.base.DTO;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

/**
 * @author Maxim Seredkin
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse<T> {

    // код
    protected final int code;

    // данные
    protected final T _embedded;

    private final Page page;

    public ApiResponse(int code) {
        this.code = code;
        this._embedded = null;
        page = null;
    }

    public ApiResponse(int code, T embedded) {
        this.code = code;
        this._embedded = embedded;
        page = null;
    }

    @JsonCreator
    private ApiResponse(@JsonProperty("code") int code, @JsonProperty("embedded") T embedded, @JsonProperty("page") Page page) {
        this.code = code;
        this._embedded = embedded;
        this.page = page;
    }

    public static <EmbeddedT> ApiResponse<EmbeddedT> ok(EmbeddedT embedded) {
        return new ApiResponse<>(200, embedded);
    }

    public static <EmbeddedT> ApiResponse<EmbeddedT> ok(EmbeddedT embedded, Page page) {
        return new ApiResponse<>(200, embedded, page);
    }

    public static ApiResponse<Collection<? extends DTO>> ok(org.springframework.data.domain.Page<? extends DTO> page) {
        return new ApiResponse<>(200,
                                 page.getContent(),
                                 Page.builder()
                                     .size((long) page.getSize())
                                     .totalElements(page.getTotalElements())
                                     .totalPages(page.getTotalPages())
                                     .number(page.getNumber() == 0 ? 1 : page.getNumber() + 1)
                                     .build());
    }

    public static ApiResponse<Collection<? extends DTO>> ok(Collection<? extends DTO> items) {
        return new ApiResponse<>(200,
                                 items,
                                 new Page((long) items.size(),
                                          (long) items.size(),
                                          1,
                                          1));
    }


    public static ApiResponse<Object> ok() {
        return new ApiResponse<>(200);
    }

    public static ApiResponse<Object> code(int code) {
        return new ApiResponse<>(code, null);
    }

    public static ApiResponse<Object> fail(ApiStatusCode code) {
        return new ApiResponse<>(code.getValue());
    }

    public static ApiResponse<Object> fail(ApiStatusCode code, Object embedded) {
        return new ApiResponse<>(code.getValue(), embedded);
    }

    public static ApiResponse<Object> fail() {
        return new ApiResponse<>(500);
    }

}
