package com.ws.sa.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Maxim Seredkin
 */
@Data
public class ApiRequest<FormT/* extends Form*/> {

    @NotNull
    @JsonProperty("form")
    FormT form;

}
