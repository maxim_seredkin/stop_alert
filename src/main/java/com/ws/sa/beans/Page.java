package com.ws.sa.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Maxim Seredkin
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class Page {

    private final Long size;
    private final Long totalElements;
    private final Integer totalPages;
    private final Integer number;

}
