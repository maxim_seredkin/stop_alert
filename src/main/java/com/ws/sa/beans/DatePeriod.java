package com.ws.sa.beans;

import com.ws.sa.utils.CalendarUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Период дат
 *
 * @author Evgeny Albinets
 */
@Getter
@Setter
@AllArgsConstructor
public class DatePeriod {

    private final LocalDate startDate;
    private final LocalDate endDate;

    public DatePeriod(Date startDate, Date endDate) {
        this.startDate = CalendarUtil.toLocalDate(startDate);
        this.endDate = CalendarUtil.toLocalDate(endDate);
    }

    public long getStartDateInMills() {
        return getLongValue(startDate);
    }

    public long getEndDateInMills() {
        return getLongValue(endDate);
    }

    private long getLongValue(LocalDate date) {
        return date.atStartOfDay(ZoneId.systemDefault()).toEpochSecond() * 1000;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DatePeriod)) return false;

        DatePeriod that = (DatePeriod) o;

        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = startDate != null ? startDate.hashCode() : 0;
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }

}
