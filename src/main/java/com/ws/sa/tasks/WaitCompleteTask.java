package com.ws.sa.tasks;

import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import lombok.AllArgsConstructor;

/**
 * @author Maxim Seredkin
 * @since 02.04.2017
 */
@AllArgsConstructor
public class WaitCompleteTask implements Runnable {

    private final Task task;
    private final TaskService taskService;

    @Override
    public void run() {
        taskService.complete(task.getId());
    }
}
