package com.ws.sa.tasks;

import com.ws.sa.entities.task.Task;
import com.ws.sa.executors.TaskExecutor;

/**
 * @author Maxim Seredkin
 * @since 28.02.2017
 */
public class WaitExecutionTask implements Runnable {

    private final Task task;
    private final TaskExecutor taskExecutor;

    public WaitExecutionTask(Task task, TaskExecutor taskExecutor) {
        this.task = task;
        this.taskExecutor = taskExecutor;
    }

    @Override
    public void run() {
        taskExecutor.run(task);
    }
}
