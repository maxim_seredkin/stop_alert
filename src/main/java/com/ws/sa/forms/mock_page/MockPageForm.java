package com.ws.sa.forms.mock_page;

import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class MockPageForm implements Form {

    private String name;

    @Override
    public boolean validate() {
        return isNotBlank(name);
    }

}
