package com.ws.sa.forms.mail_template_group;

import com.ws.sa.forms.base.Form;
import lombok.Data;
import lombok.NoArgsConstructor;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Data
@NoArgsConstructor
public class MailTemplateGroupForm implements Form {

    private String name;

    @Override
    public boolean validate() {
        return isNotBlank(name);
    }

}
