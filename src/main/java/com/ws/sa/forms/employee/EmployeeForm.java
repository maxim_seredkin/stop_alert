package com.ws.sa.forms.employee;

import com.ws.sa.forms.base.Form;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@NoArgsConstructor
@Data
public class EmployeeForm implements Form {

    private String secondName;
    private String firstName;
    private String middleName;
    private String email;
    private String position;
    private UUID unit;
    private List<UUID> employeeTags;

    @Override
    public boolean validate() {
        return isNotBlank(secondName)
               && isNotBlank(firstName)
               && isNotBlank(email)
               && nonNull(unit);
    }

}
