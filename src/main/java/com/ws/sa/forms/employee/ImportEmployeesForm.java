package com.ws.sa.forms.employee;

import com.ws.sa.forms.base.Form;
import com.ws.sa.forms.unit.UnitNodeForm;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collection;

import static java.util.Objects.nonNull;

/**
 * Created by Vladislav Shelengovskiy on 03.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
@NoArgsConstructor
public class ImportEmployeesForm implements Form {

    private Collection<EmployeeForm> employees;

    @Override
    public boolean validate() {
        return nonNull(employees)
               && employees.stream().allMatch(Form::validate);
    }
}
