package com.ws.sa.forms.authority;

import com.ws.sa.forms.base.Form;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class AuthorityForm implements Form {

    private UUID id;
    private String authority;
    private String displayName;
    private String description;

    @Override
    public boolean validate() {
        return nonNull(id)
               && isNotBlank(authority)
               && isNotBlank(displayName);
    }

}
