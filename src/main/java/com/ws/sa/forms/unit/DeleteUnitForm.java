package com.ws.sa.forms.unit;

import com.ws.sa.forms.base.Form;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Man on 14.03.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DeleteUnitForm implements Form {

    private UUID successor;

    @Override
    public boolean validate() {
        return true;
    }

}
