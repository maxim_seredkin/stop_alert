package com.ws.sa.forms.unit;

import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.UUID;

import static java.util.Objects.nonNull;

/**
 * Created by Vladislav Shelengovskiy on 30.06.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */

@Getter
@NoArgsConstructor
public class ImportUnitsForm implements Form {

    private UUID id;
    private Collection<UnitNodeForm> units;

    @Override
    public boolean validate() {
        return nonNull(units)
               && units.stream().allMatch(Form::validate);
    }
}
