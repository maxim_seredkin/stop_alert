package com.ws.sa.forms.unit;

import com.ws.sa.forms.base.Form;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Vladislav Shelengovksiy on 31.01.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UnitForm implements Form {

    private String name;

    private UUID parent;

    private Collection<UUID> users;

    @Override
    public boolean validate() {
        return isNotBlank(name);
    }
}