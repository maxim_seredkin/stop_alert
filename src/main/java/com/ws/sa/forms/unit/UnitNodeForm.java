package com.ws.sa.forms.unit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Created by Vladislav Shelengovskiy on 08.06.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitNodeForm extends UnitForm {

    private Collection<UnitNodeForm> nodes;

    @Override
    public boolean validate() {
        return super.validate()
               && (isEmpty(nodes) || nodes.stream().allMatch(UnitNodeForm::validate));
    }

}
