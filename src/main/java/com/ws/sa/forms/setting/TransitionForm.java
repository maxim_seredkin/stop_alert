package com.ws.sa.forms.setting;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 14.07.2017.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransitionForm {

    private String uri;
}
