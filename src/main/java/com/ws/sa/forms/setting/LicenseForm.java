package com.ws.sa.forms.setting;

import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * Created by Vladislav Shelengovskiy on 02.04.2017.
 */
@Getter
public class LicenseForm {

    @NotNull
    private String code;
}
