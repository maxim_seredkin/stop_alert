package com.ws.sa.forms.setting;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 30.10.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LdapForm {

    private String url;
    private Integer port;
    private String dn;
    private Boolean useSSL;
    private String username;
    private String password;
}
