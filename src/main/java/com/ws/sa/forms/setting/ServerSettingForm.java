package com.ws.sa.forms.setting;

import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * Created by Vladislav Shelengovskiy on 30.03.2017.
 */
@Getter
public class ServerSettingForm {

    @NotNull
    private Boolean anonymous;

    @NotNull
    private String host;

    @NotNull
    private Integer port;

    @NotNull
    private Boolean useSSL;

    private String username;

    private String password;
}
