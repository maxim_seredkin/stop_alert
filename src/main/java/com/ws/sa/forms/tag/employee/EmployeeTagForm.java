package com.ws.sa.forms.tag.employee;

import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeTagForm implements Form {

    private String name;
    private UUID parent;
    private Boolean used;

    @Override
    public boolean validate() {
        return isNotBlank(name)
               && nonNull(used);
    }

}
