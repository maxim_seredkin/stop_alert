package com.ws.sa.forms.base;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public interface Form {

    boolean validate();

}
