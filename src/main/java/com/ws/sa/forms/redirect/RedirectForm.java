package com.ws.sa.forms.redirect;

import com.ws.sa.enums.RedirectType;
import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.UUID;

import static com.ws.sa.enums.RedirectType.CUSTOM_LINK;
import static com.ws.sa.enums.RedirectType.MOCK_PAGE_LINK;
import static com.ws.sa.enums.RedirectType.TEMPLATE_LINK;
import static java.util.Objects.nonNull;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class RedirectForm implements Form {

    private RedirectType redirectType;

    private UUID mailTemplate;

    private UUID mockPage;

    private String customUri;

    @Override
    public boolean validate() {
        return (Objects.equals(redirectType, TEMPLATE_LINK) && nonNull(mailTemplate))
               || (Objects.equals(redirectType, MOCK_PAGE_LINK) && nonNull(mockPage))
               || (Objects.equals(redirectType, CUSTOM_LINK) && nonNull(customUri));
    }
}
