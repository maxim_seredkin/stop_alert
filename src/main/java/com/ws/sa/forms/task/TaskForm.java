package com.ws.sa.forms.task;

import com.ws.sa.forms.base.Form;
import com.ws.sa.forms.redirect.RedirectForm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class TaskForm implements Form {

    private String name;

    private List<UUID> units;

    private List<UUID> employeeTags;

    private List<UUID> employeesByFilter;

    private List<UUID> employeesByHand;

    private RedirectForm redirect;

    private UUID template;

    private Date startDate;

    private Date endDate;

    @Override
    public boolean validate() {
        return isNotBlank(name)
               && nonNull(redirect)
               && redirect.validate()
               && nonNull(template)
               && nonNull(startDate)
               && nonNull(endDate);
    }

}
