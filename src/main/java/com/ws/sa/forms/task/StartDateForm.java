package com.ws.sa.forms.task;

import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

import static java.util.Objects.nonNull;

/**
 * Created by Vladislav Shelengovskiy on 26.02.2017.
 */
@Getter
@NoArgsConstructor
public class StartDateForm implements Form {

    private Date startDate;

    @Override
    public boolean validate() {
        return nonNull(startDate);
    }

}
