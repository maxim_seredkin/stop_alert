package com.ws.sa.forms.mail_template;

import com.ws.sa.enums.EntityStatus;
import com.ws.sa.forms.base.Form;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Data
@NoArgsConstructor
public class MailTemplateForm implements Form {

    private String name;
    private String subject;
    private String fromPersonal;
    private String fromAddress;
    private String redirectUri;
    private UUID group;
    private String description;
    private EntityStatus status;

    @Override
    public boolean validate() {
        return isNotBlank(name)
               && isNotBlank(subject)
               && isNotBlank(fromPersonal)
               && isNotBlank(fromAddress)
               && isNotBlank(redirectUri);
    }

}
