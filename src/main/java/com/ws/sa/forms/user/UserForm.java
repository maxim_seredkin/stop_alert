package com.ws.sa.forms.user;

import com.ws.sa.forms.base.Form;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserForm implements Form {

    private String username;
    private String secondName;
    private String firstName;
    private String middleName;
    private List<UUID> units;
    private String email;
    private UUID authority;
    private String password;

    @Override
    public boolean validate() {
        return isNotBlank(username)
               && isNotBlank(email)
               && nonNull(authority);
    }

}
