package com.ws.sa.forms.user;

import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by vlad_shelengovskiy on 19.12.2016.
 */
@Getter
@Setter
@NoArgsConstructor
public class UpdateAccountForm implements Form {

//    private String username;
    private String secondName;
    private String firstName;
    private String middleName;
//    private List<UUID> units;
    private String email;
    private UUID authority;
    private PasswordForm password;

    @Override
    public boolean validate() {
        return //isNotBlank(username)
               /*&&*/ isNotBlank(email)
               && nonNull(authority)
               && (isNull(password) || password.validate());
    }

}
