package com.ws.sa.forms.user;

import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

import static com.ws.sa.constants.Constants.CHECK_PASS_REQUIRED_PATTERN;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Created by vlad_shelengovskiy on 19.12.2016.
 */
@Getter
@Setter
@NoArgsConstructor
public class PasswordForm implements Form {

    private String old;
    private String renewed;

    @Override
    public boolean validate() {
        return isNotBlank(old)
               && isNotBlank(renewed)
               && renewed.matches(CHECK_PASS_REQUIRED_PATTERN)
               && !Objects.equals(old, renewed);
    }

}
