package com.ws.sa.forms.user;

import com.ws.sa.forms.base.Form;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Created by vlad_shelengovskiy on 19.12.2016.
 */
@Getter
@Setter
@NoArgsConstructor
public class ResetPasswordForm implements Form {

    private String username;

    @Override
    public boolean validate() {
        return isNotBlank(username);
    }

}
