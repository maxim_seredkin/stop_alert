package com.ws.sa.queries.filter;

import com.ws.sa.enums.LogicOperator;

import java.util.Collection;

/**
 * Фильтр
 *
 * @author Evgeny Albinets
 * @version 1.0
 */
public interface Filter<T> {

    T getValue();

    Collection<T> getValues();

    LogicOperator getOperator();

    String getAttribute();

    Collection<String> getAttributePath();
}
