package com.ws.sa.queries.filter;

import com.ws.sa.enums.LogicOperator;

import java.util.Collection;

/**
 * Фильтр для выборки сущностей для примитивов
 *
 * @author Evgeny Albinets
 */
public class PrimitiveFilter<T> extends AbstractFilter {

    PrimitiveFilter(String attribute, LogicOperator operator, T value) {
        super(attribute, value, operator);
    }

    public PrimitiveFilter(String attribute, LogicOperator operator, Collection<T> values) {
        super(attribute, values, operator);
    }

}
