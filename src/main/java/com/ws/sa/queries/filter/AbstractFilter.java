package com.ws.sa.queries.filter;

import com.ws.sa.enums.LogicOperator;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Evgeny Albinets
 */
@Getter
@EqualsAndHashCode
public abstract class AbstractFilter<T> implements Filter<T> {

    // Атрибут по которому нужно искать
    private final String attribute;

    // Путь до аттрибута
    private final List<String> attributePath;

    // множество значений
    private final Collection<T> values;

    // оператор
    private final LogicOperator operator;

    public AbstractFilter(String attribute, Collection<T> values, LogicOperator operator) {
        this.values = values;
        this.attribute = attribute;
        this.operator = operator;
        attributePath = new LinkedList<>();
        attributePath.addAll(Arrays.asList(attribute.split("\\.")));
    }

    public AbstractFilter(String attribute, T value, LogicOperator operator) {
        this(attribute, Stream.of(value).collect(Collectors.toSet()), operator);
    }

    @Override
    public T getValue() {
        return values.iterator().next();
    }
}
