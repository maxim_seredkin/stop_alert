package com.ws.sa.queries.filter;

import com.ws.sa.enums.LogicOperator;

import java.util.Collection;

/**
 * Расширенный фильтр для ручной выборки
 *
 * @author Evgeny Albinets
 */
public class ExtraFilter<T> extends AbstractFilter {

    ExtraFilter(String attribute, LogicOperator operator, T value) {
        super(attribute, value, operator);
    }

    public ExtraFilter(String attribute, LogicOperator operator, Collection<T> values) {
        super(attribute, values, operator);
    }

}
