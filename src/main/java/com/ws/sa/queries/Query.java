package com.ws.sa.queries;

import com.ws.sa.queries.filter.PrimitiveFilter;
import com.ws.sa.queries.sort.QuerySort;
import lombok.Data;
import org.springframework.data.domain.PageRequest;

import java.util.Collection;
import java.util.Set;

/**
 * @author Maxim Seredkin
 */
@Data
public class Query {

    private Set<PrimitiveFilter> filters;

    /**
     * Сортировка
     */
    private Collection<QuerySort> sort;

    /**
     * Параметры  постраничного отображения
     */
    private PageRequest paging;

}
