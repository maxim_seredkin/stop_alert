package com.ws.sa.queries;

import com.google.common.collect.Sets;
import com.ws.sa.enums.LogicOperator;
import com.ws.sa.enums.Projection;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.queries.filter.ExtraFilter;
import com.ws.sa.queries.filter.Filter;
import com.ws.sa.queries.filter.PrimitiveFilter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * <h1>Динамический запрос на получения сущности из хранилища</h1>
 *
 * @author Evgeny Albinets
 */
@Getter
@EqualsAndHashCode
public class DynamicQuery {

    // корневая сущность которая возвращается при запросе
    private final Class root;
    // фильтры
    private final Set<Filter> filters;
    // поиск
    private final Set<Filter> searchs;
    // сортировка
    private final Sort sort;
    // параметры  постраничного отображения
    private final PageRequest paging;

    private final Projection projection;

    private DynamicQuery(Builder builder) {
        this.root = builder.root;
        this.filters = builder.filters;
        this.paging = builder.paging;
        this.sort = builder.sort;
        this.searchs = builder.searchs;
        this.projection = builder.projection;
    }

    public static Builder newBuilder(Class root) {
        return new Builder(root);
    }

    public Set<Filter> getFilters() {
        return Collections.unmodifiableSet(filters);
    }

    public List<Filter> getFilters(Class<? extends Filter> clazz) {
        return filters.stream().filter(f -> f.getClass().equals(clazz)).collect(Collectors.toList());
    }

    public Set<Filter> getSearchs() {
        return Collections.unmodifiableSet(searchs);
    }

    public List<Filter> getSearchs(Class<? extends Filter> clazz) {
        return searchs.stream().filter(f -> f.getClass().equals(clazz)).collect(Collectors.toList());
    }

    public Filter getFilter(String name) {
        return filters.stream().filter(f -> f.getAttribute().equals(name)).findFirst().orElse(null);
    }

    public boolean isFilterExits(String name) {
        return getFilter(name) != null;
    }

    public static class Builder {

        private final Set<Filter> filters;
        private final Set<Filter> searchs;
        private final Class root;
        private PageRequest paging;
        private Sort sort;
        private Projection projection = Projection.LIGHT;

        private Builder(Class root) {
            this.root = root;
            this.searchs = new HashSet<>();
            this.filters = new HashSet<>();
        }

        public Builder withPrimitiveFilter(String attribute, LogicOperator operator, Collection<String> values) {
            Guard.checkEntityArgumentExists(attribute, ApiStatusCode.INVALID_PARAM);
            if (values != null && !values.isEmpty()) {
                Filter existsFilter = this.filters.stream().filter(f -> f.getAttribute().equals(attribute) && f.getOperator().equals(operator)).findFirst().orElse(null);
                if (existsFilter != null)
                    existsFilter.getValues().addAll(values);
                else
                    this.filters.add(new PrimitiveFilter<String>(attribute, operator, values));
            }
            return this;
        }

        public Builder withPrimitiveFilter(String attribute, LogicOperator operator, String value) {
            Guard.checkEntityArgumentExists(attribute, ApiStatusCode.INVALID_PARAM);
            Filter existsFilter = this.filters.stream().filter(f -> f.getAttribute().equals(attribute) && f.getOperator().equals(operator)).findFirst().orElse(null);
            if (existsFilter != null)
                existsFilter.getValues().add(value);
            else
                this.filters.add(new PrimitiveFilter<Object>(attribute, operator, Sets.newHashSet(value)));
            return this;
        }

        private Builder withPrimitiveFilters(Collection<String> filters) {
            if (filters != null && !filters.isEmpty())

                filters.forEach(filter -> {
                    Pattern pattern = Pattern.compile("^'([\\w.]+?)'\\s(eq|ne|ge|gt|le|lt|like|in|EQ|NE|GE|GT|LE|LT|LIKE|IN)\\s'(.*)'$");
                    Matcher matcher = pattern.matcher(filter);
                    if (matcher.find())
                        withPrimitiveFilter(matcher.group(1), LogicOperator.valueOf(matcher.group(2).toUpperCase()), matcher.group(3));
                });

            return this;
        }

        public Builder withExtraFilter(String attribute, LogicOperator operator, String value) {
            Guard.checkEntityArgumentExists(attribute, ApiStatusCode.INVALID_PARAM);
            Filter existsFilter = this.filters.stream().filter(f -> f.getAttribute().equals(attribute) && f.getOperator().equals(operator)).findFirst().orElse(null);
            if (existsFilter != null)
                existsFilter.getValues().add(value);
            else
                this.filters.add(new ExtraFilter(attribute, operator, Sets.newHashSet(value)));
            return this;
        }

        public Builder withExtraFilter(String attribute, LogicOperator operator, Collection<String> values) {
            Guard.checkEntityArgumentExists(attribute, ApiStatusCode.INVALID_PARAM);
            if (values != null && !values.isEmpty()) {
                Filter existsFilter = this.filters.stream().filter(f -> f.getAttribute().equals(attribute) && f.getOperator().equals(operator)).findFirst().orElse(null);
                if (existsFilter != null)
                    existsFilter.getValues().addAll(values);
                else
                    this.filters.add(new ExtraFilter(attribute, operator, values));
            }

            return this;
        }

        private Builder withExtraFilters(Collection<String> filters) {
            if (filters != null && !filters.isEmpty())

                filters.forEach(filter -> {
                    Pattern pattern = Pattern.compile("^EXTRA:'([\\w.]+?)'\\s(eq|ne|ge|gt|le|lt|like|in|EQ|NE|GE|GT|LE|LT|LIKE|IN)\\s'(.*)'$");
                    Matcher matcher = pattern.matcher(filter);
                    if (matcher.find())
                        withExtraFilter(matcher.group(1), LogicOperator.valueOf(matcher.group(2).toUpperCase()), matcher.group(3));
                });

            return this;
        }

        public Builder withFilters(Collection<String> filters) {
            this.withPrimitiveFilters(filters);
            this.withExtraFilters(filters);

            return this;
        }

        public Builder withSearch(String attribute, String search) {
            // Guard.checkEntityArgumentExists(attribute, ApiStatusCode.INVALID_PARAM);
            Filter existsSearch = this.searchs.stream().filter(s -> s.getAttribute().equals(attribute)).findFirst().orElse(null);
            if (existsSearch != null)
                existsSearch.getValues().add(search);
            else
                this.searchs.add(new PrimitiveFilter<String>(attribute, LogicOperator.EQ, Sets.newHashSet(search)));
            return this;
        }

        public Builder withSearch(String attribute, Set<String> values) {
            // Guard.checkEntityArgumentExists(attribute, ApiStatusCode.INVALID_PARAM);
            Filter existsSearch = this.searchs.stream().filter(f -> f.getAttribute().equals(attribute)).findFirst().orElse(null);
            if (existsSearch != null)
                existsSearch.getValues().addAll(values);
            else
                this.searchs.add(new PrimitiveFilter<String>(attribute, LogicOperator.EQ, values));
            return this;
        }

        public Builder withSearchs(Collection<String> searchs) {
            if (searchs != null && !searchs.isEmpty())
                searchs.forEach(filter -> {
                    Pattern pattern = Pattern.compile("^'([\\w.]+?)':'(.*)'$");
                    Matcher matcher = pattern.matcher(filter);
                    if (matcher.find())
                        withSearch(matcher.group(1), matcher.group(2));
                });

            return this;
        }

        public Builder withPaging(Integer page, Integer size) {
            if (page != null && size != null)
                this.paging = new PageRequest(page, size);
            return this;
        }

        public Builder withSorting(Collection<String> sorts) {
            if (sorts != null && !sorts.isEmpty())
                this.sort = new Sort(
                        sorts.stream().map(sort -> {
                            Pattern pattern = Pattern.compile("^([\\w.]+?):(asc|desc|ASC|DESC)$");
                            Matcher matcher = pattern.matcher(sort);
                            if (matcher.find())
                                return new Sort.Order(Sort.Direction.fromString(matcher.group(2).toUpperCase()), matcher.group(1), Sort.NullHandling.NULLS_LAST);
                            else return null;
                        }).collect(Collectors.toList()));
            return this;
        }

        public Builder projection(Projection projection) {
            this.projection = projection;
            return this;
        }

        public DynamicQuery build() {
            return new DynamicQuery(this);
        }
    }

}
