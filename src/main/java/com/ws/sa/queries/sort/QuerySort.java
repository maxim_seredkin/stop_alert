package com.ws.sa.queries.sort;

import lombok.Data;
import org.springframework.data.domain.Sort;

/**
 * Сортировка
 *
 * @author Evgeny Albinets
 * @version 1.0
 */
@Data
public class QuerySort {

    private final String field;

    private final Sort.Direction direction;

}
