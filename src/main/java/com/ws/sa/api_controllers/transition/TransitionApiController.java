package com.ws.sa.api_controllers.transition;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.transition.TransitionActions;
import com.ws.sa.actions.transition.arguments.CreateTransitionArgument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

import static com.ws.sa.actions.transition.TransitionActions.CREATE;
import static com.ws.sa.actions.transition.arguments.CreateTransitionArgument.from;

/**
 * @author Maxim Seredkin
 * @since 28.02.2017
 */
@Controller
@RequestMapping("/api")
public class TransitionApiController {

    private final ActionAllocator<TransitionActions> allocator;

    @Autowired
    public TransitionApiController(@Qualifier("transitionActionAllocator") ActionAllocator<TransitionActions> allocator) {
        this.allocator = allocator;
    }

    @GetMapping("/transitions/{id}")
    public String accept(@PathVariable("id") UUID mailId,
                         @RequestHeader("User-Agent") String headerUserAgent,
                         @RequestHeader(value = "x-real-ip", required = false) String realIp,
                         HttpServletRequest request) {
        String ip = realIp != null ? realIp : request.getRemoteAddr();

        return allocator.<CreateTransitionArgument, String>instanceOf(CREATE)
                .execute(from(mailId, headerUserAgent, ip));
    }

}
