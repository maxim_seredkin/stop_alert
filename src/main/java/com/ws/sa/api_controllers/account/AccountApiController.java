package com.ws.sa.api_controllers.account;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.EmptyArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.actions.user.arguments.UpdateAccountArgument;
import com.ws.sa.entities.user.User;
import com.ws.sa.forms.user.UpdateAccountForm;
import com.ws.sa.mappers.user.UserMapper;
import com.ws.sa.actions.account.AccountActions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.ws.sa.enums.Projection.FULL;
import static com.ws.sa.actions.account.AccountActions.GET;
import static com.ws.sa.actions.account.AccountActions.UPDATE;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@RestController
@RequestMapping("/api/account")
public class AccountApiController {

    private final UserMapper mapper;
    private final ActionAllocator<AccountActions> allocator;

    @Autowired
    public AccountApiController(UserMapper mapper,
                                @Qualifier("accountActionAllocator") ActionAllocator<AccountActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    /**
     * <h1>Получение информации об авторизованном пользователе</h1>
     *
     * @return информация об авторизованном пользователе
     */
    @RequestMapping(method = RequestMethod.GET)
    public ApiResponse<? extends DTO> account() {
        return ApiResponse.ok(mapper.toDTO(allocator.<EmptyArgument, User>instanceOf(GET)
                                                   .execute(EmptyArgument.create()),
                                           FULL));
    }

    /**
     * @param request форма редактирования аккаунта
     *
     * @return уведомление об успешном выполнении операции редактирования профиля авторизованного пользователя
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ApiResponse<? extends DTO> account(@RequestBody @Valid ApiRequest<UpdateAccountForm> request) {
        return ApiResponse.ok(mapper.toDTO(allocator.<UpdateAccountArgument, User>instanceOf(UPDATE)
                                                   .execute(UpdateAccountArgument.from(request.getForm())),
                                           FULL));
    }

}
