package com.ws.sa.api_controllers.mail_template_group;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.mail_template.MailTemplateActions;
import com.ws.sa.actions.mail_template_group.arguments.CreateMailTemplateGroupArgument;
import com.ws.sa.actions.mail_template_group.arguments.UpdateMailTemplateGroupArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.enums.Projection;
import com.ws.sa.forms.mail_template_group.MailTemplateGroupForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.mappers.mail_template_group.MailTemplateGroupMapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.mail_template.MailTemplateActions.*;
import static com.ws.sa.actions.mail_template_group.arguments.CreateMailTemplateGroupArgument.from;
import static com.ws.sa.actions.mail_template_group.arguments.UpdateMailTemplateGroupArgument.from;
import static com.ws.sa.enums.Projection.FULL;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@RestController
@RequestMapping("/api/mail-template-groups")
public class MailTemplateGroupApiController {

    private final MailTemplateGroupMapper mapper;
    private final ActionAllocator<MailTemplateActions> allocator;

    @Autowired
    public MailTemplateGroupApiController(MailTemplateGroupMapper mapper,
                                          @Qualifier("mailTemplateAllocator") ActionAllocator<MailTemplateActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    /**
     * <h1>Создать новую группу шаблона письма</h1>
     *
     * @param request форма создания группы шаблона письма
     *
     * @return информация о созданной группе шаблона письма
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.POST, value = "")
    public
    @ResponseBody
    ApiResponse<? extends DTO> create(@RequestBody @Valid ApiRequest<MailTemplateGroupForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateMailTemplateGroupArgument, MailTemplateGroup>instanceOf(CREATE)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    /**
     * <h1>Получить список шаблонов писем</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     * @param sorts   сортировка
     * @param page    страница
     * @param size    размер страницы
     *
     * @return список шаблонов
     */
    @RequestMapping(method = RequestMethod.GET, value = "")
    @ResponseBody
    public ApiResponse<Collection<? extends DTO>> get(@RequestParam(required = false) Collection<String> filters,
                                                      @RequestParam(required = false) Collection<String> searchs,
                                                      @RequestParam(required = false) Collection<String> sorts,
                                                      @RequestParam(required = false, defaultValue = "ROW") Projection projection,
                                                      @RequestParam(required = false) Integer page,
                                                      @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(MailTemplateGroup.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .projection(Projection.ROW)
                                         .withPaging(page, size)
                                         .build();

        return mapper.getPageMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<MailTemplateGroup>>instanceOf(FIND_ALL)
                                    .execute(from(query)),
                            projection);
    }

    /**
     * <h1>Получить группу шаблона письма</h1>
     *
     * @param mailTemplateGroupId идентификатор группы шаблона письма
     *
     * @return информация о группе шаблона письма
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @ResponseBody
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID mailTemplateGroupId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, MailTemplateGroup>instanceOf(FIND_ONE)
                                    .execute(from(mailTemplateGroupId)),
                            FULL);
    }

    /**
     * <h1>Редактировать группу шаблона письма</h1>
     *
     * @param mailTemplateId идентификатор шаблона письма
     * @param request        форма редактирования шаблона письма
     *
     * @return информация об отредактированном шаблоне письма
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public
    @ResponseBody
    ApiResponse<? extends DTO> update(@PathVariable("id") UUID mailTemplateId, @RequestBody @Valid ApiRequest<MailTemplateGroupForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateMailTemplateGroupArgument, MailTemplateGroup>instanceOf(UPDATE)
                                    .execute(from(mailTemplateId, request.getForm())),
                            FULL);
    }

    /**
     * <h1>Удалить группу шаблона письма</h1>
     *
     * @param mailTemplateGroupId идентификатор группы шаблона письма
     *
     * @return уведомления
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public
    @ResponseBody
    ApiResponse<Void> delete(@PathVariable("id") UUID mailTemplateGroupId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE)
                                    .execute(from(mailTemplateGroupId)));
    }

    /**
     * <h1>Удалить шаблон группы шалона письма</h1>
     *
     * @param mockPageId идентификатор группы шалона письма
     *
     * @return уведомление
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/delete")
    public
    @ResponseBody
    ApiResponse<Void> deleteLogical(@PathVariable("id") UUID mockPageId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE_LOGICAL)
                                    .execute(from(mockPageId)));
    }

    /**
     * <h1>Восстановить группу шаблона письма</h1>
     *
     * @param mockPageId идентификатор группы шаблона письма
     *
     * @return уведомление
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/recovery")
    public
    @ResponseBody
    ApiResponse<Void> recovery(@PathVariable("id") UUID mockPageId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(RECOVERY)
                                    .execute(from(mockPageId)));
    }

    /**
     * <h1>Проверка на существование группы шаблона письма с заданными параметрами</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     *
     * @return true, если существует, false, иначе
     */
    @RequestMapping(method = RequestMethod.GET, value = "/exists")
    @ResponseBody
    public ApiResponse<Boolean> exists(@RequestParam(required = false) Collection<String> filters,
                                       @RequestParam(required = false) Collection<String> searchs) {

        DynamicQuery query = DynamicQuery.newBuilder(MailTemplateGroup.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .build();

        return Mapper.getPrimitiveMapper(Boolean.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Boolean>instanceOf(EXISTS)
                                    .execute(from(query)));
    }

}
