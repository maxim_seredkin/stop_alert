package com.ws.sa.api_controllers.task;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.task.TaskActions;
import com.ws.sa.actions.task.arguments.CreateTaskArgument;
import com.ws.sa.actions.task.arguments.UpdateStartDateTaskArgument;
import com.ws.sa.actions.task.arguments.UpdateTaskArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.task.Task;
import com.ws.sa.enums.Projection;
import com.ws.sa.forms.task.StartDateForm;
import com.ws.sa.forms.task.TaskForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.mappers.employee.EmployeeMapper;
import com.ws.sa.mappers.task.TaskMapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.task.TaskActions.*;
import static com.ws.sa.actions.task.arguments.CreateTaskArgument.from;
import static com.ws.sa.actions.task.arguments.UpdateStartDateTaskArgument.from;
import static com.ws.sa.actions.task.arguments.UpdateTaskArgument.from;
import static com.ws.sa.enums.Projection.*;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
@RestController
@RequestMapping("/api")
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class TaskApiController {

    private final TaskMapper mapper;
    private final EmployeeMapper employeeMapper;
    private final ActionAllocator<TaskActions> allocator;

    @Autowired
    public TaskApiController(TaskMapper mapper,
                             EmployeeMapper employeeMapper,
                             @Qualifier("taskActionAllocator") ActionAllocator<TaskActions> allocator) {
        this.mapper = mapper;
        this.employeeMapper = employeeMapper;
        this.allocator = allocator;
    }

    @GetMapping("/tasks")
    public ApiResponse<Collection<? extends DTO>> get(@RequestParam(required = false) Collection<String> filters,
                                                      @RequestParam(required = false) Collection<String> searchs,
                                                      @RequestParam(required = false) Collection<String> sorts,
                                                      @RequestParam(required = false, defaultValue = "ROW") Projection projection,
                                                      @RequestParam(required = false) Integer page,
                                                      @RequestParam(required = false) Integer size) {
        DynamicQuery query = DynamicQuery.newBuilder(Task.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .withPaging(page, size)
                                         .build();


        return mapper.getPageMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<Task>>instanceOf(FIND_ALL)
                                    .execute(from(query)),
                            projection);
    }

    @PostMapping("/tasks")
    @PreAuthorize("@taskAccess.accessCreate()")
    public ApiResponse<? extends DTO> create(@RequestBody @Valid ApiRequest<TaskForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateTaskArgument, Task>instanceOf(CREATE)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    @GetMapping("/tasks/{id}")
    @PreAuthorize("@taskAccess.accessRead(#taskId)")
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID taskId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Task>instanceOf(FIND_ONE)
                                    .execute(from(taskId)),
                            FULL);
    }

    @PutMapping("/tasks/{id}")
    @PreAuthorize("@taskAccess.accessUpdate(#taskId)")
    public ApiResponse<? extends DTO> update(@PathVariable("id") UUID taskId,
                                             @RequestBody @Valid ApiRequest<TaskForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateTaskArgument, Task>instanceOf(UPDATE)
                                    .execute(from(taskId, request.getForm())),
                            FULL);
    }

    @PutMapping("/tasks/{id}/resume")
    @PreAuthorize("@taskAccess.accessUpdate(#taskId)")
    public ApiResponse<? extends DTO> resume(@PathVariable("id") UUID taskId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Task>instanceOf(RESUME)
                                    .execute(from(taskId)),
                            FULL);
    }

    @PutMapping("/tasks/{id}/pause")
    @PreAuthorize("@taskAccess.accessUpdate(#taskId)")
    public ApiResponse<? extends DTO> pause(@PathVariable("id") UUID taskId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Task>instanceOf(PAUSE)
                                    .execute(from(taskId)),
                            FULL);
    }

    @PutMapping("/tasks/{id}/cancel")
    @PreAuthorize("@taskAccess.accessDelete(#taskId)")
    public ApiResponse<Void> cancel(@PathVariable("id") UUID taskId) {
        return (Mapper.getPrimitiveMapper(Void.class))
                .andThen(ApiResponse::ok)
                .apply(allocator.<UuidArgument, Void>instanceOf(CANCEL)
                               .execute(from(taskId)));
    }

    @PutMapping("/tasks/{id}/update/start-date")
    @PreAuthorize("@taskAccess.accessUpdate(#taskId)")
    public ApiResponse<? extends DTO> updateStartDate(@PathVariable("id") UUID taskId,
                                                      @RequestBody @Valid ApiRequest<StartDateForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateStartDateTaskArgument, Task>instanceOf(UPDATE_START_DATE)
                                    .execute(from(taskId, request.getForm())),
                            FULL);
    }

    @PreAuthorize("@taskAccess.accessRead(#taskId)")
    @GetMapping("/tasks/{id}/employees/count-attackable")
    public ApiResponse<Integer> countAttackableEmployees(@PathVariable("id") UUID taskId) {
        return Mapper.getPrimitiveMapper(Integer.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Integer>instanceOf(GET_COUNT_ATTACKABLE_EMPLOYEES)
                                    .execute(from(taskId)));
    }

    @PreAuthorize("@taskAccess.accessRead(#taskId)")
    @RequestMapping(method = RequestMethod.GET, value = "/tasks/{id}/employees/filter")
    @ResponseBody
    public ApiResponse<Collection<? extends DTO>> getFilterEmployees(@PathVariable("id") UUID taskId) {
        return employeeMapper.getCollectionMapper()
                             .andThen(ApiResponse::ok)
                             .apply(allocator.<UuidArgument, Collection<Employee>>instanceOf(GET_FILTER_EMPLOYEES)
                                            .execute(from(taskId)),
                                    SELECTOR);
    }

    @PreAuthorize("@taskAccess.accessRead(#taskId)")
    @RequestMapping(method = RequestMethod.GET, value = "/tasks/{id}/employees/hand")
    @ResponseBody
    public ApiResponse<?> getHandEmployees(@PathVariable("id") UUID taskId) {
        return employeeMapper.getCollectionMapper()
                             .andThen(ApiResponse::ok)
                             .apply(allocator.<UuidArgument, Collection<Employee>>instanceOf(GET_HAND_EMPLOYEES)
                                            .execute(from(taskId)),
                                    SELECTOR);
    }

}
