package com.ws.sa.api_controllers.employee;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.GetFileArgument;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.employee.EmployeeActions;
import com.ws.sa.actions.employee.arguments.CreateEmployeeArgument;
import com.ws.sa.actions.employee.arguments.ImportEmployeesArgument;
import com.ws.sa.actions.employee.arguments.ImportFromCSVEmployeeArgument;
import com.ws.sa.actions.employee.arguments.UpdateEmployeeArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.employee.ImportDTO;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.enums.Projection;
import com.ws.sa.forms.employee.EmployeeForm;
import com.ws.sa.forms.employee.ImportEmployeesForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.mappers.employee.EmployeeMapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.GetFileArgument.from;
import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.employee.EmployeeActions.*;
import static com.ws.sa.actions.employee.arguments.CreateEmployeeArgument.from;
import static com.ws.sa.actions.employee.arguments.ImportEmployeesArgument.from;
import static com.ws.sa.actions.employee.arguments.UpdateEmployeeArgument.from;
import static com.ws.sa.beans.ApiResponse.ok;
import static com.ws.sa.enums.Projection.FULL;
import static com.ws.sa.enums.ResourceType.IMPORT;
import static com.ws.sa.utils.FileUtils.fillResponse;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@RestController
@RequestMapping("/api")
public class EmployeeApiController {

    private final EmployeeMapper mapper;
    private final ActionAllocator<EmployeeActions> allocator;

    @Autowired
    public EmployeeApiController(EmployeeMapper mapper, ActionAllocator<EmployeeActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    /**
     * <h1>Получить список сотрудников</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     * @param sorts   сортировка
     * @param page    страница
     * @param size    размер страницы
     *
     * @return список сотрудников
     */

    @RequestMapping(method = RequestMethod.GET, value = "/employees")
    @ResponseBody
    public ApiResponse<Collection<? extends DTO>> get(@RequestParam(required = false) Collection<String> filters,
                                                      @RequestParam(required = false) Collection<String> searchs,
                                                      @RequestParam(required = false) Collection<String> sorts,
                                                      @RequestParam(required = false, defaultValue = "ROW") Projection projection,
                                                      @RequestParam(required = false) Integer page,
                                                      @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(Employee.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .projection(projection)
                                         .withPaging(page, size)
                                         .build();

        return mapper.getPageMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<Employee>>instanceOf(FIND_ALL)
                                    .execute(from(query)),
                            projection);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/employees/count")
    @ResponseBody
    public ApiResponse<Long> count(@RequestParam(required = false) Collection<String> filters,
                                   @RequestParam(required = false) Collection<String> searchs) {

        DynamicQuery query = DynamicQuery.newBuilder(Employee.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .build();

        return Mapper.getPrimitiveMapper(Long.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Long>instanceOf(COUNT)
                                    .execute(from(query)));
    }

    @PreAuthorize("@employeeAccess.accessCreate()")
    @RequestMapping(method = RequestMethod.POST, value = "/employees")
    public
    @ResponseBody
    ApiResponse<? extends DTO> create(@RequestBody @Valid ApiRequest<EmployeeForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateEmployeeArgument, Employee>instanceOf(CREATE)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    @PreAuthorize("@employeeAccess.accessRead(#employeeId)")
    @RequestMapping(method = RequestMethod.GET, value = "/employees/{id}")
    @ResponseBody
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID employeeId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Employee>instanceOf(FIND_ONE)
                                    .execute(from(employeeId)),
                            FULL);
    }

    @PreAuthorize("@employeeAccess.accessRead(#employeeId)")
    @RequestMapping(method = RequestMethod.PUT, value = "/employees/{id}")
    public
    @ResponseBody
    ApiResponse<? extends DTO> update(@PathVariable("id") UUID employeeId, @RequestBody @Valid ApiRequest<EmployeeForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateEmployeeArgument, Employee>instanceOf(UPDATE)
                                    .execute(from(employeeId, request.getForm())),
                            FULL);
    }

    @PreAuthorize("@employeeAccess.accessRead(#employeeId)")
    @RequestMapping(method = RequestMethod.PUT, value = "/employees/{id}/delete")
    public
    @ResponseBody
    ApiResponse<Void> delete(@PathVariable("id") UUID employeeId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE_LOGICAL)
                                    .execute(from(employeeId)));
    }

    @PreAuthorize("@employeeAccess.accessRead(#employeeId)")
    @RequestMapping(method = RequestMethod.PUT, value = "/employees/{id}/recovery")
    public
    @ResponseBody
    ApiResponse<Void> recovery(@PathVariable("id") UUID employeeId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(RECOVERY)
                                    .execute(from(employeeId)));
    }

    @PreAuthorize("@employeeAccess.accessCreate()")
    @RequestMapping(method = RequestMethod.POST, value = "/employees/import")
    public
    @ResponseBody
    ApiResponse<ImportDTO> importFromCSV(@RequestParam("file") MultipartFile file) throws Exception {
        return Mapper.getPrimitiveMapper(ImportDTO.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<ImportFromCSVEmployeeArgument, ImportDTO>instanceOf(IMPORT_FROM_CSV)
                                    .execute(ImportFromCSVEmployeeArgument.from(file)));
    }

    @PreAuthorize("@employeeAccess.accessCreate()")
    @RequestMapping(method = RequestMethod.POST, value = "/employees/import/save")
    public
    @ResponseBody
    ApiResponse<Collection<? extends DTO>> saveImport(@RequestBody @Valid ApiRequest<ImportEmployeesForm> request) throws Exception {
        return mapper.getCollectionMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<ImportEmployeesArgument, Collection<Employee>>instanceOf(IMPORT_EMPLOYEES)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    @PreAuthorize("@employeeAccess.accessCreate()")
    @RequestMapping(method = RequestMethod.GET, value = "/employees/import/sample")
    @ResponseBody
    public void downloadSampleCSV(HttpServletResponse servletResponse) throws Exception {
        fillResponse(allocator.<GetFileArgument, File>instanceOf(DOWNLOAD_SAMPLE_CSV)
                             .execute(from("EmployeeImportSample.csv", IMPORT)),
                     "application/csv",
                     servletResponse);
    }
}