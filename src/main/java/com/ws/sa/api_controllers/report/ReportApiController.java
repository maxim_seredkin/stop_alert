package com.ws.sa.api_controllers.report;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.employee.EmployeeActions;
import com.ws.sa.actions.task.TaskActions;
import com.ws.sa.actions.task.arguments.CreateTaskReportArgument;
import com.ws.sa.actions.unit.UnitActions;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.task.Task;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.enums.LogicOperator;
import com.ws.sa.mappers.report.employee.EmployeeReportMapper;
import com.ws.sa.mappers.report.employee.EmployeeTaskResultMapper;
import com.ws.sa.mappers.report.task.TaskEmployeeResultMapper;
import com.ws.sa.mappers.report.task.TaskReportMapper;
import com.ws.sa.mappers.report.unit.UnitEmployeeResultMapper;
import com.ws.sa.mappers.report.unit.UnitReportMapper;
import com.ws.sa.mappers.report.unit.UnitTaskResultMapper;
import com.ws.sa.queries.DynamicQuery;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.task.TaskActions.*;
import static com.ws.sa.enums.Projection.FULL;
import static com.ws.sa.enums.Projection.ROW;

/**
 * @author Maxim Seredkin
 * @since 22.03.2017
 */
@RestController
@RequestMapping("/api/reports")
public class ReportApiController {

    private final TaskReportMapper taskReportMapper;
    private final ActionAllocator<UnitActions> unitAllocator;
    private final ActionAllocator<TaskActions> taskAllocator;
    private final ActionAllocator<EmployeeActions> employeeAllocator;
    private final TaskEmployeeResultMapper taskEmployeeResultMapper;
    private final EmployeeTaskResultMapper employeeTaskResultMapper;
    private final UnitTaskResultMapper unitTaskResultMapper;
    private final UnitEmployeeResultMapper unitEmployeeResultMapper;
    private final UnitReportMapper unitReportMapper;
    private final EmployeeService employeeService;
    private final EmployeeReportMapper employeeReportMapper;
    private final TaskService taskService;

    @Autowired
    public ReportApiController(TaskService taskService,
                               TaskReportMapper taskReportMapper,
                               TaskEmployeeResultMapper taskEmployeeResultMapper,
                               EmployeeService employeeService,
                               EmployeeReportMapper employeeReportMapper,
                               EmployeeTaskResultMapper employeeTaskResultMapper,
                               UnitReportMapper unitReportMapper,
                               UnitEmployeeResultMapper unitEmployeeResultMapper,
                               UnitTaskResultMapper unitTaskResultMapper,
                               @Qualifier("taskActionAllocator") ActionAllocator<TaskActions> taskAllocator,
                               @Qualifier("employeeActionAllocator") ActionAllocator<EmployeeActions> employeeAllocator,
                               @Qualifier("unitActionAllocator") ActionAllocator<UnitActions> unitAllocator) {
        this.taskReportMapper = taskReportMapper;
        this.taskEmployeeResultMapper = taskEmployeeResultMapper;
        this.taskAllocator = taskAllocator;
        this.employeeReportMapper = employeeReportMapper;
        this.employeeAllocator = employeeAllocator;
        this.employeeService = employeeService;
        this.taskService = taskService;
        this.employeeTaskResultMapper = employeeTaskResultMapper;
        this.unitAllocator = unitAllocator;
        this.unitReportMapper = unitReportMapper;
        this.unitTaskResultMapper = unitTaskResultMapper;
        this.unitEmployeeResultMapper = unitEmployeeResultMapper;
    }

    @RequestMapping("/tasks")
    public ApiResponse tasks(@RequestParam(required = false) Collection<String> filters,
                             @RequestParam(required = false) Collection<String> searchs,
                             @RequestParam(required = false) Collection<String> sorts,
                             @RequestParam(required = false) Integer page,
                             @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(Task.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .withPaging(page, size)
                                         .build();

        return taskReportMapper.getPageMapper()
                               .andThen(ApiResponse::ok)
                               .apply(taskAllocator.<QueryArgument, Page<Task>>instanceOf(FIND_ALL)
                                              .execute(from(query)),
                                      ROW);
    }

    @PreAuthorize("@taskAccess.accessRead(#id)")
    @RequestMapping("/tasks/{id}")
    public ApiResponse task(@PathVariable(name = "id") UUID id) {
        return taskReportMapper.getSimpleMapper()
                               .andThen(ApiResponse::ok)
                               .apply(taskAllocator.<UuidArgument, Task>instanceOf(FIND_ONE)
                                              .execute(UuidArgument.from(id)),
                                      FULL);
    }

    @PreAuthorize("@taskAccess.accessRead(#id)")
    @RequestMapping("/tasks/{id}/employees")
    public ApiResponse getTaskResultByEmployees(@PathVariable(name = "id") UUID id,
                                                @RequestParam(required = false) Collection<String> filters,
                                                @RequestParam(required = false) Collection<String> searchs,
                                                @RequestParam(required = false) Collection<String> sorts,
                                                @RequestParam(required = false) Integer page,
                                                @RequestParam(required = false) Integer size) {

        DynamicQuery.Builder builder = DynamicQuery.newBuilder(Employee.class)
                                                   .withFilters(filters)
                                                   .withSearchs(searchs)
                                                   .withSorting(sorts)
                                                   .withPaging(page, size);

        builder.withExtraFilter("task", LogicOperator.EQ, id.toString());

        return taskEmployeeResultMapper.getEmployeePageMapper()
                                       .andThen(ApiResponse::ok)
                                       .apply(id,
                                              employeeAllocator.<QueryArgument, Page<Employee>>instanceOf(EmployeeActions.FIND_ALL)
                                                      .execute(from(builder.build())));
    }

    @PreAuthorize("@taskAccess.accessRead(#id)")
    @RequestMapping("/tasks/{id}/document")
    public void taskDocument(@PathVariable(name = "id") UUID id, HttpServletResponse servletResponse) throws IOException {
        taskAllocator.<CreateTaskReportArgument, Void>instanceOf(CREATE_REPORT)
                .execute(CreateTaskReportArgument.from(id, servletResponse));
    }

    @RequestMapping("/units")
    public ApiResponse units(@RequestParam(required = false) Collection<String> filters,
                             @RequestParam(required = false) Collection<String> searchs,
                             @RequestParam(required = false) Collection<String> sorts,
                             @RequestParam(required = false) Integer page,
                             @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(Unit.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .withPaging(page, size)
                                         .build();

        return unitReportMapper.getPageMapper()
                               .andThen(ApiResponse::ok)
                               .apply(unitAllocator.<QueryArgument, Page<Unit>>instanceOf(UnitActions.FIND_ALL)
                                              .execute(from(query)),
                                      ROW);
    }

    @PreAuthorize("@unitAccess.accessRead(#id)")
    @RequestMapping("/units/{id}")
    public ApiResponse unit(@PathVariable(name = "id") UUID id) {
        return unitReportMapper.getSimpleMapper()
                               .andThen(ApiResponse::ok)
                               .apply(unitAllocator.<UuidArgument, Unit>instanceOf(UnitActions.FIND_ONE)
                                              .execute(UuidArgument.from(id)),
                                      FULL);
    }

    @PreAuthorize("@unitAccess.accessRead(#id)")
    @RequestMapping("/units/{id}/tasks")
    public ApiResponse getUnitResultByTasks(@PathVariable(name = "id") UUID id,
                                            @RequestParam(required = false) Collection<String> filters,
                                            @RequestParam(required = false) Collection<String> searchs,
                                            @RequestParam(required = false) Collection<String> sorts,
                                            @RequestParam(required = false) Integer page,
                                            @RequestParam(required = false) Integer size) {

        DynamicQuery.Builder builder = DynamicQuery.newBuilder(Task.class)
                                                   .withFilters(filters)
                                                   .withSearchs(searchs)
                                                   .withSorting(sorts)
                                                   .withPaging(page, size);

        builder.withExtraFilter("unit", LogicOperator.EQ, id.toString());

        return unitTaskResultMapper.getTaskPageMapper()
                                   .andThen(ApiResponse::ok)
                                   .apply(id,
                                          taskAllocator.<QueryArgument, Page<Task>>instanceOf(TaskActions.FIND_ALL)
                                                  .execute(from(builder.build())));
    }

    @PreAuthorize("@unitAccess.accessRead(#id)")
    @RequestMapping("/units/{id}/employees")
    public ApiResponse getUnitResultByEmployees(@PathVariable(name = "id") UUID id,
                                                @RequestParam(required = false) Collection<String> filters,
                                                @RequestParam(required = false) Collection<String> searchs,
                                                @RequestParam(required = false) Collection<String> sorts,
                                                @RequestParam(required = false) Integer page,
                                                @RequestParam(required = false) Integer size) {

        DynamicQuery.Builder builder = DynamicQuery.newBuilder(Employee.class)
                                                   .withFilters(filters)
                                                   .withSearchs(searchs)
                                                   .withSorting(sorts)
                                                   .withPaging(page, size);

        builder.withExtraFilter("unit", LogicOperator.EQ, id.toString());

        return unitEmployeeResultMapper.getEmployeePageMapper()
                                       .andThen(ApiResponse::ok)
                                       .apply(employeeAllocator.<QueryArgument, Page<Employee>>instanceOf(EmployeeActions.FIND_ALL)
                                                      .execute(from(builder.build())));

    }

    @RequestMapping("/employees")
    public ApiResponse employees(@RequestParam(required = false) Collection<String> filters,
                                 @RequestParam(required = false) Collection<String> searchs,
                                 @RequestParam(required = false) Collection<String> sorts,
                                 @RequestParam(required = false) Integer page,
                                 @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(Employee.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .withPaging(page, size)
                                         .build();

        return employeeReportMapper.getPageMapper()
                                   .andThen(ApiResponse::ok)
                                   .apply(employeeAllocator.<QueryArgument, Page<Employee>>instanceOf(EmployeeActions.FIND_ALL)
                                                  .execute(from(query)),
                                          ROW);
    }

    @PreAuthorize("@employeeAccess.accessRead(#id)")
    @RequestMapping("/employees/{id}")
    public ApiResponse employee(@PathVariable(name = "id") UUID id) {
        return employeeReportMapper.getSimpleMapper()
                                   .andThen(ApiResponse::ok)
                                   .apply(employeeAllocator.<UuidArgument, Employee>instanceOf(EmployeeActions.FIND_ONE)
                                                  .execute(UuidArgument.from(id)),
                                          FULL);
    }

    @PreAuthorize("@employeeAccess.accessRead(#id)")
    @RequestMapping("/employees/{id}/tasks")
    public ApiResponse getEmployeeResultByTasks(@PathVariable(name = "id") UUID id,
                                                @RequestParam(required = false) Collection<String> filters,
                                                @RequestParam(required = false) Collection<String> searchs,
                                                @RequestParam(required = false) Collection<String> sorts,
                                                @RequestParam(required = false) Integer page,
                                                @RequestParam(required = false) Integer size) {

        DynamicQuery.Builder builder = DynamicQuery.newBuilder(Task.class)
                                                   .withFilters(filters)
                                                   .withSearchs(searchs)
                                                   .withSorting(sorts)
                                                   .withPaging(page, size);

        builder.withExtraFilter("employee", LogicOperator.EQ, id.toString());

        return employeeTaskResultMapper.getTaskPageMapper()
                                       .andThen(ApiResponse::ok)
                                       .apply(id,
                                              taskAllocator.<QueryArgument, Page<Task>>instanceOf(TaskActions.FIND_ALL)
                                                      .execute(from(builder.build())));
    }
}
