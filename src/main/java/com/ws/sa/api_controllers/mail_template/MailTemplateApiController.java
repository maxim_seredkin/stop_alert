package com.ws.sa.api_controllers.mail_template;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.GetFileArgument;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.mail_template.MailTemplateActions;
import com.ws.sa.actions.mail_template.arguments.CreateMailTemplateArgument;
import com.ws.sa.actions.mail_template.arguments.UpdateMailTemplateArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.mail_template.MailTemplateExampleDTO;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.enums.Projection;
import com.ws.sa.forms.mail_template.MailTemplateForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.GetFileArgument.from;
import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.mail_template.MailTemplateActions.*;
import static com.ws.sa.actions.mail_template.arguments.CreateMailTemplateArgument.from;
import static com.ws.sa.actions.mail_template.arguments.UpdateMailTemplateArgument.from;
import static com.ws.sa.enums.Projection.FULL;
import static com.ws.sa.enums.ResourceType.TEMPLATE;
import static com.ws.sa.utils.FileUtils.fillResponse;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@RestController
@RequestMapping("/api/mail-templates")
public class MailTemplateApiController {

    private final MailTemplateMapper mapper;
    private final ActionAllocator<MailTemplateActions> allocator;

    @Autowired
    public MailTemplateApiController(MailTemplateMapper mapper,
                                     @Qualifier("mailTemplateAllocator") ActionAllocator<MailTemplateActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    /**
     * <h1>Создать новый шаблон письма</h1>
     *
     * @param request форма создания шаблона письма
     *
     * @return информация о созданном шаблоне письма
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PostMapping(value = "", consumes = {"multipart/form-data"})
    public ApiResponse<? extends DTO> create(@RequestPart("payload") @Valid ApiRequest<MailTemplateForm> request,
                                             @Valid @NotNull @RequestPart("file") MultipartFile file) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateMailTemplateArgument, MailTemplate>instanceOf(CREATE)
                                    .execute(from(request.getForm(), file)),
                            FULL);
    }

    /**
     * <h1>Получить список шаблонов писем</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     * @param sorts   сортировка
     * @param page    страница
     * @param size    размер страницы
     *
     * @return список шаблонов
     */
    @GetMapping("")
    public ApiResponse<Collection<? extends DTO>> get(@RequestParam(required = false) Collection<String> filters,
                                                      @RequestParam(required = false) Collection<String> searchs,
                                                      @RequestParam(required = false) Collection<String> sorts,
                                                      @RequestParam(required = false, defaultValue = "ROW") Projection projection,
                                                      @RequestParam(required = false) Integer page,
                                                      @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(MailTemplate.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .withPaging(page, size)
                                         .build();

        return mapper.getPageMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<MailTemplate>>instanceOf(FIND_ALL)
                                    .execute(from(query)),
                            projection);
    }

    /**
     * <h1>Получить шаблон письма</h1>
     *
     * @param mailTemplateId идентификатор шаблона письма
     *
     * @return информация о шаблоне письма
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID mailTemplateId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, MailTemplate>instanceOf(FIND_ONE)
                                    .execute(from(mailTemplateId)),
                            FULL);
    }

    /**
     * <h1>Редактировать шаблон письма</h1>
     *
     * @param mailTemplateId идентификатор шаблона письма
     * @param request        форма редактирования шаблона письма
     *
     * @return информация об отредактированном шаблоне письма
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PutMapping(value = "/{id}", consumes = {"multipart/form-data"})
    public ApiResponse<? extends DTO> update(@PathVariable("id") UUID mailTemplateId,
                                             @RequestPart("payload") @Valid ApiRequest<MailTemplateForm> request,
                                             @RequestPart(value = "file", required = false) MultipartFile file) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateMailTemplateArgument, MailTemplate>instanceOf(UPDATE)
                                    .execute(from(mailTemplateId, request.getForm(), file)),
                            FULL);
    }

    /**
     * <h1>Удалить шаблон письма</h1>
     *
     * @param mailTemplateId идентификатор шаблона письма
     *
     * @return уведомления
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> delete(@PathVariable("id") UUID mailTemplateId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE)
                                    .execute(from(mailTemplateId)));
    }

    /**
     * <h1>Удалить шаблон письма логически</h1>
     *
     * @param mailTemplateId идентификатор шаблона письма
     *
     * @return уведомление
     */
    @PutMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> deleteLogical(@PathVariable("id") UUID mailTemplateId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE_LOGICAL)
                                    .execute(from(mailTemplateId)));
    }

    /**
     * <h1>Восстановить шаблон письма</h1>
     *
     * @param mailTemplateId идентификатор пользователя
     *
     * @return уведомление
     */
    @PutMapping("/{id}/recovery")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> recovery(@PathVariable("id") UUID mailTemplateId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(RECOVERY)
                                    .execute(from(mailTemplateId)));
    }

    /**
     * <h1>Проверка на существование шаблона письма с заданными параметрами</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     *
     * @return true, если существует, false, иначе
     */
    @GetMapping("/exists")
    public ApiResponse<Boolean> exists(@RequestParam(required = false) Collection<String> filters,
                                       @RequestParam(required = false) Collection<String> searchs) {

        DynamicQuery query = DynamicQuery.newBuilder(MailTemplate.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .build();

        return Mapper.getPrimitiveMapper(Boolean.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Boolean>instanceOf(EXISTS)
                                    .execute(from(query)));
    }

    @GetMapping("/{id}/example")
    public ApiResponse<? extends DTO> example(@PathVariable("id") UUID mailTemplateId) {
        return Mapper.getPrimitiveMapper(MailTemplateExampleDTO.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, MailTemplateExampleDTO>instanceOf(EXAMPLE)
                                    .execute(from(mailTemplateId)));
    }

    @GetMapping("/sample")
    public void sample(HttpServletResponse servletResponse) throws IOException {
        fillResponse(allocator.<GetFileArgument, File>instanceOf(DOWNLOAD_SAMPLE_HTML)
                             .execute(from("sample.vm", TEMPLATE)),
                     "text/html",
                     servletResponse);
    }

}
