package com.ws.sa.api_controllers.employee_tag;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.employee_tag.EmployeeTagActions;
import com.ws.sa.actions.employee_tag.arguments.CreateEmployeeTagArgument;
import com.ws.sa.actions.employee_tag.arguments.UpdateEmployeeTagArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.base.TreeNode;
import com.ws.sa.dtos.employee_tag.EmployeeTagNodeDTO;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.forms.tag.employee.EmployeeTagForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.employee_tag.EmployeeTagActions.*;
import static com.ws.sa.actions.employee_tag.arguments.CreateEmployeeTagArgument.from;
import static com.ws.sa.enums.EntityStatus.ENABLED;
import static com.ws.sa.enums.LogicOperator.EQ;
import static com.ws.sa.enums.Projection.FULL;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@RestController
@RequestMapping("/api")
public class EmployeeTagApiController {

    private final EmployeeTagMapper mapper;
    private final ActionAllocator<EmployeeTagActions> allocator;

    @Autowired
    public EmployeeTagApiController(EmployeeTagMapper mapper,
                                    @Qualifier("employeeTagAllocator") ActionAllocator<EmployeeTagActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    @GetMapping(value = "/employee-tags/tree")
    public ApiResponse<Collection<TreeNode<EmployeeTagNodeDTO, EmployeeTagNodeDTO>>> getTree() {
        DynamicQuery query = DynamicQuery.newBuilder(EmployeeTag.class)
                                         .withPrimitiveFilter("status", EQ, ENABLED.name())
                                         .build();

        return mapper.getTreeMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<EmployeeTag>>instanceOf(FIND_ALL)
                                    .execute(from(query))
                                    .getContent());
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PostMapping(value = "/employee-tags")
    public ApiResponse<? extends DTO> create(@RequestBody @Valid ApiRequest<EmployeeTagForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateEmployeeTagArgument, EmployeeTag>instanceOf(CREATE)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    @GetMapping(value = "/employee-tags/{id}")
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID employeeTagId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, EmployeeTag>instanceOf(FIND_ONE)
                                    .execute(from(employeeTagId)),
                            FULL);
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PutMapping(value = "/employee-tags/{id}")
    public ApiResponse<? extends DTO> update(@PathVariable("id") UUID employeeTagId,
                                             @RequestBody @Valid ApiRequest<EmployeeTagForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateEmployeeTagArgument, EmployeeTag>instanceOf(UPDATE)
                                    .execute(UpdateEmployeeTagArgument.from(employeeTagId, request.getForm())),
                            FULL);
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @PutMapping(value = "/employee-tags/{id}/delete")
    public ApiResponse<Void> delete(@PathVariable("id") UUID employeeTagId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE)
                                    .execute(from(employeeTagId)));
    }

}
