package com.ws.sa.api_controllers.setting;

import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.forms.setting.*;
import com.ws.sa.services.setting.AddressService;
import com.ws.sa.services.setting.LdapService;
import com.ws.sa.services.setting.LicenseService;
import com.ws.sa.services.setting.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

/**
 * Created by Vladislav Shelengovskiy on 30.03.2017.
 */
@RestController
@RequestMapping("/api/settings")
public class SettingApiController {

    private final ServerService serverService;
    private final LicenseService licenseService;
    private final AddressService addressService;
    private final LdapService ldapService;

    @Autowired
    public SettingApiController(ServerService serverService,
                                LicenseService licenseService,
                                AddressService addressService,
                                LdapService ldapService) {
        this.serverService = serverService;
        this.licenseService = licenseService;
        this.addressService = addressService;
        this.ldapService = ldapService;
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/server")
    @ResponseBody
    public ApiResponse<?> getServerSettings() throws IOException {
        return ApiResponse.ok(serverService.getSettings());
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/server")
    @ResponseBody
    public ApiResponse<?> setServerSettings(@RequestBody @Valid ApiRequest<ServerSettingForm> request) throws IOException {
        serverService.setSettings(request.getForm());
        return ApiResponse.ok();
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/transition")
    @ResponseBody
    public ApiResponse<?> getTransitionAddress() throws IOException {
        return ApiResponse.ok(addressService.getTransitionAddress());
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/transition")
    @ResponseBody
    public ApiResponse<?> setTransitionAddress(@RequestBody @Valid ApiRequest<TransitionForm> request) {
        addressService.setTransitionAddress(request.getForm());
        return ApiResponse.ok();
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/address")
    @ResponseBody
    public ApiResponse<?> getExternalAddress() throws IOException {
        return ApiResponse.ok(addressService.getExternalAddress());
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/address")
    @ResponseBody
    public ApiResponse<?> setExternalAddress(@RequestBody @Valid ApiRequest<AddressForm> request) {
        addressService.setExternalAddress(request.getForm());
        return ApiResponse.ok();
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/server/verify")
    @ResponseBody
    public ApiResponse<?> verifyConnectionState(@RequestBody @Valid ApiRequest<ServerSettingForm> request) {
        return ApiResponse.ok(serverService.verifyConnectionState(request.getForm()));
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/server/verify")
    @ResponseBody
    public ApiResponse<?> verifyCurrentConnectionState() throws IOException {
        return ApiResponse.ok(serverService.verifyConnectionState());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/license")
    @ResponseBody
    public ApiResponse<?> getLicense() {
        return ApiResponse.ok(licenseService.getLicense());
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/license")
    @ResponseBody
    public ApiResponse<?> activateLicense(@RequestBody @Valid ApiRequest<LicenseForm> request) {
        return ApiResponse.ok(licenseService.activateLicense(request.getForm()));
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/ldap")
    @ResponseBody
    public ApiResponse<?> getLdapSetting() throws IOException {
        return ApiResponse.ok(ldapService.getLdapSetting());
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/ldap")
    @ResponseBody
    public ApiResponse<?> setLdapSetting(@RequestBody @Valid ApiRequest<LdapForm> request) {
        ldapService.setLdapSetting(request.getForm());
        return ApiResponse.ok();
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/ldap/verify")
    @ResponseBody
    public ApiResponse<?> verifyLdapConnectionState() {
        return ApiResponse.ok(true);
    }
}
