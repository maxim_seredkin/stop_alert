package com.ws.sa.api_controllers.user;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.user.UserActions;
import com.ws.sa.actions.user.arguments.CreateUserArgument;
import com.ws.sa.actions.user.arguments.ResetPasswordArgument;
import com.ws.sa.actions.user.arguments.UpdateUserArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.entities.user.User;
import com.ws.sa.enums.Projection;
import com.ws.sa.forms.user.ResetPasswordForm;
import com.ws.sa.forms.user.UserForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.user.UserActions.*;
import static com.ws.sa.actions.user.arguments.CreateUserArgument.from;
import static com.ws.sa.actions.user.arguments.ResetPasswordArgument.from;
import static com.ws.sa.actions.user.arguments.UpdateUserArgument.from;
import static com.ws.sa.enums.Projection.FULL;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@RestController
@RequestMapping("/api/users")
public class UserApiController {

    private final Mapper<User> mapper;
    private final ActionAllocator<UserActions> allocator;

    @Autowired
    public UserApiController(@Qualifier("userMapperImpl") Mapper<User> mapper,
                             @Qualifier("userActionAllocator") ActionAllocator<UserActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    // region CRUD

    /**
     * <h1>Создать нового пользователя</h1>
     *
     * @param request форма создания пользователя
     *
     * @return информация о созданном пользователе
     */
    @PostMapping(value = "")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<? extends DTO> create(@RequestBody @Valid ApiRequest<UserForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateUserArgument, User>instanceOf(CREATE)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    /**
     * <h1>Получить список пользователей</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     * @param sorts   сортировка
     * @param page    страница
     * @param size    размер страницы
     *
     * @return список пользователей
     */
    @GetMapping("")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Collection<? extends DTO>> get(@RequestParam(required = false) Collection<String> filters,
                                                      @RequestParam(required = false) Collection<String> searchs,
                                                      @RequestParam(required = false) Collection<String> sorts,
                                                      @RequestParam(required = false, defaultValue = "ROW") Projection projection,
                                                      @RequestParam(required = false) Integer page,
                                                      @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(User.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .withPaging(page, size)
                                         .build();

        return mapper.getPageMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<User>>instanceOf(FIND_ALL)
                                    .execute(from(query)),
                            projection);
    }

    /**
     * <h1>Получить пользователя</h1>
     *
     * @param userId идентификатор пользователя
     *
     * @return информация о пользователе
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID userId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, User>instanceOf(FIND_ONE)
                                    .execute(from(userId)),
                            FULL);
    }

    /**
     * <h1>Редактировать пользователя</h1>
     *
     * @param userId  идентификатор пользователя
     * @param request форма редактирования пользователя
     *
     * @return информация об отредактированном пользователе
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<? extends DTO> update(@PathVariable("id") UUID userId,
                                             @RequestBody @Valid ApiRequest<UserForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateUserArgument, User>instanceOf(UPDATE)
                                    .execute(from(userId, request.getForm())),
                            FULL);
    }

    /**
     * <h1>Удалить пользователя</h1>
     *
     * @param userId идентификатор пользователя
     *
     * @return уведомления
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> delete(@PathVariable("id") UUID userId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE)
                                    .execute(from(userId)));
    }

    // endregion

    // region Business

    /**
     * <h1>Удалить пользователя логически</h1>
     *
     * @param userId идентификатор пользователя
     *
     * @return уведомление
     */
    @PutMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> deleteLogical(@PathVariable("id") UUID userId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE_LOGICAL)
                                    .execute(from(userId)));
    }

    /**
     * <h1>Восстановить пользователя</h1>
     *
     * @param userId идентификатор пользователя
     *
     * @return уведомление
     */
    @PutMapping("/{id}/recovery")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> recovery(@PathVariable("id") UUID userId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(RECOVERY)
                                    .execute(from(userId)));
    }

    /**
     * <h1>Активировать пользователя</h1>
     *
     * @param userId идентификатор пользователя
     *
     * @return уведомление
     */
    @PutMapping("/{id}/enable")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> enable(@PathVariable("id") UUID userId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(ENABLE)
                                    .execute(from(userId)));
    }

    /**
     * <h1>Заблокировать пользователя</h1>
     *
     * @param userId идентификатор пользователя
     *
     * @return уведомление
     */
    @PutMapping("/{id}/disable")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Void> disable(@PathVariable("id") UUID userId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DISABLE)
                                    .execute(from(userId)));
    }

    /**
     * <h1>Проверка на существования пользователя с заданными параметрами</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     *
     * @return true, если существует, false, иначе
     */
    @GetMapping("/exists")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ApiResponse<Boolean> exists(@RequestParam(required = false) Collection<String> filters,
                                       @RequestParam(required = false) Collection<String> searchs) {

        DynamicQuery.Builder builder = DynamicQuery.newBuilder(User.class)
                                                   .withFilters(filters)
                                                   .withSearchs(searchs);

        return Mapper.getPrimitiveMapper(Boolean.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Boolean>instanceOf(EXISTS)
                                    .execute(from(builder.build())));
    }

    /**
     * Сброс пароля по логину
     *
     * @param request форма сброса пароля
     *
     * @return уведомление
     */
    @PutMapping("/password/reset")
    public ApiResponse<Void> resetPassword(@RequestBody @Valid ApiRequest<ResetPasswordForm> request) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<ResetPasswordArgument, Void>instanceOf(RESET_PASSWORD)
                                    .execute(from(request.getForm())));
    }

    // endregion

}
