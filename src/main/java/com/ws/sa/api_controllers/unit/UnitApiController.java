package com.ws.sa.api_controllers.unit;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.GetFileArgument;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.unit.UnitActions;
import com.ws.sa.actions.unit.arguments.*;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.base.TreeNode;
import com.ws.sa.dtos.unit.ImportDTO;
import com.ws.sa.dtos.unit.UnitNodeDTO;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.forms.unit.DeleteUnitForm;
import com.ws.sa.forms.unit.ImportUnitsForm;
import com.ws.sa.forms.unit.UnitForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.GetFileArgument.from;
import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.unit.UnitActions.*;
import static com.ws.sa.actions.unit.arguments.CreateCollectionUnitArgument.from;
import static com.ws.sa.actions.unit.arguments.CreateUnitArgument.from;
import static com.ws.sa.actions.unit.arguments.DeleteUnitArgument.from;
import static com.ws.sa.actions.unit.arguments.ImportFromCSVUnitArgument.from;
import static com.ws.sa.actions.unit.arguments.ImportUnitsArgument.from;
import static com.ws.sa.actions.unit.arguments.UpdateUnitArgument.from;
import static com.ws.sa.enums.Projection.FULL;
import static com.ws.sa.enums.ResourceType.IMPORT;
import static com.ws.sa.utils.FileUtils.fillResponse;

/**
 * Created by Vladislav Shelengovskiy on 03.02.2017.
 */
@RestController
@RequestMapping("/api/units")
public class UnitApiController {

    private final UnitMapper mapper;
    private final ActionAllocator<UnitActions> allocator;

    @Autowired
    public UnitApiController(UnitMapper mapper,
                             @Qualifier("unitActionAllocator") ActionAllocator<UnitActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    @GetMapping("/tree")
    public ApiResponse<Collection<TreeNode<UnitNodeDTO, UnitNodeDTO>>> getTree(@RequestParam(required = false) Collection<String> filters) {
        DynamicQuery query = DynamicQuery.newBuilder(Unit.class)
                                         .withFilters(filters)
                                         .build();

        return mapper.getTreeMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<Unit>>instanceOf(FIND_ALL)
                                    .execute(from(query))
                                    .getContent());
    }

    @PostMapping("")
    @PreAuthorize("@unitAccess.accessCreate()")
    public ApiResponse<? extends DTO> create(@RequestBody @Valid ApiRequest<UnitForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateUnitArgument, Unit>instanceOf(CREATE)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    @PostMapping("/list")
    @PreAuthorize("@unitAccess.accessCreate()")
    public ApiResponse<Collection<? extends DTO>> createList(@RequestBody @Valid ApiRequest<Collection<UnitForm>> request) {
        return mapper.getCollectionMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateCollectionUnitArgument, Collection<Unit>>instanceOf(CREATE_COLLECTION)
                                    .execute(from(request.getForm())),
                            FULL);
    }

    @GetMapping("/{id}")
    @PreAuthorize("@unitAccess.accessRead(#unitId)")
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID unitId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Unit>instanceOf(FIND_ONE)
                                    .execute(from(unitId)),
                            FULL);
    }

    @PutMapping("/{id}")
    @PreAuthorize("@unitAccess.accessUpdate(#unitId)")
    public ApiResponse<? extends DTO> update(@PathVariable("id") UUID unitId,
                                             @RequestBody @Valid ApiRequest<UnitForm> request) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateUnitArgument, Unit>instanceOf(UPDATE)
                                    .execute(from(unitId, request.getForm())),
                            FULL);
    }

    @PutMapping("/{id}/delete")
    @PreAuthorize("@unitAccess.accessDelete(#unitId)")
    public ApiResponse<Void> delete(@PathVariable("id") UUID unitId,
                                    @RequestBody @Valid ApiRequest<DeleteUnitForm> request) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<DeleteUnitArgument, Void>instanceOf(DELETE)
                                    .execute(from(unitId, request.getForm())));
    }

    /**
     * <h1>Проверка на существования организации с заданными параметрами</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     *
     * @return true, если существует, false, иначе
     */
    @GetMapping("/exists")
    public ApiResponse<Boolean> exists(@RequestParam(required = false) Collection<String> filters,
                                       @RequestParam(required = false) Collection<String> searchs) {

        DynamicQuery query = DynamicQuery.newBuilder(Unit.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .build();

        return Mapper.getPrimitiveMapper(Boolean.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Boolean>instanceOf(EXISTS)
                                    .execute(from(query)));
    }

    @PostMapping("/import")
    @PreAuthorize("@unitAccess.accessCreate()")
    public ApiResponse<Collection<? extends DTO>> importFromCSV(@RequestParam("file") MultipartFile file,
                                                                @RequestParam(value = "id", required = false) UUID id) {
        return mapper.getCollectionMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<ImportFromCSVUnitArgument, Collection<Unit>>instanceOf(IMPORT_FROM_CSV)
                                    .execute(from(id, file)),
                            FULL);
    }

    @PostMapping("/import/save")
    @PreAuthorize("@unitAccess.accessCreate()")
    public ApiResponse<ImportDTO> saveImport(@RequestBody @Valid ApiRequest<ImportUnitsForm> request) {
        return Mapper.getPrimitiveMapper(ImportDTO.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<ImportUnitsArgument, ImportDTO>instanceOf(IMPORT_UNITS)
                                    .execute(from(request.getForm())));
    }

    @GetMapping("/import/sample")
    @PreAuthorize("@unitAccess.accessCreate()")
    public void downloadSampleCSV(HttpServletResponse servletResponse) throws Exception {
        fillResponse(allocator.<GetFileArgument, File>instanceOf(DOWNLOAD_SAMPLE_CSV)
                             .execute(from("UnitImportSample.csv", IMPORT)),
                     "application/csv",
                     servletResponse);
    }

}
