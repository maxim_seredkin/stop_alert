package com.ws.sa.api_controllers.statistic;

import com.ws.sa.beans.ApiResponse;
import com.ws.sa.services.statistic.StatisticService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Vladislav Shelengovskiy on 29.03.2017.
 */
@RestController
@RequestMapping("/api/stats")
public class StatApiController {

    private final StatisticService service;

    public StatApiController(StatisticService service){
        this.service = service;
    }

    @RequestMapping("/mails")
    public ApiResponse getMailStat() {
        return ApiResponse.ok(service.getMailStat());
    }
}
