package com.ws.sa.api_controllers.authority;

import com.ws.sa.beans.ApiResponse;
import com.ws.sa.repositories.authority.AuthorityRepository;
import com.ws.sa.mappers.authority.AuthorityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Vladislav Shelengovskiy on 02.02.2017.
 */
@RestController
@RequestMapping("/api")
public class AuthorityApiController {

    private final AuthorityRepository repository;
    private final AuthorityMapper mapper;

    @Autowired
    public AuthorityApiController(AuthorityRepository repository, AuthorityMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/authorities")
    public
    @ResponseBody
    ApiResponse<?> get() {
        return ApiResponse.ok(mapper.toFulls(repository.findAll()));
    }

}
