package com.ws.sa.api_controllers.mock_page;

import com.ws.sa.actions.base.ActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.mock_page.MockPageActions;
import com.ws.sa.actions.mock_page.arguments.CreateMockPageArgument;
import com.ws.sa.actions.mock_page.arguments.UpdateMockPageArgument;
import com.ws.sa.beans.ApiRequest;
import com.ws.sa.beans.ApiResponse;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.enums.Projection;
import com.ws.sa.forms.mock_page.MockPageForm;
import com.ws.sa.mappers.base.Mapper;
import com.ws.sa.mappers.mock_page.MockPageMapper;
import com.ws.sa.queries.DynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.actions.base.arguments.QueryArgument.from;
import static com.ws.sa.actions.base.arguments.UuidArgument.from;
import static com.ws.sa.actions.mock_page.MockPageActions.*;
import static com.ws.sa.actions.mock_page.arguments.CreateMockPageArgument.from;
import static com.ws.sa.actions.mock_page.arguments.UpdateMockPageArgument.from;
import static com.ws.sa.enums.Projection.FULL;
import static com.ws.sa.enums.Projection.ROW;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@RestController
@RequestMapping("/api/mock-pages")
public class MockPageApiController {

    private final MockPageMapper mapper;
    private final ActionAllocator<MockPageActions> allocator;

    @Autowired
    public MockPageApiController(MockPageMapper mapper,
                                 @Qualifier("mockPageActionAllocator") ActionAllocator<MockPageActions> allocator) {
        this.mapper = mapper;
        this.allocator = allocator;
    }

    /**
     * <h1>Создать новую страницу-заглушку</h1>
     *
     * @param request форма создания страницы-заглушки
     *
     * @return информация о созданной странице-заглушке
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.POST, value = "", consumes = {"multipart/form-data"})
    public
    @ResponseBody
    ApiResponse<? extends DTO> create(@RequestPart("payload") @Valid ApiRequest<MockPageForm> request,
                                      @Valid @NotNull @RequestPart("file") MultipartFile file) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<CreateMockPageArgument, MockPage>instanceOf(CREATE)
                                    .execute(from(request.getForm(), file)),
                            FULL);
    }

    /**
     * <h1>Получить список страниц-заглушек/h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     * @param sorts   сортировка
     * @param page    страница
     * @param size    размер страницы
     *
     * @return список страниц-заглушек
     */
    @RequestMapping(method = RequestMethod.GET, value = "")
    @ResponseBody
    public ApiResponse<Collection<? extends DTO>> get(@RequestParam(required = false) Collection<String> filters,
                                                      @RequestParam(required = false) Collection<String> searchs,
                                                      @RequestParam(required = false) Collection<String> sorts,
                                                      @RequestParam(required = false, defaultValue = "ROW") Projection projection,
                                                      @RequestParam(required = false) Integer page,
                                                      @RequestParam(required = false) Integer size) {

        DynamicQuery query = DynamicQuery.newBuilder(MockPage.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .withSorting(sorts)
                                         .withPaging(page, size)
                                         .build();

        return mapper.getPageMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Page<MockPage>>instanceOf(FIND_ALL)
                                    .execute(from(query)),
                            projection);
    }

    /**
     * <h1>Получить страницу-заглушку</h1>
     *
     * @param mockPageId идентификатор страницы-заглушки
     *
     * @return информация о странице-заглушке
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    @ResponseBody
    public ApiResponse<? extends DTO> get(@PathVariable("id") UUID mockPageId) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, MockPage>instanceOf(FIND_ONE)
                                    .execute(from(mockPageId)),
                            FULL);
    }

    /**
     * <h1>Редактировать страницу-заглушку</h1>
     *
     * @param mockPageId идентификатор страницы-заглушки
     * @param request    форма редактирования страницы-заглушки
     *
     * @return информация об отредактированной странице-заглушке
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}", consumes = {"multipart/form-data"})
    public
    @ResponseBody
    ApiResponse<? extends DTO> update(@PathVariable("id") UUID mockPageId, @RequestPart("payload") @Valid ApiRequest<MockPageForm> request,
                                      @RequestPart(value = "file", required = false) MultipartFile file) {
        return mapper.getSimpleMapper()
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UpdateMockPageArgument, MockPage>instanceOf(UPDATE)
                                    .execute(from(mockPageId, request.getForm(), file)),
                            FULL);
    }

    /**
     * <h1>Удалить страницу-заглушку</h1>
     *
     * @param mockPageId идентификатор страницы-заглушки
     *
     * @return уведомления
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public
    @ResponseBody
    ApiResponse<Void> delete(@PathVariable("id") UUID mockPageId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE)
                                    .execute(from(mockPageId)));
    }

    /**
     * <h1>Удалить шаблон страницы-заглушки</h1>
     *
     * @param mockPageId идентификатор страницы-заглушки
     *
     * @return уведомление
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/delete")
    public
    @ResponseBody
    ApiResponse<Void> deleteLogical(@PathVariable("id") UUID mockPageId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(DELETE_LOGICAL)
                                    .execute(from(mockPageId)));
    }

    /**
     * <h1>Восстановить страницу-заглушку</h1>
     *
     * @param mockPageId идентификатор страницы-заглушки
     *
     * @return уведомление
     */
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/recovery")
    public
    @ResponseBody
    ApiResponse<Void> recovery(@PathVariable("id") UUID mockPageId) {
        return Mapper.getPrimitiveMapper(Void.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<UuidArgument, Void>instanceOf(RECOVERY)
                                    .execute(from(mockPageId)));
    }

    /**
     * <h1>Проверка на существование страницы-заглушки с заданными параметрами</h1>
     *
     * @param filters фильтра
     * @param searchs поиск
     *
     * @return true, если существует, false, иначе
     */
    @RequestMapping(method = RequestMethod.GET, value = "/exists")
    @ResponseBody
    public ApiResponse<Boolean> exists(@RequestParam(required = false) Collection<String> filters,
                                       @RequestParam(required = false) Collection<String> searchs) {

        DynamicQuery query = DynamicQuery.newBuilder(MockPage.class)
                                         .withFilters(filters)
                                         .withSearchs(searchs)
                                         .build();

        return Mapper.getPrimitiveMapper(Boolean.class)
                     .andThen(ApiResponse::ok)
                     .apply(allocator.<QueryArgument, Boolean>instanceOf(EXISTS)
                                    .execute(from(query)));
    }

}
