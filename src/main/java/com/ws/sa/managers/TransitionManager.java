package com.ws.sa.managers;

import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.transition.Transition;
import com.ws.sa.listeners.transition.events.TransitionEvent;
import com.ws.sa.services.mail.MailService;
import com.ws.sa.services.transition.TransitionService;
import com.ws.sa.utils.CalendarUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @author Maxim Seredkin
 * @since 27.02.2017
 */
@Component
public class TransitionManager {

    private final TransitionService transitionService;
    private final MailService mailService;

    @Autowired
    public TransitionManager(TransitionService transitionService, MailService mailService) {
        this.transitionService = transitionService;
        this.mailService = mailService;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void transition(TransitionEvent event) {
        Mail mail = mailService.findOne(event.getMail());

        if (Objects.isNull(mail) || !CalendarUtil.isNoneDateExpired(mail.getTask().getEndDate()))
            return;

        Transition transition = new Transition();
        transition.setBrowser(event.getBrowser());
        transition.setIp(event.getIp());
        transition.setAdditionalInfo(event.getAdditionalInfo());
        transition.setOs(event.getOs());
        transition.setTransitionDate(event.getTransitionDate());
        transition.setMail(mail);

        // добавляем переход
        mail.addCount();

        transitionService.save(transition);
    }
}
