package com.ws.sa.managers;

import com.ws.sa.entities.task.Task;
import com.ws.sa.executors.TaskExecutor;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.tasks.WaitCompleteTask;
import com.ws.sa.tasks.WaitExecutionTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;

/**
 * @author Maxim Seredkin
 * @since 27.02.2017
 */
@Component
public class TaskManager {

    private final TaskScheduler scheduler;
    private final TaskService taskService;

    private final Map<UUID, ScheduledFuture> tasksWaitingExecution = new HashMap<>();
    private final Map<UUID, ScheduledFuture> tasksWaitingComplete = new HashMap<>();

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    public TaskManager(TaskScheduler scheduler, TaskService taskService) {
        this.scheduler = scheduler;
        this.taskService = taskService;
    }

    public void start(Task task) {
        startTask(task);
    }

    public void pause(Task task) {
        stopTask(task.getId());
    }

    public void proceed(Task task) {
        startTask(task);
    }

    public void resume(Task task) {
        startTask(task);
    }

    public void update(Task task) {
        stopTask(task.getId());
        startTask(task);
    }

    public void cancel(Task task) {
        stopTask(task.getId());
    }

    private void startTask(Task task) {
        tasksWaitingExecution.put(task.getId(), scheduler.schedule(new WaitExecutionTask(task, taskExecutor), task.getStartDate()));
    }

    private void stopTask(UUID taskId) {
        stopTasks(taskId, tasksWaitingExecution);
        stopTasks(taskId, tasksWaitingComplete);
    }

    private void stopTasks(UUID taskId, Map<UUID, ScheduledFuture> tasks) {
        Optional.ofNullable(tasks.get(taskId)).ifPresent(task -> {
            task.cancel(true);
            tasks.remove(taskId);
        });
    }

    public void addToWaitingCompleteQueue(Task task) {
        tasksWaitingExecution.put(task.getId(), scheduler.schedule(new WaitCompleteTask(task, taskService), task.getEndDate()));
    }

    public void execute(Task task) {
        taskExecutor.run(task);
    }
}
