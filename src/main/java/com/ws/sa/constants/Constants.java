package com.ws.sa.constants;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public class Constants {

    public static final String CHECK_PASS_REQUIRED_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,64}$";

    public static final int MAX_ATTEMPT = 10;

    public static final int BLOCKING_TIME_IN_MINUTS = 15;

}
