package com.ws.sa.mappers.unit;

import com.ws.sa.dtos.unit.*;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.forms.unit.UnitForm;
import com.ws.sa.mappers.base.BaseAccessEntityMapper;
import com.ws.sa.mappers.base.HierarchyMapper;
import com.ws.sa.mappers.user.UserMapper;
import com.ws.sa.security.base.Access;
import com.ws.sa.services.unit.UnitService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import static java.util.Objects.isNull;

/**
 * Created by Vladislav Shelengovksiy on 31.01.2017.
 */
@Mapper(uses = {UserMapper.class})
public abstract class UnitMapper extends BaseAccessEntityMapper<Unit, UnitLightDTO, UnitRowDTO, UnitFullDTO>
        implements HierarchyMapper<Unit, UnitNodeDTO, UnitNodeDTO> {

    @Autowired
    private UnitService unitService;

    @Override
    @Autowired
    public void setAccess(@Qualifier("unitAccess") Access access) {
        super.setAccess(access);
    }

    @Override
    @Mappings({@Mapping(target = "path", ignore = true),
               @Mapping(target = "childs", source = "enabledChilds")})
    public abstract UnitFullDTO toFull(Unit entity);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "parent", ignore = true),
               @Mapping(target = "status", ignore = true),
               @Mapping(target = "employees", ignore = true),
               @Mapping(target = "users", ignore = true),
               @Mapping(target = "path", ignore = true),
               @Mapping(target = "managers", ignore = true),
               @Mapping(target = "officers", ignore = true),
               @Mapping(target = "childs", ignore = true)})
    public abstract Unit toEntity(UnitForm form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "parent", ignore = true),
               @Mapping(target = "status", ignore = true),
               @Mapping(target = "employees", ignore = true),
               @Mapping(target = "users", ignore = true),
               @Mapping(target = "path", ignore = true),
               @Mapping(target = "managers", ignore = true),
               @Mapping(target = "officers", ignore = true),
               @Mapping(target = "childs", ignore = true)})
    public abstract void updateEntity(UnitForm form, @MappingTarget Unit entity);

    @Mappings({@Mapping(target = "errors", ignore = true),})
    public abstract UnitImportDTO toImport(Unit entity);

    @Override
    @AfterMapping
    public void toFull(Unit entity, @MappingTarget UnitFullDTO dto) {
        super.toFull(entity, dto);
        dto.setPath(this.toLights(unitService.getUnitPath(entity.getPath())));
    }

    @AfterMapping
    public void afterToNode(Unit entity, @MappingTarget UnitNodeDTO dto) {
        if (isNull(dto))
            return;

        dto.setCountEmployees(unitService.countBranchEmployees(entity.getId()));
    }

    @AfterMapping
    public void afterToFull(Unit entity, @MappingTarget UnitFullDTO dto) {
        if (isNull(dto))
            return;

        dto.setCountEmployees(unitService.countBranchEmployees(entity.getId()));
    }

}