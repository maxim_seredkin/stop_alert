package com.ws.sa.mappers.mock_page;


import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.mappers.mail_template_group.MailTemplateGroupMapper;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.dtos.mock_page.MockPageFullDTO;
import com.ws.sa.dtos.mock_page.MockPageLightDTO;
import com.ws.sa.dtos.mock_page.MockPageRowDTO;
import com.ws.sa.forms.mock_page.MockPageForm;
import com.ws.sa.configs.AppConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@Mapper(uses = {MailTemplateGroupMapper.class})
public abstract class MockPageMapper extends BaseEntityMapper<MockPage, MockPageLightDTO, MockPageRowDTO, MockPageFullDTO> {

    @Autowired
    protected AppConfig appConfig;

    @Override
    @Mappings({@Mapping(target = "redirectUri", expression = "java( appConfig.getExternalAddress().concat(entity.getRedirectUri()) )")})
    public abstract MockPageRowDTO toRow(MockPage entity);

    @Override
    @Mappings({@Mapping(target = "redirectUri", expression = "java( appConfig.getExternalAddress().concat(entity.getRedirectUri()) )")})
    public abstract MockPageFullDTO toFull(MockPage entity);

    @Override
    @Mappings({@Mapping(target = "redirectUri", expression = "java( appConfig.getExternalAddress().concat(entity.getRedirectUri()) )")})
    public abstract MockPageLightDTO toLight(MockPage entity);

    public abstract Collection<MockPage> toEntities(Collection<MockPageForm> form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "path", ignore = true),
               @Mapping(target = "redirectUri", ignore = true),})
    public abstract MockPage toEntity(MockPageForm form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "path", ignore = true),
               @Mapping(target = "redirectUri", ignore = true),})
    public abstract void updateEntity(MockPageForm form, @MappingTarget MockPage entity);
}
