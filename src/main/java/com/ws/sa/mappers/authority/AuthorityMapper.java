package com.ws.sa.mappers.authority;

import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.entities.authority.Authority;
import com.ws.sa.dtos.authority.AuthorityFullDTO;
import com.ws.sa.dtos.authority.AuthorityLightDTO;
import com.ws.sa.dtos.authority.AuthorityRowDTO;
import org.mapstruct.Mapper;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@Mapper(uses = {GrantedAuthoritiesMapper.class})
public abstract class AuthorityMapper extends BaseEntityMapper<Authority, AuthorityLightDTO, AuthorityRowDTO, AuthorityFullDTO> {
}
