package com.ws.sa.mappers.mail_template;


import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.dtos.mail_template.MailTemplateFullDTO;
import com.ws.sa.dtos.mail_template.MailTemplateLightDTO;
import com.ws.sa.dtos.mail_template.MailTemplateRowDTO;
import com.ws.sa.forms.mail_template.MailTemplateForm;
import com.ws.sa.mappers.mail_template_group.MailTemplateGroupMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@Mapper(uses = {MailTemplateGroupMapper.class})
public abstract class MailTemplateMapper extends BaseEntityMapper<MailTemplate, MailTemplateLightDTO, MailTemplateRowDTO, MailTemplateFullDTO> {

    @Override
    @Mappings({})
    public abstract MailTemplateRowDTO toRow(MailTemplate entity);

    @Override
    @Mappings({})
    public abstract MailTemplateFullDTO toFull(MailTemplate entity);

    public abstract Collection<MailTemplate> toEntities(Collection<MailTemplateForm> form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "group", ignore = true),
               @Mapping(target = "path", ignore = true),})
    public abstract MailTemplate toEntity(MailTemplateForm form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "group", ignore = true),
               @Mapping(target = "path", ignore = true),})
    public abstract void updateEntity(MailTemplateForm form, @MappingTarget MailTemplate entity);
}
