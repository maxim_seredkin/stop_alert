package com.ws.sa.mappers.mail_template_group;


import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.dtos.mail_template_group.MailTemplateGroupFullDTO;
import com.ws.sa.dtos.mail_template_group.MailTemplateGroupLightDTO;
import com.ws.sa.dtos.mail_template_group.MailTemplateGroupRowDTO;
import com.ws.sa.forms.mail_template_group.MailTemplateGroupForm;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
@Mapper
public abstract class MailTemplateGroupMapper extends BaseEntityMapper<MailTemplateGroup, MailTemplateGroupLightDTO, MailTemplateGroupRowDTO, MailTemplateGroupFullDTO> {

    public abstract Collection<MailTemplateGroup> toEntities(Collection<MailTemplateGroupForm> form);

    @Mappings({@Mapping(target = "id", ignore = true),})
    public abstract MailTemplateGroup toEntity(MailTemplateGroupForm form);

    @Mappings({@Mapping(target = "id", ignore = true),})
    public abstract void updateEntity(MailTemplateGroupForm form, @MappingTarget MailTemplateGroup entity);

}
