package com.ws.sa.mappers.employee_tag;

import com.ws.sa.dtos.employee_tag.EmployeeTagFullDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagNodeDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagRowDTO;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.forms.tag.employee.EmployeeTagForm;
import com.ws.sa.mappers.base.BaseAccessEntityMapper;
import com.ws.sa.mappers.base.HierarchyMapper;
import com.ws.sa.security.base.Access;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Mapper
public abstract class EmployeeTagMapper extends BaseAccessEntityMapper<EmployeeTag, EmployeeTagLightDTO, EmployeeTagRowDTO, EmployeeTagFullDTO>
        implements HierarchyMapper<EmployeeTag, EmployeeTagNodeDTO, EmployeeTagNodeDTO> {

    @Autowired
    private EmployeeTagService service;

    @Override
    @Autowired
    public void setAccess(@Qualifier("employeeTagAccess") Access access) {
        super.setAccess(access);
    }

    @Override
    @Mappings({@Mapping(target = "path", ignore = true),})
    public abstract EmployeeTagFullDTO toFull(EmployeeTag entity);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "parent", ignore = true),
               @Mapping(target = "status", ignore = true),
               @Mapping(target = "path", ignore = true),
               @Mapping(target = "employees", ignore = true),})
    public abstract EmployeeTag toEntity(EmployeeTagForm form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "parent", ignore = true),
               @Mapping(target = "status", ignore = true),
               @Mapping(target = "path", ignore = true),
               @Mapping(target = "employees", ignore = true),})
    public abstract void updateEntity(EmployeeTagForm form, @MappingTarget EmployeeTag entity);

    @Override
    @AfterMapping
    public void toFull(EmployeeTag entity, @MappingTarget EmployeeTagFullDTO dto) {
        dto.setPath(this.toLights(service.getEmployeeTagPath(entity.getPath())));
    }

}
