package com.ws.sa.mappers.employee;

import com.ws.sa.dtos.employee.EmployeeFullDTO;
import com.ws.sa.dtos.employee.EmployeeLightDTO;
import com.ws.sa.dtos.employee.EmployeeRowDTO;
import com.ws.sa.dtos.employee.EmployeeSelectorDTO;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.forms.employee.EmployeeForm;
import com.ws.sa.mappers.base.BaseAccessSelectableEntityMapper;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.security.base.Access;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Mapper(uses = {UnitMapper.class, EmployeeTagMapper.class})
public abstract class EmployeeMapper extends BaseAccessSelectableEntityMapper<Employee, EmployeeLightDTO, EmployeeRowDTO, EmployeeFullDTO, EmployeeSelectorDTO> {

    @Override
    @Autowired
    public void setAccess(@Qualifier("employeeAccess") Access access) {
        super.setAccess(access);
    }

    public abstract Collection<Employee> toEntities(Collection<EmployeeForm> form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "unit", ignore = true),
               @Mapping(target = "employeeTags", ignore = true),
               @Mapping(target = "status", ignore = true)})
    public abstract Employee toEntity(EmployeeForm form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "unit", ignore = true),
               @Mapping(target = "employeeTags", ignore = true),
               @Mapping(target = "status", ignore = true)})
    public abstract Employee updateEntity(EmployeeForm form, @MappingTarget Employee entity);

}
