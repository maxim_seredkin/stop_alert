package com.ws.sa.mappers.base;

/**
 * Created by Seredkin M. on 07.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public interface Enum {

    String name();

    String getDisplayName();

}
