package com.ws.sa.mappers.base;

import com.ws.sa.enums.Projection;
import com.ws.sa.dtos.base.DTO;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by Seredkin M. on 11.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public interface Mapper<FromT> {

    DTO toDTO(FromT entity, final Projection projection);

    Collection<? extends DTO> toDTOs(Collection<FromT> entities, final Projection projection);

    Page<? extends DTO> toPage(Page<FromT> page, Projection projection);

    /**
     *
     * @return функця маппинга (Page:FromT, Projection) -> Page:(? extends DTO)
     */
    default BiFunction<Page<FromT>, Projection, Page<? extends DTO>> getPageMapper() {
        return this::toPage;
    }

    /**
     *
     * @return функця маппинга (Collection:FromT, Projection) -> Collection:(? extends DTO)
     */
    default BiFunction<Collection<FromT>, Projection, Collection<? extends DTO>> getCollectionMapper() {
        return this::toDTOs;
    }

    /**
     *
     * @return функця маппинга (Page:FromT, Projection) -> DTO
     */
    default BiFunction<FromT, Projection, DTO> getSimpleMapper() {
        return this::toDTO;
    }

    /**
     * Маппер для маппинга простых сущностей, чтобы можно было успользовать единую систему маппинга в контроллерах
     *
     * @param clazz   класс
     * @param <FromT> преобразуемый класс
     *
     * @return функция маппинга FromT -> FromT
     */
    static <FromT> Function<FromT, FromT> getPrimitiveMapper(Class<FromT> clazz) {
        return from -> from;
    }

}
