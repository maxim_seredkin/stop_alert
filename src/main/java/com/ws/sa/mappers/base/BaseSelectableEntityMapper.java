package com.ws.sa.mappers.base;

import com.ws.sa.enums.Projection;
import com.ws.sa.exceptions.ApiException;
import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.base.DTO;

import java.util.Collection;

import static com.ws.sa.errors.ApiStatusCode.ILLEGAL_PROJECTION;

/**
 * @author Maxim Seredkin
 */
public abstract class BaseSelectableEntityMapper<EntityT extends BaseEntity, LightT extends BaseDTO, RowT extends BaseDTO, FullT extends BaseDTO, SelectT extends BaseDTO>
        extends BaseEntityMapper<EntityT, LightT, RowT, FullT> {

    protected abstract SelectT toSelector(EntityT entity);

    protected abstract Collection<SelectT> toSelectors(Collection<EntityT> entities);

    @Override
    public DTO toDTO(EntityT entity, Projection projection) {
        switch (projection) {
            case LIGHT:
                return toLight(entity);
            case ROW:
                return toRow(entity);
            case FULL:
                return toFull(entity);
            case SELECTOR:
                return toSelector(entity);
            default:
                throw new ApiException(ILLEGAL_PROJECTION);
        }
    }

    @Override
    public Collection<? extends DTO> toDTOs(Collection<EntityT> entities, Projection projection) {
        switch (projection) {
            case LIGHT:
                return toLights(entities);
            case ROW:
                return toRows(entities);
            case FULL:
                return toFulls(entities);
            case SELECTOR:
                return toSelectors(entities);
            default:
                throw new ApiException(ILLEGAL_PROJECTION);
        }
    }

}
