package com.ws.sa.mappers.base;

import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.enums.Operation;
import com.ws.sa.security.base.Access;
import org.mapstruct.AfterMapping;
import org.mapstruct.MappingTarget;

import java.util.Objects;

import static com.ws.sa.enums.Operation.*;

/**
 * Created by Vladislav Shelengovskiy on 28.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public abstract class BaseAccessSimpleMapper<EntityT extends BaseEntity, FullT extends AccessDTO>
        extends BaseSimpleMapper<EntityT, FullT> {

    protected Access access;

    public void setAccess(Access access) {
        this.access = access;
    }

    @AfterMapping
    public void toFull(EntityT entity, @MappingTarget FullT dto) {
        this.setPermissions(dto, READ, UPDATE, DELETE);
    }

    /**
     * Задать разрешения
     */
    private void setPermissions(AccessDTO accessDTO, Operation... operations) {
        if (Objects.isNull(accessDTO)) return;

        for (Operation operation : operations)
            if (access.access(accessDTO.getId(), operation))
                accessDTO.getOperations().add(operation);
    }

}
