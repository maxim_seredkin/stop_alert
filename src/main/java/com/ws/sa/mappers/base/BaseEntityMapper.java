package com.ws.sa.mappers.base;

import com.ws.sa.enums.Projection;
import com.ws.sa.exceptions.ApiException;
import com.ws.sa.entities.base.Entity;
import com.ws.sa.dtos.base.DTO;

import java.util.Collection;

import static com.ws.sa.errors.ApiStatusCode.ILLEGAL_PROJECTION;

/**
 * @author Maxim Seredkin
 */
public abstract class BaseEntityMapper<EntityT extends Entity, LightT extends DTO, RowT extends DTO, FullT extends DTO>
        extends BaseSimpleMapper<EntityT, FullT> {

    public abstract Collection<LightT> toLights(Collection<EntityT> entities);

    public abstract Collection<RowT> toRows(Collection<EntityT> entities);

    public abstract LightT toLight(EntityT entity);

    public abstract RowT toRow(EntityT entity);

    @Override
    public DTO toDTO(EntityT entity, final Projection projection) {
        switch (projection) {
            case LIGHT:
                return toLight(entity);
            case ROW:
                return toRow(entity);
            case FULL:
                return toFull(entity);
            default:
                throw new ApiException(ILLEGAL_PROJECTION);
        }
    }

    @Override
    public Collection<? extends DTO> toDTOs(Collection<EntityT> entities, final Projection projection) {
        switch (projection) {
            case LIGHT:
                return toLights(entities);
            case ROW:
                return toRows(entities);
            case FULL:
                return toFulls(entities);
            default:
                throw new ApiException(ILLEGAL_PROJECTION);
        }
    }
}
