package com.ws.sa.mappers.base;

import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.base.TreeNode;
import com.ws.sa.entities.base.HierarchyDTO;
import com.ws.sa.utils.TreeUtils;

import java.util.Collection;
import java.util.function.Function;

/**
 * Created by Seredkin M. on 11.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public interface HierarchyMapper<FromT, ToT extends HierarchyDTO<ParentT>, ParentT extends BaseDTO> extends Mapper<FromT> {

    ToT toNode(FromT entity);

    Collection<ToT> toTree(Collection<FromT> entities);

    /**
     * @return функця маппинга (Page:FromT, Projection) -> Page:(? extends DTO)
     */
    default Function<Collection<FromT>, Collection<TreeNode<ToT, ParentT>>> getTreeMapper() {
        return entities -> TreeUtils.buildTree(toTree(entities));
    }

}
