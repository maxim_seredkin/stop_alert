package com.ws.sa.mappers.base;

import com.ws.sa.dtos.base.EnumDTO;
import org.mapstruct.Mapper;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

/**
 * Created by Seredkin M. on 07.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Mapper
public abstract class EnumMapper<EnumT extends Enum> {

    public EnumDTO toDTO(EnumT en) {
        if (isNull(en))
            return null;

        return new EnumDTO(en.name(), en.getDisplayName());
    }

    public List<EnumDTO> toDTOs(EnumT... ens) {
        if (isNull(ens))
            return null;

        return stream(ens).map(this::toDTO)
                          .collect(toList());
    }

}
