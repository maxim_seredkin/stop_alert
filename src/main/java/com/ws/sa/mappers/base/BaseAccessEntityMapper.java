package com.ws.sa.mappers.base;

import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.enums.Operation;
import com.ws.sa.security.base.Access;
import org.mapstruct.AfterMapping;
import org.mapstruct.MappingTarget;

import java.util.Objects;

import static com.ws.sa.enums.Operation.*;

/**
 * Created by Vladislav Shelengovskiy on 28.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public abstract class BaseAccessEntityMapper<EType extends BaseEntity, LType extends AccessDTO, RType extends AccessDTO, FType extends AccessDTO>
        extends BaseEntityMapper<EType, LType, RType, FType> {

    protected Access access;

    public void setAccess(Access access) {
        this.access = access;
    }

    @AfterMapping
    public void toLight(EType entity, @MappingTarget LType dto) {
        this.setPermissions(dto, READ);
    }

    @AfterMapping
    public void toRow(EType entity, @MappingTarget RType dto) {
        this.setPermissions(dto, UPDATE, DELETE);
    }

    @AfterMapping
    public void toFull(EType entity, @MappingTarget FType dto) {
        this.setPermissions(dto, READ, UPDATE, DELETE);
    }

    /**
     * Задать разрешения
     */
    private void setPermissions(AccessDTO accessDTO, Operation... operations) {
        if (Objects.isNull(accessDTO)) return;

        for (Operation operation : operations)
            if (access.access(accessDTO.getId(), operation))
                accessDTO.getOperations().add(operation);
    }

}
