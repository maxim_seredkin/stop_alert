package com.ws.sa.mappers.base;

import com.google.common.collect.Lists;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.enums.Projection;
import com.ws.sa.exceptions.ApiException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Collection;

import static com.ws.sa.errors.ApiStatusCode.ILLEGAL_PROJECTION;

/**
 * Created by Seredkin M. on 11.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseSimpleMapper<FromT, ToT extends DTO> implements Mapper<FromT> {

    public abstract Collection<ToT> toFulls(Collection<FromT> entities);

    public abstract ToT toFull(FromT entity);

    @Override
    public DTO toDTO(FromT entity, final Projection projection) {
        switch (projection) {
            case FULL:
                return toFull(entity);
            default:
                throw new ApiException(ILLEGAL_PROJECTION);
        }
    }

    @Override
    public Collection<? extends DTO> toDTOs(Collection<FromT> entities, final Projection projection) {
        switch (projection) {
            case FULL:
                return toFulls(entities);
            default:
                throw new ApiException(ILLEGAL_PROJECTION);
        }
    }

    @Override
    public Page<? extends DTO> toPage(Page<FromT> page, Projection projection) {
        return new PageImpl<>(Lists.newArrayList(toDTOs(page.getContent(), projection)),
                              new PageResponse(page.getNumber(), page.getSize()),
                              page.getTotalElements());
    }

}
