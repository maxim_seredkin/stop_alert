package com.ws.sa.mappers.report.employee;

import com.ws.sa.dtos.report.employee.EmployeeReportFullDTO;
import com.ws.sa.dtos.report.employee.EmployeeReportLightDTO;
import com.ws.sa.dtos.report.employee.EmployeeReportRowDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.services.employee.EmployeeService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 15.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Mapper(uses = {EmployeeTagMapper.class, UnitMapper.class, MailTemplateMapper.class})
public abstract class EmployeeReportMapper extends BaseEntityMapper<Employee, EmployeeReportLightDTO, EmployeeReportRowDTO, EmployeeReportFullDTO> {

    @Autowired
    public EmployeeService employeeService;

    @Override
    @Mappings({@Mapping(target = "result", ignore = true),})
    public abstract EmployeeReportFullDTO toFull(Employee entity);

    @AfterMapping
    public void toFull(Employee entity, @MappingTarget EmployeeReportFullDTO dto) {
        if (Objects.isNull(entity) || Objects.isNull(dto)) return;

        dto.setResult(getEmployeeResult(entity.getId()));
    }

    @Override
    @Mappings({@Mapping(target = "result", ignore = true),})
    public abstract EmployeeReportRowDTO toRow(Employee entity);

    @AfterMapping
    public void toRow(Employee entity, @MappingTarget EmployeeReportRowDTO dto) {
        if (Objects.isNull(entity) || Objects.isNull(dto)) return;

        dto.setResult(getEmployeeResult(entity.getId()));
    }

    private Result getEmployeeResult(UUID id) {
        return new Result(employeeService.countAllTasks(id),
                          employeeService.countAttackedTasks(id));
    }
}
