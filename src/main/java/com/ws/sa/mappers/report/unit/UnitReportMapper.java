package com.ws.sa.mappers.report.unit;

import com.ws.sa.dtos.report.unit.UnitReportFullDTO;
import com.ws.sa.dtos.report.unit.UnitReportLightDTO;
import com.ws.sa.dtos.report.unit.UnitReportRowDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.mappers.user.UserMapper;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.unit.UnitService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 17.09.2017.
 */
@Mapper(uses = {EmployeeTagMapper.class, UnitMapper.class, MailTemplateMapper.class, UserMapper.class})
public abstract class UnitReportMapper extends BaseEntityMapper<Unit, UnitReportLightDTO, UnitReportRowDTO, UnitReportFullDTO> {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private UnitMapper unitMapper;

    @Override
    @Mappings({@Mapping(target = "result", ignore = true),
            @Mapping(target = "path", ignore = true)})
    public abstract UnitReportFullDTO toFull(Unit entity);

    @AfterMapping
    public void toFull(Unit entity, @MappingTarget UnitReportFullDTO dto) {
        if (Objects.isNull(entity) || Objects.isNull(dto))
            return;

        dto.setPath(unitMapper.toLights(unitService.getUnitPath(entity.getPath())));
        dto.setResult(getResult(entity.getId()));
    }

    @Override
    @Mappings({@Mapping(target = "result", ignore = true),})
    public abstract UnitReportRowDTO toRow(Unit entity);

    @AfterMapping
    public void toRow(Unit entity, @MappingTarget UnitReportRowDTO dto) {
        if (Objects.isNull(entity) || Objects.isNull(dto))
            return;

        dto.setResult(getResult(entity.getId()));
    }

    private Result getResult(UUID unitId) {
        return new Result(employeeService.countAllByUnit(unitId),
                employeeService.countAttackedByUnit(unitId));
    }
}
