package com.ws.sa.mappers.report.task;

import com.google.common.collect.Lists;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.report.AddInfoFullDTO;
import com.ws.sa.dtos.report.task.TaskEmployeeResultDTO;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.transition.Transition;
import com.ws.sa.enums.task_report.TaskResultType;
import com.ws.sa.mappers.base.BaseAccessSimpleMapper;
import com.ws.sa.mappers.base.PageResponse;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.security.base.Access;
import com.ws.sa.services.transition.TransitionService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Collection;
import java.util.UUID;
import java.util.function.BiFunction;

import static com.ws.sa.enums.task_report.TaskResultType.FAIL;
import static com.ws.sa.enums.task_report.TaskResultType.SUCCESS;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

/**
 * @author Maxim Seredkin
 * @since 26.03.17
 */
@Mapper(uses = {EmployeeTagMapper.class, UnitMapper.class, MailTemplateMapper.class})
public abstract class TaskEmployeeResultMapper extends BaseAccessSimpleMapper<Employee, TaskEmployeeResultDTO> {

    @Autowired
    private TransitionService transitionService;

    @Override
    @Autowired
    public void setAccess(@Qualifier("employeeAccess") Access access) {
        super.setAccess(access);
    }

    @Override
    @Mappings({@Mapping(target = "result", ignore = true)})
    public abstract TaskEmployeeResultDTO toFull(Employee entity);

    /**
     * @return функця маппинга (Page:FromT, Projection) -> Page:(? extends DTO)
     */
    public BiFunction<UUID, Page<Employee>, Page<? extends DTO>> getEmployeePageMapper() {
        return this::toPage;
    }

    public Page<? extends DTO> toPage(UUID taskId, Page<Employee> page) {
        return new PageImpl<>(Lists.newArrayList(toDTOs(taskId, page.getContent())),
                              new PageResponse(page.getNumber(), page.getSize()),
                              page.getTotalElements());
    }

    private Collection<? extends DTO> toDTOs(UUID taskId, Collection<Employee> entities) {
        if (entities == null)
            return null;

        return entities.stream()
                       .map(this::toFull)
                       .peek(dto -> afterMapping(taskId, dto))
                       .collect(toList());
    }

    private void afterMapping(UUID taskId, TaskEmployeeResultDTO dto) {
        if (isNull(dto))
            return;

        Collection<Transition> transitions = transitionService.findAllByTaskAndEmployee(taskId, dto.getId());

        dto.setResult(transitions.isEmpty() ? SUCCESS : FAIL);

        dto.setAddInfo(transitions.stream().map(transition -> new AddInfoFullDTO(transition.getTransitionDate(),
                                                                                 transition.getIp(),
                                                                                 transition.getOs(),
                                                                                 transition.getBrowser())).collect(toList()));
    }
}
