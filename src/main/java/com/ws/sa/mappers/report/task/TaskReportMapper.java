package com.ws.sa.mappers.report.task;

import com.ws.sa.dtos.report.task.TaskReportFullDTO;
import com.ws.sa.dtos.report.task.TaskReportLightDTO;
import com.ws.sa.dtos.report.task.TaskReportRowDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.entities.task.Task;
import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.services.employee.EmployeeService;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 14.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Mapper(uses = {EmployeeTagMapper.class, UnitMapper.class, MailTemplateMapper.class})
public abstract class TaskReportMapper extends BaseEntityMapper<Task, TaskReportLightDTO, TaskReportRowDTO, TaskReportFullDTO> {

    @Autowired
    private EmployeeService employeeService;

    @Mappings({@Mapping(target = "taskResult", ignore = true),
               @Mapping(target = "shortName", source = "author.shortName"),})
    public abstract TaskReportRowDTO toRow(Task entity);

    @AfterMapping
    public void toRow(Task entity, @MappingTarget TaskReportRowDTO dto) {
        dto.setTaskResult(getTaskResult(entity.getId()));
    }

    @Mappings({@Mapping(target = "taskResult", ignore = true),
               @Mapping(target = "fullName", source = "author.fullName"),})
    public abstract TaskReportFullDTO toFull(Task entity);

    @AfterMapping
    public void toFull(Task entity, @MappingTarget TaskReportFullDTO dto) {
        dto.setTaskResult(getTaskResult(entity.getId()));
    }

    private Result getTaskResult(UUID taskId) {
        return new Result(employeeService.countAllByTask(taskId),
                          employeeService.countAttackedEmployeesByTask(taskId));
    }
}
