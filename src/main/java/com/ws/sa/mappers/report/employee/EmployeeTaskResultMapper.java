package com.ws.sa.mappers.report.employee;

import com.google.common.collect.Lists;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.report.AddInfoFullDTO;
import com.ws.sa.dtos.report.employee.EmployeeTaskResultDTO;
import com.ws.sa.entities.task.Task;
import com.ws.sa.entities.transition.Transition;
import com.ws.sa.enums.task_report.TaskResultType;
import com.ws.sa.mappers.base.BaseAccessSimpleMapper;
import com.ws.sa.mappers.base.PageResponse;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.security.base.Access;
import com.ws.sa.services.transition.TransitionService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Collection;
import java.util.UUID;
import java.util.function.BiFunction;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

/**
 * Created by Vladislav Shelengovskiy on 17.09.2017.
 */
@Mapper(uses = {MailTemplateMapper.class})
public abstract class EmployeeTaskResultMapper extends BaseAccessSimpleMapper<Task, EmployeeTaskResultDTO> {

    @Autowired
    private TransitionService transitionService;

    @Override
    @Autowired
    public void setAccess(@Qualifier("taskAccess") Access access) {
        super.setAccess(access);
    }

    @Override
    @Mappings({@Mapping(target = "result", ignore = true),
               @Mapping(target = "shortName", source = "author.shortName")})
    public abstract EmployeeTaskResultDTO toFull(Task entity);

    public BiFunction<UUID, Page<Task>, Page<? extends DTO>> getTaskPageMapper() {
        return this::toPage;
    }

    public Page<? extends DTO> toPage(UUID employeeId, Page<Task> page) {
        return new PageImpl<>(Lists.newArrayList(toDTOs(employeeId, page.getContent())),
                              new PageResponse(page.getNumber(), page.getSize()),
                              page.getTotalElements());
    }

    private Collection<? extends DTO> toDTOs(UUID employeeId, Collection<Task> entities) {
        if (entities == null)
            return null;

        return entities.stream()
                       .map(this::toFull)
                       .peek(dto -> afterMapping(employeeId, dto))
                       .collect(toList());
    }

    private void afterMapping(UUID employeeId, EmployeeTaskResultDTO dto) {
        if (isNull(dto))
            return;

        Collection<Transition> transitions = transitionService.findAllByTaskAndEmployee(dto.getId(), employeeId);

        dto.setResult(transitions.isEmpty() ? TaskResultType.SUCCESS : TaskResultType.FAIL);

        dto.setAddInfo(transitions.stream().map(transition -> new AddInfoFullDTO(transition.getTransitionDate(),
                                                                                 transition.getIp(),
                                                                                 transition.getOs(),
                                                                                 transition.getBrowser())).collect(toList()));
    }
}

