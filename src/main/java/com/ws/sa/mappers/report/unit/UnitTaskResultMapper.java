package com.ws.sa.mappers.report.unit;

import com.google.common.collect.Lists;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.report.unit.UnitTaskResultDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.entities.task.Task;
import com.ws.sa.mappers.base.BaseAccessSimpleMapper;
import com.ws.sa.mappers.base.PageResponse;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.security.base.Access;
import com.ws.sa.services.employee.EmployeeService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Collection;
import java.util.UUID;
import java.util.function.BiFunction;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

/**
 * Created by Vladislav Shelengovskiy on 17.09.2017.
 */
@Mapper(uses = {EmployeeTagMapper.class, UnitMapper.class, MailTemplateMapper.class})
public abstract class UnitTaskResultMapper extends BaseAccessSimpleMapper<Task, UnitTaskResultDTO> {

    @Autowired
    private EmployeeService employeeService;

    @Override
    @Autowired
    public void setAccess(@Qualifier("taskAccess") Access access) {
        super.setAccess(access);
    }

    @Mappings({@Mapping(target = "taskResult", ignore = true),
               @Mapping(target = "shortName", source = "author.shortName"),})
    public abstract UnitTaskResultDTO toFull(Task entity);

    public BiFunction<UUID, Page<Task>, Page<? extends DTO>> getTaskPageMapper() {
        return this::toPage;
    }

    public Page<? extends DTO> toPage(UUID employeeId, Page<Task> page) {
        return new PageImpl<>(Lists.newArrayList(toDTOs(employeeId, page.getContent())),
                              new PageResponse(page.getNumber(), page.getSize()),
                              page.getTotalElements());
    }

    private Collection<? extends DTO> toDTOs(UUID unitId, Collection<Task> entities) {
        if (entities == null)
            return null;

        return entities.stream()
                       .map(this::toFull)
                       .peek(dto -> afterMapping(unitId, dto))
                       .collect(toList());
    }

    private void afterMapping(UUID unitId, UnitTaskResultDTO dto) {
        if (isNull(dto))
            return;

        dto.setTaskResult(new Result(employeeService.countAllByUnitAndTask(unitId, dto.getId()),
                                     employeeService.countAttackedByUnitAndTask(unitId, dto.getId())));
    }
}
