package com.ws.sa.mappers.report.unit;

import com.google.common.collect.Lists;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.dtos.report.unit.UnitEmployeeResultDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.mappers.base.BaseAccessSimpleMapper;
import com.ws.sa.mappers.base.PageResponse;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.security.base.Access;
import com.ws.sa.services.employee.EmployeeService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.Collection;
import java.util.function.Function;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;

/**
 * Created by Vladislav Shelengovskiy on 17.09.2017.
 */
@Mapper(uses = {EmployeeTagMapper.class, UnitMapper.class, MailTemplateMapper.class})
public abstract class UnitEmployeeResultMapper extends BaseAccessSimpleMapper<Employee, UnitEmployeeResultDTO> {

    @Autowired
    private EmployeeService employeeService;

    @Override
    @Autowired
    public void setAccess(@Qualifier("employeeAccess") Access access) {
        super.setAccess(access);
    }

    @Override
    @Mappings({@Mapping(target = "result", ignore = true)})
    public abstract UnitEmployeeResultDTO toFull(Employee entity);

    /**
     * @return функця маппинга (Page:FromT, Projection) -> Page:(? extends DTO)
     */
    public Function<Page<Employee>, Page<? extends DTO>> getEmployeePageMapper() {
        return this::toPage;
    }

    public Page<? extends DTO> toPage(Page<Employee> page) {
        return new PageImpl<>(Lists.newArrayList(toDTOs(page.getContent())),
                              new PageResponse(page.getNumber(), page.getSize()),
                              page.getTotalElements());
    }

    private Collection<? extends DTO> toDTOs(Collection<Employee> entities) {
        if (entities == null)
            return null;

        return entities.stream()
                       .map(this::toFull)
                       .peek(this::afterMapping)
                       .collect(toList());
    }

    private void afterMapping(UnitEmployeeResultDTO dto) {
        if (isNull(dto))
            return;

        dto.setResult(new Result(employeeService.countAllTasks(dto.getId()),
                                 employeeService.countAttackedTasks(dto.getId())));
    }
}
