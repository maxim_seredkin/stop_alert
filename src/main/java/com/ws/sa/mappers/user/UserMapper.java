package com.ws.sa.mappers.user;

import com.ws.sa.dtos.user.UserFullDTO;
import com.ws.sa.dtos.user.UserLightDTO;
import com.ws.sa.dtos.user.UserRowDTO;
import com.ws.sa.entities.user.User;
import com.ws.sa.forms.user.UpdateAccountForm;
import com.ws.sa.forms.user.UserForm;
import com.ws.sa.mappers.authority.AuthorityMapper;
import com.ws.sa.mappers.base.BaseEntityMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import java.util.Collection;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@Mapper(uses = {AuthorityMapper.class, UnitMapper.class})
public abstract class UserMapper extends BaseEntityMapper<User, UserLightDTO, UserRowDTO, UserFullDTO> {

    @Override
    @Mappings({@Mapping(target = "status", expression = "java( entity.getStatus() )"),})
    public abstract UserRowDTO toRow(User entity);

    @Override
    @Mappings({@Mapping(target = "status", expression = "java( entity.getStatus() )"),})
    public abstract UserFullDTO toFull(User entity);

    public abstract Collection<User> toEntities(Collection<UserForm> form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "authority", ignore = true),
               @Mapping(target = "authorities", ignore = true),
               @Mapping(target = "password", ignore = true),
               @Mapping(target = "accountNonExpired", ignore = true),
               @Mapping(target = "accountNonLocked", ignore = true),
               @Mapping(target = "registerDate", ignore = true),
               @Mapping(target = "credentialsNonExpired", ignore = true),
               @Mapping(target = "enabled", ignore = true),
               @Mapping(target = "units", ignore = true),
               @Mapping(target = "blocked", ignore = true),})
    public abstract User toEntity(UserForm form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "authority", ignore = true),
               @Mapping(target = "authorities", ignore = true),
               @Mapping(target = "password", ignore = true),
               @Mapping(target = "accountNonExpired", ignore = true),
               @Mapping(target = "accountNonLocked", ignore = true),
               @Mapping(target = "registerDate", ignore = true),
               @Mapping(target = "credentialsNonExpired", ignore = true),
               @Mapping(target = "enabled", ignore = true),
               @Mapping(target = "units", ignore = true),
               @Mapping(target = "blocked", ignore = true),})
    public abstract void updateEntity(UserForm form, @MappingTarget User entity);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "authority", ignore = true),
               @Mapping(target = "authorities", ignore = true),
               @Mapping(target = "password", ignore = true),
               @Mapping(target = "accountNonExpired", ignore = true),
               @Mapping(target = "accountNonLocked", ignore = true),
               @Mapping(target = "registerDate", ignore = true),
               @Mapping(target = "credentialsNonExpired", ignore = true),
               @Mapping(target = "enabled", ignore = true),
               @Mapping(target = "units", ignore = true),
               @Mapping(target = "blocked", ignore = true),})
    public abstract void updateEntity(UpdateAccountForm form, @MappingTarget User entity);

}
