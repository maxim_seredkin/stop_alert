package com.ws.sa.mappers.redirect;

import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.services.mock_page.MockPageService;
import com.ws.sa.entities.redirect.Redirect;
import com.ws.sa.dtos.redirect.RedirectDTO;
import com.ws.sa.forms.redirect.RedirectForm;
import com.ws.sa.configs.AppConfig;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;
import java.util.Optional;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Mapper
public abstract class RedirectMapper {

    @Autowired
    protected AppConfig appConfig;

    @Autowired
    private MailTemplateService mailTemplateService;

    @Autowired
    private MockPageService mockPageService;

    public RedirectDTO toForm(Redirect entity) {

        if (Objects.isNull(entity))
            return null;

        RedirectDTO dto = new RedirectDTO();
        dto.setRedirectType(entity.getRedirectType());

        switch (entity.getRedirectType()) {
            case CUSTOM_LINK:
                dto.setCustomUri(entity.getRedirectUri());
                break;
            case MOCK_PAGE_LINK:
                dto.setMockPage(Optional.ofNullable(entity.getMockPage()).map(MockPage::getId).orElse(null));
                break;
            case TEMPLATE_LINK:
                dto.setMailTemplate(Optional.ofNullable(entity.getMailTemplate()).map(MailTemplate::getId).orElse(null));
                break;
        }

        return dto;
    }

    public Redirect toEntity(RedirectForm form) {

        if (Objects.isNull(form))
            return null;

        Redirect entity = new Redirect();
        entity.setRedirectType(form.getRedirectType());

        switch (form.getRedirectType()) {
            case CUSTOM_LINK:
                entity.setRedirectUri(form.getCustomUri());
                break;
            case MOCK_PAGE_LINK:
                Guard.checkEntityArgumentExists(form.getMockPage(), ApiStatusCode.INVALID_PARAM);
                entity.setMockPage(mockPageService.findOneExisting(form.getMockPage()));
                entity.setRedirectUri(Optional.of(entity)
                                              .map(Redirect::getMockPage)
                                              .map(MockPage::getRedirectUri)
                                              .map(redirectUri -> appConfig.getExternalAddress().concat(redirectUri))
                                              .orElse(null));
                break;
            case TEMPLATE_LINK:
                Guard.checkEntityArgumentExists(form.getMailTemplate(), ApiStatusCode.INVALID_PARAM);
                entity.setMailTemplate(mailTemplateService.findOneExisting(form.getMailTemplate()));
                entity.setRedirectUri(Optional.of(entity)
                                              .map(Redirect::getMailTemplate)
                                              .map(MailTemplate::getRedirectUri)
                                              .orElse(null));
                break;
        }

        return entity;
    }

}
