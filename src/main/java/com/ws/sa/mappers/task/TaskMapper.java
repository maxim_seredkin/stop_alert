package com.ws.sa.mappers.task;


import com.ws.sa.mappers.base.BaseAccessEntityMapper;
import com.ws.sa.mappers.employee.EmployeeMapper;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.security.base.Access;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.entities.task.Task;
import com.ws.sa.dtos.task.TaskFullDTO;
import com.ws.sa.dtos.task.TaskLightDTO;
import com.ws.sa.dtos.task.TaskRowDTO;
import com.ws.sa.forms.task.TaskForm;
import com.ws.sa.mappers.redirect.RedirectMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
@Mapper(uses = {UnitMapper.class, EmployeeTagMapper.class, EmployeeMapper.class, MailTemplateMapper.class, RedirectMapper.class})
public abstract class TaskMapper extends BaseAccessEntityMapper<Task, TaskLightDTO, TaskRowDTO, TaskFullDTO> {

    @Override
    @Autowired
    public void setAccess(@Qualifier("taskAccess") Access access) {
        super.setAccess(access);
    }

    @Override
    @Mappings({@Mapping(source = "author.shortName", target = "author"),})
    public abstract TaskRowDTO toRow(Task entity);

    @Override
    @Mappings({@Mapping(source = "author.shortName", target = "author")})
    public abstract TaskLightDTO toLight(Task entity);

    @Override
    @Mappings({@Mapping(source = "author.shortName", target = "author"),})
    public abstract TaskFullDTO toFull(Task entity);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "units", ignore = true),
               @Mapping(target = "author", ignore = true),
               @Mapping(target = "employeeTags", ignore = true),
               @Mapping(target = "createdDate", ignore = true),
               @Mapping(target = "endDate", ignore = true),
               @Mapping(target = "status", ignore = true),
               @Mapping(target = "employeesByHand", ignore = true),
               @Mapping(target = "employeesByFilter", ignore = true),
               @Mapping(target = "template", ignore = true),
               @Mapping(target = "mails", ignore = true),})
    public abstract Task toEntity(TaskForm form);

    @Mappings({@Mapping(target = "id", ignore = true),
               @Mapping(target = "units", ignore = true),
               @Mapping(target = "author", ignore = true),
               @Mapping(target = "employeeTags", ignore = true),
               @Mapping(target = "createdDate", ignore = true),
               @Mapping(target = "endDate", ignore = true),
               @Mapping(target = "status", ignore = true),
               @Mapping(target = "employeesByHand", ignore = true),
               @Mapping(target = "employeesByFilter", ignore = true),
               @Mapping(target = "template", ignore = true),
               @Mapping(target = "mails", ignore = true),})
    public abstract Task updateEntity(TaskForm form, @MappingTarget Task entity);
}
