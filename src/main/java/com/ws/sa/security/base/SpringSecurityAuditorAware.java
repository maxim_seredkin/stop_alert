package com.ws.sa.security.base;

import com.ws.sa.entities.user.User;
import com.ws.sa.services.user.UserService;
import com.ws.sa.security.user.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import static com.ws.sa.security.base.SecurityHelper.getUser;

/**
 * @author Maxim Seredkin
 */
public class SpringSecurityAuditorAware implements AuditorAware<User> {

    @Autowired
    private UserService service;

    @Override
    public User getCurrentAuditor() {
        try {
            UserDetailsImpl userDetails = getUser();
            return service.findOneExisting(userDetails.getId());
        } catch (Exception e) {
            return null;
        }
    }
}
