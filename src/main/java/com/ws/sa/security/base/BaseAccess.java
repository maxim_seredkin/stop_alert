package com.ws.sa.security.base;

import com.ws.sa.enums.Operation;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 28.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public abstract class BaseAccess implements Access {

    public abstract boolean accessCreate();

    public abstract boolean accessRead(UUID id);

    public abstract boolean accessUpdate(UUID id);

    public abstract boolean accessDelete(UUID id);

    @Override
    public boolean access(UUID employeeId, Operation operation) {
        switch (operation) {
            case READ:
                return accessRead(employeeId);
            case DELETE:
                return accessDelete(employeeId);
            case UPDATE:
                return accessUpdate(employeeId);
            case CREATE:
                return accessCreate();
            default:
                return false;
        }
    }

}
