package com.ws.sa.security.base;

import com.google.common.collect.Lists;
import com.ws.sa.enums.Role;
import com.ws.sa.security.user.UserDetailsImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.Objects;

import static com.ws.sa.enums.Role.*;
import static com.ws.sa.exceptions.Guard.checkAuthorization;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author Maxim Seredkin
 */
public class SecurityHelper {

    public static boolean isAuthenticated() {
        if (isNull(SecurityContextHolder.getContext()))
            return false;

        return checkAuthentication(getAuthentication());
    }

    public static UserDetailsImpl getUser() {
        checkAuthorization();

        return (UserDetailsImpl) getAuthentication().getPrincipal();
    }

    private static boolean checkAuthentication(Authentication auth) {
        return nonNull(auth) && nonNull(auth.getPrincipal()) && !Objects.equals(auth.getPrincipal().getClass(), String.class);
    }

    public static Collection<? extends GrantedAuthority> getAuthorities() {
        UserDetailsImpl user = getUser();
        return nonNull(user) ? user.getAuthorities() : Lists.newArrayList();
    }

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static boolean isAdministrator() {
        return isRole(getUser(), ADMINISTRATOR);
    }

    public static boolean isSecurityOfficer() {
        return isRole(getUser(), SECURITY_OFFICER);
    }

    public static boolean isManager() {
        return isRole(getUser(), MANAGER);
    }

    private static boolean isRole(UserDetailsImpl user, Role role) {
        return user != null && user.getAuthorities().stream().anyMatch(a -> Objects.equals(a.getAuthority(), role.getAuthority()));
    }

}