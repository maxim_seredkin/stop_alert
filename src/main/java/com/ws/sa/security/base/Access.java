package com.ws.sa.security.base;

import com.ws.sa.enums.Operation;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 28.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
public interface Access {

    boolean access(UUID employeeId, Operation operation);

}
