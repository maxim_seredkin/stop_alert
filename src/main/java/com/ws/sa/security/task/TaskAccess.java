package com.ws.sa.security.task;

import com.ws.sa.security.base.BaseAccess;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.services.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static com.ws.sa.security.base.SecurityHelper.*;

/**
 * Created by Vladislav Shelengovskiy on 23.03.2017.
 */
@Component
public class TaskAccess extends BaseAccess {

    @Autowired
    private TaskService service;

    public boolean accessRead(UUID taskId) {
        if (isAdministrator())
            return true;
        else if (isManager())
            return false;

        return service.hasAccess(taskId, SecurityHelper.getUser().getId());
    }

    public boolean accessCreate() {
        return isAdministrator() || isSecurityOfficer();
    }

    public boolean accessUpdate(UUID taskId) {
        return accessRead(taskId);
    }

    public boolean accessDelete(UUID taskId) {
        return accessRead(taskId);
    }
}
