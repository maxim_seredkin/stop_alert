package com.ws.sa.security.user;

/**
 * @author Maxim Seredkin
 * @since 15.04.2017
 */
public interface LoginAttemptService {

    void loginSucceeded(String key);

    void loginFailed(String key);

    boolean isBlocked(String key);

    boolean nonBlocked(String key);
    
}
