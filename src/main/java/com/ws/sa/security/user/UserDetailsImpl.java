package com.ws.sa.security.user;

import com.google.common.collect.Lists;
import com.ws.sa.enums.UserStatus;
import com.ws.sa.entities.authority.Authority;
import com.ws.sa.entities.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Maxim Seredkin
 */
@Setter
@Getter
@NoArgsConstructor
public class UserDetailsImpl implements UserDetails {

    private UUID id;

    private String username;

    private String password;

    private Authority authority;

    private boolean accountNonLocked;

    private boolean accountNonExpired;

    private boolean credentialsNonExpired;

    private boolean enabled;

    public static UserDetailsImpl create(User user) {
        if (user == null)
            return null;

        UserDetailsImpl userDetails = new UserDetailsImpl();
        userDetails.setEnabled(user.isEnabled());
        userDetails.setAccountNonExpired(user.isAccountNonExpired());
        userDetails.setAccountNonLocked(user.isAccountNonLocked());
        userDetails.setAuthority(user.getAuthority());
        userDetails.setCredentialsNonExpired(user.isCredentialsNonExpired());
        userDetails.setId(user.getId());
        userDetails.setPassword(user.getPassword());
        userDetails.setUsername(user.getUsername());
        return userDetails;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Lists.newArrayList(authority);
    }

    public boolean isDisabled() {
        return !enabled;
    }

    public boolean isAccountLocked() {
        return !accountNonLocked;
    }

    public UserStatus getStatus() {
        return UserStatus.getStatus(isDisabled(), isAccountLocked());
    }
}
