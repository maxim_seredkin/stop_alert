package com.ws.sa.security.unit;

import com.ws.sa.security.base.BaseAccess;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.security.user.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 28.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Component
public class UnitAccess extends BaseAccess {

    @Autowired
    private UnitService service;

    public boolean accessCreate(){
        return SecurityHelper.isAdministrator();
    }

    public boolean accessRead(UUID unitId) {
        if (SecurityHelper.isAdministrator()) return true;

        UserDetailsImpl user = SecurityHelper.getUser();

        return service.hasAccess(unitId, user.getId());
    }

    public boolean accessUpdate(UUID unitId){
        return SecurityHelper.isAdministrator();
    }

    public boolean accessDelete(UUID unitId){
        return SecurityHelper.isAdministrator();
    }
}
