package com.ws.sa.security.employee;

import com.ws.sa.security.base.BaseAccess;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.services.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 25.03.2017.
 */
@Component
public class EmployeeAccess extends BaseAccess {

    @Autowired
    private EmployeeService service;

    public boolean accessRead(UUID employeeId) {
        return SecurityHelper.isAdministrator() || service.hasAccess(employeeId, SecurityHelper.getUser().getId());
    }

    public boolean accessCreate() {
        return SecurityHelper.isAdministrator() || SecurityHelper.isSecurityOfficer();
    }

    public boolean accessUpdate(UUID employeeId) {
        if (SecurityHelper.isAdministrator()) return true;
        else if (SecurityHelper.isManager()) return false;
        return service.hasAccess(employeeId, SecurityHelper.getUser().getId());
    }

    public boolean accessDelete(UUID employeeId) {
        if (SecurityHelper.isAdministrator()) return true;
        else if (SecurityHelper.isManager()) return false;
        return service.hasAccess(employeeId, SecurityHelper.getUser().getId());
    }
}

