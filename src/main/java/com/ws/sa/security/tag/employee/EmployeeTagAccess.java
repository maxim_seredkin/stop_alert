package com.ws.sa.security.tag.employee;

import com.ws.sa.security.base.BaseAccess;
import com.ws.sa.security.base.SecurityHelper;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 28.03.2017.
 */
@Component
public class EmployeeTagAccess extends BaseAccess {

    public boolean accessRead(UUID employeeTagId) {
        return SecurityHelper.isAuthenticated();
    }

    public boolean accessCreate() {
        return SecurityHelper.isAdministrator();
    }

    public boolean accessUpdate(UUID employeeTagId) {
        return SecurityHelper.isAdministrator();
    }

    public boolean accessDelete(UUID employeeTagId) {
        return SecurityHelper.isAdministrator();
    }

}
