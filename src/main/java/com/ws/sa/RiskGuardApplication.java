package com.ws.sa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.WebApplicationInitializer;

@EnableAsync
@EnableScheduling
@SpringBootApplication
@EntityScan("com.ws.sa.entities")
@ComponentScan({"com.ws.sa.controllers",
                "com.ws.sa.api_controllers",
                "com.ws.sa.handlers",
                "com.ws.sa.actions",
                "com.ws.sa.data_loaders",
                "com.ws.sa.executors",
                "com.ws.sa.listeners",
                "com.ws.sa.managers",
                "com.ws.sa.mappers",
                "com.ws.sa.queries",
                "com.ws.sa.renders",
                "com.ws.sa.security",
                "com.ws.sa.senders",
                "com.ws.sa.services",
                "com.ws.sa.specifications",
                "com.ws.sa.validators",
                "com.ws.sa.repositories",
                "com.ws.sa.configs",
                "com.ws.sa.builders"})
public class RiskGuardApplication extends SpringBootServletInitializer implements WebApplicationInitializer {

    public static void main(String[] args) {
        SpringApplication.run(RiskGuardApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RiskGuardApplication.class);
    }
}
