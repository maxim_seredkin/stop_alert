package com.ws.sa.specifications.unit;

import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import com.ws.sa.enums.EntityStatus;
import com.ws.sa.enums.LogicOperator;
import com.ws.sa.queries.DynamicQuery;
import com.ws.sa.queries.filter.ExtraFilter;
import com.ws.sa.queries.filter.Filter;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.services.user.UserService;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.*;

/**
 * Created by Man on 15.03.2017.
 */
public class UnitJpaQuerySpecification extends DefaultJpaQuerySpecification<Unit> {

    private UnitService unitService;
    private UserService userService;

    public UnitJpaQuerySpecification(DynamicQuery query) {
        super(query);
    }

    public UnitJpaQuerySpecification(DynamicQuery query, UnitService unitService, UserService userService) {
        super(query);
        this.userService = userService;
        this.unitService = unitService;
    }

    @Override
    protected Collection<Specification<Unit>> createExtraSpecifications() {
        Collection<Filter> filters = getQuery().getFilters(ExtraFilter.class);
        List<Specification<Unit>> specifications = new ArrayList<>();
        specifications.add((root, q, cb) -> cb.equal(root.get("status"), EntityStatus.ENABLED));
        if (filters != null) {
            filters.forEach(filter -> {
                if (filter.getAttribute().equals("units") && filter.getOperator().equals(LogicOperator.NE)) {
                    UUID id = UUID.fromString((String) filter.getValue());
                    specifications.add((root, q, cb) -> cb.or(cb.equal(root.get("id"), id),
                                                              cb.like(root.get("path"), "%".concat(id.toString()).concat("%"))).not());
                } else if (filter.getAttribute().equals("units")) {
                    specifications.add((root, q, cb) -> {
                        Subquery subQ = q.subquery(Unit.class);
                        Root subRoot = subQ.from(Unit.class);

                        return cb.and((Predicate[]) getUUIDCollection(filter).stream()
                                                                             .map(id -> root.get("id").in(
                                                                                     subQ.select(subRoot.get("id")).where(cb.or(
                                                                                             cb.equal(subRoot.get("id"), id),
                                                                                             cb.like(subRoot.get("path"), "%".concat(id.toString()).concat("%"))))))
                                                                             .toArray(Predicate[]::new));
                    });
                } else if (Objects.equals(filter.getAttribute(), "officers") || Objects.equals(filter.getAttribute(), "managers")) {
                    specifications.add((root, q, cb) -> {
                        Subquery subQ = q.subquery(Unit.class);
                        Root subRoot = subQ.from(Unit.class);
                        Collection<Unit> units = unitService.findByUser(UUID.fromString((String) filter.getValue()));

                        return root.get("id")
                                   .in(subQ.select(subRoot.get("id")).where(cb.or(
                                           units
                                                   .stream()
                                                   .map(unit -> cb.or(
                                                           cb.equal(subRoot.get("id"), unit.getId()),
                                                           cb.like(subRoot.get("path"), "%".concat(unit.getId().toString()).concat("%"))))
                                                   .toArray(Predicate[]::new))));

                    });
                }
            });
        }
        addSecuritySpecification(specifications);
        return specifications;
    }

    private void addSecuritySpecification(List<Specification<Unit>> specifications) {
        if (SecurityHelper.isAdministrator())
            return;

        User user = userService.findOneExisting(SecurityHelper.getUser().getId());
        specifications.add((root, q, cb) -> cb.or(
                user.getUnits()
                    .stream()
                    .map(unit -> cb.or(
                            cb.equal(root.get("id"), unit.getId()),
                            cb.like(root.get("path"), "%".concat(unit.getId().toString()).concat("%"))))
                    .toArray(Predicate[]::new)));
    }
}
