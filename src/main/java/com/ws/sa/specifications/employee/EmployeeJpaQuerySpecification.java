package com.ws.sa.specifications.employee;

import com.google.common.collect.Lists;
import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.transition.Transition;
import com.ws.sa.entities.user.User;
import com.ws.sa.enums.LogicOperator;
import com.ws.sa.queries.DynamicQuery;
import com.ws.sa.queries.filter.ExtraFilter;
import com.ws.sa.queries.filter.Filter;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.services.user.UserService;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
public class EmployeeJpaQuerySpecification extends DefaultJpaQuerySpecification<Employee> {

    private UserService userService;

    public EmployeeJpaQuerySpecification(DynamicQuery query) {
        super(query);
    }

    public EmployeeJpaQuerySpecification(DynamicQuery query, UserService userService) {
        super(query);
        this.userService = userService;
    }

    @Override
    protected Collection<Specification<Employee>> createExtraSpecifications() {
        Collection<Filter> filters = getQuery().getFilters(ExtraFilter.class);
        List<Specification<Employee>> specifications = Lists.newArrayList();
        addExtraSpecifications(filters, specifications);
        addSecuritySpecification(specifications);
        return specifications;
    }

    private void addExtraSpecifications(Collection<Filter> filters, List<Specification<Employee>> specifications) {
        if (filters == null)
            return;

        filters.forEach(filter -> {
            switch (filter.getAttribute()) {
                case "units":
                    addUnitsSpecification(specifications, getUUIDCollection(filter));
                    break;
                case "unit":
                    addUnitSpecification(specifications, filter);
                    break;
                case "employeeTags":
                    addEmployeeTagSpecification(specifications, getUUIDCollection(filter));
                    break;
                case "fullName":
                    addFullNameSpecification(specifications, filter);
                    break;
                case "task":
                    addTaskSpecification(specifications, filter);
                    break;
                case "result":
                    addResultSpecification(specifications, filter);
                    break;
            }
        });
    }

    private void addUnitSpecification(List<Specification<Employee>> specifications, Filter filter) {
        specifications.add((root, q, cb) -> cb.equal(root.get("unit").get("id"),
                                                     UUID.fromString(String.valueOf(filter.getValue()))));
    }

    private void addResultSpecification(List<Specification<Employee>> specifications, Filter filter) {
        specifications.add((root, q, cb) -> {
            Subquery<Transition> subQ = q.subquery(Transition.class);
            Root<Transition> subRoot = subQ.from(Transition.class);

            Predicate predicate = cb.exists(subQ.select(subRoot.get("id"))
                                                .where(cb.and(cb.equal(subRoot.get("mail")
                                                                              .get("employee")
                                                                              .get("id"),
                                                                       root.get("id"))),
                                                       cb.equal(subRoot.get("mail")
                                                                       .get("task")
                                                                       .get("id"),
                                                                UUID.fromString(String.valueOf(filter.getValue())))));

            if (Objects.equals(filter.getOperator(), LogicOperator.NE)) {
                predicate = cb.not(predicate);
            }

            return predicate;
        });
    }

    private void addTaskSpecification(List<Specification<Employee>> specifications, Filter filter) {
        specifications.add((root, q, cb) -> {
            Subquery subQ = q.subquery(Mail.class);
            Root subRoot = subQ.from(Mail.class);

            return cb.exists(subQ.select(subRoot.get("id")).where(cb.and(
                    cb.equal(subRoot.get("task").get("id"), UUID.fromString(filter.getValue().toString())),
                    cb.equal(subRoot.get("employee").get("id"), root.get("id")))));

        });
    }

    private void addFullNameSpecification(List<Specification<Employee>> specifications, Filter filter) {
        specifications.add((root, q, cb) -> cb.and(Stream.of(filter.getValue().toString().split(" "))
                                                         .map(str -> "%".concat(str.toLowerCase()).concat("%"))
                                                         .map(str -> cb.or(cb.like(cb.lower(root.get("secondName")), str),
                                                                           cb.like(cb.lower(root.get("middleName")), str),
                                                                           cb.like(cb.lower(root.get("firstName")), str)))
                                                         .toArray(Predicate[]::new)));

    }

    private void addSecuritySpecification(List<Specification<Employee>> specifications) {
        if (SecurityHelper.isAdministrator())
            return;

        User user = userService.findOneExisting(SecurityHelper.getUser().getId());
        addUnitsSpecification(specifications,
                              user.getUnits().stream().map(BaseEntity::getId).collect(Collectors.toSet()));

    }

    private void addUnitsSpecification(List<Specification<Employee>> specifications, Collection<UUID> units) {
        specifications.add((root, q, cb) -> cb.or(
                units.stream()
                     .map(id -> cb.or(cb.equal(root.get("unit").get("id"), id),
                                      cb.like(root.get("unit").get("path"), "%".concat(id.toString()).concat("%"))))
                     .toArray(Predicate[]::new)));
    }

    private void addEmployeeTagSpecification(List<Specification<Employee>> specifications, Collection<UUID> employeeTags) {
        specifications.add((root, q, cb) -> {
            Subquery<EmployeeTag> subQ = q.subquery(EmployeeTag.class);
            Root<EmployeeTag> subRoot = subQ.from(EmployeeTag.class);

            return cb.exists(subQ.select(subRoot.get("id")).where(cb.and(
                    subRoot.get("id").in(root.join("employeeTags").get("id")),
                    cb.or(subRoot.get("id").in(employeeTags),
                          cb.or(employeeTags.stream()
                                            .map(id -> cb.like(subRoot.get("path"), String.join("", "%", String.valueOf(id), "%")))
                                            .toArray(Predicate[]::new))))));
        });
    }
}
