package com.ws.sa.specifications.base;

import com.ws.sa.entities.base.Entity;
import com.ws.sa.enums.Projection;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author Maxim Seredkin
 */
public interface JpaQuerySpecification<EntityT extends Entity> {

    Specification<EntityT> getQueryPredicate();

    Class getEntityClass();

    /**
     * Получить сортировку
     *
     * @return Sort
     */
    Sort getSort();

    /**
     * Получить пейдженированный запрос
     *
     * @return PageRequest
     */
    PageRequest getPageRequest();

    /**
     * Получить проекцию
     *
     * @return Projection
     */
    Projection getProjection();

}
