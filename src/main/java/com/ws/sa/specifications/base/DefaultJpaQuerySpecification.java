package com.ws.sa.specifications.base;

import com.ws.sa.entities.base.Entity;
import com.ws.sa.enums.EntityStatus;
import com.ws.sa.enums.LogicOperator;
import com.ws.sa.enums.Projection;
import com.ws.sa.enums.TaskStatus;
import com.ws.sa.queries.DynamicQuery;
import com.ws.sa.queries.filter.Filter;
import com.ws.sa.queries.filter.PrimitiveFilter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.metamodel.internal.SingularAttributeImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.*;

import static com.ws.sa.utils.PageUtils.createPageRequest;

/**
 * Спецификация по умолчанию для динамических запросов к базе данных
 *
 * @author Evgeny Albinets
 * @version 1.0
 */
@NoArgsConstructor
public class DefaultJpaQuerySpecification<EntityT extends Entity> implements JpaQuerySpecification<EntityT> {

    @Getter
    protected Specification<EntityT> queryPredicate;

    @Getter
    protected DynamicQuery query;

    public DefaultJpaQuerySpecification(DynamicQuery query) {
        this.query = query;
        this.queryPredicate = createSpecification();
    }

    /**
     * <h1>Создание спецификации</h1>
     *
     * @return
     */
    protected Specification<EntityT> createSpecification() {
        return this::createPredicates;
    }

    /**
     * <h1>Создание предикатов</h1>
     *
     * @param root          корень запроса
     * @param criteriaQuery запрос
     * @param cb            билдер предикатов
     *
     * @return предикат
     */
    protected Predicate createPredicates(Root<EntityT> root, CriteriaQuery criteriaQuery, CriteriaBuilder cb) {
//        criteriaQuery.distinct(true);
        // список предикатов
        Set<Predicate> predicates = new HashSet<>();
        // создаем предикаты для фильтров
        query.getFilters(PrimitiveFilter.class).forEach((f) -> {
            if (f != null) { // && f.getClass().equals(PrimitiveFilter.class)
                Path path = getAttributePath(f, root);
                Set<Predicate> filterPredicates = new HashSet<>();
                f.getValues().forEach(value -> {
                    filterPredicates.add(createFilterPredicate(f.getOperator(), path, value, cb));
                });
                predicates.add(cb.or(filterPredicates.toArray(new Predicate[0])));
            }
        });
        // создаем предикаты для поиска
        Set<Predicate> searchs = new HashSet<>();
        query.getSearchs().forEach((search) -> {
            if (search != null && search.getClass().equals(PrimitiveFilter.class)) {
                Path path = getAttributePath(search, root);
                search.getValues().forEach(value -> {
                    searchs.add((cb.like(cb.lower(path), "%" + value.toString().toLowerCase() + "%")));
                });
            }
        });
        // добавляем предикаты для поиска
        if (!searchs.isEmpty())
            predicates.add(cb.or(searchs.toArray(new Predicate[0])));
        // добавляем дополнительные предикаты
        createExtraSpecifications().forEach(s -> Optional.ofNullable(s).ifPresent(p -> predicates.add(p.toPredicate(root, criteriaQuery, cb))));
        // возвращаем полученные предикаты
        return predicates.size() == 1 ? predicates.iterator().next() : cb.and(predicates.toArray(new Predicate[0]));
    }

    /**
     * <h1>Создание предиката фильтров</h1>
     *
     * @param operator оператор
     * @param path     путь
     * @param value    значение
     * @param cb       билдер предикатов
     *
     * @return предикат
     */
    private final Predicate createFilterPredicate(LogicOperator operator, Path path, Object value, CriteriaBuilder cb) {
        if (operator.equals(LogicOperator.GE)) { // если операция "не меньше"
            if (path.getJavaType().equals(String.class))
                return (cb.greaterThanOrEqualTo(path, (String) value));
            else if (path.getJavaType().equals(Date.class))
                return (cb.greaterThanOrEqualTo(path, new Date(Long.parseLong((String) value))));
            else return (cb.greaterThanOrEqualTo(path, Double.parseDouble((String) value)));
        } else if (operator.equals(LogicOperator.GT)) { // если операция "больше"
            if (path.getJavaType().equals(String.class))
                return (cb.greaterThan(path, (String) value));
            else if (path.getJavaType().equals(Date.class))
                return (cb.greaterThan(path, new Date(Long.parseLong((String) value))));
            else return (cb.greaterThan(path, Double.parseDouble((String) value)));
        } else if (operator.equals(LogicOperator.LE)) { // если операция "не больше"
            if (path.getJavaType().equals(String.class))
                return (cb.lessThanOrEqualTo(path, (String) value));
            else if (path.getJavaType().equals(Date.class))
                return (cb.lessThanOrEqualTo(path, new Date(Long.parseLong((String) value))));
            else
                return (cb.lessThanOrEqualTo(path, Double.parseDouble((String) value)));
        } else if (operator.equals(LogicOperator.LT)) { // если операция "меньше"
            if (path.getJavaType().equals(String.class))
                return (cb.lessThan(path, (String) value));
            else if (path.getJavaType().equals(Date.class))
                return (cb.lessThan(path, new Date(Long.parseLong((String) value))));
            else return (cb.lessThan(path, Double.parseDouble((String) value)));
        } else if (operator.equals(LogicOperator.NE)) { // если операция "не равны"
            if (path.getJavaType().equals(String.class))
                return (cb.notLike(path, value.toString()));
            else if (path.getJavaType().equals(Date.class))
                return (cb.notEqual(path, new Date(Long.parseLong((String) value))));
            else if (path.getJavaType().equals(UUID.class))
                return (cb.notEqual(path, UUID.fromString((String) value)));
            else if (path.getJavaType().equals(Date.class))
                return (cb.notEqual(path, new Date(Long.parseLong((String) value))));
            else return (cb.notEqual(path, Double.parseDouble((String) value)));
        } else if (operator.equals(LogicOperator.LIKE)) { // если операция "подстрока"
            if (path.getJavaType().equals(String.class))
                return (cb.like(cb.lower(path), "%" + value.toString().toLowerCase() + "%"));
            else return (cb.equal(path, Double.parseDouble((String) value)));
        } else if (operator.equals(LogicOperator.IN)) { // если операция "принадлежит коллекции"
            return (cb.isMember(value, path));
        } else { // операция по умолчанию "эквиваленты"
            //if (operator.equals(LogicOperator.EQ)) {
            if (path.getJavaType().equals(String.class))
                return (cb.like(path, value.toString()));
            else if (path.getJavaType().equals(boolean.class) || path.getJavaType().equals(Boolean.class))
                return (cb.equal(path, Boolean.parseBoolean((String) value)));
            else if (path.getJavaType().equals(EntityStatus.class))
                return (cb.equal(path, EntityStatus.valueOf((String) value)));
            else if (path.getJavaType().equals(TaskStatus.class))
                return (cb.equal(path, TaskStatus.valueOf((String) value)));
            else if (((String) value).equals("null"))
                return (cb.isNull(path));
            else if (path.getJavaType().equals(Date.class))
                return (cb.equal(path, new Date(Long.parseLong((String) value))));
            else if (path.getJavaType().equals(UUID.class))
                return (cb.equal(path, UUID.fromString((String) value)));
            else
                return (cb.equal(path, Double.parseDouble((String) value)));
        }
    }

    /**
     * <h1>Получение пути для запроса</h1>
     *
     * @param filter фильтер
     * @param root   корень запроса
     *
     * @return путь
     */
    private Path getAttributePath(Filter<?> filter, Root<EntityT> root) {
        // Guard.checkEntityArgument(!filter.getAttributePath().isEmpty(), ApiStatusCode.INVALID_PARAM);
        Path path = root;
        for (String p : filter.getAttributePath()) {
            //  Получаем аттрибут
            SingularAttributeImpl attribute = (SingularAttributeImpl) path.get(p).getModel();
            //  Если аттриубут связка, то джйонем их
            if (attribute.isAssociation()) {
                if (path instanceof From || attribute.isId())
                    path = ((From) path).join(p, JoinType.LEFT);
                else
                    path = path.get(p);
            } else
                path = path.get(p); //  Если простой аттрибут
        }
        // Guard.checkEntityArgumentExists(path, ApiStatusCode.INVALID_PARAM);
        return path;
    }

    /**
     * Возвращает предикат для сортировки
     *
     * @return
     */
    public Sort getSort() {
        return query.getSort();
    }

    public Class getEntityClass() {
        return query.getRoot();
    }

    /**
     * <h1>Создание пейджинированного запроса</h1>
     *
     * @return пейджинированный запрос
     */
    public PageRequest getPageRequest() {
        PageRequest paging = query.getPaging();
        // если запрос без пейджинации
        if (paging == null || paging.getPageSize() == 0)
            return null;

        return createPageRequest(paging.getPageNumber(), paging.getPageSize(), getSort());
    }

    @Override
    public Projection getProjection() {
        return query != null ? query.getProjection() : Projection.LIGHT;
    }

    /**
     * <h1>Создание дополнительных спецификаций</h1>
     *
     * @return коллекция спецификаций
     */
    protected Collection<Specification<EntityT>> createExtraSpecifications() {
        return Collections.EMPTY_LIST;
    }

    protected List<UUID> getUUIDCollection(Filter filter) {
        List<UUID> uuidCollection = new ArrayList<>();
        filter.getValues().forEach(r -> uuidCollection.add(UUID.fromString(r.toString())));
        return uuidCollection;
    }
}
