package com.ws.sa.specifications.base;

import com.ws.sa.enums.Projection;
import com.ws.sa.entities.base.BaseEntity;
import org.springframework.data.jpa.domain.Specification;

/**
 * Created by maxim on 01.10.2016.
 */
public interface RepositoryQuerySpecification<SpecificationT extends Specification<EntityT>, EntityT extends BaseEntity> {

    SpecificationT getQueryPredicate();

    Class getEntityClass();

    Projection getProjection();

}
