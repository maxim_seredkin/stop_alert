package com.ws.sa.specifications.user;

import com.google.common.collect.Lists;
import com.ws.sa.queries.DynamicQuery;
import com.ws.sa.queries.filter.ExtraFilter;
import com.ws.sa.queries.filter.Filter;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import com.ws.sa.security.user.UserDetailsImpl;
import edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;


/**
 * Created by vlad_shelengovskiy on 15.12.2016.
 */
public class UserJpaQuerySpecification extends DefaultJpaQuerySpecification<User> {

    public UserJpaQuerySpecification(DynamicQuery query) {
        super(query);
    }

    @Override
    protected Collection<Specification<User>> createExtraSpecifications() {
        Collection<Filter> filters = getQuery().getFilters(ExtraFilter.class);
        List<Specification<User>> specifications = new ArrayList<>();

        specifications.addAll(createFilterSpecifications(filters));
        specifications.add(createSecuritySpecification(SecurityHelper.getUser()));

        return specifications;
    }

    private List<Specification<User>> createFilterSpecifications(Collection<Filter> filters) {
        if (filters == null)
            return Collections.emptyList();

        return Lists.newArrayList(filters.stream()
                                         .map(this::createFilterSpecification)
                                         .filter(Objects::nonNull)
                                         .toArray(Specification[]::new));
    }

    private Specification createFilterSpecification(Filter filter) {
        if (filter.getAttribute().equals("units"))
            return createUnitSpecification(filter);

        else if (filter.getAttribute().equals("fullName")) {
            return (root, q, cb) -> cb.and(Stream.of(filter.getValue().toString().split(" "))
                                                            .map(str -> "%".concat(str.toLowerCase()).concat("%"))
                                                            .map(str -> cb.or(cb.like(cb.lower(root.get("secondName")), str),
                                                                              cb.like(cb.lower(root.get("middleName")), str),
                                                                              cb.like(cb.lower(root.get("firstName")), str)))
                                                            .toArray(Predicate[]::new));
        }
        return null;
    }

    private Specification<User> createUnitSpecification(Filter filter) {
        return (root, q, cb) -> {
            Subquery subQ = q.subquery(Unit.class);
            Root subRoot = subQ.from(Unit.class);
            return root.join("units").get("id").in(subQ.select(subRoot.get("id")).where(cb.or(
                    getUUIDCollection(filter).stream()
                                             .map(id -> cb.or(cb.equal(subRoot.get("id"), id),
                                                              cb.like(subRoot.get("path"), "%".concat(id.toString()).concat("%"))))
                                             .toArray(Predicate[]::new))));
        };
    }

    private Specification<User> createSecuritySpecification(UserDetailsImpl user) {
        return (root, q, cb) -> cb.equal(root.get("id"), user.getId()).not();
    }

}

