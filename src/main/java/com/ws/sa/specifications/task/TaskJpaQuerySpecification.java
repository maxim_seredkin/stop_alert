package com.ws.sa.specifications.task;

import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.entities.mail.Mail;
import com.ws.sa.entities.task.Task;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.queries.DynamicQuery;
import com.ws.sa.queries.filter.ExtraFilter;
import com.ws.sa.queries.filter.Filter;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.security.user.UserDetailsImpl;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Created by Vladislav Shelengovskiy on 26.02.2017.
 */
public class TaskJpaQuerySpecification extends DefaultJpaQuerySpecification<Task> {

    public TaskJpaQuerySpecification(DynamicQuery query) {
        super(query);
    }

    @Override
    protected Collection<Specification<Task>> createExtraSpecifications() {
        Collection<Filter> filters = getQuery().getFilters(ExtraFilter.class);
        List<Specification<Task>> specifications = new ArrayList<>();
        if (filters != null) {
            filters.forEach((Filter filter) -> {
                if (filter.getAttribute().equals("author")) {
                    specifications.add((root, q, cb) -> cb.and(Stream.of(filter.getValue().toString().split(" "))
                                                                     .map(str -> "%".concat(str.toLowerCase()).concat("%"))
                                                                     .map(str -> cb.or(cb.like(cb.lower(root.get("author").get("secondName")), str),
                                                                                       cb.like(cb.lower(root.get("author").get("middleName")), str),
                                                                                       cb.like(cb.lower(root.get("author").get("firstName")), str)))
                                                                     .toArray(Predicate[]::new)));
                } else if (filter.getAttribute().equals("units")) {
                    specifications.add((root, q, cb) -> {
                        Subquery subQ = q.subquery(Unit.class);
                        Root subRoot = subQ.from(Unit.class);
                        return cb.and((Predicate[]) getUUIDCollection(filter).stream()
                                                                             .map(id -> root.join("units").get("id").in(
                                                                                     subQ.select(subRoot.get("id")).where(cb.or(
                                                                                             cb.equal(subRoot.get("id"), id),
                                                                                             cb.like(subRoot.get("path"), "%".concat(id.toString()).concat("%"))))))
                                                                             .toArray(Predicate[]::new));
                    });
                } else if (filter.getAttribute().equals("employeeTags")) {
                    specifications.add((root, q, cb) -> {
                        Subquery subQ = q.subquery(EmployeeTag.class);
                        Root subRoot = subQ.from(EmployeeTag.class);

                        return cb.and((Predicate[]) getUUIDCollection(filter).stream()
                                                                             .map(id -> root.join("employeeTags").get("id").in(
                                                                                     subQ.select(subRoot.get("id")).where(cb.or(
                                                                                             cb.equal(subRoot.get("id"), id),
                                                                                             cb.like(subRoot.get("path"), "%".concat(id.toString()).concat("%"))))))
                                                                             .toArray(Predicate[]::new));
                    });
                } else if (filter.getAttribute().equals("employee")) {
                    specifications.add((root, q, cb) -> {
                        Subquery subQ = q.subquery(Mail.class);
                        Root subRoot = subQ.from(Mail.class);

                        return cb.exists(subQ.select(subRoot.get("id")).where(cb.and(
                                cb.equal(subRoot.get("employee").get("id"), UUID.fromString(filter.getValue().toString())),
                                cb.equal(subRoot.get("task").get("id"), root.get("id")))));
                    });
                } else if (filter.getAttribute().equals("unit")) {
                    specifications.add((root, q, cb) -> {
                        Subquery subQ = q.subquery(Mail.class);
                        Root subRoot = subQ.from(Mail.class);

                        return cb.exists(subQ.select(subRoot.get("id")).where(cb.and(
                                cb.or(cb.equal(subRoot.get("unit").get("id"), UUID.fromString(filter.getValue().toString())),
                                      cb.like(subRoot.get("unitPath"), "%".concat(filter.getValue().toString()).concat("%"))),
                                cb.equal(subRoot.get("task").get("id"), root.get("id")))));
                    });
                }
            });
        }
        addSecuritySpecification(specifications);
        return specifications;
    }

    private void addSecuritySpecification(List<Specification<Task>> specifications) {
        if (SecurityHelper.isAdministrator()) return;

        UserDetailsImpl user = SecurityHelper.getUser();

        specifications.add((root, q, cb) -> cb.equal(root.get("author").get("id"), user.getId()));
    }
}
