package com.ws.sa.dtos.redirect;

import com.ws.sa.enums.RedirectType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class RedirectDTO {

    private RedirectType redirectType;

    private UUID mailTemplate;

    private UUID mockPage;

    private String customUri;

}
