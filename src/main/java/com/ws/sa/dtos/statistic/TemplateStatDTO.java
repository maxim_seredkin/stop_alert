package com.ws.sa.dtos.statistic;

import com.ws.sa.dtos.mail_template.MailTemplateLightDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 29.03.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TemplateStatDTO {

    private MailTemplateLightDTO template;
    private long countMailIncorrect;

}
