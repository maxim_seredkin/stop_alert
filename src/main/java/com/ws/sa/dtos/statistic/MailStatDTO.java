package com.ws.sa.dtos.statistic;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by Vladislav Shelengovskiy on 29.03.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class MailStatDTO {

    private double percentMailCorrect;

    private double percentMailIncorrect;

    private long countMail;

    private long countMailCorrect;

    private long countMailIncorrect;

    private List<TemplateStatDTO> templateStats;

    public MailStatDTO(long countMail, long countMailCorrect, List<TemplateStatDTO> templateStats) {
        this.countMail = countMail;
        this.countMailCorrect = countMailCorrect;
        this.countMailIncorrect = countMail - countMailCorrect;
        this.percentMailIncorrect = countMail > 0 ? new BigDecimal(countMailIncorrect / (double) countMail * 100.0).setScale(2, RoundingMode.UP).doubleValue() : 0.0;
        this.percentMailCorrect = 100 - percentMailIncorrect;
        this.templateStats = templateStats;
    }
}
