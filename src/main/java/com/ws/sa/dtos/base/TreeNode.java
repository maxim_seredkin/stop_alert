package com.ws.sa.dtos.base;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.ws.sa.entities.base.HierarchyDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Maxim Seredkin
 */
@Getter
@Setter
public class TreeNode<NodeT extends HierarchyDTO<ParentT>, ParentT extends BaseDTO> {

    @JsonUnwrapped
    private final NodeT item;
    private final List<TreeNode<NodeT, ParentT>> nodes;

    public TreeNode(NodeT item) {
        this.item = item;
        nodes = new ArrayList<>();
    }

    public ParentT getParent() {
        return item.getParent();
    }

    public void addNode(TreeNode<NodeT, ParentT> node) {
        this.nodes.add(node);
    }

    public void addNodes(Collection<TreeNode<NodeT, ParentT>> nodes) {
        this.nodes.addAll(nodes);
    }

}
