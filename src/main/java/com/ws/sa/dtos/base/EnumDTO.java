package com.ws.sa.dtos.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by valen on 24.02.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnumDTO {

    private String name;

    private String value;

}
