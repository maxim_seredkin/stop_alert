package com.ws.sa.dtos.base;

import com.ws.sa.enums.Operation;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 28.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
@NoArgsConstructor
public abstract class AccessDTO extends BaseDTO {

    List<Operation> operations = new ArrayList<>();

    public AccessDTO(UUID id) {
        super(id);
    }

}
