package com.ws.sa.dtos.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * Created by Seredkin M. on 07.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseDTO implements DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected UUID id;

}
