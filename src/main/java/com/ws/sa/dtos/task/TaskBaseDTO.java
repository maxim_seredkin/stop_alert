package com.ws.sa.dtos.task;

import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;

/**
 * Created by Vladislav Shelengovskiy on 28.03.2017.
 */
@Getter
public class TaskBaseDTO extends AccessDTO {

}
