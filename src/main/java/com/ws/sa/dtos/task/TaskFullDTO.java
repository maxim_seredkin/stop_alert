package com.ws.sa.dtos.task;

import com.ws.sa.enums.TaskStatus;
import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.mail_template.MailTemplateLightDTO;
import com.ws.sa.dtos.redirect.RedirectDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class TaskFullDTO extends AccessDTO {

    private String name;

    private String author;

    private Date startDate;

    private Date endDate;

    private Date createdDate;

    private List<UnitLightDTO> units;

    private List<EmployeeTagLightDTO> employeeTags;

    private TaskStatus status;

    private MailTemplateLightDTO template;

    private RedirectDTO redirect;

}
