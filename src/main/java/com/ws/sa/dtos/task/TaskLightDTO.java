package com.ws.sa.dtos.task;

import com.ws.sa.enums.TaskStatus;
import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class TaskLightDTO extends AccessDTO {

    private String name;

    private String author;

    private TaskStatus status;

}
