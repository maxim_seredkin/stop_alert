package com.ws.sa.dtos.mail_template;

import com.ws.sa.enums.EntityStatus;
import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.mail_template_group.MailTemplateGroupLightDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class MailTemplateFullDTO extends BaseDTO {

    private String name;

    private String description;

    private EntityStatus status;

    private String subject;

    private String path;

    private String fromPersonal;

    private String fromAddress;

    private String redirectUri;

    private MailTemplateGroupLightDTO group;

}
