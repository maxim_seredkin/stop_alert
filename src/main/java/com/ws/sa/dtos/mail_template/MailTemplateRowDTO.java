package com.ws.sa.dtos.mail_template;

import com.ws.sa.enums.EntityStatus;
import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.mail_template_group.MailTemplateGroupLightDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Data
@NoArgsConstructor
public class MailTemplateRowDTO extends BaseDTO {

    private String name;

    private String description;

    private EntityStatus status;

    private String subject;

    private String fromPersonal;

    private String fromAddress;

    private String redirectUri;

    private MailTemplateGroupLightDTO group;

}
