package com.ws.sa.dtos.mail_template;

import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.mail_template_group.MailTemplateGroupLightDTO;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * @author maxim
 * @since 23.03.2017
 */
@Getter
@Setter
public class MailTemplateExampleDTO extends BaseDTO {

    private String name;
    private String description;
    private String subject;
    private String body;
    private MailTemplateGroupLightDTO group;

    @Builder
    public MailTemplateExampleDTO(UUID id,
                                  String name,
                                  String description,
                                  String subject,
                                  String body,
                                  MailTemplateGroupLightDTO group) {
        super(id);
        this.name = name;
        this.description = description;
        this.subject = subject;
        this.body = body;
        this.group = group;
    }

}
