package com.ws.sa.dtos.authority;

import com.ws.sa.dtos.base.BaseDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@Getter
@Setter
@NoArgsConstructor
public class AuthorityRowDTO extends BaseDTO {

    private String authority;

    private String displayName;

}