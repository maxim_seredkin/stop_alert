package com.ws.sa.dtos.setting;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 19.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {

    private String uri;
}
