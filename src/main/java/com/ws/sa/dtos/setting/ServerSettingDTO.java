package com.ws.sa.dtos.setting;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 30.03.2017.
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServerSettingDTO {

    private String host;

    private Integer port;

    private String username;

    private String password;

    private Boolean useSSL;

    private Boolean anonymous;

}
