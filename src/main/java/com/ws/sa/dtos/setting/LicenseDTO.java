package com.ws.sa.dtos.setting;

import com.ws.sa.enums.setting.ActivateStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 02.04.2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LicenseDTO {

    private String orgName;

    private String dateEnd;

    private ActivateStatus status;

    public boolean isSuccessful(){
        return status.equals(ActivateStatus.SUCCESSFUL);
    }
}
