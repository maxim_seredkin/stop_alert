package com.ws.sa.dtos.employee;

import com.ws.sa.enums.EntityStatus;
import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeRowDTO extends AccessDTO {

    private String fullName;

    private String shortName;

    private String email;

    private String position;

    private UnitLightDTO unit;

    private EntityStatus status;

}
