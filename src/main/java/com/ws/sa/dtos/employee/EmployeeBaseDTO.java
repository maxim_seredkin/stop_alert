package com.ws.sa.dtos.employee;

import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;

/**
 * Created by Vladislav Shelengovskiy on 28.03.2017.
 */
@Getter
public class EmployeeBaseDTO extends AccessDTO {
}
