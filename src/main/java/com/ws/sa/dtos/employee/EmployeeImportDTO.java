package com.ws.sa.dtos.employee;

import com.google.common.collect.Sets;
import com.ws.sa.errors.EmployeeError;
import com.ws.sa.dtos.unit.UnitImportDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

/**
 * @author Maxim Seredkin
 * @since 22.06.2017
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeImportDTO {

    private UUID id;

    private String secondName;

    private String firstName;

    private String middleName;

    private String email;

    private String position;

    private UnitImportDTO unit;

    //ошибки
    private Set<EmployeeError> errors = Sets.newHashSet();

}
