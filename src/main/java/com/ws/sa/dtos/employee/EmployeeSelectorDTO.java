package com.ws.sa.dtos.employee;

import com.ws.sa.dtos.base.BaseDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Seredkin M. on 24.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeSelectorDTO extends BaseDTO {

    private String fullName;
    private String unitName;

}
