package com.ws.sa.dtos.employee;

import com.ws.sa.enums.EntityStatus;
import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeFullDTO extends AccessDTO {

    private String secondName;

    private String firstName;

    private String middleName;

    private String fullName;

    private String shortName;

    private String email;

    private String position;

    private UnitLightDTO unit;

    private List<EmployeeTagLightDTO> employeeTags;

    private EntityStatus status;

}
