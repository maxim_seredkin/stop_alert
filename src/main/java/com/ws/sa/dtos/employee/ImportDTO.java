package com.ws.sa.dtos.employee;

import com.ws.sa.dtos.unit.UnitImportDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

/**
 * @author Maxim Seredkin
 * @since 22.06.2017
 */
@Data
@AllArgsConstructor
public class ImportDTO {

    private Collection<UnitImportDTO> invalidUnits;

    private Collection<EmployeeImportDTO> invalidEmployees;

    private Collection<EmployeeImportDTO> employees;

}
