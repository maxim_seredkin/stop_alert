package com.ws.sa.dtos.mock_page;

import com.ws.sa.dtos.base.BaseDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class MockPageFullDTO extends BaseDTO {

    private String name;

    private String redirectUri;

}
