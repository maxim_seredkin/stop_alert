package com.ws.sa.dtos.mock_page;

import com.ws.sa.dtos.base.BaseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Data
@NoArgsConstructor
public class MockPageLightDTO extends BaseDTO {

    private String name;

    private String redirectUri;

}
