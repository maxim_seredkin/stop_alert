package com.ws.sa.dtos.unit;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.enums.EntityStatus;
import com.ws.sa.entities.base.HierarchyDTO;
import com.ws.sa.dtos.user.UserLightDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Objects;

/**
 * Created by Vladislav Shelengovksiy on 31.01.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitNodeDTO extends AccessDTO implements HierarchyDTO<UnitNodeDTO> {
    // наименование
    private String name;

    @JsonIgnore
    private UnitNodeDTO parent;

    private Collection<UserLightDTO> managers;

    private Collection<UserLightDTO> officers;

    private Integer countEmployees;

    private EntityStatus status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitNodeDTO that = (UnitNodeDTO) o;
        return Objects.equals(id, that.id);
    }

}
