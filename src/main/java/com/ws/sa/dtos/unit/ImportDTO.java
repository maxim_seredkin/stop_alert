package com.ws.sa.dtos.unit;

import com.ws.sa.dtos.base.TreeNode;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 02.06.2017.
 */
@Data
@AllArgsConstructor
public class ImportDTO {

    private UnitLightDTO root;

    private Collection<UnitImportDTO> invalids;

    private Collection<? extends TreeNode> units;

    private Collection<UnitUserDTO> officers;

    private Collection<UnitUserDTO> managers;
}
