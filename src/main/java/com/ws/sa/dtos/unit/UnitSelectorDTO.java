package com.ws.sa.dtos.unit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Seredkin M. on 24.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitSelectorDTO {

    // идентификатор
    private UUID id;

    // наименование
    private String name;

}
