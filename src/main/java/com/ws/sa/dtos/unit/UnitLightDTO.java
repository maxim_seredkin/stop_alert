package com.ws.sa.dtos.unit;

import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovksiy on 31.01.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitLightDTO extends AccessDTO {

    // наименование
    private String name;

    public UnitLightDTO(UUID id, String name) {
        super(id);
        this.name = name;
    }
    
}
