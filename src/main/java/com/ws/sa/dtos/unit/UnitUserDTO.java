package com.ws.sa.dtos.unit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 04.06.2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnitUserDTO {

    private UUID id;

    private String fullName;

    private String path;
}
