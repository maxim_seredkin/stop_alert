package com.ws.sa.dtos.unit;

import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.enums.EntityStatus;
import com.ws.sa.dtos.user.UserLightDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 02.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitFullDTO extends AccessDTO {

    // наименование
    private String name;

    private UnitLightDTO parent;

    private Collection<UnitLightDTO> path;

    private Collection<UserLightDTO> managers;

    private Collection<UserLightDTO> officers;

    private Collection<UnitLightDTO> childs;

    private Integer countEmployees;

    private EntityStatus status;

}
