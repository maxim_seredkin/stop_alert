package com.ws.sa.dtos.unit;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Sets;
import com.ws.sa.errors.UnitError;
import com.ws.sa.entities.base.HierarchyDTO;
import com.ws.sa.dtos.base.BaseDTO;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 01.06.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitImportDTO extends BaseDTO implements HierarchyDTO<UnitImportDTO> {

    // наименование
    private String name;

    @JsonIgnore
    //родительский элемент
    private UnitImportDTO parent;

    //ошибки
    private Set<UnitError> errors = Sets.newHashSet();

    @Builder
    public UnitImportDTO(UUID id, String name, UnitImportDTO parent, Set<UnitError> errors) {
        super(id);
        this.name = name;
        this.parent = parent;
        this.errors = errors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnitImportDTO that = (UnitImportDTO) o;
        return Objects.equals(id, that.id);
    }
}
