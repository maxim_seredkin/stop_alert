package com.ws.sa.dtos.unit;

import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Vladislav Shelengovksiy on 31.01.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitRowDTO extends AccessDTO {

    // наименование
    private String name;

}
