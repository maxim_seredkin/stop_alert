package com.ws.sa.dtos.user;

import com.ws.sa.enums.UserStatus;
import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.dtos.authority.AuthorityLightDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@Getter
@Setter
@NoArgsConstructor
public class UserRowDTO extends BaseDTO {

    private String shortName;

    private String fullName;

    private String username;

    private AuthorityLightDTO authority;

    private Collection<UnitLightDTO> units;

    private UserStatus status;

}
