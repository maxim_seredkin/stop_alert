package com.ws.sa.dtos.user;

import com.ws.sa.dtos.base.DTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * @author Maxim Seredkin
 * @since 01.02.2017
 */
@Getter
@Setter
@NoArgsConstructor
public class UserLightDTO implements DTO {

    private UUID id;

    private String shortName;

    private String fullName;

    private String username;

}
