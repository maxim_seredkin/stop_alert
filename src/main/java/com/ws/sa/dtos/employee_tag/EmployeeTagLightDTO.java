package com.ws.sa.dtos.employee_tag;

import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeTagLightDTO extends AccessDTO {

    //наименование
    private String name;

}
