package com.ws.sa.dtos.employee_tag;

import com.ws.sa.enums.EntityStatus;
import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeTagFullDTO extends AccessDTO {

    //наименование
    private String name;

    private Collection<EmployeeTagLightDTO> path;

    private boolean used;

    private EntityStatus status;

}
