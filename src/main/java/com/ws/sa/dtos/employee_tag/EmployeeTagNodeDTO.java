package com.ws.sa.dtos.employee_tag;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ws.sa.entities.base.HierarchyDTO;
import com.ws.sa.dtos.base.AccessDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;


/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeTagNodeDTO extends AccessDTO implements HierarchyDTO<EmployeeTagNodeDTO> {

    //наименование
    private String name;

    @JsonIgnore
    private EmployeeTagNodeDTO parent;

    private boolean used;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeTagNodeDTO that = (EmployeeTagNodeDTO) o;
        return Objects.equals(id, that.id);
    }

}
