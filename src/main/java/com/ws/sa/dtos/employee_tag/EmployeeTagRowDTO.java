package com.ws.sa.dtos.employee_tag;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.enums.EntityStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class EmployeeTagRowDTO extends AccessDTO {

    private String name;

    @JsonIgnore
    private EmployeeTagNodeDTO parent;

    private boolean used;

    private EntityStatus status;

}
