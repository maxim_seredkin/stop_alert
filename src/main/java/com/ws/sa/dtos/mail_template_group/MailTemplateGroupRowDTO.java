package com.ws.sa.dtos.mail_template_group;

import com.ws.sa.dtos.base.BaseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
@Data
@NoArgsConstructor
public class MailTemplateGroupRowDTO extends BaseDTO{

    private String name;

}
