package com.ws.sa.dtos.report.task;

import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.mail_template.MailTemplateRowDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.enums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 14.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TaskReportFullDTO extends BaseDTO {

    private UUID id;
    private String name;
    private String fullName;
    private TaskStatus status;
    private Date startDate;
    private Date endDate;
    private Collection<UnitLightDTO> units;
    private Collection<EmployeeTagLightDTO> employeeTags;
    private MailTemplateRowDTO template;
    private Result taskResult;
}
