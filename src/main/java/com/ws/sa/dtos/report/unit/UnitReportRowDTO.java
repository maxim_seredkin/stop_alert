package com.ws.sa.dtos.report.unit;

import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.dtos.user.UserLightDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.enums.EntityStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

/**
 * @author Maxim Seredkin
 * @since 29.03.2017
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitReportRowDTO extends BaseDTO {

    private String name;
    private Collection<UserLightDTO> officers;
    private Collection<UserLightDTO> managers;
    private EntityStatus status;
    private Result result;
}
