package com.ws.sa.dtos.report.employee;

import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.dtos.base.DTO;
import lombok.Data;

import java.util.Collection;
import java.util.UUID;

/**
 * @author Maxim Seredkin
 * @since 21.03.2017
 */
@Data
public class EmployeeReportFullDTO implements DTO {

    private UUID id;
    private String fullName;
    private String email;
    private String position;
    private UnitLightDTO unit;
    private Collection<EmployeeTagLightDTO> employeeTags;
    private Result result;
}
