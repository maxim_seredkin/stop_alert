package com.ws.sa.dtos.report.unit;

import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.dtos.mail_template.MailTemplateRowDTO;
import com.ws.sa.entities.base.Result;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Created by Vladislav Shelengovskiy on 17.09.2017.
 */
@Setter
@Getter
@NoArgsConstructor
public class UnitTaskResultDTO extends AccessDTO {

    private String name;
    private String shortName;
    private Date startDate;
    private Date endDate;
    private MailTemplateRowDTO template;
    private Result taskResult;

}
