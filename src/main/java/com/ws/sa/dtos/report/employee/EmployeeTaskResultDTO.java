package com.ws.sa.dtos.report.employee;

import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.dtos.mail_template.MailTemplateRowDTO;
import com.ws.sa.dtos.report.AddInfoFullDTO;
import com.ws.sa.enums.task_report.TaskResultType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Vladislav Shelengovskiy on 14.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmployeeTaskResultDTO extends AccessDTO {

    private String name;
    private String shortName;
    private Date startDate;
    private Date endDate;
    private MailTemplateRowDTO template;
    private TaskResultType result;
    private Collection<AddInfoFullDTO> addInfo;

}
