package com.ws.sa.dtos.report.task;

import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.mail_template.MailTemplateRowDTO;
import com.ws.sa.entities.base.Result;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 14.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskReportRowDTO extends BaseDTO {

    private UUID id;
    private String name;
    private String shortName;
    private Date startDate;
    private Date endDate;
    private MailTemplateRowDTO template;
    private Result taskResult;
}
