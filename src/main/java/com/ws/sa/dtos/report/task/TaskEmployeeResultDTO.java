package com.ws.sa.dtos.report.task;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.report.AddInfoFullDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.enums.task_report.TaskResultType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

import static com.ws.sa.enums.task_report.TaskResultType.FAIL;
import static com.ws.sa.enums.task_report.TaskResultType.SUCCESS;

/**
 * @author Maxim Seredkin
 */
@Getter
@Setter
@NoArgsConstructor
public class TaskEmployeeResultDTO extends AccessDTO {

    private String shortName;
    private String fullName;
    private String email;
    private Collection<EmployeeTagLightDTO> employeeTags;
    private UnitLightDTO unit;

    private TaskResultType result;
    private Collection<AddInfoFullDTO> addInfo;


    @JsonIgnore
    public boolean isFailResult() {
        return result.equals(FAIL);
    }

    @JsonIgnore
    public boolean isSuccessResult() {
        return result.equals(SUCCESS);
    }

    public TaskEmployeeResultDTO(UUID id,
                                 String shortName,
                                 String fullName,
                                 String email,
                                 Collection<EmployeeTagLightDTO> employeeTags,
                                 UnitLightDTO unit,
                                 TaskResultType result,
                                 Collection<AddInfoFullDTO> addInfo) {
        super(id);
        this.shortName = shortName;
        this.fullName = fullName;
        this.email = email;
        this.employeeTags = employeeTags;
        this.unit = unit;
        this.result = result;
        this.addInfo = addInfo;
    }
}
