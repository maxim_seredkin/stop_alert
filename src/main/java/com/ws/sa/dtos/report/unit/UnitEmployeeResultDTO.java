package com.ws.sa.dtos.report.unit;

import com.ws.sa.dtos.base.AccessDTO;
import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.entities.base.Result;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

/**
 * Created by Vladislav Shelengovskiy on 17.09.2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitEmployeeResultDTO extends AccessDTO {

    private String shortName;
    private String email;
    private UnitLightDTO unit;
    private Collection<EmployeeTagLightDTO> employeeTags;
    private Result result;

}
