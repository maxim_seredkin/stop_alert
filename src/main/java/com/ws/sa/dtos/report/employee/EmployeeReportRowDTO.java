package com.ws.sa.dtos.report.employee;

import com.ws.sa.dtos.employee_tag.EmployeeTagLightDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.dtos.base.DTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

/**
 * @author maxim
 * @since 29.03.2017
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeReportRowDTO implements DTO {

    private UUID id;
    private String shortName;
    private String email;
    private UnitLightDTO unit;
    private Collection<EmployeeTagLightDTO> employeeTags;
    private Result result;
}
