package com.ws.sa.dtos.report.unit;

import com.ws.sa.dtos.base.BaseDTO;
import com.ws.sa.dtos.unit.UnitLightDTO;
import com.ws.sa.dtos.user.UserLightDTO;
import com.ws.sa.entities.base.Result;
import com.ws.sa.dtos.base.DTO;
import com.ws.sa.enums.EntityStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Maxim_Seredkin on 21-Mar-17.
 */
@Getter
@Setter
@NoArgsConstructor
public class UnitReportFullDTO extends BaseDTO {

    private String name;
    private Collection<UserLightDTO> officers;
    private Collection<UserLightDTO> managers;
    private EntityStatus status;
    private Collection<UnitLightDTO> path;
    private Collection<UnitLightDTO> childs;
    private Result result;
}
