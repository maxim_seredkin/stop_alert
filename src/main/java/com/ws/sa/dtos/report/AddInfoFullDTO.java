package com.ws.sa.dtos.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Maxim Seredkin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddInfoFullDTO {

    private Date transitionDate;
    private String ipAddress;
    private String os;
    private String browser;

}
