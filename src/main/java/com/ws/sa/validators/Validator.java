package com.ws.sa.validators;


import com.ws.sa.exceptions.InitializationException;
import com.ws.sa.exceptions.ValidationException;

import java.io.File;
import java.io.IOException;

public interface Validator {
    void validate(File file) throws ValidationException, IOException;

    void init() throws InitializationException;

    boolean isEnabled();
}
