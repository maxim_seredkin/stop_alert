package com.ws.sa.validators;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ValidatorsService {

    private static final List<? extends Validator> ALL_VALIDATORS =
            new ArrayList<>(Arrays.asList(new VelocityParserValidatorImpl(),
                                          new RegexValidatorImpl()));
    private static boolean initialized = false;

    public static void init() throws Exception {
        for (Validator v : ALL_VALIDATORS)
            v.init();
        initialized = true;
    }

    public static List<? extends Validator> getAllValidators() {
        if (!initialized) {
            throw new RuntimeException("Service not initialized!");
        }
        return ALL_VALIDATORS;
    }
}
