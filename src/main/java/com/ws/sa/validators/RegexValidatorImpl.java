package com.ws.sa.validators;

import com.ws.sa.exceptions.InitializationException;
import com.ws.sa.exceptions.ValidationException;
import com.ws.sa.utils.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexValidatorImpl implements Validator {

    private static final String LINES_DELIMITER = "\n";
    private static List<Pattern> patterns = new ArrayList<>();
    private boolean enabled = false;

    public void validate(File file) throws ValidationException, IOException {

        if (!enabled) {
            return;
        }

        List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()), StandardCharsets.UTF_8);
        for (Pattern pattern : patterns) {
            String text = StringUtils.join(lines, LINES_DELIMITER);
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                String found = matcher.group(0);
                int startAt = text.indexOf(found);
                int line = text.substring(0, startAt).split(LINES_DELIMITER).length;
                String[] parts = found.split(LINES_DELIMITER);
                int column = lines.get(line - 1).indexOf(parts[0]) + 1;
                throw createException(file.getAbsolutePath(), pattern, found, line, column);
            }
        }
    }

    public void init() throws InitializationException {
        enabled = false;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    private ValidationException createException(String file, Pattern pattern, String found,
                                                int line, int column) throws ValidationException {
        return new ValidationException(
                "Encountered \"" + pattern.toString() + "\" at " + file + "[line " + line
                + ", column " + column + "]" + LINES_DELIMITER + "    " + found);
    }
}
