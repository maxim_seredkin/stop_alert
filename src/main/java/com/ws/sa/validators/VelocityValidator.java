package com.ws.sa.validators;

import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

import static java.lang.String.format;

@NoArgsConstructor
public class VelocityValidator {

    private static final Logger log = LoggerFactory.getLogger(VelocityValidator.class);

    public static boolean validate(File file) {
        if (!isVelocityFile(file)) {
            return false;
        }

        try {
            ValidatorsService.init();
        } catch (Exception exception) {
            return false;
        }

        List<? extends Validator> validators = ValidatorsService.getAllValidators();
        int errors = 0;
        for (Validator validator : validators) {
            if (validator.isEnabled()) {
                try {
                    validator.validate(file);
                    log.info("File OK: " + getFilePrintPath(file));
                } catch (Exception e) {
                    errors++;
                    log.error("Error in file " + getFilePrintPath(file));
                    log.error("    " + e.getMessage());
                }
            }
        }
        if (errors == 0) {
            log.info("No errors found in given path");
        } else if (errors > 0) {
            log.error(format("Done, Found %d errors", errors));
            return false;
        }
        return true;
    }

    private static String getFilePrintPath(File f) {
        return f.getAbsolutePath();
    }

    private static boolean isVelocityFile(File file) {
        return file.isFile() && file.getAbsolutePath().endsWith(".vm");
    }
}

