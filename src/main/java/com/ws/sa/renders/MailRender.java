package com.ws.sa.renders;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static org.springframework.ui.velocity.VelocityEngineUtils.mergeTemplateIntoString;

/**
 * @author maxim
 * @since 24.03.2017
 */
@Component
public class MailRender {

    private final VelocityEngine velocityEngine;

    @Autowired
    public MailRender(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    public String render(String path, Map<String, Object> params) {
        //  рендеринг тела письма
        return mergeTemplateIntoString(velocityEngine, path, "UTF-8", params);
    }

}
