package com.ws.sa.enums;

/**
 * @author Maxim Seredkin
 * @since 02.02.2017
 */
public enum Projection {

    LIGHT,
    ROW,
    SELECTOR,
    FULL;

}
