package com.ws.sa.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Vladislav Shelengovskiy on 28.03.2017.
 */
@Getter
public enum Role {

    ADMINISTRATOR("ROLE_ADMINISTRATOR", "Администратор"),
    SECURITY_OFFICER("ROLE_SECURITY_OFFICER", "Сотрудник службы безопасности"),
    MANAGER("ROLE_MANAGER", "Менеджер");

    private String authority;
    private String displayName;

    Role(String authority, String displayName) {
        this.authority = authority;
        this.displayName = displayName;
    }

    public static Role findByAuthority(String authority) {
        return Arrays.stream(Role.values())
                     .filter(auth -> Objects.equals(auth.authority, authority))
                     .findAny()
                     .orElse(null);
    }
}
