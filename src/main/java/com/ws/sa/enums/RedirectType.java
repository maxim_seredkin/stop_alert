package com.ws.sa.enums;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 *
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
public enum RedirectType {

    TEMPLATE_LINK,
    MOCK_PAGE_LINK,
    CUSTOM_LINK;

}
