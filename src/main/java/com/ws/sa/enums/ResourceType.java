package com.ws.sa.enums;

import lombok.Getter;

/**
 * Created by Vladislav Shelengovskiy on 17.07.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
public enum  ResourceType {
    TEMPLATE("/templates/", ".vm"),
    IMPORT("/import/", ".csv"),
    LICENSE("/license/", ".lic"),
    KEY("/license/", ".key"),
    PROPERTY("", ".properties")
    ;

    private String folder;

    private String extension;

    ResourceType(String folder, String extension) {
        this.extension = extension;
        this.folder = folder;
    }
}
