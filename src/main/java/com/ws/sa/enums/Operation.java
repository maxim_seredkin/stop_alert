package com.ws.sa.enums;

/**
 * Created by Vladislav Shelengovskiy on 28.03.2017.
 */
public enum Operation {
    CREATE,
    READ,
    UPDATE,
    DELETE,
}
