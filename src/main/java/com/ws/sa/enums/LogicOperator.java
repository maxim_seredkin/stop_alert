package com.ws.sa.enums;

/**
 * @author Maxim Seredkin
 */
public enum LogicOperator {

    EQ, // equals
    NE, // not equals
    LT, // less than
    GT, // greater than
    LE, // less than or equal to
    GE, // greater than or equal to
    BETWEEN, // операция поиска между двумя значениями,
    LIKE, // поиск по подстроке
    IN // содержит хотя бы 1 значение

}
