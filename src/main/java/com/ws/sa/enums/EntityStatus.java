package com.ws.sa.enums;

/**
 * @author Maxim Seredkin
 */
public enum EntityStatus {
    ENABLED,
    DELETED
}
