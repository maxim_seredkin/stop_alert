package com.ws.sa.enums.setting;

import lombok.Getter;

/**
 * Created by Vladislav Shelengovskiy on 30.03.2017.
 */
@Getter
public enum AppSettingProperty {

    MAIL_ANONYMOUS("app.setting.mail.anonymous"),
    ;


    private String propName;

    AppSettingProperty(String propName) {
        this.propName = propName;
    }
}
