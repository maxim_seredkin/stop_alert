package com.ws.sa.enums.setting;

/**
 * Created by Vladislav Shelengovskiy on 02.04.2017.
 */
public enum ActivateStatus {
    SUCCESSFUL,
    FAILURE,
    EXPIRED,
}
