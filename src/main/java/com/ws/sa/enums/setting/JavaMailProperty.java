package com.ws.sa.enums.setting;

import lombok.Getter;

/**
 * Created by Vladislav Shelengovskiy on 31.03.2017.
 */
@Getter
public enum  JavaMailProperty {

    PROTOCOL("mail.transport.protocol"),
    HOST("mail.smtp.host"),
    PORT("mail.smtp.port"),
    USERNAME("mail.smtp.username"),
    PASSWORD("mail.smtp.password"),
    AUTH("mail.smtp.auth"),
    SSL("mail.smtp.ssl.enable");

    private String propName;

    JavaMailProperty(String propName) {
        this.propName = propName;
    }
}
