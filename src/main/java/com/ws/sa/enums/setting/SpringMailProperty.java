package com.ws.sa.enums.setting;

import lombok.Getter;

/**
 * Created by Vladislav Shelengovskiy on 30.03.2017.
 */
@Getter
public enum SpringMailProperty {

    PROTOCOL("spring.mail.protocol"),
    HOST("spring.mail.host"),
    PORT("spring.mail.port"),
    USERNAME("spring.mail.username"),
    PASSWORD("spring.mail.password");

    private String propName;

    SpringMailProperty(String propName) {
        this.propName = propName;
    }
}
