package com.ws.sa.enums.task_report;

/**
 * @author Maxim Seredkin
 */
public enum TaskResultType {
    FAIL,
    SUCCESS
}
