package com.ws.sa.enums;

/**
 * Created by vlad_shelengovskiy on 26.01.2017.
 */
public enum UserStatus {
    ENABLED,
    LOCKED,
    DELETED;

    public static UserStatus getStatus(boolean disabled, boolean locked) {
        if (disabled)
            return UserStatus.DELETED;

        if (locked)
            return UserStatus.LOCKED;

        return UserStatus.ENABLED;
    }
}
