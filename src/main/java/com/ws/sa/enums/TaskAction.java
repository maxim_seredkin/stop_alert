package com.ws.sa.enums;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
public enum TaskAction {
    START,
    PAUSE,
    RESUME,
    EXECUTE,
    CANCEL,
    UPDATE;
}
