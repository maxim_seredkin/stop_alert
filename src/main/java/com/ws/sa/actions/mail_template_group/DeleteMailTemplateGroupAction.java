package com.ws.sa.actions.mail_template_group;

import com.ws.sa.actions.base.BaseDeleteAction;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteMailTemplateGroupAction extends BaseDeleteAction<MailTemplateGroup> {

    @Autowired
    public DeleteMailTemplateGroupAction(MailTemplateGroupService service) {
        super(service);
    }

}
