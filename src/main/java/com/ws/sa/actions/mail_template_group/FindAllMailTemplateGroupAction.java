package com.ws.sa.actions.mail_template_group;

import com.ws.sa.actions.base.BaseFindAllAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindAllMailTemplateGroupAction extends BaseFindAllAction<MailTemplateGroup> {

    @Autowired
    public FindAllMailTemplateGroupAction(DynamicQueryExecutor executor) {
        super(executor);
    }

    @Override
    protected Page<MailTemplateGroup> executeImpl(QueryArgument argument) {
        return executor.findAll(new DefaultJpaQuerySpecification<MailTemplateGroup>(argument.getQuery()));
    }

}
