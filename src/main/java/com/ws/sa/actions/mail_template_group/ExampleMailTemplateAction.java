package com.ws.sa.actions.mail_template_group;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.dtos.mail_template.MailTemplateExampleDTO;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.renders.MailRender;
import com.ws.sa.services.mail_template.MailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by Seredkin M. on 14.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ExampleMailTemplateAction extends BaseAction<UuidArgument, MailTemplateExampleDTO> {

    private final MailRender mailRender;
    private final MailTemplateService service;

    @Autowired
    public ExampleMailTemplateAction(MailRender mailRender, MailTemplateService service) {
        this.mailRender = mailRender;
        this.service = service;
    }

    @Override
    protected MailTemplateExampleDTO executeImpl(UuidArgument argument) {
        MailTemplate mailTemplate = service.findOneExisting(argument.getId());

        Employee employee = createExampleEmployee();
        Map<String, Object> params = createMailParams(employee);

        return createMailTemplateExample(mailTemplate, params);
    }

    private Employee createExampleEmployee() {
        return Employee.builder()
                       .email("test@test.ru")
                       .secondName("Иванов")
                       .firstName("Иван")
                       .middleName("Иванович")
                       .build();
    }

    private Map<String, Object> createMailParams(Employee employee) {
        return ImmutableMap.<String, Object>builder()
                .put("employee", employee)
                .put("link", "http://192.168.0.124:8090/api/transitions/")
                .build();
    }

    private MailTemplateExampleDTO createMailTemplateExample(MailTemplate mailTemplate, Map<String, Object> params) {
        return MailTemplateExampleDTO.builder()
                                     .id(mailTemplate.getId())
                                     .name(mailTemplate.getName())
                                     .description(mailTemplate.getDescription())
                                     .subject(mailTemplate.getSubject())
                                     .body(mailRender.render(mailTemplate.getPath(), params))
                                     .build();
    }

}
