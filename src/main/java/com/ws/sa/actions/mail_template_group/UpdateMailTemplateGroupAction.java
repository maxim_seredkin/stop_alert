package com.ws.sa.actions.mail_template_group;

import com.ws.sa.actions.mail_template_group.arguments.UpdateMailTemplateGroupArgument;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.mail_template_group.MailTemplateGroupForm;
import com.ws.sa.mappers.mail_template_group.MailTemplateGroupMapper;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.ws.sa.errors.ApiStatusCode.ALREADY_EXISTS;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class UpdateMailTemplateGroupAction extends BaseMailTemplateGroupAction<UpdateMailTemplateGroupArgument> {

    @Autowired
    public UpdateMailTemplateGroupAction(MailTemplateGroupMapper mapper,
                                         MailTemplateGroupService service) {
        super(mapper, service);
    }

    @Override
    @Transactional
    public MailTemplateGroup execute(UpdateMailTemplateGroupArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected MailTemplateGroup executeImpl(UpdateMailTemplateGroupArgument argument) {
        UUID mockPageId = argument.getMailTemplateGroupId();
        MailTemplateGroupForm form = argument.getForm();

        Guard.checkEntityArgument(!service.existsByName(form.getName()), ALREADY_EXISTS);

        MailTemplateGroup mailTemplateGroup = service.findOneExisting(mockPageId);
        mapper.updateEntity(form, mailTemplateGroup);

        return service.save(mailTemplateGroup);
    }

}
