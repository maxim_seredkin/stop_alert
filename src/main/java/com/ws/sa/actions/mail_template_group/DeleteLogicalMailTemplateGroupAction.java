package com.ws.sa.actions.mail_template_group;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteLogicalMailTemplateGroupAction extends BaseVoidAction<UuidArgument> {

    private final MailTemplateGroupService service;

    @Autowired
    public DeleteLogicalMailTemplateGroupAction(MailTemplateGroupService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        MailTemplateGroup mailTemplateGroup = service.findOneExisting(argument.getId());
        mailTemplateGroup.delete();
        service.save(mailTemplateGroup);
    }

}
