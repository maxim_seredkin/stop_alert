package com.ws.sa.actions.mail_template_group;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.mail_template_group.arguments.CreateMailTemplateGroupArgument;
import com.ws.sa.actions.mail_template_group.arguments.UpdateMailTemplateGroupArgument;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import static com.ws.sa.actions.mail_template_group.MailTemplateGroupActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class MailTemplateGroupAllocator extends BaseActionAllocator<MailTemplateGroupActions> {

    public MailTemplateGroupAllocator(@Qualifier("createMailTemplateGroupAction") Action<CreateMailTemplateGroupArgument, MailTemplateGroup> createUserAction,
                                      @Qualifier("deleteLogicalMailTemplateGroupAction") Action<UuidArgument, Void> deleteLogicalAction,
                                      @Qualifier("deleteMailTemplateGroupAction") Action<UuidArgument, Void> deleteAction,
                                      @Qualifier("existsMailTemplateGroupAction") Action<QueryArgument, Boolean> existsAction,
                                      @Qualifier("findAllMailTemplateGroupAction") Action<QueryArgument, Page<MailTemplateGroup>> findAllAction,
                                      @Qualifier("findOneMailTemplateGroupAction") Action<UuidArgument, MailTemplateGroup> findOneAction,
                                      @Qualifier("recoveryMailTemplateGroupAction") Action<UuidArgument, Void> recoveryAction,
                                      @Qualifier("updateMailTemplateGroupAction") Action<UpdateMailTemplateGroupArgument, MailTemplateGroup> updateUserAction) {
        super(ImmutableMap.<MailTemplateGroupActions, Action>builder()
                      .put(CREATE, createUserAction)
                      .put(DELETE_LOGICAL, deleteLogicalAction)
                      .put(DELETE, deleteAction)
                      .put(EXISTS, existsAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(RECOVERY, recoveryAction)
                      .put(UPDATE, updateUserAction)
                      .build());
    }

}
