package com.ws.sa.actions.mail_template_group;

import com.ws.sa.actions.mail_template_group.arguments.CreateMailTemplateGroupArgument;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.mail_template_group.MailTemplateGroupForm;
import com.ws.sa.mappers.mail_template_group.MailTemplateGroupMapper;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static com.ws.sa.errors.ApiStatusCode.ALREADY_EXISTS;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CreateMailTemplateGroupAction extends BaseMailTemplateGroupAction<CreateMailTemplateGroupArgument> {

    @Autowired
    public CreateMailTemplateGroupAction(MailTemplateGroupMapper mapper,
                                         MailTemplateGroupService service) {
        super(mapper, service);
    }

    @Override
    @Transactional
    public MailTemplateGroup execute(CreateMailTemplateGroupArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected MailTemplateGroup executeImpl(CreateMailTemplateGroupArgument argument) {
        MailTemplateGroupForm form = argument.getForm();

        Guard.checkEntityArgument(!service.existsByName(form.getName()), ALREADY_EXISTS);

        MailTemplateGroup mailTemplateGroup = mapper.toEntity(form);

        return service.save(mailTemplateGroup);
    }

}
