package com.ws.sa.actions.mail_template_group.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.mail_template_group.MailTemplateGroupForm;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class UpdateMailTemplateGroupArgument implements Argument {

    private final UUID mailTemplateGroupId;
    private final MailTemplateGroupForm form;

    public static UpdateMailTemplateGroupArgument from(UUID mailTemplateGroupId, MailTemplateGroupForm form) {
        return new UpdateMailTemplateGroupArgument(mailTemplateGroupId, form);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}
