package com.ws.sa.actions.mail_template_group.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.mail_template_group.MailTemplateGroupForm;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class CreateMailTemplateGroupArgument implements Argument {

    private final MailTemplateGroupForm form;

    public static CreateMailTemplateGroupArgument from(MailTemplateGroupForm form) {
        return new CreateMailTemplateGroupArgument(form);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}
