package com.ws.sa.actions.mail_template_group;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.mappers.mail_template_group.MailTemplateGroupMapper;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseMailTemplateGroupAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, MailTemplateGroup> {

    protected final MailTemplateGroupMapper mapper;
    protected final MailTemplateGroupService service;

    protected BaseMailTemplateGroupAction(MailTemplateGroupMapper mapper,
                                          MailTemplateGroupService service) {
        this.mapper = mapper;
        this.service = service;
    }

}
