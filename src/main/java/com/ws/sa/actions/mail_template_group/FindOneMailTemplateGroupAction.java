package com.ws.sa.actions.mail_template_group;

import com.ws.sa.actions.base.BaseFindOneAction;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindOneMailTemplateGroupAction extends BaseFindOneAction<MailTemplateGroup> {

    @Autowired
    public FindOneMailTemplateGroupAction(MailTemplateGroupService service) {
        super(service);
    }

}
