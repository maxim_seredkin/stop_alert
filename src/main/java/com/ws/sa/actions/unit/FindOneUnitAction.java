package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.BaseFindOneAction;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.services.unit.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindOneUnitAction extends BaseFindOneAction<Unit> {

    @Autowired
    public FindOneUnitAction(UnitService service) {
        super(service);
    }

}
