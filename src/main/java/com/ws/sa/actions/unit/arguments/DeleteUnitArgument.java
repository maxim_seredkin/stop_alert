package com.ws.sa.actions.unit.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.unit.DeleteUnitForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor(access = PRIVATE)
public class DeleteUnitArgument implements Argument {

    private final UUID id;
    private final DeleteUnitForm form;

    public static DeleteUnitArgument from(UUID userId, DeleteUnitForm form) {
        return DeleteUnitArgument.builder()
                                 .id(userId)
                                 .form(form)
                                 .build();
    }

    @Override
    public boolean validate() {
        return nonNull(id)
               && nonNull(form)
               && form.validate();
    }

}
