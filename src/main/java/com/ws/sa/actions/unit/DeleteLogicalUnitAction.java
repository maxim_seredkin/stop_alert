package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.unit.arguments.DeleteUnitArgument;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.unit.DeleteUnitForm;
import com.ws.sa.services.unit.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteLogicalUnitAction extends BaseVoidAction<DeleteUnitArgument> {

    private final UnitService service;

    @Autowired
    public DeleteLogicalUnitAction(UnitService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(DeleteUnitArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(DeleteUnitArgument argument) {
        UUID unitId = argument.getId();
        DeleteUnitForm form = argument.getForm();

        Unit unit = service.findOneExisting(unitId);

        Guard.checkEntityState(unit.isEnabled(), ApiStatusCode.INVALID_STATUS);
        Guard.checkEntityState(service.notExistsWaitingTasks(unit.getId()), ApiStatusCode.ENTITY_IS_USED);

        service.deleteBranchFromUsers(unit.getId());
        service.setBranchDeleted(unit.getId());

        if (service.countBranchEmployees(unit.getId()) == 0) return;

        Unit successor = service.findOneExisting(form.getSuccessor());

        service.transferBranchEmployees(unit.getId(), successor.getId());
    }

}
