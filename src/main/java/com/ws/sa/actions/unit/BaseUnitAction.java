package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.forms.unit.UnitForm;
import com.ws.sa.services.user.UserService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseUnitAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, Unit> {

    protected final UnitMapper mapper;
    protected final UnitService service;
    private final UserService userService;

    @Override
    @Transactional
    public Unit execute(ArgumentT argument) {
        return super.execute(argument);
    }

    public BaseUnitAction(UnitMapper mapper, UnitService service,
                          UserService userService) {
        this.mapper = mapper;
        this.service = service;
        this.userService = userService;
    }

    protected void fill(UnitForm form, Unit unit) {
        updateParent(unit, form.getParent());
        updateUsers(unit, form.getUsers());
    }

    private void updateUsers(Unit unit, Collection<UUID> userIds) {
        unit.removeUsers();

        if (isEmpty(userIds)) return;

        userIds.stream()
                .map(userService::findOneExisting)
                .forEach(unit::addUser);
    }

    private void updateParent(Unit unit, UUID parentId) {
        if (Objects.equals(ofNullable(unit.getParent())
                        .map(Unit::getId)
                        .orElse(null),
                parentId)) return;

        Unit parent = null;

        if (nonNull(parentId))
            parent = service.findOneExisting(parentId);

        unit.setParent(parent);
        updatePath(unit, parent);
    }

    private void updatePath(Unit unit, Unit parent) {
        String oldPath = unit.getPath();
        String path = isNull(parent) ? ""
                : format("%s/%s", parent.getPath().trim(), parent.getId());
        unit.setPath(path);

        if (isNull(unit.getId()) || unit.getChilds().isEmpty()) return;

        String unitId = unit.getId().toString();
        service.updatePath(format("%s/%s", oldPath, unitId),
                format("%s/%s", path, unitId));
    }

}
