package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public enum UnitActions implements ActionEnum {
    FIND_ALL,
    FIND_ONE,
//    GET_TREE,
    CREATE,
    CREATE_COLLECTION,
    UPDATE,
    DELETE,
    EXISTS,
    DOWNLOAD_SAMPLE_CSV,
    IMPORT_FROM_CSV,
    IMPORT_UNITS;
}
