package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.BaseFindAllAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.services.user.UserService;
import com.ws.sa.specifications.unit.UnitJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindAllUnitAction extends BaseFindAllAction<Unit> {

    private final UnitService unitService;
    private final UserService userService;

    @Autowired
    public FindAllUnitAction(DynamicQueryExecutor executor, UnitService unitService, UserService userService) {
        super(executor);
        this.unitService = unitService;
        this.userService = userService;
    }

    @Override
    protected Page<Unit> executeImpl(QueryArgument argument) {
        return executor.findAll(new UnitJpaQuerySpecification(argument.getQuery(), unitService, userService));
    }

}
