package com.ws.sa.actions.unit.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor(access = PRIVATE)
public class ImportFromCSVUnitArgument implements Argument {

    private final UUID id;
    private final MultipartFile file;

    public static ImportFromCSVUnitArgument from(UUID userId, MultipartFile file) {
        return ImportFromCSVUnitArgument.builder()
                                        .id(userId)
                                        .file(file)
                                        .build();
    }

    @Override
    public boolean validate() {
        return nonNull(id)
               && nonNull(file);
    }

}
