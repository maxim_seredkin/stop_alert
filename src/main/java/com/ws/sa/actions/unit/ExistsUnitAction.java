package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.BaseExistsAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.services.user.UserService;
import com.ws.sa.specifications.unit.UnitJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Seredkin M. on 14.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ExistsUnitAction extends BaseExistsAction {

    private final UnitService unitService;
    private final UserService userService;

    @Autowired
    public ExistsUnitAction(DynamicQueryExecutor executor, UnitService unitService, UserService userService) {
        super(executor);
        this.unitService = unitService;
        this.userService = userService;
    }

    @Override
    protected Boolean executeImpl(QueryArgument argument) {
        return executor.exists(new UnitJpaQuerySpecification(argument.getQuery(), unitService, userService));
    }
}
