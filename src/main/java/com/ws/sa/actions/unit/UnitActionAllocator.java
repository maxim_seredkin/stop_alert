package com.ws.sa.actions.unit;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.GetFileArgument;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.unit.arguments.*;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.dtos.unit.ImportDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Collection;

import static com.ws.sa.actions.unit.UnitActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class UnitActionAllocator extends BaseActionAllocator<UnitActions> {

    public UnitActionAllocator(@Qualifier("createUnitAction") Action<CreateUnitArgument, Unit> createUnitAction,
                               @Qualifier("deleteLogicalUnitAction") Action<DeleteUnitArgument, Void> deleteAction,
                               @Qualifier("existsUnitAction") Action<QueryArgument, Boolean> existsAction,
                               @Qualifier("findAllUnitAction") Action<QueryArgument, Page<Unit>> findAllAction,
                               @Qualifier("findOneUnitAction") Action<UuidArgument, Unit> findOneAction,
                               @Qualifier("updateUnitAction") Action<UpdateUnitArgument, Unit> updateUnitAction,
                               @Qualifier("createCollectionUnitAction") Action<CreateCollectionUnitArgument, Collection<Unit>> createCollectionUnitAction,
                               @Qualifier("getFileAction") Action<GetFileArgument, File> getFileAction,
                               @Qualifier("importFromCSVUnitAction") Action<ImportFromCSVUnitArgument, ImportDTO> importFromCSVAction,
                               @Qualifier("importUnitsAction") Action<ImportUnitsArgument, Collection<Unit>> importUnitsAction) {
        super(ImmutableMap.<UnitActions, Action>builder()
                      .put(CREATE, createUnitAction)
                      .put(DELETE, deleteAction)
                      .put(EXISTS, existsAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(UPDATE, updateUnitAction)
//                      .put(GET_TREE,)
                      .put(CREATE_COLLECTION, createCollectionUnitAction)
                      .put(DOWNLOAD_SAMPLE_CSV, getFileAction)
                      .put(IMPORT_FROM_CSV, importFromCSVAction)
                      .put(IMPORT_UNITS, importUnitsAction)
                      .build());
    }

}
