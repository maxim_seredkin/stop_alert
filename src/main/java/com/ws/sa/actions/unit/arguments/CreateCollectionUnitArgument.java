package com.ws.sa.actions.unit.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.base.Form;
import com.ws.sa.forms.unit.UnitForm;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class CreateCollectionUnitArgument implements Argument {

    private final Collection<UnitForm> forms;

    public static CreateCollectionUnitArgument from(Collection<UnitForm> forms) {
        return new CreateCollectionUnitArgument(forms);
    }

    @Override
    public boolean validate() {
        return nonNull(forms)
               && forms.stream().allMatch(Form::validate);
    }

}
