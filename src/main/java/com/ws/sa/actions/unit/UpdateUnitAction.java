package com.ws.sa.actions.unit;

import com.ws.sa.actions.unit.arguments.UpdateUnitArgument;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.forms.unit.UnitForm;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class UpdateUnitAction extends BaseUnitAction<UpdateUnitArgument> {

    @Autowired
    public UpdateUnitAction(UnitMapper mapper,
                            UnitService service,
                            UserService userService) {
        super(mapper, service, userService);
    }

    @Override
    protected Unit executeImpl(UpdateUnitArgument argument) {
        UUID unitId = argument.getId();
        UnitForm form = argument.getForm();

        Unit unit = service.findOneExisting(unitId);

        mapper.updateEntity(form, unit);
        fill(form, unit);
        unit = service.save(unit);

        return unit;
    }

}
