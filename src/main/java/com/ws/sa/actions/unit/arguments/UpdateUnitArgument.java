package com.ws.sa.actions.unit.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.unit.UnitForm;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class UpdateUnitArgument implements Argument {

    private final UUID id;
    private UnitForm form;

    public static UpdateUnitArgument from(UUID id, UnitForm form) {
        return new UpdateUnitArgument(id, form);
    }

    @Override
    public boolean validate() {
        return nonNull(id)
               && nonNull(form)
               && form.validate();
    }

}
