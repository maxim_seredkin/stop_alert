package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.unit.arguments.CreateCollectionUnitArgument;
import com.ws.sa.actions.unit.arguments.CreateUnitArgument;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.forms.unit.UnitForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class CreateCollectionUnitAction extends BaseAction<CreateCollectionUnitArgument, Collection<Unit>> {

    private final Action<CreateUnitArgument, Unit> createUnitAction;

    @Autowired
    public CreateCollectionUnitAction(@Qualifier("createUnitAction") Action<CreateUnitArgument, Unit> createUnitAction) {
        this.createUnitAction = createUnitAction;
    }

    @Override
    protected Collection<Unit> executeImpl(CreateCollectionUnitArgument argument) {
        Collection<UnitForm> forms = argument.getForms();

        return forms.stream()
                    .map(CreateUnitArgument::from)
                    .map(createUnitAction::execute)
                    .collect(toList());
    }

}
