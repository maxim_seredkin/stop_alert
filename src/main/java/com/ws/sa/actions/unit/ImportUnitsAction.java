package com.ws.sa.actions.unit;

import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.unit.arguments.CreateUnitArgument;
import com.ws.sa.actions.unit.arguments.ImportUnitsArgument;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.forms.unit.ImportUnitsForm;
import com.ws.sa.forms.unit.UnitNodeForm;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ImportUnitsAction extends BaseAction<ImportUnitsArgument, Collection<Unit>> {

    private final Action<CreateUnitArgument, Unit> createUnitAction;

    @Autowired
    public ImportUnitsAction(@Qualifier("createUnitAction") Action<CreateUnitArgument, Unit> createUnitAction) {
        this.createUnitAction = createUnitAction;
    }

    @Override
    protected Collection<Unit> executeImpl(ImportUnitsArgument argument) {
        ImportUnitsForm form = argument.getForm();

        return saveUnits(form.getId(), form.getUnits());
    }

    private Collection<Unit> saveUnits(UUID unitId, Collection<UnitNodeForm> forms) {
        Collection<Unit> units = Lists.newArrayList();

        forms.stream()
             .peek(unit -> unit.setParent(unitId))
             .forEach(unitNodeForm -> {
                 Unit unit = createUnitAction.execute(CreateUnitArgument.from(unitNodeForm));

                 units.add(unit);
                 units.addAll(saveUnits(unit.getId(), unitNodeForm.getNodes()));
             });

        return units;
    }

}
