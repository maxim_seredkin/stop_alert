package com.ws.sa.actions.unit;

import com.google.common.base.Joiner;
import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.enums.Role;
import com.ws.sa.errors.UnitError;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.actions.unit.arguments.ImportFromCSVUnitArgument;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.dtos.unit.ImportDTO;
import com.ws.sa.dtos.unit.UnitImportDTO;
import com.ws.sa.dtos.unit.UnitUserDTO;
import com.ws.sa.utils.FileUtils;
import com.ws.sa.utils.TreeUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.ws.sa.enums.Role.MANAGER;
import static com.ws.sa.enums.Role.SECURITY_OFFICER;
import static com.ws.sa.errors.UnitError.PARENT_UNIT_NOT_EXISTS;
import static java.util.Objects.isNull;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ImportFromCSVUnitAction extends BaseAction<ImportFromCSVUnitArgument, ImportDTO> {

    private final UnitMapper mapper;
    private final UnitService service;

    @Autowired
    public ImportFromCSVUnitAction(UnitMapper mapper,
                                   UnitService service) {
        this.mapper = mapper;
        this.service = service;
    }

    @Override
    protected ImportDTO executeImpl(ImportFromCSVUnitArgument argument) {
        UUID unitId = argument.getId();
        MultipartFile file = argument.getFile();

        try {
            Guard.checkEntityState(!file.isEmpty(), ApiStatusCode.NO_CONTENT);
            Guard.checkEntityArgument(FileUtils.validCSVFormat(file), ApiStatusCode.UNSUPPORTED_MEDIA_TYPE);
            Reader in = new InputStreamReader(file.getInputStream(), "UTF-8");
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader(Headers.class).parse(in);
            //маппим подразделения
            Map<String, UUID> idToUUID = new HashMap<>();
            Map<UUID, String> uuidToParentId = new HashMap<>();
            Set<UnitImportDTO> units = StreamSupport.stream(records.spliterator(), false).map(record -> {
                String id = record.get(Headers.Id);
                UUID uuid = UUID.randomUUID();
                if (StringUtils.isNotBlank(id))
                    idToUUID.put(id, uuid);

                Guard.checkEntityArgument(Objects.nonNull(id), ApiStatusCode.INVALID_PARAM);

                String name = record.get(Headers.Name);
                String parentId = record.get(Headers.ParentId);
                if (StringUtils.isNotBlank(parentId))
                    uuidToParentId.put(uuid, parentId);

                return UnitImportDTO.builder()
                                    .id(uuid)
                                    .name(isNull(name) ? null : name.trim())
                                    .build();
            }).collect(Collectors.toSet());

            units.forEach(unit -> {
                unit.setErrors(new HashSet<>());
                String parentId = uuidToParentId.get(unit.getId());
                if (Objects.nonNull(parentId)) {
                    UUID parentUuid = idToUUID.get(uuidToParentId.get(unit.getId()));
                    Optional<UnitImportDTO> parent = units.stream()
                                                          .filter(temp -> Objects.nonNull(temp.getId()) && Objects.equals(temp.getId(), parentUuid))
                                                          .findFirst();
                    if (parent.isPresent()) {
                        unit.setParent(parent.get());
                    } else {
                        unit.getErrors().add(PARENT_UNIT_NOT_EXISTS);
                    }
                }
                if (StringUtils.isBlank(unit.getName()))
                    unit.getErrors().add(UnitError.EMPTY_NAME);
                else if (service.existsByNameAndParentId(unit.getName(), Optional.ofNullable(unitId)
                                                                                 .orElse(null))) {
                    unit.getErrors().add(UnitError.UNIT_EXISTS);
                }
            });

            units.forEach(unit -> {
                if (StringUtils.isNotBlank(unit.getName()) && existsWithSameName(units, unit))
                    unit.getErrors().add(UnitError.UNIT_EXISTS);
            });

            Unit root = isNull(unitId) ? null : service.findOne(unitId)
                                                       .orElse(null);

            in.close();
            //собираем ответ
            return new ImportDTO(
                    mapper.toLight(root),
                    units.stream().filter(unit -> !unit.getErrors().isEmpty()).collect(Collectors.toSet()),
                    TreeUtils.buildTree(units),
                    isNull(root) ? null : buildCollection(root, SECURITY_OFFICER),
                    isNull(root) ? null : buildCollection(root, MANAGER)
            );
        } catch (IOException exception) {
            return null;
        }
    }

    private boolean existsWithSameName(Set<UnitImportDTO> units, UnitImportDTO unit) {
        return units.stream().anyMatch(u -> !Objects.equals(u.getId(), unit.getId())
                                            && Objects.equals(u.getName(), unit.getName())
                                            && Objects.equals(u.getParent(), unit.getParent()));
    }

    private Collection<UnitUserDTO> buildCollection(Unit root, Role role) {
        // маппинг пользователей назначенных подразделению
        Set<UnitUserDTO> unitUsers = (SECURITY_OFFICER.equals(role) ? root.getOfficers() : root.getManagers())
                .stream()
                .map(user -> new UnitUserDTO(user.getId(), user.getFullName().concat(String.format(" (%s)", user.getUsername())), root.getName()))
                .collect(Collectors.toSet());

        // маппинг пользователей вышестоящих подразделений
        //TODO переписить без использования 3х getUnitPath
        service.findUsersByPathAndRole(root.getId(), role).forEach(user -> {
            Optional<Unit> temp = service.getUnitPath(root.getPath())
                                         .stream()
                                         .filter(unit -> user.getUnits().contains(unit))
                                         .findFirst();
            if (temp.isPresent()) {
                Unit parent = temp.get();
                List<String> path = service.getUnitPath(root.getPath())
                                           .stream()
                                           .filter(unit -> service.getUnitPath(unit.getPath()).contains(parent))
                                           .map(Unit::getName)
                                           .collect(Collectors.toList());
                path.add(0, parent.getName());
                path.add(root.getName());
                unitUsers.add(new UnitUserDTO(
                        user.getId(),
                        user.getFullName().concat(String.format(" (%s)", user.getUsername())),
                        Joiner.on(" - ").join(path)
                ));
            }
        });
        return unitUsers;
    }

    public enum Headers {
        Id,
        Name,
        ParentId,
    }

}
