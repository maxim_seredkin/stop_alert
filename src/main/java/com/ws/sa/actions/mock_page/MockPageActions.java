package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Возможные действия пользователя
 *
 * @author Maxim_Seredkin
 * @since 9/8/2017
 */
public enum MockPageActions implements ActionEnum {
    CREATE,
    DELETE_LOGICAL,
    DELETE,
    EXISTS,
    FIND_ALL,
    FIND_ONE,
    RECOVERY,
    UPDATE;
}
