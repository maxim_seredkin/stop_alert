package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.base.BaseDeleteAction;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.entities.user.User;
import com.ws.sa.services.mock_page.MockPageService;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteMockPageAction extends BaseDeleteAction<MockPage> {

    @Autowired
    public DeleteMockPageAction(MockPageService service) {
        super(service);
    }

}
