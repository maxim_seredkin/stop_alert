package com.ws.sa.actions.mock_page.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.mock_page.MockPageForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class CreateMockPageArgument implements Argument {

    private final MockPageForm form;
    private final MultipartFile file;

    public static CreateMockPageArgument from(MockPageForm form, MultipartFile file) {
        return new CreateMockPageArgument(form, file);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate()
               && nonNull(file);
    }

}
