package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.base.BaseExistsAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Seredkin M. on 14.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ExistsMockPageAction extends BaseExistsAction {

    @Autowired
    public ExistsMockPageAction(DynamicQueryExecutor executor) {
        super(executor);
    }

    @Override
    protected Boolean executeImpl(QueryArgument argument) {
        return executor.exists(new DefaultJpaQuerySpecification<MockPage>(argument.getQuery()));
    }
}
