package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.base.BaseFindAllAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindAllMockPageAction extends BaseFindAllAction<MockPage> {

    @Autowired
    public FindAllMockPageAction(DynamicQueryExecutor executor) {
        super(executor);
    }

    @Override
    protected Page<MockPage> executeImpl(QueryArgument argument) {
        return executor.findAll(new DefaultJpaQuerySpecification<>(argument.getQuery()));
    }

}
