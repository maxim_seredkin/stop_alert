package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.base.BaseFindOneAction;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.services.mock_page.MockPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindOneMockPageAction extends BaseFindOneAction<MockPage> {

    @Autowired
    public FindOneMockPageAction(MockPageService service) {
        super(service);
    }

}
