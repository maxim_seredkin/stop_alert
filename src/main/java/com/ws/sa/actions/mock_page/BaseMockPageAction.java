package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.mappers.mock_page.MockPageMapper;
import com.ws.sa.services.mock_page.MockPageService;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseMockPageAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, MockPage> {

    protected final MockPageMapper mapper;
    protected final MockPageService service;

    protected BaseMockPageAction(MockPageMapper mapper,
                                 MockPageService service) {
        this.mapper = mapper;
        this.service = service;
    }

}
