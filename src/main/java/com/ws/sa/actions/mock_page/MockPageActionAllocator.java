package com.ws.sa.actions.mock_page;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.mock_page.arguments.CreateMockPageArgument;
import com.ws.sa.actions.mock_page.arguments.UpdateMockPageArgument;
import com.ws.sa.entities.mock_page.MockPage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import static com.ws.sa.actions.mock_page.MockPageActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class MockPageActionAllocator extends BaseActionAllocator<MockPageActions> {

    public MockPageActionAllocator(@Qualifier("createMockPageAction") Action<CreateMockPageArgument, MockPage> createUserAction,
                                   @Qualifier("deleteLogicalMockPageAction") Action<UuidArgument, Void> deleteLogicalAction,
                                   @Qualifier("deleteMockPageAction") Action<UuidArgument, Void> deleteAction,
                                   @Qualifier("existsMockPageAction") Action<QueryArgument, Boolean> existsAction,
                                   @Qualifier("findAllMockPageAction") Action<QueryArgument, Page<MockPage>> findAllAction,
                                   @Qualifier("findOneMockPageAction") Action<UuidArgument, MockPage> findOneAction,
                                   @Qualifier("recoveryMockPageAction") Action<UuidArgument, Void> recoveryAction,
                                   @Qualifier("updateMockPageAction") Action<UpdateMockPageArgument, MockPage> updateUserAction) {
        super(ImmutableMap.<MockPageActions, Action>builder()
                      .put(CREATE, createUserAction)
                      .put(DELETE_LOGICAL, deleteLogicalAction)
                      .put(DELETE, deleteAction)
                      .put(EXISTS, existsAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(RECOVERY, recoveryAction)
                      .put(UPDATE, updateUserAction)
                      .build());
    }

}
