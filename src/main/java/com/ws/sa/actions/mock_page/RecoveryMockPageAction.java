package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.services.mock_page.MockPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class RecoveryMockPageAction extends BaseVoidAction<UuidArgument> {

    private final MockPageService service;

    @Autowired
    public RecoveryMockPageAction(MockPageService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        MockPage mockPage = service.findOneExisting(argument.getId());
        mockPage.enable();
        service.save(mockPage);
    }

}
