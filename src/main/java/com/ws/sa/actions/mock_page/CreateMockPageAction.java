package com.ws.sa.actions.mock_page;

import com.ws.sa.actions.mock_page.arguments.CreateMockPageArgument;
import com.ws.sa.entities.mock_page.MockPage;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.mock_page.MockPageForm;
import com.ws.sa.mappers.mock_page.MockPageMapper;
import com.ws.sa.services.mock_page.MockPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.transaction.annotation.Transactional;

import static com.ws.sa.errors.ApiStatusCode.ALREADY_EXISTS;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CreateMockPageAction extends BaseMockPageAction<CreateMockPageArgument> {

    @Autowired
    public CreateMockPageAction(MockPageMapper mapper,
                                MockPageService service) {
        super(mapper, service);
    }

    @Override
    @Transactional
    public MockPage execute(CreateMockPageArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected MockPage executeImpl(CreateMockPageArgument argument) {
        MockPageForm form = argument.getForm();
        MultipartFile file = argument.getFile();

        Guard.checkEntityArgument(!service.existsByName(form.getName()), ALREADY_EXISTS);

        MockPage mockPage = mapper.toEntity(form);

        return service.save(mockPage);
    }

}
