package com.ws.sa.actions.account;

import com.ws.sa.actions.base.ActionEnum;

/**
 * @author Maxim_Seredkin
 * @since 9/8/2017
 */
public enum AccountActions implements ActionEnum {
    GET,
    UPDATE;
}
