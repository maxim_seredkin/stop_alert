package com.ws.sa.actions.account;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.user.arguments.UpdateAccountArgument;
import com.ws.sa.entities.user.User;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.user.PasswordForm;
import com.ws.sa.forms.user.UpdateAccountForm;
import com.ws.sa.mappers.user.UserMapper;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static com.ws.sa.errors.ApiStatusCode.INVALID_PARAM;
import static com.ws.sa.security.base.SecurityHelper.getUser;
import static com.ws.sa.security.base.SecurityHelper.isAdministrator;
import static java.util.Objects.isNull;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class UpdateAccountAction extends BaseAction<UpdateAccountArgument, User> {

    private final UserMapper mapper;
    private final UserService service;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UpdateAccountAction(UserMapper mapper,
                               UserService service,
                               PasswordEncoder passwordEncoder) {
        this.mapper = mapper;
        this.service = service;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public User execute(UpdateAccountArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected User executeImpl(UpdateAccountArgument argument) {
        UpdateAccountForm form = argument.getForm();

        Guard.checkAuthorization();
        User user = service.findOneExisting(getUser().getId());

        changePassword(form.getPassword(), user);

        if (isAdministrator())
            mapper.updateEntity(form, user);

        return service.save(user);
    }

    private void changePassword(PasswordForm form, User user) {
        if (isNull(form))
            return;

        // проверка старого пароля
        Guard.checkEntityArgument(passwordEncoder.matches(form.getOld(), user.getPassword()), INVALID_PARAM);

        user.setPassword(passwordEncoder.encode(form.getRenewed().trim()));
    }

}
