package com.ws.sa.actions.account;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.EmptyArgument;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.entities.user.User;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.ws.sa.security.base.SecurityHelper.getUser;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class AccountAction extends BaseAction<EmptyArgument, User> {

    private final UserService service;

    @Autowired
    public AccountAction(UserService service) {
        this.service = service;
    }

    @Override
    protected User executeImpl(EmptyArgument argument) {
        Guard.checkAuthorization();
        return service.findOneExisting(getUser().getId());
    }

}
