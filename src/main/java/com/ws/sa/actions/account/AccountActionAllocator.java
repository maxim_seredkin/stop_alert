package com.ws.sa.actions.account;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.EmptyArgument;
import com.ws.sa.actions.user.arguments.UpdateAccountArgument;
import com.ws.sa.entities.user.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import static com.ws.sa.actions.account.AccountActions.GET;
import static com.ws.sa.actions.account.AccountActions.UPDATE;


/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class AccountActionAllocator extends BaseActionAllocator<AccountActions> {

    public AccountActionAllocator(@Qualifier("accountAction") Action<EmptyArgument, User> accountAction,
                                  @Qualifier("updateAccountAction") Action<UpdateAccountArgument, User> updateAccountAction) {
        super(ImmutableMap.<AccountActions, Action>builder()
                      .put(GET, accountAction)
                      .put(UPDATE, updateAccountAction)
                      .build());
    }

}
