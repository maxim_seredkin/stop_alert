package com.ws.sa.actions.account;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.exceptions.ApiException;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.listeners.mail.events.SendMailEvent;
import com.ws.sa.actions.user.arguments.ResetPasswordArgument;
import com.ws.sa.entities.user.User;
import com.ws.sa.services.user.UserService;
import com.ws.sa.forms.user.ResetPasswordForm;
import com.ws.sa.configs.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static com.ws.sa.constants.Constants.CHECK_PASS_REQUIRED_PATTERN;
import static com.ws.sa.errors.ApiStatusCode.NOT_FOUND;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.validator.EmailValidator.getInstance;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class ResetPasswordAction extends BaseVoidAction<ResetPasswordArgument> {

    private final AppConfig config;
    private final UserService service;
    private final PasswordEncoder passwordEncoder;
    private final ApplicationEventPublisher publisher;

    @Autowired
    public ResetPasswordAction(AppConfig config,
                               UserService service,
                               PasswordEncoder passwordEncoder,
                               ApplicationEventPublisher publisher) {
        this.config = config;
        this.service = service;
        this.passwordEncoder = passwordEncoder;
        this.publisher = publisher;
    }

    @Override
    @Transactional
    public Void execute(ResetPasswordArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(ResetPasswordArgument argument) {
        ResetPasswordForm form = argument.getForm();

        User user = service.findActiveUsersByUsernameOrEmail(form.getUsername())
                           .stream()
                           .findAny()
                           .orElseThrow(() -> new ApiException(NOT_FOUND));

        Guard.checkEntityArgument(getInstance().isValid(user.getEmail()), ApiStatusCode.INVALID_EMAIL);

        String newPassword = setPassword(user);
        service.save(user);

        SendMailEvent sendMailEvent = SendMailEvent.builder()
                                                   .toAddress(user.getEmail())
                                                   .toPersonal(user.getFullName())
                                                   .fromPersonal("Администратор")
                                                   .fromAddress(config.getDefaultMailUsername())
                                                   .subject("Сброс пароля")
                                                   .templatePath("user/reset_password.vm")
                                                   .params(buildMailParams(user, newPassword))
                                                   .build();

        publisher.publishEvent(sendMailEvent);
    }

    /**
     * Задать новый пароль
     *
     * @param user пользователь
     *
     * @return новый пароль
     */
    private String setPassword(User user) {
        String password = getRandomPassword();

        Guard.checkEntityArgument(password.matches(CHECK_PASS_REQUIRED_PATTERN), ApiStatusCode.INVALID_PARAM);
        user.setPassword(passwordEncoder.encode(password));

        return password;
    }

    /**
     * Сгенерировать рандомный пароль длиной 8 символов
     *
     * @return сгенерированный пароль
     */
    private String getRandomPassword() {
        return randomAlphanumeric(8);
    }

    /**
     * Собрать параметры письма
     *
     * @param user        пользователь
     * @param newPassword новый пароль
     *
     * @return параметры письма
     */
    private Map<String, Object> buildMailParams(User user, String newPassword) {
        return ImmutableMap.of("user", buildUserParams(user, newPassword));
    }

    /**
     * Собрать параметры пользователя
     *
     * @param user        пользователь
     * @param newPassword новый пароль
     *
     * @return параметры пользователя
     */
    private Map<String, Object> buildUserParams(User user, String newPassword) {
        return ImmutableMap.<String, Object>builder()
                .put("shortName", user.getShortName())
                .put("email", user.getEmail())
                .put("fullName", user.getFullName())
                .put("username", user.getUsername())
                .put("registerDate", user.getRegisterDate())
                .put("password", newPassword)
                .build();
    }

}
