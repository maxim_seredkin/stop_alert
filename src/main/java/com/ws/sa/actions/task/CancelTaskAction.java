package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static com.ws.sa.listeners.task.events.TaskEvent.cancel;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CancelTaskAction extends BaseVoidAction<UuidArgument> {

    private final TaskService service;
    private final ApplicationEventPublisher publisher;

    @Autowired
    public CancelTaskAction(TaskService service,
                            ApplicationEventPublisher publisher) {
        this.service = service;
        this.publisher = publisher;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        Task task = service.findOneExisting(argument.getId());

        service.delete(task);

        publisher.publishEvent(cancel(task));
    }
}
