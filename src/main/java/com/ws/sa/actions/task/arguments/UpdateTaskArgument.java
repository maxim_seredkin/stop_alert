package com.ws.sa.actions.task.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.task.TaskForm;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class UpdateTaskArgument implements Argument {

    private final UUID taskId;
    private final TaskForm form;

    public static UpdateTaskArgument from(UUID taskId, TaskForm form) {
        return new UpdateTaskArgument(taskId, form);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}
