package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.task.arguments.CreateTaskReportArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.services.employee.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by Vladislav Shelengovskiy on 18.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Component
@Slf4j
public class CreateTaskReportAction extends BaseVoidAction<CreateTaskReportArgument> {

    private final EmployeeService employeeService;

    @Autowired
    public CreateTaskReportAction(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    protected void executeImpl(CreateTaskReportArgument argument) {
        HttpServletResponse response = argument.getResponse();

        XWPFDocument document = new XWPFDocument();
        fillDocument(document, employeeService.getAttackedEmployeeByTask(argument.getId()));

        try {

            response.setContentType("application/vnd.ms-word");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode("ОФициальный отчет"
                                                                                                          .concat(".docx"), "UTF-8").replace("+", "%20"));
            document.write(response.getOutputStream());
            response.getOutputStream().flush();
        } catch (Exception ex) {
            log.error("Error create task report", ex);
        }
    }

    private void fillDocument(XWPFDocument document, List<Employee> employees) {

        XWPFParagraph p1 = document.createParagraph();
        p1.setAlignment(ParagraphAlignment.RIGHT);
        p1.setVerticalAlignment(TextAlignment.TOP);

        XWPFRun r1 = p1.createRun();
        r1.setText("ПРИЛОЖЕНИЕ 1");
        r1.setFontSize(14);
        r1.setFontFamily("Times New Roman");

        XWPFParagraph p2 = document.createParagraph();
        p2.setAlignment(ParagraphAlignment.CENTER);
        p2.setVerticalAlignment(TextAlignment.TOP);

        XWPFRun r2 = p2.createRun();
        r2.setFontSize(14);
        r2.setFontFamily("Times New Roman");
        r2.setText("Список сотрудников");
        r2.addBreak();
        r2.setText("перешедших по ссылке");

        XWPFParagraph p3 = document.createParagraph();
        p3.setAlignment(ParagraphAlignment.LEFT);
        p3.setVerticalAlignment(TextAlignment.TOP);

        XWPFRun r3 = p3.createRun();
        r3.setFontSize(14);
        r3.setFontFamily("Times New Roman");

        final int[] numb = {1};

        employees.forEach(employee -> {
            r3.setText(String.valueOf(numb[0]).concat(".\t").concat(employee.getFullName()));
            r3.addBreak();
            ++numb[0];
        });
    }
}
