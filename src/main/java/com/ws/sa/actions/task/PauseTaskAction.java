package com.ws.sa.actions.task;

import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.listeners.task.events.TaskEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import static com.ws.sa.enums.TaskStatus.SUSPENDED;
import static com.ws.sa.errors.ApiStatusCode.INVALID_STATUS;
import static com.ws.sa.exceptions.Guard.checkEntityState;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class PauseTaskAction extends BaseStatusTaskAction {

    @Autowired
    public PauseTaskAction(TaskService service,
                           ApplicationEventPublisher publisher) {
        super(service, publisher);
    }

    @Override
    protected void validate(Task task) {
        checkEntityState(task.isWaiting(), INVALID_STATUS);
    }

    @Override
    protected Task executeImpl(UuidArgument argument) {
        return change(argument.getId(), SUSPENDED, TaskEvent::pause);
    }

}
