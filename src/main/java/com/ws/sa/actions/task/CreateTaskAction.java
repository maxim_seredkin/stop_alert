package com.ws.sa.actions.task;

import com.ws.sa.mappers.task.TaskMapper;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import com.ws.sa.security.employee.EmployeeAccess;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.actions.task.arguments.CreateTaskArgument;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.forms.task.TaskForm;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.security.unit.UnitAccess;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import static com.ws.sa.listeners.task.events.TaskEvent.start;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CreateTaskAction extends BaseTaskAction<CreateTaskArgument> {

    @Autowired
    protected CreateTaskAction(TaskMapper mapper,
                               TaskService service,
                               UnitAccess unitAccess,
                               UnitService unitService,
                               UserService userService,
                               EmployeeAccess employeeAccess,
                               EmployeeService employeeService,
                               ApplicationEventPublisher publisher,
                               EmployeeTagService employeeTagService,
                               MailTemplateService mailTemplateService) {
        super(mapper, service, unitAccess, unitService, userService, employeeAccess, employeeService, publisher, employeeTagService, mailTemplateService);
    }

    @Override
    protected Task executeImpl(CreateTaskArgument argument) {
        TaskForm form = argument.getForm();

        Task task = mapper.toEntity(form);
        fill(form, task);
        task = service.save(task);

        publisher.publishEvent(start(task));

        return task;
    }
}
