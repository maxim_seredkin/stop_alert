package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.services.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by Seredkin M. on 13.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class GetFilterEmployeesUnitAction extends BaseAction<UuidArgument, Collection<Employee>> {

    private final EmployeeService employeeService;

    @Autowired
    public GetFilterEmployeesUnitAction(EmployeeService employeeService) {this.employeeService = employeeService;}

    @Override
    @Transactional(readOnly = true)
    public Collection<Employee> execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected Collection<Employee> executeImpl(UuidArgument argument) {
        return employeeService.getFilterEmployeesByTask(argument.getId());
    }

}
