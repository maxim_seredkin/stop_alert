package com.ws.sa.actions.task.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.task.StartDateForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor
public class UpdateStartDateTaskArgument implements Argument {

    private UUID taskId;
    private final StartDateForm form;

    public static UpdateStartDateTaskArgument from(UUID taskId, StartDateForm form) {
        return new UpdateStartDateTaskArgument(taskId, form);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}
