package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseFindOneAction;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindOneTaskAction extends BaseFindOneAction<Task> {

    @Autowired
    public FindOneTaskAction(TaskService service) {
        super(service);
    }

}
