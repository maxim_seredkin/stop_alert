package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseFindAllAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.entities.task.Task;
import com.ws.sa.specifications.task.TaskJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindAllTaskAction extends BaseFindAllAction<Task> {

    @Autowired
    public FindAllTaskAction(DynamicQueryExecutor executor) {
        super(executor);
    }

    @Override
    protected Page<Task> executeImpl(QueryArgument argument) {
        return executor.findAll(new TaskJpaQuerySpecification(argument.getQuery()));
    }

}
