package com.ws.sa.actions.task.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

import static java.util.Objects.nonNull;

/**
 * Created by Vladislav Shelengovskiy on 18.09.2017.
 *
 * @author Vladislav Shelengovskiy
 * @version 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
public class CreateTaskReportArgument implements Argument {

    private UUID id;
    private HttpServletResponse response;

    public static CreateTaskReportArgument from(UUID id, HttpServletResponse response) {
        return new CreateTaskReportArgument(id, response);
    }

    @Override
    public boolean validate() {
        return nonNull(id)
               && nonNull(response);
    }
}
