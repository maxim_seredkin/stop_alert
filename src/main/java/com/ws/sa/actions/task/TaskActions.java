package com.ws.sa.actions.task;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Возможные действия пользователя
 *
 * @author Maxim_Seredkin
 * @since 9/8/2017
 */
public enum TaskActions implements ActionEnum {
    CANCEL,
    CREATE,
    FIND_ALL,
    FIND_ONE,
    GET_COUNT_ATTACKABLE_EMPLOYEES,
    PAUSE,
    RESUME,
    UPDATE_START_DATE,
    UPDATE,
    CREATE_REPORT,
    GET_FILTER_EMPLOYEES,
    GET_HAND_EMPLOYEES;
}
