package com.ws.sa.actions.task.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.task.TaskForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import static java.util.Objects.nonNull;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor
public class CreateTaskArgument implements Argument {

    private final TaskForm form;

    public static CreateTaskArgument from(TaskForm form) {
        return new CreateTaskArgument(form);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}
