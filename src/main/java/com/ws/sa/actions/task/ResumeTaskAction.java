package com.ws.sa.actions.task;

import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.listeners.task.events.TaskEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import static com.ws.sa.enums.TaskStatus.WAITING;
import static com.ws.sa.errors.ApiStatusCode.INVALID_DATE;
import static com.ws.sa.errors.ApiStatusCode.INVALID_STATUS;
import static com.ws.sa.exceptions.Guard.checkEntityArgument;
import static com.ws.sa.exceptions.Guard.checkEntityState;
import static com.ws.sa.utils.CalendarUtil.isNoneDateExpired;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class ResumeTaskAction extends BaseStatusTaskAction {

    @Autowired
    public ResumeTaskAction(TaskService service,
                            ApplicationEventPublisher publisher) {
        super(service, publisher);
    }

    @Override
    protected void validate(Task task) {
        checkEntityState(task.isSuspended() || task.isExpired(), INVALID_STATUS);
        checkEntityArgument(isNoneDateExpired(task.getStartDate()), INVALID_DATE);
    }

    @Override
    protected Task executeImpl(UuidArgument argument) {
        return change(argument.getId(), WAITING, TaskEvent::resume);
    }

}
