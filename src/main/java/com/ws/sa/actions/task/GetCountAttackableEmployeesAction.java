package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.services.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim Seredkin
 * @since 10.09.2017
 */
@Component
public class GetCountAttackableEmployeesAction extends BaseAction<UuidArgument, Integer> {

    private final EmployeeService employeeService;

    @Autowired
    public GetCountAttackableEmployeesAction(EmployeeService employeeService) {this.employeeService = employeeService;}

    @Override
    @Transactional(readOnly = true)
    public Integer execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected Integer executeImpl(UuidArgument argument) {
        return employeeService.countAttackedEmployeesByTask(argument.getId());
    }

}
