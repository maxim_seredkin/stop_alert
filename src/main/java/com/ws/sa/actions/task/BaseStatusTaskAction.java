package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.enums.TaskStatus;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.listeners.task.events.TaskEvent;
import org.springframework.context.ApplicationEventPublisher;

import org.springframework.transaction.annotation.Transactional;
import java.util.UUID;
import java.util.function.Function;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseStatusTaskAction extends BaseAction<UuidArgument, Task> {

    private final TaskService service;
    protected final ApplicationEventPublisher publisher;

    public BaseStatusTaskAction(TaskService service,
                                ApplicationEventPublisher publisher) {
        this.service = service;
        this.publisher = publisher;
    }

    @Override
    @Transactional
    public Task execute(UuidArgument argument) {
        return super.execute(argument);
    }

    protected Task change(UUID taskId, TaskStatus status, Function<Task, TaskEvent> createEvent) {
        Task task = service.findOneExisting(taskId);

        validate(task);

        task = updateStatus(status, task);

        publisher.publishEvent(createEvent.apply(task));

        return task;
    }

    private Task updateStatus(TaskStatus status, Task task) {
        task.setStatus(status);
        task = service.save(task);
        return task;
    }

    protected abstract void validate(Task task);

}
