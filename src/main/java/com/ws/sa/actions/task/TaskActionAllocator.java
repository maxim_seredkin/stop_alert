package com.ws.sa.actions.task;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.task.arguments.CreateTaskArgument;
import com.ws.sa.actions.task.arguments.CreateTaskReportArgument;
import com.ws.sa.actions.task.arguments.UpdateStartDateTaskArgument;
import com.ws.sa.actions.task.arguments.UpdateTaskArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.task.Task;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static com.ws.sa.actions.task.TaskActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class TaskActionAllocator extends BaseActionAllocator<TaskActions> {

    public TaskActionAllocator(@Qualifier("cancelTaskAction") Action<UuidArgument, Void> cancelTaskAction,
                               @Qualifier("createTaskAction") Action<CreateTaskArgument, Task> createTaskAction,
                               @Qualifier("findAllTaskAction") Action<QueryArgument, Page<Task>> findAllAction,
                               @Qualifier("findOneTaskAction") Action<UuidArgument, Task> findOneAction,
                               @Qualifier("getCountAttackableEmployeesAction") Action<UuidArgument, Integer> getCountAttackableEmployeesAction,
                               @Qualifier("pauseTaskAction") Action<UuidArgument, Task> pauseTaskAction,
                               @Qualifier("resumeTaskAction") Action<UuidArgument, Task> resumeTaskAction,
                               @Qualifier("updateStartDateTaskAction") Action<UpdateStartDateTaskArgument, Task> updateStartDateTaskAction,
                               @Qualifier("updateTaskAction") Action<UpdateTaskArgument, Task> updateTaskAction,
                               @Qualifier("getFilterEmployeesUnitAction") Action<UuidArgument, Collection<Employee>> getFilterEmployeesAction,
                               @Qualifier("getHandEmployeesUnitAction") Action<UuidArgument, Collection<Employee>> getHandEmployeesAction,
                               @Qualifier("createTaskReportAction") Action<CreateTaskReportArgument, Void> createTaskReportAction) {
        super(ImmutableMap.<TaskActions, Action>builder()
                      .put(CANCEL, cancelTaskAction)
                      .put(CREATE, createTaskAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(GET_COUNT_ATTACKABLE_EMPLOYEES, getCountAttackableEmployeesAction)
                      .put(PAUSE, pauseTaskAction)
                      .put(RESUME, resumeTaskAction)
                      .put(UPDATE_START_DATE, updateStartDateTaskAction)
                      .put(UPDATE, updateTaskAction)
                      .put(CREATE_REPORT, createTaskReportAction)
                      .put(GET_HAND_EMPLOYEES, getHandEmployeesAction)
                      .put(GET_FILTER_EMPLOYEES, getFilterEmployeesAction)
                      .build());
    }

}
