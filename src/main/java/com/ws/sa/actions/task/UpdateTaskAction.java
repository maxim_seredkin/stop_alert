package com.ws.sa.actions.task;

import com.ws.sa.mappers.task.TaskMapper;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import com.ws.sa.security.employee.EmployeeAccess;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.actions.task.arguments.UpdateTaskArgument;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.forms.task.TaskForm;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.security.unit.UnitAccess;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static com.ws.sa.errors.ApiStatusCode.INVALID_STATUS;
import static com.ws.sa.exceptions.Guard.checkEntityState;
import static com.ws.sa.listeners.task.events.TaskEvent.update;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class UpdateTaskAction extends BaseTaskAction<UpdateTaskArgument> {

    @Autowired
    protected UpdateTaskAction(TaskMapper mapper,
                               TaskService service,
                               UnitAccess unitAccess,
                               UnitService unitService,
                               UserService userService,
                               EmployeeAccess employeeAccess,
                               EmployeeService employeeService,
                               ApplicationEventPublisher publisher,
                               EmployeeTagService employeeTagService,
                               MailTemplateService mailTemplateService) {
        super(mapper, service, unitAccess, unitService, userService, employeeAccess, employeeService, publisher, employeeTagService, mailTemplateService);
    }

    @Override
    protected Task executeImpl(UpdateTaskArgument argument) {
        UUID id = argument.getTaskId();
        TaskForm form = argument.getForm();

        Task task = service.findOneExisting(id);

        checkEntityState(task.isSuspended() || task.isExpired(), INVALID_STATUS);
        task = mapper.updateEntity(form, task);
        fill(form, task);
        task = service.save(task);

        publisher.publishEvent(update(task));

        return task;
    }

}
