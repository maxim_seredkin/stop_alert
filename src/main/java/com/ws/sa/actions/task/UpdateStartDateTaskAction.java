package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.task.arguments.UpdateStartDateTaskArgument;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.listeners.task.events.TaskEvent;
import com.ws.sa.forms.task.StartDateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.ws.sa.errors.ApiStatusCode.INVALID_DATE;
import static com.ws.sa.errors.ApiStatusCode.INVALID_STATUS;
import static com.ws.sa.exceptions.Guard.checkEntityArgument;
import static com.ws.sa.exceptions.Guard.checkEntityState;
import static com.ws.sa.utils.CalendarUtil.isNoneDateExpired;

/**
 * @author Maxim Seredkin
 * @since 10.09.2017
 */
@Component
public class UpdateStartDateTaskAction extends BaseAction<UpdateStartDateTaskArgument, Task> {

    private final TaskService service;
    private final ApplicationEventPublisher publisher;

    @Autowired
    public UpdateStartDateTaskAction(TaskService service,
                                     ApplicationEventPublisher publisher) {
        this.service = service;
        this.publisher = publisher;
    }

    @Override
    @Transactional
    public Task execute(UpdateStartDateTaskArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected Task executeImpl(UpdateStartDateTaskArgument argument) {
        UUID id = argument.getTaskId();
        StartDateForm form = argument.getForm();

        Task task = service.findOneExisting(id);

        checkEntityState(task.isWaiting() || task.isSuspended() || task.isExpired(), INVALID_STATUS);
        checkEntityArgument(isNoneDateExpired(form.getStartDate()), INVALID_DATE);
        task.setStartDate(form.getStartDate());
        task = service.save(task);

        publisher.publishEvent(TaskEvent.update(task));

        return task;
    }

}
