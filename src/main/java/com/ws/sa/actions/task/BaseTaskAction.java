package com.ws.sa.actions.task;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.mappers.task.TaskMapper;
import com.ws.sa.security.base.SecurityHelper;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import com.ws.sa.security.employee.EmployeeAccess;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.entities.task.Task;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.forms.task.TaskForm;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.security.unit.UnitAccess;
import com.ws.sa.services.user.UserService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static com.google.common.collect.Iterables.isEmpty;
import static com.ws.sa.errors.ApiStatusCode.*;
import static com.ws.sa.exceptions.Guard.checkEntityArgument;
import static com.ws.sa.exceptions.Guard.checkEntitySecurity;
import static com.ws.sa.utils.CalendarUtil.isNoneDateExpired;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;


/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseTaskAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, Task> {

    protected final TaskMapper mapper;
    protected final TaskService service;
    private final UnitAccess unitAccess;
    private final UnitService unitService;
    private final UserService userService;
    private final EmployeeAccess employeeAccess;
    private final EmployeeService employeeService;
    protected final ApplicationEventPublisher publisher;
    private final EmployeeTagService employeeTagService;
    private final MailTemplateService mailTemplateService;

    protected BaseTaskAction(TaskMapper mapper,
                             TaskService service,
                             UnitAccess unitAccess,
                             UnitService unitService,
                             UserService userService,
                             EmployeeAccess employeeAccess,
                             EmployeeService employeeService,
                             ApplicationEventPublisher publisher,
                             EmployeeTagService employeeTagService,
                             MailTemplateService mailTemplateService) {
        this.mapper = mapper;
        this.service = service;
        this.unitAccess = unitAccess;
        this.unitService = unitService;
        this.userService = userService;
        this.employeeAccess = employeeAccess;
        this.employeeService = employeeService;
        this.publisher = publisher;
        this.employeeTagService = employeeTagService;
        this.mailTemplateService = mailTemplateService;
    }

    @Override
    @Transactional
    public Task execute(ArgumentT argument) {
        return super.execute(argument);
    }

    /**
     * Заполнение общих полей при создании и обновлении
     *
     * @param form форма пользователя
     * @param task задача
     */
    protected void fill(TaskForm form, Task task) {
        fillPeriod(form.getStartDate(), form.getEndDate(), task);
        fillEmployees(form, task);
        fillMailTemplate(form.getTemplate(), task);
        fillAuthor(task);
        fillUnits(form.getUnits(), task.getUnits());
        fillEmployeeTags(form.getEmployeeTags(), task.getEmployeeTags());
    }

    private void fillPeriod(Date startDate, Date endDate, Task task) {
        checkEntityArgument(isNoneDateExpired(startDate), INVALID_DATE);
        checkEntityArgument(startDate.before(endDate), END_DATE_BEFORE_START_DATE);

        task.setStartDate(startDate);
        task.setEndDate(endDate);
    }

    private void fillEmployees(TaskForm form, Task task) {
        fillEmployees(form.getEmployeesByFilter(), task.getEmployeesByFilter());
        fillEmployees(form.getEmployeesByHand(), task.getEmployeesByHand());

        checkEntityArgument(existsEmployees(task.getEmployeesByFilter(),
                                            task.getEmployeesByHand()),
                            EMPTY_EMPLOYEES);
    }

    private void fillEmployees(List<UUID> employeeIds, Collection<Employee> employees) {
        employees.clear();

        if (isEmpty(employeeIds))
            return;

        // TODO refactor this
        employeeIds.stream()
                   .peek(id -> checkEntitySecurity(employeeAccess.accessRead(id), FORBIDDEN))
                   .map(employeeService::findOneExisting)
                   .forEach(employee -> add(employee, employees));
    }

    private boolean existsEmployees(List<Employee> employeesByFilter, List<Employee> employeesByHand) {
        return isNotEmpty(employeesByFilter) || isNotEmpty(employeesByHand);
    }

    private void fillMailTemplate(UUID mailTemplateId, Task task) {
        task.setTemplate(mailTemplateService.findOneExisting(mailTemplateId));
    }

    private void fillAuthor(Task task) {
        Guard.checkAuthorization();

        task.setAuthor(userService.findOneExisting(SecurityHelper.getUser().getId()));
    }

    private void fillUnits(List<UUID> unitIds, List<Unit> units) {
        units.clear();

        if (isEmpty(unitIds))
            return;

        // TODO refactor this
        unitIds.stream()
               .peek(id -> checkEntitySecurity(unitAccess.accessRead(id), FORBIDDEN))
               .map(unitService::findOneExisting)
               .forEach(unit -> add(unit, units));
    }

    private void fillEmployeeTags(List<UUID> employeeTagIds, List<EmployeeTag> employeeTags) {
        employeeTags.clear();

        if (isEmpty(employeeTagIds))
            return;

        employeeTagIds.stream()
                      .map(employeeTagService::findOneExisting)
                      .forEach(employeeTag -> add(employeeTag, employeeTags));
    }

    private <ItemT> void add(ItemT employee, Collection<ItemT> employees) {
        if (employees.contains(employee)) { return;}

        employees.add(employee);
    }

}
