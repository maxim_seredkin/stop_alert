package com.ws.sa.actions.base.arguments;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@NoArgsConstructor(access = PRIVATE)
public class EmptyArgument implements Argument {

    public static EmptyArgument create() {
        return new EmptyArgument();
    }

    @Override
    public boolean validate() {
        return true;
    }

}
