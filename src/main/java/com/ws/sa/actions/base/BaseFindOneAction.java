package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.services.base.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseFindOneAction<ReturnT extends BaseEntity> extends BaseAction<UuidArgument, ReturnT> {

    private final Service<ReturnT> service;

    public BaseFindOneAction(Service<ReturnT> service) {
        this.service = service;
    }

    @Override
    @Transactional(readOnly = true)
    public ReturnT execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected ReturnT executeImpl(UuidArgument argument) {
        return service.findOneExisting(argument.getId());
    }

}
