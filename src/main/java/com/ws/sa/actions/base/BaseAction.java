package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.exceptions.Guard;

import static com.ws.sa.errors.ApiStatusCode.INVALID_PARAM;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseAction<ArgumentT extends Argument, ReturnT> implements Action<ArgumentT, ReturnT> {

    public ReturnT execute(ArgumentT argument) {
        Guard.checkEntityArgumentExists(argument, INVALID_PARAM);
        Guard.checkEntityArgument(argument.validate(), INVALID_PARAM);
        return this.executeImpl(argument);
    }

    protected abstract ReturnT executeImpl(ArgumentT argument);

}
