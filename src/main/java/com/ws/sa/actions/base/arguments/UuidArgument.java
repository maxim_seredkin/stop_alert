package com.ws.sa.actions.base.arguments;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Getter
@AllArgsConstructor
public class UuidArgument implements Argument {

    private final UUID id;

    public static UuidArgument from(UUID id) {
        return new UuidArgument(id);
    }

    @Override
    public boolean validate() {
        return nonNull(id);
    }

}
