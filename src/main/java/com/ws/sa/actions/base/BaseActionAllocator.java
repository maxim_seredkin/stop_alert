package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.Argument;

import java.util.Map;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseActionAllocator<ActionT extends ActionEnum> implements ActionAllocator<ActionT> {

    private final Map<ActionT, Action> actions;

    public BaseActionAllocator(Map<ActionT, Action> actions) {
        this.actions = actions;
    }

    public <ArgumentT extends Argument, ReturnT> Action<ArgumentT, ReturnT> instanceOf(ActionT action) {
        return actions.get(action);
    }

}
