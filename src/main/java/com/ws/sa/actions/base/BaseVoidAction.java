package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.exceptions.Guard;

import java.io.IOException;

import static com.ws.sa.errors.ApiStatusCode.INVALID_PARAM;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseVoidAction<ArgumentT extends Argument> implements Action<ArgumentT, Void> {

    public Void execute(ArgumentT argument) {
        Guard.checkEntityArgumentExists(argument, INVALID_PARAM);
        Guard.checkEntityArgument(argument.validate(), INVALID_PARAM);
        this.executeImpl(argument);
        return null;
    }

    protected abstract void executeImpl(ArgumentT argument);
}
