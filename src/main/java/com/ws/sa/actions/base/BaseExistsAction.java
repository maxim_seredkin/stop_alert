package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.executors.DynamicQueryExecutor;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseExistsAction extends BaseAction<QueryArgument, Boolean> {

    protected final DynamicQueryExecutor executor;

    public BaseExistsAction(DynamicQueryExecutor executor) {
        this.executor = executor;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean execute(QueryArgument argument) {
        return super.execute(argument);
    }

}
