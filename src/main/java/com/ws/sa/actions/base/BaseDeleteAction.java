package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.base.BaseEntity;
import com.ws.sa.services.base.Service;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseDeleteAction<ReturnT extends BaseEntity> extends BaseVoidAction<UuidArgument> {

    private final Service<ReturnT> service;

    public BaseDeleteAction(Service<ReturnT> service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        ReturnT entity = service.findOneExisting(argument.getId());
        service.delete(entity);
    }

}
