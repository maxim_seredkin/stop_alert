package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.GetFileArgument;
import com.ws.sa.services.base.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author Maxim Seredkin
 * @since 09.09.2017
 */
@Component
public class GetFileAction extends BaseAction<GetFileArgument, File> {

    private final ResourceService resourceService;

    @Autowired
    public GetFileAction(ResourceService resourceService) {this.resourceService = resourceService;}

    @Override
    protected File executeImpl(GetFileArgument argument) {
        return resourceService.get(argument.getName(), argument.getType());
    }
}
