package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.base.Entity;
import com.ws.sa.executors.DynamicQueryExecutor;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseFindAllAction<EntityT extends Entity> extends BaseAction<QueryArgument, Page<EntityT>> {

    protected final DynamicQueryExecutor executor;

    @Override
    @Transactional(readOnly = true)
    public Page<EntityT> execute(QueryArgument argument) {
        return super.execute(argument);
    }

    public BaseFindAllAction(DynamicQueryExecutor executor) {
        this.executor = executor;
    }

}
