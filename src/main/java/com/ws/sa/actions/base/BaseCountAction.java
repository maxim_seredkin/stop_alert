package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.executors.DynamicQueryExecutor;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public abstract class BaseCountAction extends BaseAction<QueryArgument, Long> {

    protected final DynamicQueryExecutor executor;

    @Override
    @Transactional(readOnly = true)
    public Long execute(QueryArgument argument) {
        return super.execute(argument);
    }

    public BaseCountAction(DynamicQueryExecutor executor) {
        this.executor = executor;
    }

}
