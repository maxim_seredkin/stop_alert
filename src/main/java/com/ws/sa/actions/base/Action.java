package com.ws.sa.actions.base;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public interface Action<ArgumentT, ReturnT> {

    ReturnT execute(ArgumentT argument);

}
