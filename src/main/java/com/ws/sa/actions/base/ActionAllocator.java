package com.ws.sa.actions.base;

import com.ws.sa.actions.base.arguments.Argument;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public interface ActionAllocator<ActionT extends ActionEnum> {

    <ArgumentT extends Argument, ReturnT> Action<ArgumentT, ReturnT> instanceOf(ActionT action);

}
