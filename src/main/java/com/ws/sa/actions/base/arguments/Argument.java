package com.ws.sa.actions.base.arguments;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
public interface Argument {

    boolean validate();

}
