package com.ws.sa.actions.base.arguments;

import com.ws.sa.queries.DynamicQuery;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class QueryArgument implements Argument {

    private final DynamicQuery query;

    public static QueryArgument from(DynamicQuery dynamicQuery) {
        return new QueryArgument(dynamicQuery);
    }

    @Override
    public boolean validate() {
        return nonNull(query);
    }

}
