package com.ws.sa.actions.base.arguments;

import com.ws.sa.enums.ResourceType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * @author Maxim Seredkin
 * @since 09.09.2017
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class GetFileArgument implements Argument {

    private final String name;
    private final ResourceType type;

    public static GetFileArgument from(String name, ResourceType type) {
        return new GetFileArgument(name, type);
    }

    @Override
    public boolean validate() {
        return isNotBlank(name)
               && nonNull(type);
    }
}
