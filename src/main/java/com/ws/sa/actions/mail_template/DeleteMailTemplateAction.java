package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.base.BaseDeleteAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.services.base.ResourceService;
import com.ws.sa.services.mail_template.MailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.ws.sa.enums.ResourceType.TEMPLATE;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteMailTemplateAction extends BaseDeleteAction<MailTemplate> {

    private final MailTemplateService service;
    private final ResourceService resourceService;

    @Autowired
    public DeleteMailTemplateAction(MailTemplateService service, ResourceService resourceService) {
        super(service);
        this.service = service;
        this.resourceService = resourceService;
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        MailTemplate mailTemplate = service.findOneExisting(argument.getId());
        service.delete(mailTemplate);

        this.resourceService.delete(mailTemplate.getPath(), TEMPLATE);
    }
}
