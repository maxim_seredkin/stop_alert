package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import com.ws.sa.exceptions.EntityArgumentException;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.services.base.ResourceService;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;

import java.io.File;
import java.util.UUID;

import static com.ws.sa.enums.ResourceType.TEMPLATE;
import static com.ws.sa.errors.ApiStatusCode.NOT_VELOCITY_TEMPLATE;
import static com.ws.sa.validators.VelocityValidator.validate;
import static java.util.Objects.isNull;
import static org.springframework.util.FileSystemUtils.deleteRecursively;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseMailTemplateAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, MailTemplate> {

    protected final MailTemplateMapper mapper;
    protected final MailTemplateService service;
    protected final ResourceService resourceService;
    private final MailTemplateGroupService mailTemplateGroupService;

    protected BaseMailTemplateAction(MailTemplateMapper mapper,
                                     MailTemplateService service,
                                     ResourceService resourceService,
                                     MailTemplateGroupService mailTemplateGroupService) {
        this.mapper = mapper;
        this.service = service;
        this.resourceService = resourceService;
        this.mailTemplateGroupService = mailTemplateGroupService;
    }

    protected void checkVelocityTemplate(MailTemplate template) {
        File velocityFile = resourceService.get(template.getPath(), TEMPLATE);
        if (!validate(velocityFile)) {
            deleteRecursively(velocityFile);
            throw new EntityArgumentException(NOT_VELOCITY_TEMPLATE);
        }
    }

    protected MailTemplateGroup getGroup(UUID group) {
        if (isNull(group))
            return null;

        return mailTemplateGroupService.findOneExisting(group);
    }

}
