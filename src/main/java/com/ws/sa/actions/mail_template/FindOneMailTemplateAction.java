package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.base.BaseFindOneAction;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.services.mail_template.MailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindOneMailTemplateAction extends BaseFindOneAction<MailTemplate> {

    @Autowired
    public FindOneMailTemplateAction(MailTemplateService service) {
        super(service);
    }

}
