package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Возможные действия пользователя
 *
 * @author Maxim_Seredkin
 * @since 9/8/2017
 */
public enum MailTemplateActions implements ActionEnum {
    CREATE,
    DELETE_LOGICAL,
    DELETE,
    EXISTS,
    FIND_ALL,
    FIND_ONE,
    RECOVERY,
    UPDATE,
    DOWNLOAD_SAMPLE_HTML,
    EXAMPLE;
}
