package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.mail_template.arguments.UpdateMailTemplateArgument;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.mail_template.MailTemplateForm;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.services.base.ResourceService;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static com.ws.sa.enums.ResourceType.TEMPLATE;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class UpdateMailAction extends BaseMailTemplateAction<UpdateMailTemplateArgument> {

    @Autowired
    public UpdateMailAction(MailTemplateMapper mapper,
                            MailTemplateService service,
                            ResourceService resourceService,
                            MailTemplateGroupService mailTemplateGroupService) {
        super(mapper, service, resourceService, mailTemplateGroupService);
    }

    @Override
    @Transactional
    public MailTemplate execute(UpdateMailTemplateArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected MailTemplate executeImpl(UpdateMailTemplateArgument argument) {
        UUID mailTemplateId = argument.getMailTemplateId();
        MailTemplateForm form = argument.getForm();
        MultipartFile file = argument.getFile();

        Guard.checkEntityArgument(StringUtils.isNotBlank(form.getName()), ApiStatusCode.EMPTY_PARAM);

        MailTemplate template = service.findOneExisting(mailTemplateId);
        mapper.updateEntity(form, template);
        template.setGroup(getGroup(form.getGroup()));

        resourceService.update(template.getPath(), file, TEMPLATE);
        checkVelocityTemplate(template);

        return service.save(template);
    }

}
