package com.ws.sa.actions.mail_template.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.mail_template.MailTemplateForm;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class UpdateMailTemplateArgument implements Argument {

    private final UUID mailTemplateId;
    private final MailTemplateForm form;
    private final MultipartFile file;

    public static UpdateMailTemplateArgument from(UUID mailTemplateId, MailTemplateForm form, MultipartFile file) {
        return new UpdateMailTemplateArgument(mailTemplateId, form, file);
    }

    @Override
    public boolean validate() {
        return nonNull(mailTemplateId)
               && nonNull(form)
               && form.validate();
    }

}
