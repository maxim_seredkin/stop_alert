package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.services.mail_template.MailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteLogicalMailTemplateAction extends BaseVoidAction<UuidArgument> {

    private final MailTemplateService service;

    @Autowired
    public DeleteLogicalMailTemplateAction(MailTemplateService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        MailTemplate mailTemplate = service.findOneExisting(argument.getId());
        mailTemplate.delete();
        service.save(mailTemplate);
    }

}
