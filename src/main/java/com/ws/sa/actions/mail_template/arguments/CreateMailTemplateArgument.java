package com.ws.sa.actions.mail_template.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.mail_template.MailTemplateForm;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class CreateMailTemplateArgument implements Argument {

    private final MailTemplateForm form;
    private final MultipartFile file;

    public static CreateMailTemplateArgument from(MailTemplateForm form, MultipartFile file) {
        return new CreateMailTemplateArgument(form, file);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate()
               && nonNull(file);
    }

}
