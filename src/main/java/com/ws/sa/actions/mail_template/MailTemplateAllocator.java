package com.ws.sa.actions.mail_template;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.GetFileArgument;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.mail_template.arguments.CreateMailTemplateArgument;
import com.ws.sa.actions.mail_template.arguments.UpdateMailTemplateArgument;
import com.ws.sa.dtos.mail_template.MailTemplateExampleDTO;
import com.ws.sa.entities.mail_template.MailTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.io.File;

import static com.ws.sa.actions.mail_template.MailTemplateActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class MailTemplateAllocator extends BaseActionAllocator<MailTemplateActions> {

    public MailTemplateAllocator(@Qualifier("createMailTemplateAction") Action<CreateMailTemplateArgument, MailTemplate> createUserAction,
                                 @Qualifier("deleteLogicalMailTemplateAction") Action<UuidArgument, Void> deleteLogicalAction,
                                 @Qualifier("deleteMailTemplateAction") Action<UuidArgument, Void> deleteAction,
                                 @Qualifier("existsMailTemplateAction") Action<QueryArgument, Boolean> existsAction,
                                 @Qualifier("findAllMailTemplateAction") Action<QueryArgument, Page<MailTemplate>> findAllAction,
                                 @Qualifier("findOneMailTemplateAction") Action<UuidArgument, MailTemplate> findOneAction,
                                 @Qualifier("recoveryMailTemplateAction") Action<UuidArgument, Void> recoveryAction,
                                 @Qualifier("updateMailAction") Action<UpdateMailTemplateArgument, MailTemplate> updateUserAction,
                                 @Qualifier("getFileAction") Action<GetFileArgument, File> getFileAction,
                                 @Qualifier("exampleMailTemplateAction") Action<UuidArgument, MailTemplateExampleDTO> exampleMailTemplateAction) {
        super(ImmutableMap.<MailTemplateActions, Action>builder()
                      .put(CREATE, createUserAction)
                      .put(DELETE_LOGICAL, deleteLogicalAction)
                      .put(DELETE, deleteAction)
                      .put(EXISTS, existsAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(RECOVERY, recoveryAction)
                      .put(UPDATE, updateUserAction)
                      .put(DOWNLOAD_SAMPLE_HTML, getFileAction)
                      .put(EXAMPLE, exampleMailTemplateAction)
                      .build());
    }

}
