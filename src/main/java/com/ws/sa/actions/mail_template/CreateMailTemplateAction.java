package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.mail_template.arguments.CreateMailTemplateArgument;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.mail_template.MailTemplateForm;
import com.ws.sa.mappers.mail_template.MailTemplateMapper;
import com.ws.sa.services.base.ResourceService;
import com.ws.sa.services.mail_template.MailTemplateService;
import com.ws.sa.services.mail_template_group.MailTemplateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.transaction.annotation.Transactional;

import static com.ws.sa.enums.ResourceType.TEMPLATE;
import static com.ws.sa.errors.ApiStatusCode.ALREADY_EXISTS;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CreateMailTemplateAction extends BaseMailTemplateAction<CreateMailTemplateArgument> {

    @Autowired
    public CreateMailTemplateAction(MailTemplateMapper mapper,
                                    MailTemplateService service,
                                    ResourceService resourceService,
                                    MailTemplateGroupService mailTemplateGroupService) {
        super(mapper, service, resourceService, mailTemplateGroupService);
    }

    @Override
    @Transactional
    public MailTemplate execute(CreateMailTemplateArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected MailTemplate executeImpl(CreateMailTemplateArgument argument) {
        MailTemplateForm form = argument.getForm();
        MultipartFile file = argument.getFile();

        Guard.checkEntityArgument(!service.existsByName(form.getName()), ALREADY_EXISTS);

        MailTemplate template = mapper.toEntity(form);
        template.setGroup(getGroup(form.getGroup()));
        template.setPath(resourceService.create(file, TEMPLATE));

        checkVelocityTemplate(template);

        return service.save(template);
    }

}
