package com.ws.sa.actions.mail_template;

import com.ws.sa.actions.base.BaseExistsAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Seredkin M. on 14.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ExistsMailTemplateAction extends BaseExistsAction {

    @Autowired
    public ExistsMailTemplateAction(DynamicQueryExecutor executor) {
        super(executor);
    }

    @Override
    protected Boolean executeImpl(QueryArgument argument) {
        return executor.exists(new DefaultJpaQuerySpecification<MailTemplate>(argument.getQuery()));
    }

}
