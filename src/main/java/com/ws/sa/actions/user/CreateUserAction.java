package com.ws.sa.actions.user;

import com.ws.sa.actions.user.arguments.CreateUserArgument;
import com.ws.sa.entities.user.User;
import com.ws.sa.forms.user.UserForm;
import com.ws.sa.mappers.user.UserMapper;
import com.ws.sa.services.authority.AuthorityService;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CreateUserAction extends BaseUserAction<CreateUserArgument> {

    @Autowired
    public CreateUserAction(UserMapper mapper,
                            UserService service,
                            UnitService unitService,
                            PasswordEncoder passwordEncoder,
                            AuthorityService authorityService) {
        super(mapper, service, unitService, passwordEncoder, authorityService);
    }

    @Override
    protected User executeImpl(CreateUserArgument argument) {
        UserForm form = argument.getForm();

        User user = mapper.toEntity(form);
        fill(form, user);

        return service.save(user);
    }

}
