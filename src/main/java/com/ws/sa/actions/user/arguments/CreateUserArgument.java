package com.ws.sa.actions.user.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.user.UserForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import static com.ws.sa.constants.Constants.CHECK_PASS_REQUIRED_PATTERN;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor
public class CreateUserArgument implements Argument {

    private final UserForm form;

    public static CreateUserArgument from(UserForm form) {
        return CreateUserArgument.builder()
                                 .form(form)
                                 .build();
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate()
               && isNotBlank(form.getPassword())
               && form.getPassword().matches(CHECK_PASS_REQUIRED_PATTERN);
    }

}
