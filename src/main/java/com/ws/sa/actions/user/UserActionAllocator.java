package com.ws.sa.actions.user;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.user.arguments.CreateUserArgument;
import com.ws.sa.actions.user.arguments.ResetPasswordArgument;
import com.ws.sa.actions.user.arguments.UpdateUserArgument;
import com.ws.sa.entities.user.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import static com.ws.sa.actions.user.UserActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class UserActionAllocator extends BaseActionAllocator<UserActions> {

    public UserActionAllocator(@Qualifier("createUserAction") Action<CreateUserArgument, User> createUserAction,
                               @Qualifier("deleteLogicalUserAction") Action<UuidArgument, Void> deleteLogicalAction,
                               @Qualifier("deleteUserAction") Action<UuidArgument, Void> deleteAction,
                               @Qualifier("disableUserAction") Action<UuidArgument, Void> disableAction,
                               @Qualifier("enableUserAction") Action<UuidArgument, Void> enableAction,
                               @Qualifier("existsUserAction") Action<QueryArgument, Boolean> existsAction,
                               @Qualifier("findAllUserAction") Action<QueryArgument, Page<User>> findAllAction,
                               @Qualifier("findOneUserAction") Action<UuidArgument, User> findOneAction,
                               @Qualifier("recoveryUserAction") Action<UuidArgument, Void> recoveryAction,
                               @Qualifier("resetPasswordAction") Action<ResetPasswordArgument, Void> resetPasswordAction,
                               @Qualifier("updateUserAction") Action<UpdateUserArgument, User> updateUserAction) {
        super(ImmutableMap.<UserActions, Action>builder()
                      .put(CREATE, createUserAction)
                      .put(DELETE_LOGICAL, deleteLogicalAction)
                      .put(DELETE, deleteAction)
                      .put(DISABLE, disableAction)
                      .put(ENABLE, enableAction)
                      .put(EXISTS, existsAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(RECOVERY, recoveryAction)
                      .put(RESET_PASSWORD, resetPasswordAction)
                      .put(UPDATE, updateUserAction)
                      .build());
    }

}
