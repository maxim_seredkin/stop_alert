package com.ws.sa.actions.user;

import com.ws.sa.actions.base.BaseFindOneAction;
import com.ws.sa.entities.user.User;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindOneUserAction extends BaseFindOneAction<User> {

    @Autowired
    public FindOneUserAction(UserService service) {
        super(service);
    }

}
