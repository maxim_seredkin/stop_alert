package com.ws.sa.actions.user.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.authority.AuthorityForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import static java.util.Objects.nonNull;

/**
 * Created by vlad_shelengovskiy on 21.11.2016.
 */
@Getter
@Builder
@AllArgsConstructor
public class AuthorityArgument implements Argument {

    private final AuthorityForm form;

    public static AuthorityArgument from(AuthorityForm form) {
        return AuthorityArgument.builder()
                                .form(form)
                                .build();
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}