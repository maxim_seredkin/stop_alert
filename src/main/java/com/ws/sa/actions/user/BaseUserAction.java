package com.ws.sa.actions.user;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.entities.authority.Authority;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import com.ws.sa.forms.user.UserForm;
import com.ws.sa.mappers.user.UserMapper;
import com.ws.sa.services.authority.AuthorityService;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.services.user.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseUserAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, User> {

    protected final UserMapper mapper;
    protected final UserService service;
    private final UnitService unitService;
    private final PasswordEncoder passwordEncoder;
    private final AuthorityService authorityService;

    protected BaseUserAction(UserMapper mapper,
                             UserService service,
                             UnitService unitService,
                             PasswordEncoder passwordEncoder,
                             AuthorityService authorityService) {
        this.mapper = mapper;
        this.service = service;
        this.unitService = unitService;
        this.passwordEncoder = passwordEncoder;
        this.authorityService = authorityService;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public User execute(ArgumentT argument) {
        return super.execute(argument);
    }

    /**
     * Заполнение общих полей при создании и обновлении
     *
     * @param form форма пользователя
     * @param user пользователь
     */
    protected void fill(UserForm form, User user) {
        fillAuthority(form.getAuthority(), user);
        fillPassword(form.getPassword(), user);
        fillUnits(form.getUnits(), user.getUnits());
    }

    public void fillAuthority(UUID authorityId, User user) {
        Authority authority = authorityService.findOneExisting(authorityId);
        user.setAuthority(authority);
    }

    private void fillPassword(String password, User user) {
        if (isNull(password))
            return;

        user.setPassword(passwordEncoder.encode(password.trim()));
    }

    /**
     * Заполнение организаций
     *
     * @param unitIds идентификаторы организаций
     * @param units   организации
     */
    private void fillUnits(List<UUID> unitIds, Set<Unit> units) {
        units.clear();

        if (isEmpty(unitIds))
            return;

        units.addAll(unitIds.stream()
                            .map(unitService::findOneExisting)
                            .collect(toList()));
    }

}
