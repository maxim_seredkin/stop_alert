package com.ws.sa.actions.user;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.user.User;
import com.ws.sa.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DisableUserAction extends BaseVoidAction<UuidArgument> {

    private final UserService service;

    @Autowired
    public DisableUserAction(UserService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        User user = service.findOneExisting(argument.getId());
        user.setAccountNonLocked(false);
        service.save(user);
    }

}
