package com.ws.sa.actions.user;

import com.ws.sa.actions.base.BaseFindAllAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.entities.user.User;
import com.ws.sa.specifications.user.UserJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindAllUserAction extends BaseFindAllAction<User> {

    @Autowired
    public FindAllUserAction(DynamicQueryExecutor executor) {
        super(executor);
    }

    @Override
    protected Page<User> executeImpl(QueryArgument argument) {
        return executor.findAll(new UserJpaQuerySpecification(argument.getQuery()));
    }

}
