package com.ws.sa.actions.user.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.user.ResetPasswordForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import static java.util.Objects.nonNull;

/**
 * Created by vlad_shelengovskiy on 19.12.2016.
 */
@Getter
@Builder
@AllArgsConstructor
public class ResetPasswordArgument implements Argument {

    private final ResetPasswordForm form;

    public static ResetPasswordArgument from(ResetPasswordForm form) {
        return ResetPasswordArgument.builder()
                                    .form(form)
                                    .build();
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}
