package com.ws.sa.actions.user;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Возможные действия пользователя
 *
 * @author Maxim_Seredkin
 * @since 9/8/2017
 */
public enum UserActions implements ActionEnum {
    CREATE,
    DELETE_LOGICAL,
    DELETE,
    DISABLE,
    ENABLE,
    EXISTS,
    FIND_ALL,
    FIND_ONE,
    RECOVERY,
    RESET_PASSWORD,
    UPDATE;
}
