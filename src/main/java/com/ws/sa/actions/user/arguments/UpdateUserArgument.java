package com.ws.sa.actions.user.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.user.UserForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

import static com.ws.sa.constants.Constants.CHECK_PASS_REQUIRED_PATTERN;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor(access = PRIVATE)
public class UpdateUserArgument implements Argument {

    private final UUID userId;
    private final UserForm form;

    public static UpdateUserArgument from(UUID userId, UserForm form) {
        return UpdateUserArgument.builder()
                                 .userId(userId)
                                 .form(form)
                                 .build();
    }

    @Override
    public boolean validate() {
        return nonNull(userId)
               && nonNull(form)
               && form.validate()
               && (isNull(form.getPassword())
                   || (isNotBlank(form.getPassword())
                       && form.getPassword().matches(CHECK_PASS_REQUIRED_PATTERN)));
    }
}
