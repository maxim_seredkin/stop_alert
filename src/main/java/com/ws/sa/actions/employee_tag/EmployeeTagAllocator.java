package com.ws.sa.actions.employee_tag;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.employee_tag.arguments.CreateEmployeeTagArgument;
import com.ws.sa.actions.employee_tag.arguments.UpdateEmployeeTagArgument;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import static com.ws.sa.actions.employee_tag.EmployeeTagActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class EmployeeTagAllocator extends BaseActionAllocator<EmployeeTagActions> {

    public EmployeeTagAllocator(@Qualifier("createEmployeeTagAction") Action<CreateEmployeeTagArgument, EmployeeTag> createEmployeeTagAction,
                                @Qualifier("deleteLogicalEmployeeTagAction") Action<UuidArgument, Void> deleteAction,
                                @Qualifier("findAllEmployeeTagAction") Action<QueryArgument, Page<EmployeeTag>> findAllAction,
                                @Qualifier("findOneEmployeeTagAction") Action<UuidArgument, EmployeeTag> findOneAction,
                                @Qualifier("updateEmployeeTagAction") Action<UpdateEmployeeTagArgument, EmployeeTag> updateEmployeeTagAction) {
        super(ImmutableMap.<EmployeeTagActions, Action>builder()
                      .put(CREATE, createEmployeeTagAction)
                      .put(DELETE, deleteAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(UPDATE, updateEmployeeTagAction)
                      .build());
    }

}
