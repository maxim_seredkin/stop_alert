package com.ws.sa.actions.employee_tag;

import com.ws.sa.actions.base.BaseFindAllAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.specifications.base.DefaultJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindAllEmployeeTagAction extends BaseFindAllAction<EmployeeTag> {

    @Autowired
    public FindAllEmployeeTagAction(DynamicQueryExecutor executor) {
        super(executor);
    }

    @Override
    protected Page<EmployeeTag> executeImpl(QueryArgument argument) {
        return executor.findAll(new DefaultJpaQuerySpecification<EmployeeTag>(argument.getQuery()));
    }

}
