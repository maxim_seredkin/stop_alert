package com.ws.sa.actions.employee_tag;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.forms.tag.employee.EmployeeTagForm;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;
import static java.util.Objects.isNull;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseEmployeeTagAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, EmployeeTag> {

    protected final EmployeeTagMapper mapper;
    protected final EmployeeTagService service;

    @Override
    @Transactional
    public EmployeeTag execute(ArgumentT argument) {
        return super.execute(argument);
    }

    public BaseEmployeeTagAction(EmployeeTagMapper mapper,
                                 EmployeeTagService service) {
        this.mapper = mapper;
        this.service = service;
    }

    protected void fill(EmployeeTagForm form, EmployeeTag employeeTag) {
        UUID parentId = form.getParent();

        if (Objects.equals(Optional.ofNullable(employeeTag.getParent())
                        .map(EmployeeTag::getId)
                        .orElse(null),
                parentId)) return;

        EmployeeTag parent = null;

        if (Objects.nonNull(parentId)) {
            parent = service.findOneExisting(parentId);
        }

        employeeTag.setParent(parent);
        updatePath(employeeTag, parent);
    }

    private void updatePath(EmployeeTag tag, EmployeeTag parent) {
        String oldPath = tag.getPath();
        String path = isNull(parent) ? ""
                : format("%s/%s", parent.getPath().trim(), parent.getId());
        tag.setPath(path);

        if (isNull(tag.getId()) || tag.getNodes().isEmpty()) return;

        String tagId = tag.getId().toString();
        service.updatePath(format("%s/%s", oldPath, tagId),
                format("%s/%s", path, tagId));
    }
}
