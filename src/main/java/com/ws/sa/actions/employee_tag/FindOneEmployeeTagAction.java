package com.ws.sa.actions.employee_tag;

import com.ws.sa.actions.base.BaseFindOneAction;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindOneEmployeeTagAction extends BaseFindOneAction<EmployeeTag> {

    @Autowired
    public FindOneEmployeeTagAction(EmployeeTagService service) {
        super(service);
    }

}
