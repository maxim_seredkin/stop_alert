package com.ws.sa.actions.employee_tag;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.ws.sa.errors.ApiStatusCode.INVALID_STATUS;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteLogicalEmployeeTagAction extends BaseVoidAction<UuidArgument> {

    private final EmployeeTagService service;

    @Autowired
    public DeleteLogicalEmployeeTagAction(EmployeeTagService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        UUID employeeTagId = argument.getId();

        EmployeeTag employeeTag = service.findOneExisting(employeeTagId);

        Guard.checkEntityState(employeeTag.isEnabled(), INVALID_STATUS);

        service.deleteBranchFromEmployees(employeeTag.getId());
        service.setBranchDeleted(employeeTag.getId());
    }

}
