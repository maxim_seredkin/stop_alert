package com.ws.sa.actions.employee_tag;

import com.ws.sa.actions.employee_tag.arguments.CreateEmployeeTagArgument;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.forms.tag.employee.EmployeeTagForm;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class CreateEmployeeTagAction extends BaseEmployeeTagAction<CreateEmployeeTagArgument> {

    @Autowired
    public CreateEmployeeTagAction(EmployeeTagMapper mapper,
                                   EmployeeTagService service) {
        super(mapper, service);
    }

    @Override
    protected EmployeeTag executeImpl(CreateEmployeeTagArgument argument) {
        EmployeeTagForm form = argument.getForm();

        EmployeeTag employeeTag = mapper.toEntity(form);
        fill(form, employeeTag);

        return service.save(employeeTag);
    }

}
