package com.ws.sa.actions.employee_tag;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public enum EmployeeTagActions implements ActionEnum {
    FIND_ALL,
    FIND_ONE,
    CREATE,
    UPDATE,
    DELETE;
}
