package com.ws.sa.actions.employee_tag.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.tag.employee.EmployeeTagForm;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class UpdateEmployeeTagArgument implements Argument {

    private final UUID employeeTagId;
    private EmployeeTagForm form;

    public static UpdateEmployeeTagArgument from(UUID employeeTagId, EmployeeTagForm form) {
        return new UpdateEmployeeTagArgument(employeeTagId, form);
    }

    @Override
    public boolean validate() {
        return nonNull(employeeTagId)
               && nonNull(form)
               && form.validate();
    }

}
