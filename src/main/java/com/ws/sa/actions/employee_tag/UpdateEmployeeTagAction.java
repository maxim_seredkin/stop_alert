package com.ws.sa.actions.employee_tag;

import com.ws.sa.actions.employee_tag.arguments.UpdateEmployeeTagArgument;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.forms.tag.employee.EmployeeTagForm;
import com.ws.sa.mappers.employee_tag.EmployeeTagMapper;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class UpdateEmployeeTagAction extends BaseEmployeeTagAction<UpdateEmployeeTagArgument> {

    @Autowired
    public UpdateEmployeeTagAction(EmployeeTagMapper mapper,
                                   EmployeeTagService service) {
        super(mapper, service);
    }

    @Override
    protected EmployeeTag executeImpl(UpdateEmployeeTagArgument argument) {
        UUID employeeTagId = argument.getEmployeeTagId();
        EmployeeTagForm form = argument.getForm();

        EmployeeTag employeeTag = service.findOneExisting(employeeTagId);

        mapper.updateEntity(form, employeeTag);
        fill(form, employeeTag);

        return service.save(employeeTag);
    }

}
