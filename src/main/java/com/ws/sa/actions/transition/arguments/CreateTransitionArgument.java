package com.ws.sa.actions.transition.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.UUID;

import static eu.bitwalker.useragentutils.UserAgent.parseUserAgentString;
import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * @author Maxim Seredkin
 * @since 10.09.2017
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class CreateTransitionArgument implements Argument {

    private final UUID mailId;
    private final String headerUserAgent;
    private final String ip;
    private final String browser;
    private final String operatingSystem;
    private final Date date;

    public static CreateTransitionArgument from(UUID mailId, String headerUserAgent, String ip) {
        UserAgent userAgent = parseUserAgentString(headerUserAgent);

        String browser = userAgent.getBrowser().getName() + " " + userAgent.getBrowserVersion();
        String operatingSystem = userAgent.getOperatingSystem().getName();
        Date date = new Date();

        return new CreateTransitionArgument(mailId, headerUserAgent, ip, browser, operatingSystem, date);
    }

    @Override
    public boolean validate() {
        return nonNull(mailId)
               && nonNull(headerUserAgent)
               && isNotBlank(ip);
    }

}
