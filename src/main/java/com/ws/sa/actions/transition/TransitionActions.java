package com.ws.sa.actions.transition;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Возможные действия с переходами
 *
 * @author Maxim_Seredkin
 * @since 9/8/2017
 */
public enum TransitionActions implements ActionEnum {
    CREATE;
}
