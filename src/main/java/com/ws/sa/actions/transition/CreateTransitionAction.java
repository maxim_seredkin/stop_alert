package com.ws.sa.actions.transition;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.services.mail.MailServiceImpl;
import com.ws.sa.actions.transition.arguments.CreateTransitionArgument;
import com.ws.sa.listeners.transition.events.TransitionEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Slf4j
@Component
public class CreateTransitionAction extends BaseAction<CreateTransitionArgument, String> {

    private final ApplicationEventPublisher publisher;
    private final MailServiceImpl mailService;

    @Autowired
    public CreateTransitionAction(ApplicationEventPublisher publisher,
                                  MailServiceImpl mailService) {
        this.publisher = publisher;
        this.mailService = mailService;
    }

    @Override
    @Transactional
    public String execute(CreateTransitionArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected String executeImpl(CreateTransitionArgument argument) {
        UUID mailId = argument.getMailId();
        String headerUserAgent = argument.getHeaderUserAgent();
        String ip = argument.getIp();
        String browser = argument.getBrowser();
        String operatingSystem = argument.getOperatingSystem();
        Date date = argument.getDate();

        log.debug(join(newArrayList("Осуществлен переход: ", mailId,
                                    ip,
                                    browser,
                                    operatingSystem,
                                    date),
                       "\n"));

        publishEvent(mailId, headerUserAgent, ip, browser, operatingSystem, date);

        return join(newArrayList("redirect",
                                 mailService.findMailRedirectUriOrGetDefault(mailId)),
                    ":");
    }

    /**
     * Отправка события о переходе
     *
     * @param mailId          идентификатор письма
     * @param headerUserAgent user-agent
     * @param ip              ip-адрес
     * @param browser         браузер
     * @param operatingSystem ОС
     * @param date            дата перехода
     */
    private void publishEvent(UUID mailId, String headerUserAgent, String ip, String browser, String operatingSystem, Date date) {
        TransitionEvent event = TransitionEvent.builder()
                                               .mail(mailId)
                                               .ip(ip)
                                               .browser(browser)
                                               .os(operatingSystem)
                                               .additionalInfo(headerUserAgent)
                                               .transitionDate(date)
                                               .build();

        publisher.publishEvent(event);
    }
}
