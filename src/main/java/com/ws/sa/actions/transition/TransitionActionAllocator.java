package com.ws.sa.actions.transition;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.transition.arguments.CreateTransitionArgument;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import static com.ws.sa.actions.transition.TransitionActions.CREATE;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class TransitionActionAllocator extends BaseActionAllocator<TransitionActions> {

    public TransitionActionAllocator(@Qualifier("createTransitionAction") Action<CreateTransitionArgument, String> createTransitionAction) {
        super(ImmutableMap.<TransitionActions, Action>builder()
                      .put(CREATE, createTransitionAction)
                      .build());
    }

}
