package com.ws.sa.actions.employee.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class ImportFromCSVEmployeeArgument implements Argument {

    private final MultipartFile file;

    public static ImportFromCSVEmployeeArgument from(MultipartFile file) {
        return new ImportFromCSVEmployeeArgument(file);
    }

    @Override
    public boolean validate() {
        return nonNull(file);
    }

}
