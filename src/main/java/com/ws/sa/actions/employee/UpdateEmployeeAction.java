package com.ws.sa.actions.employee;

import com.ws.sa.actions.employee.arguments.UpdateEmployeeArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.forms.employee.EmployeeForm;
import com.ws.sa.mappers.employee.EmployeeMapper;
import com.ws.sa.security.unit.UnitAccess;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import com.ws.sa.services.unit.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class UpdateEmployeeAction extends BaseEmployeeAction<UpdateEmployeeArgument> {


    @Autowired
    protected UpdateEmployeeAction(EmployeeMapper mapper,
                                   EmployeeService service,
                                   UnitService unitService,
                                   UnitAccess unitAccess,
                                   EmployeeTagService employeeTagService) {
        super(mapper, service, unitService, unitAccess, employeeTagService);
    }

    @Override
    @Transactional
    public Employee execute(UpdateEmployeeArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected Employee executeImpl(UpdateEmployeeArgument argument) {
        UUID employeeId = argument.getEmployeeId();
        EmployeeForm form = argument.getForm();

        Employee employee = service.findOneExisting(employeeId);

        employee = mapper.updateEntity(form, employee);
        fill(form, employee);

        return service.save(employee);
    }

}
