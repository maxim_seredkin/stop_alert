package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.services.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.ws.sa.errors.ApiStatusCode.ENTITY_IS_USED;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteLogicalEmployeeAction extends BaseVoidAction<UuidArgument> {

    private final EmployeeService service;

    @Autowired
    public DeleteLogicalEmployeeAction(EmployeeService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        UUID employeeId = argument.getId();

        Guard.checkEntityState(service.notExistsWaitingTasks(employeeId), ENTITY_IS_USED);

        Employee employee = service.findOneExisting(employeeId);

        if (employee.isDeleted()) { return; }

        employee.delete();

        service.save(employee);
    }

}
