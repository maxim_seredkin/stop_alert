package com.ws.sa.actions.employee.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.employee.EmployeeForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor(access = PRIVATE)
public class CreateEmployeeArgument implements Argument {

    private final EmployeeForm form;

    public static CreateEmployeeArgument from(EmployeeForm form) {
        return CreateEmployeeArgument.builder()
                                     .form(form)
                                     .build();
    }

    @Override
    public boolean validate() {
        return nonNull(form)
               && form.validate();
    }

}
