package com.ws.sa.actions.employee;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.employee.arguments.ImportFromCSVEmployeeArgument;
import com.ws.sa.dtos.employee.EmployeeImportDTO;
import com.ws.sa.dtos.employee.ImportDTO;
import com.ws.sa.dtos.unit.UnitImportDTO;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.errors.EmployeeError;
import com.ws.sa.errors.UnitError;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.mappers.unit.UnitMapper;
import com.ws.sa.security.unit.UnitAccess;
import com.ws.sa.services.unit.UnitService;
import com.ws.sa.utils.FileUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;

import static com.ws.sa.enums.EntityStatus.ENABLED;
import static java.util.Objects.nonNull;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ImportFromCSVEmployeeAction extends BaseAction<ImportFromCSVEmployeeArgument, ImportDTO> {


    private final UnitAccess unitAccess;
    private final UnitMapper unitMapper;
    private final UnitService unitService;

    @Autowired
    public ImportFromCSVEmployeeAction(UnitAccess unitAccess,
                                       UnitMapper unitMapper,
                                       UnitService unitService) {
        this.unitAccess = unitAccess;
        this.unitMapper = unitMapper;
        this.unitService = unitService;
    }

    @Override
    protected ImportDTO executeImpl(ImportFromCSVEmployeeArgument argument) {
        MultipartFile file = argument.getFile();

        try {
            Guard.checkEntityState(!file.isEmpty(), ApiStatusCode.NO_CONTENT);
            Guard.checkEntityArgument(FileUtils.validCSVFormat(file), ApiStatusCode.UNSUPPORTED_MEDIA_TYPE);

            Reader in = new InputStreamReader(file.getInputStream(), "UTF-8");
            final Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader(Headers.class).parse(in);

            List<EmployeeImportDTO> employees = Lists.newArrayList();
            List<UnitImportDTO> invalidUnits = Lists.newArrayList();
            Map<String, UnitImportDTO> invalidUnitsMap = Maps.newHashMap();

            for (CSVRecord record : records) {
                EmployeeImportDTO employeeImportDTO = new EmployeeImportDTO();

                employeeImportDTO.setId(UUID.randomUUID());
                employeeImportDTO.setMiddleName(record.get(Headers.MiddleName));
                employeeImportDTO.setPosition(record.get(Headers.Position));

                employeeImportDTO.setEmail(record.get(Headers.Email));
                if (StringUtils.isBlank(employeeImportDTO.getEmail()))
                    employeeImportDTO.getErrors().add(EmployeeError.EMAIL_NOT_SET);

                employeeImportDTO.setFirstName(record.get(Headers.FirstName));
                if (StringUtils.isBlank(employeeImportDTO.getFirstName()))
                    employeeImportDTO.getErrors().add(EmployeeError.FIRST_NAME_NOT_SET);

                employeeImportDTO.setSecondName(record.get(Headers.SecondName));
                if (StringUtils.isBlank(employeeImportDTO.getSecondName()))
                    employeeImportDTO.getErrors().add(EmployeeError.SECOND_NAME_NOT_SET);

                Unit unit = unitService.findByNameLike(record.get(Headers.UnitName));
                UnitImportDTO unitImportDTO = null;
                if (nonNull(unit) && Objects.equals(unit.getStatus(), ENABLED)) {
                    // TODO refactor this
                    if (unitAccess.accessRead(unit.getId()))
                        unitImportDTO = unitMapper.toImport(unit);
                    //
                } else if (StringUtils.isNotBlank(record.get(Headers.UnitName))) {
                    if (!invalidUnitsMap.keySet().contains(record.get(Headers.UnitName))) {
                        unit = new Unit();
                        unit.setId(UUID.randomUUID());
                        unit.setName(record.get(Headers.UnitName));
                        unitImportDTO = unitMapper.toImport(unit);
                        unitImportDTO.getErrors().add(UnitError.UNIT_NOT_EXISTS);

                        invalidUnitsMap.put(unitImportDTO.getName(), unitImportDTO);
                        invalidUnits.add(unitImportDTO);
                    } else
                        unitImportDTO = invalidUnitsMap.get(record.get(Headers.UnitName));
                }
                employeeImportDTO.setUnit(unitImportDTO);
                if (Objects.isNull(employeeImportDTO.getUnit()))
                    employeeImportDTO.getErrors().add(EmployeeError.UNIT_NOT_SET);

                employees.add(employeeImportDTO);
            }

            employees.sort(Comparator.comparing(EmployeeImportDTO::getSecondName)
                                     .thenComparing(EmployeeImportDTO::getFirstName)
                                     .thenComparing(EmployeeImportDTO::getMiddleName));

            return new ImportDTO(invalidUnits,
                                 employees.stream()
                                          .filter(employeeImportDTO -> !employeeImportDTO.getErrors().isEmpty()
                                                                       || !employeeImportDTO.getUnit().getErrors().isEmpty())
                                          .collect(Collectors.toList()),
                                 employees);
        } catch (IOException exception) {
            return null;
        }
    }

    public enum Headers {
        SecondName,
        FirstName,
        MiddleName,
        Email,
        Position,
        UnitName
    }

}
