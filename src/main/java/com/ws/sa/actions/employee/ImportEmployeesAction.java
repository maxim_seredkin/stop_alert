package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.employee.arguments.CreateCollectionEmployeeArgument;
import com.ws.sa.actions.employee.arguments.ImportEmployeesArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.forms.employee.ImportEmployeesForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static com.ws.sa.actions.employee.arguments.CreateCollectionEmployeeArgument.from;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ImportEmployeesAction extends BaseAction<ImportEmployeesArgument, Collection<Employee>> {

    private final Action<CreateCollectionEmployeeArgument, Collection<Employee>> createEmployeesAction;

    @Autowired
    public ImportEmployeesAction(@Qualifier("createCollectionEmployeeAction") Action<CreateCollectionEmployeeArgument, Collection<Employee>> createEmployeesAction) {
        this.createEmployeesAction = createEmployeesAction;
    }

    @Override
    protected Collection<Employee> executeImpl(ImportEmployeesArgument argument) {
        ImportEmployeesForm form = argument.getForm();

        return createEmployeesAction.execute(from(form.getEmployees()));
    }

}
