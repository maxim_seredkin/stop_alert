package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.BaseCountAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.services.user.UserService;
import com.ws.sa.specifications.employee.EmployeeJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CountEmployeeAction extends BaseCountAction {

    private final UserService userService;

    @Autowired
    public CountEmployeeAction(DynamicQueryExecutor executor, UserService userService) {
        super(executor);
        this.userService = userService;
    }

    @Override
    protected Long executeImpl(QueryArgument argument) {
        return executor.count(new EmployeeJpaQuerySpecification(argument.getQuery(), userService));
    }

}
