package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.Guard;
import com.ws.sa.forms.employee.EmployeeForm;
import com.ws.sa.mappers.employee.EmployeeMapper;
import com.ws.sa.security.unit.UnitAccess;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import com.ws.sa.services.unit.UnitService;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
public abstract class BaseEmployeeAction<ArgumentT extends Argument> extends BaseAction<ArgumentT, Employee> {

    protected final EmployeeMapper mapper;
    private final UnitService unitService;
    protected final EmployeeService service;
    private final UnitAccess unitAccess;
    private final EmployeeTagService employeeTagService;

    protected BaseEmployeeAction(EmployeeMapper mapper,
                                 EmployeeService service,
                                 UnitService unitService,
                                 UnitAccess unitAccess,
                                 EmployeeTagService employeeTagService) {
        this.mapper = mapper;
        this.service = service;
        this.unitService = unitService;
        this.unitAccess = unitAccess;
        this.employeeTagService = employeeTagService;
    }

    /**
     * Заполнение общих полей при создании и обновлении
     *
     * @param form     форма сотрудника
     * @param employee сотрудник
     */
    protected void fill(EmployeeForm form, Employee employee) {
        fillUnit(form, employee);
        fillEmployeeTags(form.getEmployeeTags(), employee.getEmployeeTags());
    }

    private void fillUnit(EmployeeForm form, Employee employee) {
        Guard.checkAuthorization();
        // TODO refactor this
        Guard.checkEntitySecurity(unitAccess.accessRead(form.getUnit()), ApiStatusCode.FORBIDDEN);

        if (isNull(form.getUnit())) { return; }

        Unit unit = unitService.findOneExisting(form.getUnit());

        employee.setUnit(unit);
    }

    private void fillEmployeeTags(List<UUID> employeeTagIds, List<EmployeeTag> employeeTags) {
        employeeTags.clear();

        if (isEmpty(employeeTagIds)) { return; }

        employeeTagIds.forEach(id -> {
            EmployeeTag employeeTag = employeeTagService.findOneExisting(id);

            employeeTags.add(employeeTag);
        });
    }
}
