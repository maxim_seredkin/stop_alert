package com.ws.sa.actions.employee.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.employee.EmployeeForm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 */
@Getter
@Builder
@AllArgsConstructor(access = PRIVATE)
public class UpdateEmployeeArgument implements Argument {

    private final UUID employeeId;
    private final EmployeeForm form;

    public static UpdateEmployeeArgument from(UUID employeeId, EmployeeForm form) {
        return UpdateEmployeeArgument.builder()
                                     .employeeId(employeeId)
                                     .form(form)
                                     .build();
    }

    @Override
    public boolean validate() {
        return nonNull(employeeId)
               && nonNull(form)
               && form.validate();
    }

}
