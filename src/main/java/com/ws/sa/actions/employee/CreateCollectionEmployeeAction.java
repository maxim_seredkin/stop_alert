package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseAction;
import com.ws.sa.actions.employee.arguments.CreateCollectionEmployeeArgument;
import com.ws.sa.actions.employee.arguments.CreateEmployeeArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.forms.employee.EmployeeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class CreateCollectionEmployeeAction extends BaseAction<CreateCollectionEmployeeArgument, Collection<Employee>> {

    private final Action<CreateEmployeeArgument, Employee> createEmployeeAction;

    @Autowired
    public CreateCollectionEmployeeAction(@Qualifier("createEmployeeAction") Action<CreateEmployeeArgument, Employee> createEmployeeAction) {
        this.createEmployeeAction = createEmployeeAction;
    }

    @Override
    protected Collection<Employee> executeImpl(CreateCollectionEmployeeArgument argument) {
        Collection<EmployeeForm> forms = argument.getForms();

        return forms.stream()
                    .map(CreateEmployeeArgument::from)
                    .map(createEmployeeAction::execute)
                    .collect(toList());
    }

}
