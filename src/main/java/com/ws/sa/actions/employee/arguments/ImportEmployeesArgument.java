package com.ws.sa.actions.employee.arguments;

import com.ws.sa.actions.base.arguments.Argument;
import com.ws.sa.forms.employee.ImportEmployeesForm;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;

/**
 * Created by Seredkin M. on 08.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class ImportEmployeesArgument implements Argument {

    private final ImportEmployeesForm form;

    public static ImportEmployeesArgument from(ImportEmployeesForm form) {
        return new ImportEmployeesArgument(form);
    }

    @Override
    public boolean validate() {
        return nonNull(form)
                && form.validate();
    }
}
