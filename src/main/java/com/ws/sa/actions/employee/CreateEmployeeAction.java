package com.ws.sa.actions.employee;

import com.ws.sa.actions.employee.arguments.CreateEmployeeArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.forms.employee.EmployeeForm;
import com.ws.sa.mappers.employee.EmployeeMapper;
import com.ws.sa.security.unit.UnitAccess;
import com.ws.sa.services.employee.EmployeeService;
import com.ws.sa.services.employee_tag.EmployeeTagService;
import com.ws.sa.services.unit.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class CreateEmployeeAction extends BaseEmployeeAction<CreateEmployeeArgument> {

    @Autowired
    public CreateEmployeeAction(EmployeeMapper mapper,
                                EmployeeService service,
                                UnitService unitService,
                                UnitAccess unitAccess,
                                EmployeeTagService employeeTagService) {
        super(mapper, service, unitService, unitAccess, employeeTagService);
    }

    @Override
    @Transactional
    public Employee execute(CreateEmployeeArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected Employee executeImpl(CreateEmployeeArgument argument) {
        EmployeeForm form = argument.getForm();

        Employee employee = mapper.toEntity(form);
        fill(form, employee);

        return service.save(employee);
    }

}
