package com.ws.sa.actions.employee;

import com.google.common.collect.ImmutableMap;
import com.ws.sa.actions.base.Action;
import com.ws.sa.actions.base.BaseActionAllocator;
import com.ws.sa.actions.base.arguments.GetFileArgument;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.actions.employee.arguments.*;
import com.ws.sa.dtos.employee.ImportDTO;
import com.ws.sa.entities.employee.Employee;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Collection;

import static com.ws.sa.actions.employee.EmployeeActions.*;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class EmployeeActionAllocator extends BaseActionAllocator<EmployeeActions> {

    public EmployeeActionAllocator(@Qualifier("createEmployeeAction") Action<CreateEmployeeArgument, Employee> createEmployeeAction,
                                   @Qualifier("deleteLogicalEmployeeAction") Action<UuidArgument, Void> deleteLogicalAction,
                                   @Qualifier("deleteEmployeeAction") Action<UuidArgument, Void> deleteAction,
                                   @Qualifier("countEmployeeAction") Action<QueryArgument, Long> countEmployeeAction,
                                   @Qualifier("existsEmployeeAction") Action<QueryArgument, Boolean> existsAction,
                                   @Qualifier("findAllEmployeeAction") Action<QueryArgument, Page<Employee>> findAllAction,
                                   @Qualifier("findOneEmployeeAction") Action<UuidArgument, Employee> findOneAction,
                                   @Qualifier("recoveryEmployeeAction") Action<UuidArgument, Void> recoveryAction,
                                   @Qualifier("updateEmployeeAction") Action<UpdateEmployeeArgument, Employee> updateEmployeeAction,
                                   @Qualifier("createCollectionEmployeeAction") Action<CreateCollectionEmployeeArgument, Collection<Employee>> createCollectionEmployeeAction,
                                   @Qualifier("getFileAction") Action<GetFileArgument, File> getFileAction,
                                   @Qualifier("importFromCSVEmployeeAction") Action<ImportFromCSVEmployeeArgument, ImportDTO> importFromCSVAction,
                                   @Qualifier("importEmployeesAction") Action<ImportEmployeesArgument, Collection<Employee>> importEmployeesAction) {
        super(ImmutableMap.<EmployeeActions, Action>builder()
                      .put(CREATE, createEmployeeAction)
                      .put(DELETE_LOGICAL, deleteLogicalAction)
                      .put(DELETE, deleteAction)
                      .put(COUNT, countEmployeeAction)
                      .put(EXISTS, existsAction)
                      .put(FIND_ALL, findAllAction)
                      .put(FIND_ONE, findOneAction)
                      .put(RECOVERY, recoveryAction)
                      .put(UPDATE, updateEmployeeAction)
                      .put(DOWNLOAD_SAMPLE_CSV, getFileAction)
                      .put(CREATE_COLLECTION, createCollectionEmployeeAction)
                      .put(IMPORT_FROM_CSV, importFromCSVAction)
                      .put(IMPORT_EMPLOYEES, importEmployeesAction)
                      .build());
    }

}
