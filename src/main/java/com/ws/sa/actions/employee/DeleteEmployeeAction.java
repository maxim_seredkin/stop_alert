package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.BaseDeleteAction;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.services.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class DeleteEmployeeAction extends BaseDeleteAction<Employee> {

    @Autowired
    public DeleteEmployeeAction(EmployeeService service) {
        super(service);
    }

}
