package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.BaseFindAllAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.services.user.UserService;
import com.ws.sa.specifications.employee.EmployeeJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class FindAllEmployeeAction extends BaseFindAllAction<Employee> {

    private final UserService userService;

    @Autowired
    public FindAllEmployeeAction(DynamicQueryExecutor executor, UserService userService) {
        super(executor);
        this.userService = userService;
    }

    @Override
    protected Page<Employee> executeImpl(QueryArgument argument) {
        return executor.findAll(new EmployeeJpaQuerySpecification(argument.getQuery(), userService));
    }

}
