package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.ActionEnum;

/**
 * Возможные действия пользователя
 *
 * @author Maxim_Seredkin
 * @since 9/8/2017
 */
public enum EmployeeActions implements ActionEnum {
    CREATE,
    DELETE_LOGICAL,
    DELETE,
    EXISTS,
    FIND_ALL,
    FIND_ONE,
    RECOVERY,
    UPDATE,
    COUNT,
    CREATE_COLLECTION,
    DOWNLOAD_SAMPLE_CSV,
    IMPORT_FROM_CSV,
    IMPORT_EMPLOYEES;
}
