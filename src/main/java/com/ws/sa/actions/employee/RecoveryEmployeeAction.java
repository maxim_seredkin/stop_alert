package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.BaseVoidAction;
import com.ws.sa.actions.base.arguments.UuidArgument;
import com.ws.sa.entities.employee.Employee;
import com.ws.sa.services.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Maxim_Seredkin
 * @since 9/7/2017
 */
@Component
public class RecoveryEmployeeAction extends BaseVoidAction<UuidArgument> {

    private final EmployeeService service;

    @Autowired
    public RecoveryEmployeeAction(EmployeeService service) {
        this.service = service;
    }

    @Override
    @Transactional
    public Void execute(UuidArgument argument) {
        return super.execute(argument);
    }

    @Override
    protected void executeImpl(UuidArgument argument) {
        Employee employee = service.findOneExisting(argument.getId());

        if (employee.isEnabled()) { return; }

        employee.enable();

        service.save(employee);
    }

}
