package com.ws.sa.actions.employee;

import com.ws.sa.actions.base.BaseExistsAction;
import com.ws.sa.actions.base.arguments.QueryArgument;
import com.ws.sa.executors.DynamicQueryExecutor;
import com.ws.sa.services.user.UserService;
import com.ws.sa.specifications.employee.EmployeeJpaQuerySpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Seredkin M. on 14.09.2017.
 * <p>
 * JavaDoc
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
@Component
public class ExistsEmployeeAction extends BaseExistsAction {

    private final UserService userService;

    @Autowired
    public ExistsEmployeeAction(DynamicQueryExecutor executor, UserService userService) {
        super(executor);
        this.userService = userService;
    }

    @Override
    protected Boolean executeImpl(QueryArgument argument) {
        return executor.exists(new EmployeeJpaQuerySpecification(argument.getQuery(), userService));
    }

}
