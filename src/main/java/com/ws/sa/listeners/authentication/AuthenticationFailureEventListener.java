package com.ws.sa.listeners.authentication;

import com.ws.sa.security.user.LoginAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Maxim Seredkin
 * @since 15.04.17
 */
@Component
public class AuthenticationFailureEventListener {

    private final LoginAttemptService loginAttemptService;

    @Autowired
    public AuthenticationFailureEventListener(LoginAttemptService loginAttemptService) {
        this.loginAttemptService = loginAttemptService;
    }

    @Async
    @EventListener
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent e) {
        if (!LinkedHashMap.class.isInstance(e.getAuthentication().getDetails()))
            return;

        Map auth = (LinkedHashMap) e.getAuthentication().getDetails();
        loginAttemptService.loginFailed((String) auth.get("username"));
    }

}
