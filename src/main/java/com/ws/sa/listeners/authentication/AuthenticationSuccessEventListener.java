package com.ws.sa.listeners.authentication;

import com.ws.sa.security.user.LoginAttemptService;
import com.ws.sa.security.user.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * @author Maxim Seredkin
 * @since 15.04.2017
 */
@Component
public class AuthenticationSuccessEventListener {

    private final LoginAttemptService loginAttemptService;

    @Autowired
    public AuthenticationSuccessEventListener(LoginAttemptService loginAttemptService) {
        this.loginAttemptService = loginAttemptService;
    }

    @Async
    @EventListener
    public void onApplicationEvent(AuthenticationSuccessEvent e) {
        if (!UserDetailsImpl.class.isInstance(e.getAuthentication().getDetails()))
            return;

        UserDetails auth = (UserDetailsImpl) e.getAuthentication().getDetails();
        loginAttemptService.loginSucceeded(auth.getUsername());
    }

}
