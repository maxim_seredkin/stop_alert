package com.ws.sa.listeners.task.events;

import com.ws.sa.enums.TaskAction;
import com.ws.sa.entities.task.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author Maxim Seredkin
 * @since 27.02.2017
 */
@Getter
@AllArgsConstructor(access = PRIVATE)
public class TaskEvent {

    private final TaskAction action;

    private final Task task;

    public static TaskEvent update(Task task) {
        return new TaskEvent(TaskAction.UPDATE, task);
    }

    public static TaskEvent cancel(Task task) {
        return new TaskEvent(TaskAction.CANCEL, task);
    }

    public static TaskEvent execute(Task task) {
        return new TaskEvent(TaskAction.EXECUTE, task);
    }

    public static TaskEvent pause(Task task) {
        return new TaskEvent(TaskAction.PAUSE, task);
    }

    public static TaskEvent resume(Task task) {
        return new TaskEvent(TaskAction.RESUME, task);
    }

    public static TaskEvent start(Task task) {
        return new TaskEvent(TaskAction.START, task);
    }


}
