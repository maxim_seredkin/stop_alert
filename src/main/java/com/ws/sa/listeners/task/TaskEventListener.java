package com.ws.sa.listeners.task;

import com.ws.sa.managers.TaskManager;
import com.ws.sa.listeners.task.events.TaskEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * @author Maxim Seredkin
 * @since 27.02.2017
 */
//@Log
@Component
public class TaskEventListener {

    private static final Logger log = LoggerFactory.getLogger(TaskEventListener.class.getName());

    @Autowired
    private TaskManager manager;

    @Async
    @TransactionalEventListener(condition = "#event.action.name() == 'START'")
    public void startEvent(TaskEvent event) {
        log.debug("Start event");
        manager.start(event.getTask());
    }

    @Async
    @TransactionalEventListener(condition = "#event.action.name() == 'PAUSE'")
    public void pauseEvent(TaskEvent event) {
        log.debug("Pause event");
        manager.pause(event.getTask());
    }

    @Async
    @TransactionalEventListener(condition = "#event.action.name() == 'RESUME'")
    public void resumeEvent(TaskEvent event) {
        log.debug("Resume event");
        manager.resume(event.getTask());
    }

    @Async
    @TransactionalEventListener(condition = "#event.action.name() == 'UPDATE'")
    public void updateEvent(TaskEvent event) {
        log.debug("Update event");
        manager.update(event.getTask());
    }

    @Async
    @TransactionalEventListener(condition = "#event.action.name() == 'CANCEL'")
    public void cancelEvent(TaskEvent event) {
        log.debug("Cancel event");
        manager.cancel(event.getTask());
    }

}
