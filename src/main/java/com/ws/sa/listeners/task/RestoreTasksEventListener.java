package com.ws.sa.listeners.task;

import com.ws.sa.managers.TaskManager;
import com.ws.sa.services.task.TaskService;
import com.ws.sa.utils.CalendarUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author Maxim Seredkin
 * @since 27.02.2017
 */
@Component
public class RestoreTasksEventListener {

    private final TaskManager manager;
    private final TaskService taskService;
    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public RestoreTasksEventListener(TaskManager manager, TaskService taskService,
                                     ApplicationEventPublisher eventPublisher) {
        this.manager = manager;
        this.taskService = taskService;
        this.eventPublisher = eventPublisher;
    }

    @EventListener
    public void pauseEvent(ContextRefreshedEvent event) {
        taskService.findActive().stream().filter(task -> task.isWaiting() || task.isExecution()).forEach(task -> {
            if (!CalendarUtil.isNoneDateExpired(task.getEndDate())) {
                taskService.expired(task.getId());
            } else if (task.isWaiting()) {
                manager.start(task);
            } else if (task.isExecution()) {
                manager.execute(task);
            }
        });
    }

}

