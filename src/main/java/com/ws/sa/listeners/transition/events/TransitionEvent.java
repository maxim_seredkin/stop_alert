package com.ws.sa.listeners.transition.events;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * @author Maxim Seredkin
 * @since 28.02.2017
 */
@Data
@Builder
public class TransitionEvent {

    private UUID mail;

    private Date transitionDate = new Date();

    private String ip;

    private String browser;

    private String os;

    private String additionalInfo;
}
