package com.ws.sa.listeners.transition;

import com.ws.sa.listeners.transition.events.TransitionEvent;
import com.ws.sa.managers.TransitionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author Maxim Seredkin
 * @since 27.02.2017
 */
@Component
public class TransitionEventListener {

    private static final Logger log = LoggerFactory.getLogger(TransitionEventListener.class.getName());

    private final TransitionManager manager;

    @Autowired
    public TransitionEventListener(TransitionManager manager) {
        this.manager = manager;
    }

    @Async
    @EventListener
    public void startEvent(TransitionEvent event) {
        log.debug("Transition event");
        manager.transition(event);
    }
}
