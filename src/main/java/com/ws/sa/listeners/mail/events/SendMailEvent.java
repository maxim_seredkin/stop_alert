package com.ws.sa.listeners.mail.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author Maxim_Seredkin
 * @since 29-Mar-17
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SendMailEvent {

    private String fromAddress;
    private String toAddress;
    private String toPersonal;
    private String fromPersonal;
    private String subject;
    private String templatePath;
    private Map<String, Object> params;
}
