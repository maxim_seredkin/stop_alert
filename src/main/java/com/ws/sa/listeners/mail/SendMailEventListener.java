package com.ws.sa.listeners.mail;

import com.google.common.collect.Lists;
import com.ws.sa.listeners.mail.events.SendMailEvent;
import com.ws.sa.builders.MailBuilder;
import com.ws.sa.senders.MailSender;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

/**
 * @author Maxim Seredkin
 * @since 27.02.2017
 */
@Component
public class SendMailEventListener {

    private static final Logger log = LoggerFactory.getLogger(SendMailEventListener.class.getName());

    private final MailBuilder mailBuilder;
    private final MailSender mailSender;

    @Autowired
    public SendMailEventListener(MailBuilder mailBuilder, MailSender mailSender) {
        this.mailBuilder = mailBuilder;
        this.mailSender = mailSender;
    }

    @Async
    @EventListener
    public void send(SendMailEvent event) {

        log.debug("Start send mail event");
        log.debug(StringUtils.join(Lists.newArrayList("Попытка отправки письма: ",
                "тема: " + event.getSubject(),
                "от: " + event.getFromPersonal() + "(" + event.getFromAddress() + ")",
                "кому: " + event.getToPersonal() + "(" + event.getToAddress() + ")",
                "параметры: " + event.getParams()), "\n"));
        try {
            MimeMessage mimeMessage = mailBuilder.build(event.getToAddress(), event.getToPersonal(), event.getFromPersonal(), event.getFromAddress(), event.getSubject(), event.getTemplatePath(), event.getParams());
            mailSender.send(mimeMessage);
        } catch (Exception e) {
            log.error("Письмо не отправлено:");
            log.error(e.getMessage());
        }
    }

}
