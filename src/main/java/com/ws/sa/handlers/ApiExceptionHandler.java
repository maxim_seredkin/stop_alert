package com.ws.sa.handlers;

import com.ws.sa.beans.ApiResponse;
import com.ws.sa.errors.ApiStatusCode;
import com.ws.sa.exceptions.ApiException;
import com.ws.sa.exceptions.EntityArgumentException;
import lombok.extern.java.Log;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


/**
 * Обработчик исключений API
 *
 * @author Evgeny Albnets
 */
@Log
@ControllerAdvice("com.ws.sa.api_controllers")
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ApiException.class})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    ApiResponse handleApiException(ApiException ex, WebRequest request) {
        return ApiResponse.fail(ex.getErrorCode());
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    ApiResponse handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
        return ApiResponse.fail(ApiStatusCode.INTERNAL_SERVER_ERROR, ApiStatusCode.INTERNAL_SERVER_ERROR.getDescription());
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    @ResponseStatus(HttpStatus.OK)
    public
    @ResponseBody
    ApiResponse handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
        return ApiResponse.fail(ApiStatusCode.INTERNAL_SERVER_ERROR, ApiStatusCode.INTERNAL_SERVER_ERROR.getDescription());
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(EntityArgumentException.class)
    public
    @ResponseBody
    ApiResponse processEntityArgumentException(EntityArgumentException exception) {
        return ApiResponse.fail(exception.getErrorCode(), exception.getMessage());
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({AccessDeniedException.class})
    public
    @ResponseBody
    ApiResponse processEntitySecurityException(AccessDeniedException exception) {
        return ApiResponse.fail(ApiStatusCode.NOT_AUTHORIZED, exception.getMessage());
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public
    @ResponseBody
    ApiResponse methodArgumentTypeMismatchExceprion(MethodArgumentTypeMismatchException exception) {
        return ApiResponse.fail(ApiStatusCode.NOT_FOUND, exception.getMessage());
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({LockedException.class})
    public
    @ResponseBody
    ApiResponse userLockedException(LockedException exception) {
        return ApiResponse.fail(ApiStatusCode.USER_LOCKED, exception.getMessage());
    }

//    @ExceptionHandler({RuntimeException.class})
//    public ResponseEntity<ApiResponse> handleRuntimeException(RuntimeException ex, WebRequest request) {
//        // Пишим стэк трейс в лог для дальнейшего анализа
//        logger.error(ExceptionUtils.getStackTrace(ex));
//        return new ResponseEntity<>(ApiResponse.fail(ApiStatusCode.INTERNAL_SERVER_ERROR,
//                ExceptionUtils.getStackTrace(ex)),
//                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
//    }
}
