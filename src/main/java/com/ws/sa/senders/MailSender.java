package com.ws.sa.senders;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;

/**
 * @author Maxim Seredkin
 * @since 28.02.2017
 */
@Log
@Component("customMailSender")
public class MailSender {

    private final JavaMailSender javaMailSender;

    @Autowired
    public MailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void send(MimeMessage message) {
        javaMailSender.send(message);
    }

}
