package com.ws.sa.repositories.employee_tag;

import com.ws.sa.entities.employee_tag.EmployeeTag;
import com.ws.sa.repositories.base.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface EmployeeTagRepository extends BaseRepository<EmployeeTag>, JpaSpecificationExecutor<EmployeeTag> {

    Collection<EmployeeTag> findByPathContaining(String input);

    @Modifying
    @Query(value = "UPDATE EMPLOYEE_TAGS SET PATH = REPLACE(PATH, ?1, ?2) WHERE PATH LIKE ?1", nativeQuery = true)
    void updateBranchPath(String oldPath, String path);

    @Query("SELECT CASE WHEN COUNT (e) > 0 THEN 'false' ELSE 'true' END FROM EmployeeTag e WHERE e.id = ?1 " +
           "AND EXISTS(SELECT t FROM Task t WHERE t.status IN (0,1) AND e IN (SELECT list FROM t.employeeTags list))")
    Boolean notExistsWaitingTasks(UUID id);

    @Modifying
    @Query(value = "delete from employee_employee_tags where tag_id in " +
                   "(select id from employee_tags where id = ?1 or path like concat('%', ?1, '%'))", nativeQuery = true)
    void deleteBranchFromEmployees(String id);

    @Modifying
    @Query(value = "update employee_tags set status = 1 where id in " +
                   "(select id from employee_tags where id = ?1 or path like concat('%', ?1, '%'))", nativeQuery = true)
    void setBranchDeleted(String id);
}
