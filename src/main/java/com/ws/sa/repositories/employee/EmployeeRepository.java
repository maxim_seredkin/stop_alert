package com.ws.sa.repositories.employee;

import com.ws.sa.entities.employee.Employee;
import com.ws.sa.repositories.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface EmployeeRepository extends BaseRepository<Employee> {

    @Query(value = "SELECT COUNT(m) FROM Mail m WHERE m.countTransitions > 0 AND m.task.id = ?1")
    Integer countAttackedByTaskId(UUID taskId);

    @Query(value = "SELECT COUNT(m) FROM Mail m WHERE m.task.id = ?1")
    Integer countAllByTaskId(UUID taskId);

    @Query("SELECT COUNT(e) " +
           "FROM Employee e " +
           "WHERE (e.unit.id = ?1 OR e.unit.path LIKE concat('%', CAST(?1 as string), '%')) " +
           "AND EXISTS (SELECT m FROM Mail m WHERE m.countTransitions > 0 AND m.employee.id = e.id)")
    Integer countAttackedByUnitId(UUID unitId);

    @Query("SELECT COUNT(e) FROM Employee e WHERE e.unit.id = ?1 OR e.unit.path LIKE concat('%', CAST(?1 as string), '%')")
    Integer countAllByUnitId(UUID unitId);

    @Query("SELECT COUNT(m) " +
           "FROM Mail m " +
           "WHERE m.task.id = ?1 AND (m.unit.id = ?2 OR m.unitPath LIKE concat('%', CAST(?2 as string), '%'))")
    Integer countAllByTaskIdAndUnitId(UUID taskId, UUID unitId);

    @Query("SELECT COUNT(m) " +
           "FROM Mail m " +
           "WHERE m.countTransitions > 0 " +
           "AND m.task.id = ?1 " +
           "AND (m.unit.id = ?2 OR m.unitPath LIKE concat('%', CAST(?2 as string), '%'))")
    Integer countAttackedByTaskIdAndUnitId(UUID taskId, UUID unitId);

    @Query("SELECT CASE WHEN COUNT(e) > 0 THEN 'true' ELSE 'false' END " +
           "FROM Employee e " +
           "WHERE e.id = ?1")
    Boolean existsById(UUID id);

    @Query("SELECT CASE WHEN COUNT (e) > 0 THEN 'false' ELSE 'true' END " +
           "FROM Employee e " +
           "WHERE e.id = ?1 " +
           "AND EXISTS(SELECT t FROM Task t WHERE t.status IN (0,1) AND ((e IN (SELECT list FROM t.employeesByHand list)) " +
           "                                                          OR (e IN (SELECT list FROM t.employeesByFilter list))))")
    Boolean notExistsWaitingTasks(UUID id);

    @Query("SELECT t.employeesByFilter FROM Task t WHERE t.id = ?1")
    List<Employee> findFilterEmployeesByTask(UUID taskId);

    @Query("SELECT t.employeesByHand FROM Task t WHERE t.id = ?1")
    List<Employee> findHandEmployeesByTask(UUID taskId);

    @Query("SELECT e FROM Employee e WHERE e.unit.id = ?1 OR e.unit.path LIKE CONCAT('%', CAST(?1 as string), '%')")
    Page<Employee> findAllByUnitId(UUID id, Pageable pageable);

    @Query("SELECT COUNT (m) FROM Mail m WHERE m.employee.id = ?1")
    Integer countAllByEmployeeId(UUID id);

    @Query("SELECT COUNT (m) FROM Mail m WHERE m.countTransitions > 0 AND m.employee.id = ?1")
    Integer countAttackedByEmployeeId(UUID id);

    @Query("SELECT e FROM Employee e WHERE EXISTS (SELECT m FROM Mail m WHERE m.employee.id = e.id AND m.task.id = ?1)")
    Page<Employee> findAllByTaskId(UUID id, Pageable pageable);

    @Query("select case when count (u) > 0 then 'true' else 'false' end from User u where u.id = ?2 and exists " +
           "(select e from Employee e where e.id = ?1 and exists" +
           "(select units from u.units units where units.id = e.unit.id " +
           "or (select unit.path from Unit unit where unit.id = e.unit.id) like concat('%', cast(units.id as string), '%')))")
    boolean hasAccess(UUID employeeId, UUID userId);

    @Query("SELECT e FROM Employee e WHERE EXISTS (SELECT m FROM Mail m WHERE m.countTransitions > 0 AND m.employee.id = e.id AND m.task.id = ?1)")
    List<Employee> findAttackedByTaskId(UUID taskId);
}
