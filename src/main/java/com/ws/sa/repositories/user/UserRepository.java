package com.ws.sa.repositories.user;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.user.User;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

/**
 * @author Maxim Seredkin
 */
public interface UserRepository extends BaseRepository<User> {

    User findOneByUsername(String username);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM User u WHERE u.username = ?1")
    boolean existsByUsername(String username);

    @Query("SELECT u FROM User u WHERE u.username = ?1 OR u.email = ?1 AND u.enabled = true AND u.accountNonExpired = true")
    Collection<User> findActiveUsersByUsernameOrEmail(String username);

}
