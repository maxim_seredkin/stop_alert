package com.ws.sa.repositories.mail_template_group;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface MailTemplateGroupRepository extends BaseRepository<MailTemplateGroup> {
    MailTemplateGroup findOneByName(String name);

    @Query("SELECT CASE WHEN COUNT(mtg) > 0 THEN 'true' ELSE 'false' END FROM MailTemplateGroup mtg WHERE mtg.name = :name")
    boolean existsByName(@Param("name") String name);
}
