package com.ws.sa.repositories.mail;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.mail.Mail;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 16.02.2017.
 */
public interface MailRepository extends BaseRepository<Mail> {

    void deleteByTaskId(UUID taskId);

    Collection<Mail> findByTaskId(UUID taskId);

    @Query("SELECT COUNT(m.id) " +
           "FROM Mail m " +
           "WHERE m.countTransitions = 0")
    long countCorrect();

    long count();

    @Query("SELECT COUNT(m.id) " +
           "FROM Mail m " +
           "WHERE m.task.template.id = ?1 " +
           "AND m.countTransitions > 0")
    long countWrongByTemplate(UUID templateId);
}
