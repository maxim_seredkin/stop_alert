package com.ws.sa.repositories.authority;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.authority.Authority;

/**
 * @author Maxim Seredkin
 */
public interface AuthorityRepository extends BaseRepository<Authority> {

    Authority findByAuthority(String authority);

}
