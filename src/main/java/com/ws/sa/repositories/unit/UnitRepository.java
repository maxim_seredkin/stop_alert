package com.ws.sa.repositories.unit;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.unit.Unit;
import com.ws.sa.entities.user.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovksiy on 31.01.2017.
 */
public interface UnitRepository extends BaseRepository<Unit>, JpaSpecificationExecutor<Unit> {

    @Query(value = "SELECT * FROM UNITS WHERE NAME ILIKE ?1 LIMIT 1", nativeQuery = true)
    Unit findByNameContainingIgnoreCase(String name);

    @Query(value = "SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM Unit u WHERE ((u.name LIKE :name) AND (u.parent.id = :parentId))")
    Boolean existsByNameAndParent_Id(@Param("name") String name, @Param("parentId") UUID parentId);

    @Query(value = "SELECT CASE WHEN COUNT(u) > 0 THEN 'true' ELSE 'false' END FROM Unit u WHERE ((u.name LIKE :name) AND (u.parent IS NULL ))")
    Boolean existsByNameAndParentIsNull(@Param("name") String name);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN 'false' ELSE 'true' END FROM Unit u WHERE (u.id = ?1 OR u.path LIKE CONCAT('%', cast(?1 as string), '%'))" +
            "AND EXISTS(SELECT t FROM Task t WHERE t.status IN (0,1) AND u IN (SELECT list FROM t.units list))")
    Boolean notExistsWaitingTasks(UUID id);

    Collection<Unit> findByUsers_Id(UUID id);

    Collection<Unit> findByPathContaining(String input);

    @Query("select count (e) from Employee e where e.status = 'ENABLED' and (e.unit.id = ?1 or e.unit.path like concat('%', cast(?1 as string), '%'))")
    int countBranchEmployees(UUID id);

    @Query("select u from User u where u.authority.authority = ?2 and exists (select units from u.units units " +
            "where (select unit.path from Unit unit where unit.id = ?1) like concat('%', cast(units.id as string), '%'))")
    Collection<User> findUsersByPathAndAuthority(UUID id, String authority);

    @Query("select case when count (u) > 0 then 'true' else 'false' end from User u where u.id = ?2 and exists " +
           "(select units from u.units units where units.id = ?1 " +
           "or (select unit.path from Unit unit where unit.id = ?1) like concat('%', cast(units.id as string), '%'))")
    boolean hasAccess(UUID unitId, UUID userId);

    @Modifying(clearAutomatically = true)
    @Query(value = "update Unit u set u.path = replace(path, ?1, ?2) where u.path like concat('%', ?1, '%')")
    void updateBranchPath(String oldPath, String path);

    @Modifying
    @Query(value = "delete from user_units where unit_id in " +
           "(select id from units where id = ?1 or path like concat('%', ?1, '%'))", nativeQuery = true)
    void deleteBranchFromUsers(String id);

    @Modifying(clearAutomatically = true)
    @Query(value = "update units set status = 'DELETED' where id in " +
           "(select id from units where id = ?1 or path like concat('%', ?1, '%'))", nativeQuery = true)
    void setBranchDeleted(String id);

    @Modifying(clearAutomatically = true)
    @Query(value = "update employees set unit_id = ?2 where unit_id in" +
                   "(select id from units where id = ?1 or path like concat('%', ?1, '%'))", nativeQuery = true)
    void transferBranchEmployees(String unitId, String successorId);
}
