package com.ws.sa.repositories.mock_page;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.mock_page.MockPage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Seredkin M. on 12.07.2017.
 *
 * @author Maxim Seredkin
 * @version 1.0
 */
public interface MockPageRepository extends BaseRepository<MockPage> {

    @Query("SELECT CASE WHEN COUNT(mp) > 0 THEN 'true' ELSE 'false' END FROM MockPage mp WHERE mp.name = :name")
    boolean existsByName(@Param("name") String name);

}
