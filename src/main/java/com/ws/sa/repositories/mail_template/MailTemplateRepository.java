package com.ws.sa.repositories.mail_template;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.mail_template.MailTemplate;
import com.ws.sa.entities.mail_template_group.MailTemplateGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Vladislav Shelengovskiy on 14.02.2017.
 */
public interface MailTemplateRepository extends BaseRepository<MailTemplate> {

    MailTemplate findOneByName(String name);

    @Query("SELECT CASE WHEN COUNT(mt) > 0 THEN 'true' ELSE 'false' END FROM MailTemplate mt WHERE mt.name = :name")
    boolean existsByName(@Param("name") String name);

    @Query("UPDATE MailTemplate mt SET mt.group = NULL WHERE mt.group = :group")
    void setNullValueGroupByGroup(@Param("group") MailTemplateGroup mailTemplateGroup);
}
