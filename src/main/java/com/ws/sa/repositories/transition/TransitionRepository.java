package com.ws.sa.repositories.transition;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.transition.Transition;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 27.02.2017.
 */
public interface TransitionRepository extends BaseRepository<Transition> {
    Collection<Transition> findAllByMail_Task_Id(UUID id);

    Collection<Transition> findAllByMail_Task_IdAndMail_Employee_Id(UUID taskId, UUID employeeId);

}
