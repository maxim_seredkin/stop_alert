package com.ws.sa.repositories.task;

import com.ws.sa.repositories.base.BaseRepository;
import com.ws.sa.entities.task.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Vladislav Shelengovskiy on 15.02.2017.
 */
public interface TaskRepository extends BaseRepository<Task> {

    @Query(value = "SELECT t FROM Task t WHERE t.status = 0 OR t.status = 2")
    Collection<Task> findActive();

    @Query("select case when count (t) > 0 then 'true' else 'false' end from Task t where t.id = ?1 and t.author.id = ?2")
    boolean hasAccess(UUID taskId, UUID userId);

    @Query(value = "SELECT t FROM Task t WHERE EXISTS " +
            "(SELECT m FROM Mail m WHERE m.unit.id = ?1 " +
            "OR m.unitPath LIKE CONCAT('%', CAST(?1 as string), '%'))")
    Page<Task> findAllTaskByUnitId(UUID unitId, Pageable pageable);

    @Query(value = "SELECT t FROM Task t WHERE EXISTS " +
            "(SELECT m FROM Mail m WHERE m.employee.id = ?1 AND m.task.id = t.id)")
    Page<Task> findAllTaskByEmployeeId(UUID employeeId, Pageable pageable);
}
