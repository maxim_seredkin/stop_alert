package com.ws.sa.repositories.base;

import com.ws.sa.entities.base.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;
import java.util.UUID;

/**
 * @author Maxim Seredkin
 */
@NoRepositoryBean
public interface BaseRepository<EntityT extends BaseEntity> extends JpaRepository<EntityT, UUID>, JpaSpecificationExecutor<EntityT> {

    Optional<EntityT> findOneOptionalById(UUID uuid);

}
